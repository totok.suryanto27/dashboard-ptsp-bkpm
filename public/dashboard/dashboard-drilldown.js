$(function(){
    // Create the chart

    $.get( base_url +  '/rest/dashboard-drilldown', function(data) {
                data_bulanan = JSON.parse(data);
                console.log(data_bulanan);
                for (var i = 0; i < data_bulanan.length ; i++) {
                	obj = data_bulanan[i];
                  var temp_op = parseInt(obj.izin_on_process);
                  var temp_selesai = parseInt(obj.total_selesai);
                  var seratusPersen = temp_op+temp_selesai;
                  var op_float = parseFloat(temp_op/seratusPersen)*100;
                  var selesai_float = parseFloat(temp_selesai/seratusPersen)*100;
                  obj.izin_on_process = op_float.toFixed(0);
                  obj.total_selesai = selesai_float.toFixed(0);


                  //obj.on_process;obj.on_process_telat
                  var temp_op_ = parseInt(obj.on_process);
                  var temp_selesai_ = parseInt(obj.on_process_telat);
                  var seratusPersen_ = temp_op_+temp_selesai_;
                  var op_float_ = parseFloat(temp_op_/seratusPersen_)*100;
                  var selesai_float_ = parseFloat(temp_selesai_/seratusPersen_)*100;
                  obj.on_process = op_float_.toFixed(0);
                  obj.on_process_telat = selesai_float_.toFixed(0);

                  //obj.total_on_time;obj.total_terlambat
                  var temp_op_a = parseInt(obj.total_on_time);
                  var temp_selesai_a = parseInt(obj.total_terlambat);
                  var seratusPersen_a = temp_op_a+temp_selesai_a;
                  var op_float_a = parseFloat(temp_op_a/seratusPersen_a)*100;
                  var selesai_float_a = parseFloat(temp_selesai_a/seratusPersen_a)*100;
                  obj.total_on_time = op_float_a.toFixed(0);
                  obj.total_terlambat = selesai_float_a.toFixed(0);


                	createDrilldown(obj);
                };
                //populate_table_bulanan();
                //chartMonthly(data_bulanan);
    });
     
});


function createDrilldown(obj){
	Highcharts.theme = {
   colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: "Dosis, sans-serif"
      }
   },
   title: {
      style: {
         fontSize: '16px',
         fontWeight: 'bold',
         textTransform: 'uppercase'
      }
   },
   tooltip: {
      borderWidth: 0,
      backgroundColor: 'rgba(219,219,216,0.8)',
      shadow: false
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      labels: {
         style: {
            fontSize: '12px'
         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      title: {
         style: {
            textTransform: 'uppercase'
         }
      },
      labels: {
         style: {
            fontSize: '12px'
         }
      }
   },
   plotOptions: {
      candlestick: {
         lineColor: '#404048'
      }
   },


   // General
   background2: '#F0F0EA'
   
};
	Highcharts.setOptions(Highcharts.theme);
	$('#Bidang_'+obj.id_bidang).highcharts({
        chart: {
            type: 'pie',
            margin: 10
        },
        title: {
            text:obj.nama_flow
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                },
            pie:{
                  showInLegend: true
            }
            }
        },
        credits:false,
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [{
            name: "-",
            colorByPoint: true,
            dataLabels: {
                            color:'#fff',
                            distance: -50,
                        },
            data: [{
                name: "On Proses",
                y: parseInt(obj.izin_on_process),
                drilldown: "Detail On Proses"
            }, { 
                name: "Selesai",
                y: parseInt(obj.total_selesai),
                drilldown: "Detail Selesai"
            }]
        }],
        drilldown: {
            series: [
            	{
                name: "Detail On Proses",
                id: "Detail On Proses",
                dataLabels: {
                            color:'white',
                            distance: -50,
                        },
                data: [
                    ["On Process", parseInt(obj.on_process)],
                    ["On Process Late", parseInt(obj.on_process_telat)]               
                 ]
            }, {
                name: "Selesai",
                id: "Detail Selesai",
                dataLabels: {
                            color:'white',
                            distance: -50,
                        },
                data: [
                    ["Finish On Time", parseInt(obj.total_on_time)],
                    ["Finish Late", parseInt(obj.total_terlambat)],
                 ]
            }]
        }
    });
}