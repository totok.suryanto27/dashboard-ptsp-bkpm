/*
- DASHBOARD TAB BIDANG
*/
$(function() {
    //no filter
    var containerChart = $('.container-chart')
    $.get(base_url + '/rest/dashboard-bidang', function(data) {
        data.forEach(function(e, i) {
            var _pieChartElement = "<div id='c" + i + "' class='col-md-4'></div>";
            // if (e.persen_on_time == 0 && e.persen_delay == 0) {
            //     _pieChartElement = "<div  class='col-md-4'>Data Belum Tersedia</div>";
            // }
            containerChart.append(_pieChartElement);
            generatePieChart('#c' + i, e);
        });
    });

    function generatePieChart(element, data) {
        console.log("their datas");
        console.log(element);
        console.log(data.persen_delay);
        // nama_flow: "Perindustrian"
        // persen_on_time: 0
        // persen_terlambat: 0
        // total_on_time: "0"
        // total_selesai: "0"
        // total_terlambat: "0"
        // Make monochrome colors and set them as default for all pies
        // Highcharts.getOptions().plotOptions.pie.colors = (function() {
        //     var colors = [],
        //         base = Highcharts.getOptions().colors[0],
        //         i;
        //     for (i = 0; i < 10; i += 1) {
        //         // Start out with a darkened base color (negative brighten), and end
        //         // up with a much brighter color
        //         colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        //     }
        //     return colors;
        // }());
        // Build the chart
        $(element).highcharts({
            credits: false,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: data.nama_flow
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white',
                            fontSize: "14px"
                        },
                        distance: -60
                    },
                    showInLegend: true,
                }
            },
            series: [{
                name: "Persentase",
                data: [{
                    name: "On Time",
                    y: parseInt(data.persen_on_time)
                }, {
                    name: "Delay",
                    y: parseInt(data.persen_terlambat)
                }]
            }]
        });
    }
});
/*
- DASHBOARD TAB KEMENTRIAN
*/
//filter
$(function() {
    //var components
    var idKementrian = $('#kementrian'),
        waktuKementrian = $('#waktu-kementrian'),
        tanggalHarianKementrian = $('#tanggal-harian-kementrian'),
        cBulanKementrian = $('#cbulan-kementrian'),
        bulanKementrian = $('#bulan-kementrian'),
        cTahunKementrian = $('#ctahun-kementrian'),
        tahunKementrian = $('#tahun-kementrian'),
        cTanggalHarian2Kementrian = $('#cTanggalHarianKementrian'),
        tanggalHarian2Kementrian = $('#tanggal-harian2-kementrian');
    var totalIzinSelesaiBidang = $('#total_selesai_bidang'),
        totalOnTimeBidang = $('#total_on_time_bidang'),
        totalTerlambatBidang = $('#total_terlambat_bidang'),
        totalIzinOnProsesBidang = $('.total_izin_on_proses_bidang'),
        totalIzinOnProsesDelayBidang = $('#total_izin_proses_delay_bidang'),
        totalPengajuan = $('#total_pengajuan');
    var containerOnProses = $('#container-on-proses');
    totalSumProses = $('.total_sum_proses');
    var loadingBar = $('#loading-bar');
    var title = $('#title');
    //init components,
    waktuKementrian.select2();
    bulanKementrian.select2();
    tahunKementrian.select2();
    tanggalHarianKementrian.datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy",
        // startView: 'decade',
        // minViewMode: "year",
        todayHighlight: true,
    });
    tanggalHarian2Kementrian.datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy",
        // startView: 'decade',
        // minViewMode: "year",
        todayHighlight: true,
    });
    //switch components
    waktuKementrian.on('change', function(e) {
        var _val = parseInt($(this).val());
        switch (_val) {
            case 1:
                tanggalHarianKementrian.fadeIn('fast');
                cBulanKementrian.fadeOut('fast');
                cTahunKementrian.fadeOut('fast');
                containerOnProses.fadeIn('fast');
                break;
            case 2:
                tanggalHarianKementrian.fadeOut('fast');
                cBulanKementrian.fadeIn('fast');
                cTahunKementrian.fadeIn('fast');
                containerOnProses.fadeOut('fast');
                break;
            case 3:
                tanggalHarianKementrian.fadeOut('fast');
                cBulanKementrian.fadeOut('fast');
                cTahunKementrian.fadeIn('fast');
                containerOnProses.fadeOut('fast');
                break;
        }
    });
    //filter by harian
    console.log("waktu kementrian");
    console.log(waktuKementrian.val());
    // if (waktuKementrian.val() == 1) {
    tanggalHarianKementrian.on('change', function(e) {
        var _tanggal = $(this).val();
        loadingBar.show();
        console.log('pilih tanggal : ' + _tanggal);
        //tanpa id kementrian
        if (idKementrian.val() == "" && waktuKementrian.val() == 1) {
            console.log('tanpa id kementrian');
            $.get(base_url + '/rest/dashboard-kementrian?filter=1&tanggal=' + _tanggal, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                }else{
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tanggal : " + _tanggal);
                }
                
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() != "" && waktuKementrian.val() == 1) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian?id=' + idKementrian.val() + "&filter=1&tanggal=" + _tanggal, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                }else{
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tanggal : " + _tanggal);
                }
                
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
    });
    // }
    //ketika pilih kementrian tapi tanggal masih kosong, minta isi dulu tanggal nya.
    idKementrian.on('change', function(e) {
        console.log('dari pilih kementrian');
        loadingBar.show();
        if ($(this).val() == "") {
            window.location.href = base_url;
        } else
        if (tanggalHarianKementrian.val() == "" && waktuKementrian.val() == 1) {
            alert('pilih tanggal dahulu');
            loadingBar.hide();
        } else if (tanggalHarianKementrian.val() != "" && waktuKementrian.val() == 1) {
            var _id = $(this).val(),
                _tanggal = tanggalHarianKementrian.val();
            $.get(base_url + '/rest/dashboard-kementrian?id=' + _id + "&filter=1&tanggal=" + _tanggal, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tanggal : " + _tanggal);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
        //bulan
        if (bulanKementrian.val() == "" && waktuKementrian.val() == 2) {
            alert('pilih bulan dahulu');
            loadingBar.hide();
        } else if (bulanKementrian.val() != "" && waktuKementrian.val() == 2) {
            var _id = $(this).val(),
                _bulan = bulanKementrian.val();
            $.get(base_url + '/rest/dashboard-kementrian?id=' + _id + "&filter=2&bulan=" + _bulan, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Bulan : " + data.result_all[0].bulan + ' Tahun : ' + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (bulanKementrian.val() != "" && tahunKementrian.val() != "" && waktuKementrian.val() == 2) {
            var _id = $(this).val(),
                _bulan = bulanKementrian.val();
            $.get(base_url + '/rest/dashboard-kementrian?id=' + _id + "&filter=2&bulan=" + _bulan + '&tahun' + tahunKementrian.val(), function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Bulan : " + data.result_all[0].bulan + ' Tahun : ' + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
        //tahun
        if (tahunKementrian.val() == "" && waktuKementrian.val() == 3) {
            alert('pilih tahun dahulu');
            loadingBar.hide();
        } else if (tahunKementrian.val() != "" && waktuKementrian.val() == 3) {
            var _id = $(this).val(),
                _tahun = tahunKementrian.val();
            $.get(base_url + '/rest/dashboard-kementrian?id=' + _id + "&filter=3&tahun=" + _tahun, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tahun : " + _tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
    });
    //filter by bulan
    bulanKementrian.on('change', function(e) {
        var _bulan = $(this).val();
        loadingBar.show();
        if (idKementrian.val() == "" && waktuKementrian.val() == 2) {
            console.log('tanpa id kementrian');
            $.get(base_url + '/rest/dashboard-kementrian?filter=2&bulan=' + _bulan, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Bulan : " + data.result_all[0].bulan + ' Tahun : ' + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() != "" && waktuKementrian.val() == 2) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian?id=' + idKementrian.val() + "&filter=2&bulan=" + _bulan, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Bulan : " + data.result_all[0].bulan + ' Tahun : ' + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() != "" && tahunKementrian.val() != "" && waktuKementrian.val() == 2) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian?id=' + idKementrian.val() + "&filter=2&bulan=" + _bulan + "&tahun=" + tahunKementrian.val(), function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Bulan : " + data.result_all[0].bulan + ' Tahun : ' + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
    })
    //filter by tahun
    tahunKementrian.on('change', function(e) {
        var _tahun = $(this).val();
        loadingBar.show();
        if (idKementrian.val() == "" && waktuKementrian.val() == 3) {
            console.log('tanpa id kementrian');
            $.get(base_url + '/rest/dashboard-kementrian?filter=3&tahun=' + _tahun, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tahun " + _tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() != "" && waktuKementrian.val() == 3) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian?id=' + idKementrian.val() + "&filter=3&tahun=" + _tahun, function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tahun " + _tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() == "" && bulanKementrian.val() != "" && waktuKementrian.val() == 2) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian' + "?filter=2&tahun=" + _tahun + "&bulan=" + bulanKementrian.val(), function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tahun " + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        } else if (idKementrian.val() != "" && bulanKementrian.val() != "" && waktuKementrian.val() == 2) {
            console.log('pake id kmenetrian');
            $.get(base_url + '/rest/dashboard-kementrian?id=' + idKementrian.val() + "&filter=2&tahun=" + _tahun + "&bulan=" + bulanKementrian.val(), function(data) {
                console.log(data);
                if (data.result_all <= 0) {
                    title.html('Belum ada informasi pada tanggal tersebut');
                } else {
                    title.html('<strong>' + (data.result_all[0].nama_instansi || 'Semua Kementrian') + "</strong> Tahun " + data.result_all[0].tahun);
                }
                totalIzinSelesaiBidang.text(data.total_selesai);
                totalOnTimeBidang.text(data.total_on_time);
                totalTerlambatBidang.text(data.total_terlambat);
                totalIzinOnProsesBidang.text(data.total_izin_on_proses);
                totalIzinOnProsesDelayBidang.text(data.total_izin_proses_delay);
                totalSumProses.text(data.total_sum_proses);
                totalPengajuan.text(data.total_pengajuan);
                loadingBar.hide();
            });
        }
    })
});