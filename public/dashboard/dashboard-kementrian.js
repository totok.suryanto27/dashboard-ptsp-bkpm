$(function(){
	var filterTypeKementrian = $('#filter-type-kementrian');
	var filterHarianKementrian = $('#harian_kementrian');
    var filterBulananKementrian = $('#bulanan_kementrian');
    var filterTahunanKementrian = $('#tahunan_kementrian');
    var filterPeriodikKementrian = $('#periodik_kementrian');

    filterTypeKementrian.select2();
    var selectKementrian = $('#kementrian');
        $('#dp_kemen_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
     
    $('#dp_kemen_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_kemen_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_kemen_daily').on('change',function(){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_judul_km();
         };
    });


    $('#dp_kemen_periodik_to').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
    });

    $('#dp_kemen_periodik_from').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
    });


    selectKementrian.on('change',function(event){
        event.preventDefault();
        change_judul_km();
        change_on_process();
    });

 
    filterTypeKementrian.on('change', function(event){
        event.preventDefault();
        filterHarianKementrian.hide();
        filterBulananKementrian.hide();
        filterTahunanKementrian.hide();
        filterPeriodikKementrian.hide();

        switch(filterTypeKementrian.val()){
            case '1' : filterHarianKementrian.show();break;
            case '2' : filterBulananKementrian.show();break;
            case '3' : filterTahunanKementrian.show();break;
            case '4' : filterPeriodikKementrian.show();break;
        }

        change_judul_km();
    });

    $('#m_kementrian_bulanan').select2();
    $('#m_kementrian_bulanan').on('change',function(event){
        change_judul_km();

    });

    $('#tahun_kementrian_bulan').select2();
	$('#tahun_kementrian_bulan').on('change',function(event){
        change_judul_km();


    });

    $('#tahun_kementrian').select2();
    $('#tahun_kementrian').on('change',function(event){
        change_judul_km();
    });


    change_judul_km();
    change_on_process();

});

function get_kemen(){
    var kementrian = " Seluruh Instansi ";
    
    var filterType = $('#filter-type').val();

    if ($('#kementrian').val() != "-") {
        kementrian = "Instansi "+$("#kementrian option:selected").text();
    };
    return kementrian;
}

function change_by_periodik_km(){
    var url = base_url + '/rest/dashboard-data-periodik?tanggal='+$('#dp_kemen_periodik_from').val()+'&tanggal_sd='+$('#dp_kemen_periodik_to').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);
    });
}

function change_on_process(){
    var url = base_url + '/rest/dashboard-on-proses';
    if ($('#kementrian').val() != "-") {
        url = url+'?kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_sum_proses').html(obj_data.izin_on_process);
        $('#total_izin_proses_delay_bidang').html(obj_data.on_process_telat);
        var izin_on_process = parseInt(obj_data.izin_on_process)- parseInt(obj_data.on_process_telat);
        $('#total_izin_on_proses_bidang').html(izin_on_process);
    });
}

 
function change_by_date_km(){
    var url = base_url + '/rest/dashboard-data-harian?tanggal='+$('#dp_kemen_daily').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
         $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);

    });
}

function change_by_month_km(){
    var url = base_url + '/rest/dashboard-data-bulanan?bulan='+$('#m_kementrian_bulanan').val()+'&tahun='+ $('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
         $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);
    });


}

function change_by_year_km(){
    var url = base_url + '/rest/dashboard-data-tahunan?tahun='+$('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_tahunan = JSON.parse(data);
        var obj_data = data_tahunan[0];
        $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);
    });

}

function change_judul_km(){
    var type = $('#filter-type-kementrian').val();
    var types = "";
    var bidang = $("#bidang").val();
    $('#container-on-proses').hide('fast');
    if (type == "1") {
        var hari = $('#dp_kemen_daily').val();
        types = "Dashboard "+get_kemen()+" <br> Tanggal : "+hari;
        change_by_date_km();
        $('#container-on-proses').show('slow');
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        change_by_month_km();
        types = "Dashboard "+get_kemen()+" <br> Bulan : "+$('#m_kementrian_bulanan option:selected').text()+" "+$('#tahun_kementrian_bulan').val();
        
    } else if (type == "3") {
        var tahun = $('#tahun_kementrian').val();
        types = "Dashboard "+get_kemen()+" <br> Tahun : "+tahun;
        change_by_year_km();

    } else if (type == "4") {
        var hari_1 = $('#dp_kemen_periodik_from').val();
        var hari_2 = $('#dp_kemen_periodik_to').val();
        types = "Dashboard "+get_kemen()+" <br> Periode : "+hari_1+" s/d "+hari_2;
        change_by_periodik_km();
        $('#container-on-proses').show('slow');
    };


    $('#judul_kementrian').hide('fast');
    $('#judul_kementrian').html(types);
    $('#judul_kementrian').show('slow');

    }