function GetDay(intDay){
    var DayArray = new Array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu")
    return DayArray[intDay]
}
function GetDayEn(intDay){
    var DayArray = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
    return DayArray[intDay]
}
function GetMonth(intMonth){
    var MonthArray = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember") 
    return MonthArray[intMonth] 	  	 
}
function GetMonthEn(intMonth){
    var MonthArray = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December") 
    return MonthArray[intMonth]          
}
function getDayStr(){
    var today = new Date()
    var todayStr = GetDay(today.getDay())
    return todayStr
}
function getDayStrEn(){
    var today = new Date()
    var todayStr = GetDayEn(today.getDay())
    return todayStr
}
function getDateStr(){
    var today = new Date()
    var year = today.getYear()
    if(year<1000) year+=1900
    var todayStr = ", "
    todayStr += today.getDate() + " " + GetMonth(today.getMonth())
    todayStr += " " + year
    return todayStr
}
function getDateStrEn(){
    var today = new Date()
    var year = today.getYear()
    if(year<1000) year+=1900
    var todayStr = ", "
    todayStr += today.getDate() + " " + GetMonthEn(today.getMonth())
    todayStr += " " + year
    return todayStr
}