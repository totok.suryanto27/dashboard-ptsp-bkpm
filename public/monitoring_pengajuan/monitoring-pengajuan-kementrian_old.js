var gaugeOptions = {

        chart: {
            type: 'bar'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false 
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 10,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
                    },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 10,
                    borderWidth: 3,
                    useHTML: true
                }
            }
        }
    };

$(function(){
	var filterTypeKementrian = $('#filter-type-kementrian');
	var filterHarianKementrian = $('#harian_kementrian');
    var filterBulananKementrian = $('#bulanan_kementrian');
    var filterTahunanKementrian = $('#tahunan_kementrian');
    var filterPeriodikKementrian = $('#periodik_kementrian');

    filterTypeKementrian.select2();
    var selectKementrian = $('#kementrian');
        $('#dp_kemen_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
    
    $('#dp_kemen_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_kemen_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_kemen_daily').on('change',function(){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_judul_km();
         };
    });


    $('#dp_kemen_periodik_to').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
        
    });

    $('#dp_kemen_periodik_from').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
    });


    selectKementrian.on('change',function(event){
        event.preventDefault();
        change_judul_km();
    });


    filterTypeKementrian.on('change', function(event){
        event.preventDefault();
        filterHarianKementrian.hide();
        filterBulananKementrian.hide();
        filterTahunanKementrian.hide();
        filterPeriodikKementrian.hide();

        switch(filterTypeKementrian.val()){
            case '1' : filterHarianKementrian.show();break;
            case '2' : filterBulananKementrian.show();break;
            case '3' : filterTahunanKementrian.show();break;
            case '4' : filterPeriodikKementrian.show();break;
        }

        change_judul_km();
       
    });

    $('#m_kementrian_bulanan').select2();
    $('#m_kementrian_bulanan').on('change',function(event){
        change_judul_km();

    });

    $('#tahun_kementrian_bulan').select2();
	$('#tahun_kementrian_bulan').on('change',function(event){
        change_judul_km();

    });

    $('#tahun_kementrian').select2();
    $('#tahun_kementrian').on('change',function(event){
        change_judul_km();
    });
    

    change_judul_km();

           // create_chart();
    

});

function change_judul_km(){

    var type = $('#filter-type-kementrian').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_kemen_daily').val();
        types = "Monitoring Pengajuan Izin "+get_kemen()+" <br> Tanggal : "+hari;
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        types = "Monitoring Pengajuan Izin "+get_kemen()+" <br> Bulan : "+$('#m_kementrian_bulanan option:selected').text()+" "+$('#tahun_kementrian_bulan').val();
        change_by_month_km();
		//location.reload();
    } else if (type == "3") {
        var tahun = $('#tahun_kementrian').val();
        types = "Monitoring Pengajuan Izin "+get_kemen()+" <br> Tahun : "+tahun;
        change_by_year_km();
		//location.reload();
    } else if (type == "4") {
        var hari_1 = $('#dp_kemen_periodik_from').val();
        var hari_2 = $('#dp_kemen_periodik_to').val();
        types = "Monitoring Pengajuan Izin "+get_kemen()+" <br> Periode : "+hari_1+" s/d "+hari_2;
		change_by_periodik_km();
		//location.reload();
    };


    $('#judul_kementrian').hide('fast');
    $('#judul_kementrian').html(types);
    $('#judul_kementrian').show('slow');

}

function get_kemen(){
    var kementrian = " Seluruh Kementrian ";
    
    var filterType = $('#filter-type').val();

    if ($('#kementrian').val() != "-") {
        kementrian = "Kementrian "+$("#kementrian option:selected").text();
    };
    return kementrian;
}
 
function change_by_month_km(){
    var url = base_url + '/rest/monitoring-pengajuan-bulanan-km?bulan='+$('#m_kementrian_bulanan').val()+'&tahun='+ $('#tahun_kementrian').val();
    
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    
    var url2 = base_url + '/rest/monitoring-pengajuan-bulanan?bulan='+$('#m_kementrian_bulanan').val()+'&tahun='+ $('#tahun_kementrian').val();
     
     $.ajax({
        url: url2,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result2 = data;
        } 
     });
     
    $.get(url, function(data) {
        populate_table_x2(data,$('#m_kementrian_bulanan').val(),$('#tahun_kementrian').val(),'1',result2);
       
    });
 
}

function change_by_year_km(){
    var url = base_url + '/rest/monitoring-pengajuan-tahunan-km?tahun='+$('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    
    var url2 = base_url + '/rest/monitoring-pengajuan-tahunan?tahun='+$('#tahun_kementrian').val();
	
	 $.ajax({
        url: url2,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result2 = data;
        } 
     });
     
    $.get(url, function(data) {
        populate_table_x2(data,$('#tahun_kementrian').val(),'Year','2',result2);
       
    }); 
 
}

function change_by_periodik_km(){
    var url = base_url + '/rest/monitoring-pengajuan-periodik-km?from_date='+$('#dp_kemen_periodik_from').val()+'&to_date='+ $('#dp_kemen_periodik_to').val();
     
    if ($('#kementrian').val() != "") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    
    var url2 = base_url + '/rest/monitoring-pengajuan-periodik?from_date='+$('#dp_kemen_periodik_from').val()+'&to_date='+ $('#dp_kemen_periodik_to').val();
	  
	  $.ajax({
        url: url2,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result2 = data;
        } 
     });
    
    $.get(url, function(data) {
        populate_table_x2(data,$('#dp_kemen_periodik_from').val(),$('#dp_kemen_periodik_to').val(),'3',result2); 
    });  
}

function populate_table_x2(data,tgl_from,tgl_to,kondisi,data2){
	 
    var instansi = [];
    var value = [];
    var objeks = [];
    var objeks2 = [];
    var hasil = [];
    var hasil2 = [];
    
	$('#tabel-kemen').empty();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var tp = parseInt(obj.total_pengajuan); 
        var performansi = 0;
        if (tp > 0 ) { 
            performansi = tp.toFixed(0);
        };
         
        var isi = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td></tr>";
        $('#tabel-kemen').append(isi);  
        
        var newObj2 = {       name : obj.nama_instansi,
							     y : parseInt(performansi),
					     drilldown : obj.nama_instansi
					};
        objeks2.push(newObj2); 
        
        hasil=[];  
		for(var x = 0; x < data2.length; x++){
		    var a = data2[x];
			if(obj.id_instansi == a.id_instansi){ 
					var value = [a.nama_jenis_perizinan,parseFloat(a.total_pengajuan)];
					hasil.push(value);  
					
					var value2 = {	name : obj.nama_instansi,
							  id : obj.nama_instansi,
							data : hasil 
						};
					hasil2.push(value2);  
				}
		}; 
				
		var newObj = {id_instansi:obj.id_m_instansi,nama_instansi:obj.nama_instansi,performansis:parseInt(performansi),from_date:tgl_from,to_date:tgl_to,v_kondisi:kondisi}; 
		objeks.push(newObj); 
    };
     
    buat_klasemen2(objeks2,hasil2);
    
    var x = $('#tbl_kemen').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
	//location.reload();
}

function buat_klasemen2(objeks,objeks2){
	 
    // chart drilldown
	$(function () {
		// Create the chart
		$('#container-speed').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Monitoring Pengajuan Izin Seluruh Kementrian '
			}, 
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Total Pengajuan Izin Kementerian'
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y:.1f}'
					}
				}
			},
			tooltip: {
						valueSuffix: ' Jumlah'
					},
			series: [{
				name: 'Instansi',
				colorByPoint: true,
				data: objeks 
			}]
			,drilldown: {
				series: objeks2
				}
		});
	});
}

/*
function change_by_month_iz2(tgl_awal,tgl_akhir,instansi){
    var url = base_url + '/rest/monitoring-pengajuan-bulanan?bulan='+tgl_awal+'&tahun='+tgl_akhir+'&instansi='+instansi;
     
     $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        } 
     });
     
     return result;
  
}

function change_by_year_iz2(tgl_awal,instansi){
    var url = base_url + '/rest/monitoring-pengajuan-tahunan?tahun='+tgl_awal+'&instansi='+instansi;
	
	 $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        } 
     });
     
     return result;
 
}

function change_by_periodik_iz2(tgl_awal,tgl_akhir,instansi){
    var url = base_url + '/rest/monitoring-pengajuan-periodik?from_date='+tgl_awal+'&to_date='+tgl_akhir+'&instansi='+instansi;
	  
	  $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        } 
     });
     
     return result;
 
}

function populate_table_izin2(data,status){ 
    
    $('#tabel-izi').empty();
    var instansi = [];
    var value = [];
    var objeks = [];
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var tp = parseInt(obj.total_pengajuan); 
        var performansi = 0;
        
		if (tp > 0 ) { 
			performansi = tp.toFixed(0);
		};
        var isi = "<tr><td>"+obj.nama_jenis_perizinan+"</td><td>"+performansi+" </td></tr>";
        $('#tabel-izi').append(isi);
        
        var newObj = {nama_jenis_perizinan:obj.nama_jenis_perizinan,performansis:parseInt(performansi)};
        objeks.push(newObj);
        
    };
    buat_klasemen_izin2(objeks);
    var x = $('#tbl_izi').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });

}

function buat_klasemen_izin2(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];
    var nama_jenis_perizinan = [];
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis);
        nama_jenis_perizinan.push(o.nama_jenis_perizinan);
    };
 
    $('#container-speed-izin').highcharts({
        chart: {
            type: 'bar',
            height: 5000
        },
        title: {
            text: 'Komparasi Persentase Ketepatan Waktu Layanan Perizinan'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_jenis_perizinan,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Jumlah'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Jumlah',
            data: value
        }]
    });
}
 
function populate_table_x(data){ 
    
    var instansi = [];
    var value = [];
    var objeks = [];
	$('#tabel-kemen').empty();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var ot = parseInt(obj.total_on_time);
        var dl = parseInt(obj.total_terlambat);
        var persen = ot+dl;
        var performansi = 0;
        if (persen > 0 ) {
            performansi =  (ot/persen)*100;
            performansi = performansi.toFixed(0);
        };
         
        var isi = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td><td>"+performansi+" % </td></tr>";
        $('#tabel-kemen').append(isi);
 
        var newObj = {nama_instansi:obj.nama_instansi,performansis:parseInt(performansi)};
        objeks.push(newObj);
        
    };
    buat_klasemen(objeks);
    var x = $('#tbl_kemen').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });

    
}




function buat_klasemen2(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];  
    var nama_instansi = [];
    var tgl_awal=objeks[0].from_date;
    var tgl_akhir=objeks[0].to_date;
    var v_kondisi=objeks[0].v_kondisi;
  
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis); 
        nama_instansi.push(o.nama_instansi);
    };

     
    $('#container-speed').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Monitoring Pengajuan Izin Layanan Instansi'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Jumlah'
        },
        plotOptions: {
			series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
							// Function After Click //
                            //alert('Category: ' + this.category + ', value: ' + this.y+ ', ser: ' + this.series.name+ ', kondisi: ' + v_kondisi); 
                            
                            //title Tab
                            $('#pressKementrian').removeClass('active');
                            $("#pressIzin").addClass("active");
                            //content Tab
                            $('#Kementrian').removeClass('tab-pane active').addClass('tab-pane'); 
                            $("#Izin").addClass("tab-pane active");
                            //function detail per Instansi 
                            
							if (v_kondisi == "1") { //by Bulan
								types = "Monitoring Pengajuan Izin <br> Bulan  : "+tgl_awal+" - "+tgl_akhir+" <br> Instansi :  "+this.category;
								change_by_month_iz2(tgl_awal,tgl_akhir,this.category,this.series.name);
							} else if (v_kondisi == "2") { //by Tahun
								types = "Monitoring Pengajuan Izin <br> Tahun : "+tgl_awal+ " <br> Instansi :  "+this.category;
								change_by_year_iz2(tgl_awal,this.category,this.series.name);
							} else if (v_kondisi == "3") { //by Periodik
								types = "Monitoring Pengajuan Izin <br> Periode : "+tgl_awal+" s/d "+tgl_akhir+ " <br> Instansi :  "+this.category;  
								change_by_periodik_iz2(tgl_awal,tgl_akhir,this.category,this.series.name);
							} 
							
                            $('#judul_izin').hide('fast');
							$('#judul_izin').html(types);
							$('#judul_izin').show('slow');
							
                        }
                    }
                }
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: { 
            align: 'left',
			verticalAlign: 'top',
			y: 20,
			floating: true,
			borderWidth: 0
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Jumlah',
            data: value
        }
        ]
    });
}
* 
function anakData(v_kondisi,tgl_awal,tgl_akhir,instansi){
	
	var value = [];   
    var hasil = []; 
    
	// chart Child
	if (v_kondisi == "1") { //by Bulan 
		var anak = change_by_month_iz2(tgl_awal,tgl_akhir,instansi);
	} else if (v_kondisi == "2") { //by Tahun 
		var anak = change_by_year_iz2(tgl_awal,instansi);
	} else if (v_kondisi == "3") { //by Periodik 
		var anak = change_by_periodik_iz2(tgl_awal,tgl_akhir,instansi);
	}  
	 
	  for(var i = 0; i < anak.length; i++){
			var a = anak[i];
			var value = [a.nama_jenis_perizinan,parseFloat(a.total_pengajuan)]
			hasil.push(value);  
		};
 
	return hasil;
}


function buat_klasemen(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];  
    var nama_instansi = [];
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis); 
        nama_instansi.push(o.nama_instansi);
    };
 
    $('#container-speed').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Komparasi Persentase Ketepatan Waktu Layanan Instansi'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Persen'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
                 series: {
                stacking: 'normal'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Persentase',
            data: value}]
    });
}
* 
*/

function create_chart(){
    
     /*$('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Komparasi Performansi',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}'+' %'+'</span><br/>' +
                       '<span style="font-size:12px;color:silver">Performa</span></div>'
            },
            
        }]

    }));*/
}
