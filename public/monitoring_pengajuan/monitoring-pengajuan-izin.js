$(function(){
	var filterTypeIzin = $('#filter-type-izin');
	var filterHarianIzin = $('#harian_izin');
    var filterBulananIzin = $('#bulanan_izin');
    var filterTahunanIzin = $('#tahunan_izin');
    var filterPeriodikIzin = $('#periodik_izin');
	
	 $('#instansi').on('change',function(){
		var instansi = $('#instansi').val();
		 if (instansi != "") {
			change_judul_iz();
		 };
	});

	
    filterTypeIzin.select2();
    var selectIzin = $('#izin');
        $('#dp_izi_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
    
    $('#dp_izi_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_izi_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_izi_daily').on('change',function(){
        var tanggal = $('#dp_izi_daily').val();
         if (tanggal != "") {
            change_judul_iz();
         };
    });


    $('#dp_izi_periodik_to').on('change',function(){
        var tanggal = $('#dp_izi_periodik_from').val();
        var tanggal_sd = $('#dp_izi_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_iz();
         }; 
    });

    $('#dp_izi_periodik_from').on('change',function(){
        var tanggal = $('#dp_izi_periodik_from').val();
        var tanggal_sd = $('#dp_izi_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_iz();
         }; 
    });


    selectIzin.on('change',function(event){
        event.preventDefault();
        change_judul_iz();
    });


    filterTypeIzin.on('change', function(event){
        event.preventDefault();
        filterHarianIzin.hide();
        filterBulananIzin.hide();
        filterTahunanIzin.hide();
        filterPeriodikIzin.hide();

        switch(filterTypeIzin.val()){
            case '1' : filterHarianIzin.show();break;
            case '2' : filterBulananIzin.show();break;
            case '3' : filterTahunanIzin.show();break;
            case '4' : filterPeriodikIzin.show();break;
        }

        change_judul_iz();
    });

    $('#m_izin_bulanan').select2();
    $('#m_izin_bulanan').on('change',function(event){
        change_judul_iz();

    });

    $('#tahun_izin_bulan').select2();
	$('#tahun_izin_bulan').on('change',function(event){
        change_judul_iz();

    });

    $('#tahun_izin').select2();
    $('#tahun_izin').on('change',function(event){
        change_judul_iz();
    });
    

    change_judul_iz();

           // create_chart();
    

});

function get_izi(){
    var izin = " Seluruh Izin ";
    
    var filterType = $('#filter-type').val();

    if ($('#izin').val() != "-") {
        izin = "Izin "+$("#izin option:selected").text();
    };
    return izin;
}
 
function change_by_periodik_iz3(){
    var url = base_url + '/rest/monitoring-pengajuan-periodik?from_date='+$('#dp_izi_periodik_from').val()+'&to_date='+$('#dp_izi_periodik_to').val();
    
    if ($('#instansi').val() != "-") { 
        url = url+'&id_instansi='+$('#instansi').val();
    };
    
    $.get(url, function(data) {
        populate_table_izin3(data,$('#dp_izi_periodik_from').val(),$('#dp_izi_periodik_to').val());
       
    });
}

function populate_table_izin3(data,tgl_from,tgl_to){
	
    //location.reload();
    var instansi = [];
    var value = [];
    var objeks = [];
    
	$('#tabel-izi').empty(); 
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var tot_peng = parseInt(obj.total_pengajuan); 
         
        var izin = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td></tr>";
        $('#tabel-izi').append(izin);  
        
        var newObj = {nama_instansi:obj.nama_instansi,jumlah:parseInt(obj.total_pengajuan)};
        objeks.push(newObj);
        
    };
    
    buat_klasemen3(objeks);
    
    var x = $('#tabel-izi').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
	//location.reload();
	 
}

function buat_klasemen3(objeks,tgl_from,tgl_to){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];  
    var nama_instansi = []; 
     
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.jumlah); 
        nama_instansi.push(o.nama_instansi);
    };

     
    $('#container-speed-izin').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Jumlah Pengajuan Izin Layanan Kementerian'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Izin'
        },
        plotOptions: {
			series: {
                cursor: 'pointer' 
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: { 
            align: 'left',
			verticalAlign: 'top',
			y: 20,
			floating: true,
			borderWidth: 0
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Jumlah Pengajuan',
            data: value
        }]
    });
}

function change_judul_iz(){

    var type = $('#filter-type-izin').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_izi_daily').val();
        types = "Monitoring Pengajuan Izin "+get_izi()+" <br> Tanggal : "+hari;
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        var instansi = $('#instansi').val();
        types = "Monitoring Pengajuan Izin "+get_izi()+"<br> Bulan : "+$('#m_izin_bulanan option:selected').text()+" "+$('#tahun_izin_bulan').val()+ " <br> Instansi :  "+$('#instansi option:selected').text();
        change_by_month_iz();
    } else if (type == "3") {
        var tahun = $('#tahun_izin').val();
        types = "Monitoring Pengajuan Izin "+get_izi()+" <br> Tahun : "+tahun+ " <br> Instansi :  "+$('#instansi option:selected').text();
        change_by_year_iz();
    } else if (type == "4") {
        var hari_1 = $('#dp_izi_periodik_from').val();
        var hari_2 = $('#dp_izi_periodik_to').val(); 
        types = "Monitoring Pengajuan Izin "+get_izi()+" <br> Periode : "+hari_1+" s/d "+hari_2+ " <br> Instansi :  "+$('#instansi option:selected').text();
		change_by_periodik_iz3();	
    };


    $('#judul_izin').hide('fast');
    $('#judul_izin').html(types);
    $('#judul_izin').show('slow');
}

/*
function buat_klasemen_izin(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];
    var nama_jenis_perizinan = [];
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis);
        nama_jenis_perizinan.push(o.nama_jenis_perizinan);
    };
 
    $('#container-speed-izin').highcharts({
        chart: {
            type: 'bar',
            height: 5000
        },
        title: {
            text: 'Monitoring Pengajuan Izin Layanan Perizinan'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_jenis_perizinan,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0, 
            title: {
                text: 'Monitoring',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Jumlah'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
			align: 'left',
			verticalAlign: 'top',
			y: 20,
			floating: true,
			borderWidth: 0 
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Jumlah',
            data: value
        }]
    });
 }


function buat_klasemen_izin(objeks){
	 var objeks2 = [];
    // chart drilldown
	$(function () {
		// Create the chart
		$('#container-speed-izin').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Monitoring Pengajuan Izin Kementrian '
			}, 
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Total Pengajuan Izin Kementerian'
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y:.1f}'
					}
				}
			},
			tooltip: {
						valueSuffix: ' Jumlah'
					},
			series: [{
				name: 'Instansi',
				colorByPoint: true,
				data: objeks 
			}]
			,drilldown: {
				series: objeks2
				}
		});
	});
}

function change_by_month_iz(){
    var url = base_url + '/rest/monitoring-pengajuan-bulanan?bulan='+$('#m_izin_bulanan').val()+'&tahun='+ $('#tahun_izin').val();
    if ($('#instansi').val() != "-") {
        url = url+'&id_instansi='+$('#instansi').val();
    };
    
    $.get(url, function(data) {
        populate_table_izin(data);
       
    });


}

function change_by_year_iz(){
    var url = base_url + '/rest/monitoring-pengajuan-tahunan?tahun='+$('#tahun_izin').val();
    if ($('#instansi').val() != "-") { 
        url = url+'&id_instansi='+$('#instansi').val();
    };
    $.get(url, function(data) {
        populate_table_izin(data);
       
    });

}
*/
function create_chart(){
    
     /*$('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Komparasi Performansi',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}'+' %'+'</span><br/>' +
                       '<span style="font-size:12px;color:silver">Performa</span></div>'
            },
            
        }]

    }));*/
}
