var data_bulanan=[];
var data_tahunan=[];
$(function(){
	$.get( base_url +  '/rest/data-performansi-bulanan/', function(data) {
                data_bulanan = JSON.parse(data);
                chartMonthly(data_bulanan);
    });

    $.get( base_url +  '/rest/data-performansi-tahunan/', function(data) {
                data_tahunan = JSON.parse(data);
                chartYearsly(data_tahunan);
    });

    var filterBulan = $('#bulan');
    var filterTahun = $('#tahunan');
    filterBulan.select2();
    filterTahun.select2();
    filterBulan.on('change',function(){
    	//console.log($('#bulan').val());
    	var url = '/rest/data-performansi-bulanan?bulan='+$('#bulan').val();
    	//console.log(url);

    	$.get(url, function(data) {
                data_bulanan = JSON.parse(data);
                chartMonthly(data_bulanan);
		    });

    });

    filterTahun.on('change',function(){
    	//console.log($('#bulan').val());
    	var url = '/rest/data-performansi-tahunan?tahun='+$('#tahunan').val();
    	//console.log(url);
    	$.get(url, function(data) {
                data_tahunan = JSON.parse(data);
                chartYearsly(data_tahunan);
		    });

    });

});

function chartMonthly(data){
	var array_kementrian = [];
	var array_value = [];

	for (var i = 0; i < data.length; i++) {
		var obj = data[i];
		var array_object = [];
		array_object.push(obj.nama_instansi);
		array_object.push(parseInt(obj.persent));
		array_value.push(array_object);
	};
	var top_5 = [];
	var bottom_5 = [];
	for (var i = 0; i < 5; i++) {
		var obj = data[i];
		var item_object = [];
		item_object.val = obj.persent+" %";
		item_object.nama = obj.nama_instansi;
		top_5.push(item_object);
	};

	for (var i = data.length-1; i > data.length-6; i--) {
		var obj = data[i];
		var item_object = [];
		item_object.val = obj.persent+" %";
		item_object.nama = obj.nama_instansi;
		bottom_5.push(item_object);
	};
	populateTopFive(top_5);
	populateBottomFive(bottom_5);
	    $('#chart_bulanan').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Performansi Kementrian'
	        },
	        
	        xAxis: {
	             type: 'category',
		            labels: {
		                rotation: -45,
		                style: {
		                    fontSize: '13px',
		                    fontFamily: 'Verdana, sans-serif'
		                }
		            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'total pengajuan / on time',
	                align: 'high'
	            },
	            labels: {
	                overflow: 'justify'
	            }
	        },
	        tooltip: {
	            valueSuffix: ''
	        },
	        plotOptions: {
	            bar: {
	                dataLabels: {
	                    enabled: true
	                }
	            }
	        },
	        series: [{
	            name: 'percent',
	            data: array_value
	        }]
	    });
	}

	function populateTopFive(data){
		$('#top-5-bulanan').empty();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			$('#top-5-bulanan').append("<li class='list-group-item'><span class='badge'>"+obj.val+"</span> "+obj.nama+"</li>");
		};
	}

	function populateBottomFive(data){
		$('#bottom-5-bulanan').empty();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			$('#bottom-5-bulanan').append("<li class='list-group-item'><span class='badge'>"+obj.val+"</span> "+obj.nama+"</li>");
		};
	}

	function populateTopFiveY(data){
		$('#top-5-tahunan').empty();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			$('#top-5-tahunan').append("<li class='list-group-item'><span class='badge'>"+obj.val+"</span> "+obj.nama+"</li>");
		};
	}

	function populateBottomFiveY(data){
		$('#bottom-5-tahunan').empty();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			$('#bottom-5-tahunan').append("<li class='list-group-item'><span class='badge'>"+obj.val+"</span> "+obj.nama+"</li>");
		};
	}

	function chartYearsly(data){
	var array_kementrian = [];
	var array_value = [];

	for (var i = 0; i < data.length; i++) {
		var obj = data[i];
		var array_object = [];
		array_object.push(obj.nama_instansi);
		array_object.push(parseInt(obj.persent));
		array_value.push(array_object);
	};
	var top_5 = [];
	var bottom_5 = [];
	for (var i = 0; i < 5; i++) {
		var obj = data[i];
		var item_object = [];
		item_object.val = obj.persent+" %";
		item_object.nama = obj.nama_instansi;
		top_5.push(item_object);
	};

	for (var i = data.length-1; i > data.length-6; i--) {
		var obj = data[i];
		var item_object = [];
		item_object.val = obj.persent+" %";
		item_object.nama = obj.nama_instansi;
		bottom_5.push(item_object);
	};
	populateTopFiveY(top_5);
	populateBottomFiveY(bottom_5);
	    $('#chart_tahunan').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Performansi Kementrian'
	        },
	        
	        xAxis: {
	             type: 'category',
		            labels: {
		                rotation: -45,
		                style: {
		                    fontSize: '13px',
		                    fontFamily: 'Verdana, sans-serif'
		                }
		            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'total pengajuan / on time',
	                align: 'high'
	            },
	            labels: {
	                overflow: 'justify'
	            }
	        },
	        tooltip: {
	            valueSuffix: ''
	        },
	        plotOptions: {
	            bar: {
	                dataLabels: {
	                    enabled: true
	                }
	            }
	        },
	        series: [{
	            name: 'percent',
	            data: array_value
	        }]
	    });
	}