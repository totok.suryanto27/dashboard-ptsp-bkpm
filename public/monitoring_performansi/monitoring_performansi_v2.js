var data_bulanan=[];
var data_tahunan=[];
$(function(){
	var filterBulan = $('#bulan');
    var filterTahun = $('#tahunan');
    var filterTahunBln = $('#tahunan_bln');
    var filterKementrian = $('#kementrian');
    var filterKementrianBulan = $('#kementrian_bln');

    filterBulan.select2();
    filterTahun.select2();
    filterKementrianBulan.select2();
    filterTahunBln.select2(); 
    gauge_performance('chart_bulanan',0);
    gauge_performance('chart_tahunan',0);
	
	//Jika terjadi error 301 moved permanently tambahkan / pada akhir url
    $.get( base_url +  '/rest/tabel-performansi-bulanan', function(data) {
                data_bulanan = JSON.parse(data);
                populate_table_bulanan();
                //chartMonthly(data_bulanan);
    }); 
	
    $.get( base_url +  '/rest/tabel-performansi-tahunan', function(data) {
                data_tahunan = JSON.parse(data); 
                populate_table_tahunan();
               // chartYearsly(data_tahunan);
    }); 

    filterBulan.on('change',function(event){
            event.preventDefault();
            $.get( base_url +  '/rest/tabel-performansi-bulanan?bulan='+filterBulan.val(), function(data) {
                data_bulanan = JSON.parse(data);
                populate_table_bulanan();
            });
    });

    filterTahun.on('change',function(event){
            event.preventDefault();
            change_tahunan();
            
    });

    filterKementrian.on('change',function(event){
        event.preventDefault();
        change_tahunan();
    });

    filterKementrianBulan.select2();
    filterTahunBln.select2();

    filterTahunBln.on('change',function(event){
        change_bulanan();
    });

    filterKementrianBulan.on('change',function(event){
        change_bulanan();
    });
    

        
});



function change_tahunan(){
    var tahun = $('#tahunan');
    var instansi = $('#kementrian');
    var bulan = $('#bulan');
    var url = base_url +  '/rest/tabel-performansi-tahunan?tahun='+tahun.val(); 
    var judul = "Monitoring Performansi Tahun "+$('#tahunan').val();
    if (instansi.val() != "-") {
        url = url +'&id_instansi='+instansi.val();
        judul = "Monitoring Performansi "+$('#kementrian option:selected').text()+" Tahun "+$('#tahunan').val();
        $('#row_tabel_tahunan').addClass('hilang');
    }else{
        $('#row_tabel_tahunan').removeClass('hilang');

    };
    console.log(url);
    $('#judul_tahun').hide('fast');
    $('#judul_tahun').html(judul);
    $('#judul_tahun').show('slow');
    $.get(url, function(data) {
                data_tahunan = JSON.parse(data);
                populate_table_tahunan();
    });
}

function change_bulanan(){
    var tahun = $('#tahunan_bln');
    var instansi = $('#kementrian_bln');
    var url = base_url +  '/rest/tabel-performansi-bulanan?bulan='+$('#bulan').val()+'&tahun='+tahun.val(); 
    var judul = "Monitoring Performansi Bulan "+$('#bulan option:selected').text()+" Tahun "+tahun.val();
    if (instansi.val() != "-") {
        url = url +'&id_instansi='+instansi.val();
        judul = "Monitoring Performansi "+$('#kementrian_bln option:selected').text()+" Bulan "+$('#bulan option:selected').text()+" Tahun "+$('#tahunan_bln').val();
        $('#row_tabel_bulanan').addClass('hilang');
    }else{
        $('#row_tabel_bulanan').removeClass('hilang');

    };
    console.log(url);
    $('#judul_tahun').hide('fast');
    $('#judul_tahun').html(judul);
    $('#judul_tahun').show('slow');
    $.get(url, function(data) {
               data_bulanan = JSON.parse(data);
                populate_table_bulanan();
    });

    $('#judul').hide('fast');
    $('#judul').html(judul);
    $('#judul').show('slow');

}

function populate_table_bulanan(){
    $('#tabel-bulanan').empty();
    var total_all_pengajuan = 0;
    var total_all_on_time = 0;
    var total_all_delay = 0;
    var total_all_selesai = 0;
    for (var i = 0; i < data_bulanan.length; i++) {
        var obj = data_bulanan[i];
        
        total_all_pengajuan  =  total_all_pengajuan+ parseInt(obj.total_pengajuan);
        total_all_on_time  =  total_all_on_time+ parseInt(obj.total_on_time);
        total_all_delay  =  total_all_delay+ parseInt(obj.total_terlambat);
        total_all_selesai  =  total_all_selesai+ parseInt(obj.total_selesai);
        var presens = parseInt(obj.total_terlambat)+parseInt(obj.total_on_time);
        console.log(obj.nama_instansi+" persen "+presens);
        var presen_delay = (parseInt(obj.total_terlambat)/presens)*100;
        console.log(obj.nama_instansi+" persen "+presen_delay.toFixed(0));
        var presen_ot  = (parseInt(obj.total_on_time)/presens)*100;
            presen_delay = presen_delay.toFixed(0);
            presen_ot = presen_ot.toFixed(0);
        var tbl = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td><td> "+presen_ot+" % </td><td> "+presen_delay+" % </td></tr>";
        $('#tabel-bulanan').append(tbl);
    };
    $('#tbl_bln').DataTable({
        processing: true,
        destroy: true,
    });
    var total = "<tr><td>"+total_all_pengajuan+"</td><td>"+total_all_selesai+"</td><td>"+total_all_delay+"</td><td>"+total_all_on_time+"</td></tr>";
    $('#resume_bulan').empty();
    $('#resume_bulan').append(total);

    if (total_all_selesai != 0) {
        var persentase = (total_all_on_time/total_all_selesai)*100;
        var n = Math.round(persentase);

        var persentase_delay = (total_all_delay/total_all_selesai)*100;
        var o = Math.round(persentase_delay);
        gauge_performance('chart_bulanan',n,'ontime / selesai','Performansi On Time','% Ontime');
        gauge_performance('chart_bulanan_delay',o,'delay / selesai','Performansi Terlambat','% Terlambat');
    }else{
        gauge_performance('chart_bulanan',0,'ontime / selesai','Performansi On Time','% Ontime');
        gauge_performance('chart_bulanan_delay',0,'delay / selesai','Performansi Terlambat','% Terlambat');
    };

    


    
}

function populate_table_tahunan(){
    $('#tabel-tahunan').empty();
    var total_all_pengajuan = 0;
    var total_all_on_time = 0;
    var total_all_delay = 0;
    var total_all_selesai = 0;
                    console.log("jumlah data = "+data_tahunan.length);
                        $('#tabel-tahunan').empty();

    for (var i = 0; i < data_tahunan.length; i++) {
        var obj = data_tahunan[i];
        total_all_pengajuan  =  total_all_pengajuan+ parseInt(obj.total_pengajuan);
        total_all_on_time  =  total_all_on_time+ parseInt(obj.total_on_time);
        total_all_delay  =  total_all_delay+ parseInt(obj.total_terlambat);
        total_all_selesai  =  total_all_selesai+ parseInt(obj.total_selesai);
        var presens = parseInt(obj.total_terlambat)+parseInt(obj.total_on_time);
        var presen_delay = (parseInt(obj.total_terlambat)/presens)*100;
        var presen_ot  = (parseInt(obj.total_on_time)/presens)*100;
            presen_delay = presen_delay.toFixed(0);
            presen_ot = presen_ot.toFixed(0);
        var tbl = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td><td>"+presen_ot+" % </td><td>"+presen_delay+" % </td></tr>";
        $('#tabel-tahunan').append(tbl);


    };
    $('#tbl_thn').DataTable({
        processing: true,
        destroy: true,
    });

    var total = "<tr><td>"+total_all_pengajuan+"</td><td>"+total_all_selesai+"</td><td>"+total_all_delay+"</td><td>"+total_all_on_time+"</td></tr>";
    $('#resume_tahun').empty();
    $('#resume_tahun').append(total);
    
    
    //
    if (total_all_selesai != 0) {
        var persentase = (total_all_on_time/total_all_selesai)*100;
        var n = Math.round(persentase);

        var persentase_delay = (total_all_delay/total_all_selesai)*100;
        var o = Math.round(persentase_delay);
        
        gauge_performance('chart_tahunan',n,'ontime / selesai','Performansi On Time','% Ontime');
      //  gauge_performance('chart_tahunan_delay',o,'delay / pengajuan','Performansi Terlambat','% Terlambat');
    }else{
        gauge_performance('chart_tahunan',0,'ontime / total_selesai','Performansi On Time','% Ontime');
      //  gauge_performance('chart_tahunan_delay',0,'delay / pengajuan','Performansi Terlambat','% Terlambat');
    };

    

}

function gauge_performance(id,values,title_,title_1,tipe_chart){
	$('#'+id).highcharts({

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        credits: false,
        title: {
            text: title_1
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 100,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: title_,
            },
            plotBands: [{
                from: 0,
                to: 60,
                color:  '#c90000'// green
            }, {
                from: 60,
                to: 80,
                color: '#DDDF0D' // yellow
            }, {
                from: 80,
                to: 100,
                color: '#55BF3B' // red
            }]
        },

        series: [{
            name: ' ',
            data: [values],
            tooltip: {
                valueSuffix: tipe_chart
            },
            dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '22px'
                    },
                    format:'{y} %'
                }
        }]

    }
        // Add some life
       /* function (chart) {
            if (!chart.renderer.forExport) {
                setInterval(function () {
                    var point = chart.series[0].points[0],
                        newVal,
                        inc = Math.round((Math.random() - 0.5) * 10);

                    newVal = point.y + inc;
                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y - inc;
                    }

                    point.update(newVal);

                }, 3000);
            }
        }*/);
}