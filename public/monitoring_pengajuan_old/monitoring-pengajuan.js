var data_bulanan;
var data_tahunan;
$(function() {
    var filterDashboard = $('select[name=filter-dashboard]');
    var filterType = $('#filter-type');
    filterType.select2();
    var filterTypeKementrian = $('#filter-type-kementrian');
    filterTypeKementrian.select2();
    var filterHarian = $('#harian');
    var filterBulanan = $('#bulanan');
    var filterTahunan = $('#tahunan'); 
    var filterPeriodik = $('#periodik');
    var filterHarianKementrian = $('#harian_kementrian');
    var filterBulananKementrian = $('#bulanan_kementrian');
    var filterTahunanKementrian = $('#tahunan_kementrian');
    var filterPeriodikKementrian = $('#periodik_kementrian');

    $('#tahun_2').select2(); //tahn
    $('#tahun_2').on('change',function(){
        change_judul();
    });
    $('#tahun_kementrian_2').select2();
    $('#tahun_kementrian_2').on('change',function(){
        change_judul_km();
    });

     $('#dp1').datepicker({
        autoclose: true, 
        showButtonPanel: true, 
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp2').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp3').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });



    $('#dp_kemen_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    
    $('#dp_kemen_daily').on('change',function(){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_by_date_km();
            change_judul_km();
         };
    });

    $('#dp_kemen_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_kemen_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_kemen_periodik_to').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
        console.log(tanggal_sd);
         if (tanggal != "" && tanggal_sd != "") {
            change_by_periodik_km();
            change_judul_km();
         }; 
    });

    $('#dp_kemen_periodik_from').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
        console.log(tanggal_sd);
         if (tanggal != "" && tanggal_sd != "") {
            change_by_periodik_km();
            change_judul_km();
         }; 
    });

    $('#m_kementrian_bulanan').select2();
    $('#m_kementrian_bulanan').on('change',function(event){
        change_by_month_km();
    });
    $('#tahun_kementrian').select2();
    $('#tahun_kementrian').on('change',function(event){
        change_by_year_km();
        change_judul_km();
    });
    $('#bulan').select2();
    $('#tahun').select2();
    /*
        on change bidang 
    */

    init_data();


    var selectBidang = $('#Bidang');

    selectBidang.on('change', function(event) {
        event.preventDefault();
        change_judul();
        //bidang ketika ganti

    });

    var selectKementrian = $('#kementrian');
    selectKementrian.on('change',function(event){
        event.preventDefault();
        change_judul_km();
    });

    filterType.on('change', function(event){
        event.preventDefault();
        filterHarian.hide();
        filterBulanan.hide();
        filterTahunan.hide();
        filterPeriodik.hide();

        switch(filterType.val()){
            case '1' : filterHarian.show();break;
            case '2' : filterBulanan.show();default_month();break;
            case '3' : filterTahunan.show();break;
            case '4' : filterPeriodik.show();break;
        }
    });

    filterTypeKementrian.on('change', function(event){
        event.preventDefault();
        filterHarianKementrian.hide();
        filterBulananKementrian.hide();
        filterTahunanKementrian.hide();
        filterPeriodikKementrian.hide();

        switch(filterTypeKementrian.val()){
            case '1' : filterHarianKementrian.show();break;
            case '2' : filterBulananKementrian.show();break;
            case '3' : filterTahunanKementrian.show();break;
            case '4' : filterPeriodikKementrian.show();break;
        }

        change_judul_km();
    });

    filterDashboard.on('change', function(event) {
        event.preventDefault();
        $('.spin-bidang').fadeIn('fast');
        
    });

    var filterType = $('select[name=filter-dashboard-type]');
        filterType.on('change',function(event){
    });

    var bidangFilterBulan = $('#bulan');
    bidangFilterBulan.val(global_month);
    bidangFilterBulan.on('change',function(event){
         event.preventDefault();
         change_judul_km();
    });


    $('#dp3').on('change',function(){
        var tanggal = $('#dp3').val();
         if (tanggal != "") {
            change_by_date();
            change_judul_km();
         };
    });

    $('#dp1').on('change',function(){
        var tanggal = $('#dp1').val();
        var tanggal_sd = $('#dp2').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_by_periodic();
            change_judul_km();
         };
    });

    $('#dp2').on('change',function(){
        var tanggal = $('#dp1').val();
        var tanggal_sd = $('#dp2').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_by_periodic();
            change_judul_km();
         };
    });

   
/*
    $('#btn_kemen_harian').on('click',function(data){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_by_date_km();
            change_judul_km();
         };
    });
    $('#btn_kemen_periodik').on('click',function(data){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
        console.log(tanggal_sd);
         if (tanggal != "" && tanggal_sd != "") {
            change_by_periodik_km();
            change_judul_km();
         }; 
    });

    /*create_pies('grfk_done_undone',0);
    create_pie('grfk_ontime_delay',0);
    create_pie('grfk_done_undone_kementrian',0);
    create_pie('grfk_ontime_delay_kementrian',0);*/
    change_by_date();
    change_by_date_km();
    change_judul();
    change_judul_km();

});


function init_data(){
   /* var url = base_url + '/rest/data-pengajuan';
        //console.log(url);

        $.get(url, function(data) {
                var data_pengajuan = JSON.parse(data);
                var obj = data_pengajuan[0];
                $('#total_izin_masuk').text(obj.total_pengajuan);
                $('#total_izin_selesai').text(obj.total_selesai);
                $('#total_izin_masuk_kementrian').text(obj.total_pengajuan);
                $('#total_izin_selesai_kementrian').text(obj.total_selesai);

            });*/
}

function change_filter_type(type){
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp3').val();
    } else if (type == "2") {
        var bulan = $('#bulan').val();
    } else if (type == "3") {
        var tahun = $('#dp3').val();
    } else if (type == "4") {
        var bulan = $('#bulan').val();

    };
    
}

function change_judul(){
    var type = $('#filter-type').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp3').val();
        types = "Monitoring Pengajuan "+get_bidang()+" <br> Tanggal : "+hari;
        change_by_date();
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        change_by_month();
        types = "Monitoring Pengajuan "+get_bidang()+" <br> Bulan : "+$('#bulan option:selected').text()+" "+$('#tahun_2').val();
    } else if (type == "3") {
        var tahun = $('#tahun').val();
        types = "Monitoring Pengajuan "+get_bidang()+" <br> Tahun : "+tahun;
        change_by_year();
    } else if (type == "4") {
        var hari_1 = $('#dp1').val();
        var hari_2 = $('#dp2').val();
        types = "Monitoring Pengajuan "+get_bidang()+" <br> Periode : "+hari_1+" s/d "+hari_2;
        change_by_periodic();

    };

    $('#judul').hide('fast');
    $('#judul').html(types);
    $('#judul').show('slow');
    //var height = $().height;
}

function change_by_periodic(){
    var url = base_url + '/rest/monitoring-pengajuan-periodik?tanggal='+$('#dp1').val()+'&tanggal_sd='+$('#dp2').val();
    if ($('#bidang').val() != "-") {
        url = url+'&bidang='+$('#bidang').val();
    };
    $.get(url, function(data) {
        console.log(data);
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk').html(obj_data.total_pengajuan);
        $('#total_izin_selesai').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay',obj_data);
        create_pies('grfk_done_undone',obj_data);
    });
}

function change_by_date(){
    var url = base_url + '/rest/monitoring-pengajuan-harian?tanggal='+$('#dp3').val();
    if ($('#bidang').val() != "-") {
        url = url+'&bidang='+$('#bidang').val();
    };
    $.get(url, function(data) {
        console.log(data);
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk').html(obj_data.total_pengajuan);
        $('#total_izin_selesai').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay',obj_data);
        create_pies('grfk_done_undone',obj_data);
    });
}

function change_by_month(){
    var url = base_url + '/rest/monitoring-pengajuan-bulanan?bulan='+$('#bulan').val()+'&tahun='+$('#tahun_2').val();
    if ($('#bidang').val() != "-") {
        url = url+'&bidang='+$('#bidang').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk').html(obj_data.total_pengajuan);
        $('#total_izin_selesai').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay',obj_data);
        create_pies('grfk_done_undone',obj_data);
    });

}

function change_by_year(){
    var url = base_url + '/rest/monitoring-pengajuan-tahunan?tahun='+$('#tahun').val();
    if ($('#bidang').val() != "-") {
        url = url+'&bidang='+$('#bidang').val();
    };
    $.get(url, function(data) {
        data_tahunan = JSON.parse(data);
        var obj_data = data_tahunan[0];
        $('#total_izin_masuk').html(obj_data.total_pengajuan);
        $('#total_izin_selesai').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay',obj_data);
        create_pies('grfk_done_undone',obj_data);
    });

}

function get_bidang(){
    var bidang = " Seluruh Bidang ";
    
    var filterType = $('#filter-type').val();

    if ($('#bidang').val() != "-") {
        bidang = "Bidang "+$("#bidang option:selected").text();
    };
    return bidang;
}

function get_kemen(){
    var kementrian = " Seluruh Instansi ";
    
    var filterType = $('#filter-type').val();

    if ($('#kementrian').val() != "-") {
        kementrian = "Instansi "+$("#kementrian option:selected").text();
    };
    return kementrian;
}

function default_month(){
    if ($('#bulan').val() == "-") {
        $('#bulan').val(global_month);

    };
}


function change_by_periodik_km(){
    var url = base_url + '/rest/monitoring-pengajuan-periodik-km?tanggal='+$('#dp_kemen_periodik_from').val()+'&tanggal_sd='+$('#dp_kemen_periodik_to').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    console.log(url);
    $.get(url, function(data) {
        console.log(data);
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk_kementrian').html(obj_data.total_pengajuan);
        $('#total_izin_selesai_kementrian').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay_kementrian',obj_data);
        create_pies('grfk_done_undone_kementrian',obj_data);
    });
}

function change_by_date_km(){
    var url = base_url + '/rest/monitoring-pengajuan-harian-km?tanggal='+$('#dp_kemen_daily').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    console.log(url);
    $.get(url, function(data) {
        console.log(data);
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk_kementrian').html(obj_data.total_pengajuan);
        $('#total_izin_selesai_kementrian').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay_kementrian',obj_data);
        create_pies('grfk_done_undone_kementrian',obj_data);
    });
}

function change_by_month_km(){
    var url = base_url + '/rest/monitoring-pengajuan-bulanan-km?bulan='+$('#m_kementrian_bulanan').val()+'&tahun='+$('#tahun_kementrian_2').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_izin_masuk_kementrian').html(obj_data.total_pengajuan);
        $('#total_izin_selesai_kementrian').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay_kementrian',obj_data);
        create_pies('grfk_done_undone_kementrian',obj_data);
    });


}

function change_by_year_km(){
    var url = base_url + '/rest/monitoring-pengajuan-tahunan-km?tahun='+$('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        data_tahunan = JSON.parse(data);
        var obj_data = data_tahunan[0];
        console.log(data);
        $('#total_izin_masuk_kementrian').html(obj_data.total_pengajuan);
        $('#total_izin_selesai_kementrian').html(obj_data.total_selesai);
        create_pie('grfk_ontime_delay_kementrian',obj_data);
        create_pies('grfk_done_undone_kementrian',obj_data);
    });

}

function change_judul_km(){
    var type = $('#filter-type-kementrian').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_kemen_daily').val();
        types = "Monitoring Pengajuan "+get_kemen()+" <br> Tanggal : "+hari;
        change_by_date_km();
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        change_by_month_km();
        types = "Monitoring Pengajuan "+get_kemen()+" <br> Bulan : "+$('#bulan option:selected').text()+" "+$('#tahun_kementrian_2').val();
    } else if (type == "3") {
        var tahun = $('#tahun_kementrian').val();
        types = "Monitoring Pengajuan "+get_kemen()+" <br> Tahun : "+tahun;
        change_by_year_km();
    } else if (type == "4") {
        var hari_1 = $('#dp_kemen_periodik_from').val();
        var hari_2 = $('#dp_kemen_periodik_to').val();
        types = "Monitoring Pengajuan "+get_kemen()+" <br> Periode : "+hari_1+" s/d "+hari_2;
        change_by_periodik_km();
 
    };


    $('#judul_kementrian').hide('fast');
    $('#judul_kementrian').html(types);
    $('#judul_kementrian').show('slow');

    }

function create_pie(id_div,data){

    if (data.total_pengajuan == "0") {
        $('#'+id_div).html("<div align='center'>belum ada data pengajuan</div>");
        return;
    };
    $('#'+id_div).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: false,
            title: {
                text: 'Komparasi'
            },
            tooltip: {
                pointFormat: '{series.name}:{point.y} <b>({point.percentage:.1f}%)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}) %',
                    },
                    showInLegend: true,
                    depth: 35
                }
            },
            series: [{
                name: "Nilai",
                colorByPoint: true,
                dataLabels: {
                            color:'black',
                        },
                data: [{
                    name: "On-time",
                    y: parseInt(data.total_on_time)
                }, {
                    name: "Delay",
                    y: parseInt(data.total_terlambat),    
                    sliced: true,
                    selected: true
                }, ]
            }]
        });

}

function create_pies(id_div,data){
    if (data.total_pengajuan == "0") {
        $('#'+id_div).html("<div align='center'>belum ada data pengajuan</div>");
        return;
    };

    $('#'+id_div).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: false,
            title: {
                text: 'Grafik selesai'
            },
            tooltip: {
                pointFormat: '{series.name}:{point.y} <b>({point.percentage:.1f}%)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}) %',
                    },
                    showInLegend: true,
                    depth: 35
                }
            },
            series: [{
                name: "Nilai",
                colorByPoint: true,
                dataLabels: {
                            color:'black',
                        },
                data: [{
                    name: "Selesai",
                    y: parseInt(data.total_selesai)
                }, {
                    name: "Tidak Selesai",
                    y: parseInt(data.total_pengajuan)-parseInt(data.total_selesai),
                    sliced: true,
                    selected: true
                }, ]
            }]
        });

}