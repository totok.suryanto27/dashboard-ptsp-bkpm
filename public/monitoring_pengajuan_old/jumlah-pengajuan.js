var bulan =['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
//Jika terjadi error 301 moved permanently tambahkan/hapus / pada akhir url
$(function(){
	 $.get( base_url +  '/rest/tabel-komparasi-tahunan', function(data) {
                grafik('grafik_tahunan',data,'Jumlah Pengajuan Izin Perinstansi<br> Tahun '+global_year);
               // chartYearsly(data_tahunan);
    }); 
	 $.get( base_url +  '/rest/tabel-komparasi-bulanan', function(data) {
                //var data_bulanan = JSON.parse(data); 
                grafik('grafik_bulanan',data,'Jumlah Pengajuan Izin Perinstansi<br> Bulan '+bulan[global_month-1]+' Tahun '+global_year);
               // chartYearsly(data_tahunan);
    }); 
	
});

function grafik(id,data,title){
	var nama_instansi = [];
	var on_time = [];
	var delay = [];
	for (var i = data.length - 1; i >= 0; i--) {
		var obj = data[i];
		nama_instansi.push(obj.nama_instansi);
		on_time.push(parseInt(obj.total_on_time));
		delay.push(parseInt(obj.total_selesai));
	};

	  $('#'+id).highcharts({
        chart: {
            type: 'column',
            height:600,
        },
        credits: {
            enabled: false
        },
        title: {
            text: title,
        },
        xAxis: {
            categories: nama_instansi
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Dokumen'
            }
        },

        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40
            }
        },

        series: [{
            name: 'On Time',
            data: on_time,
        }, {
            name: 'Delay',
            data: delay,
        }]
    });

// console.log("Tes");
}