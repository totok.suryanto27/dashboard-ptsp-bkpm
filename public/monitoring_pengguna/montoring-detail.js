$(function(){
	$('#tahun').on('change',function(){
		get_data_tahunan();
        get_data_bulanan();
	});

	$('#bulan').on('change',function(){
		//var key = $('#bulan').val()+'-b';
        //user

		get_data_bulanan();
	});

    get_data_tahunan();
    get_data_bulanan();
});

function get_data_tahunan(){
    var url = base_url + '/rest/durasi-pengguna-tahunan?user='+user+'&tahun='+$('#tahun').val();
    console.log(url);
    $.get( url, function(data) {
       var data_bulanan = JSON.parse(data);
       var values = [];
       for (var i = 0; i <data_bulanan.length; i++) {
           var temp = data_bulanan[i];
           values.push(parseInt(temp['total_menit']));
       };

       buat_bar_char(values);
    });
}


function get_data_bulanan(){
    var url = base_url + '/rest/durasi-pengguna-bulanan?user='+user+'&tahun='+$('#tahun').val()+'&bulan='+$('#bulan').val();
    //console.log(url);
    $.get( url, function(data) {
       // console.log(data);
       var data_harian = JSON.parse(data);
       var values = [];
       var tanggal = [];
       for (var i = 0; i <data_harian.length; i++) {
           var temp = data_harian[i];
           values.push(parseInt(temp['total_menit']));
           tanggal.push(parseInt(temp['hari']));
       };
      buat_chart_bulan(tanggal,values);
    });
}


function buat_chart_bulan(tanggal,value){

    $('#grf-aktivitas-bulan').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Keaktifan Pengguna per Bulan '
        },
        subtitle: {
            text: 'Tahun '+$('#tahun').val()
        },
        xAxis: {
            categories: tanggal,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Menit',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Menit'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Waktu Aktif',
            data: value
        }]
    });
}

function buat_bar_char(data){

	$('#grf-aktivitas-tahun').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Keaktifan Pengguna per Bulan'
        },
        subtitle: {
            text: 'Tahun '+$('#tahun').val()
        },
        xAxis: {
            categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Menit'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                '<td style="padding:0"><b>{point.y:.1f} Menit</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Menit',
            data: data

        }]
    });  	
};