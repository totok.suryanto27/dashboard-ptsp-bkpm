$('#tabel-pengguna').DataTable();
$(function() {
    $.get( base_url + '/rest/durasi-pengguna', function(data) {
        generaldata = data.data;
        var nama = [];
        var value = [];
        for (var i = generaldata.length - 1; i >= 0; i--) {
            var obj = generaldata[i];
            nama.push(obj.nama);
            value.push(parseInt(obj.total_menit));
        };

        var high = parseInt(data.total)*100;

        if (parseInt(data.total) > 10) {
            var high = parseInt(data.total)*50;            
        } else if (parseInt(data.total) > 20) {
            var high = parseInt(data.total)*25;            
        };;
       $('#grf-aktivitas-pengguna').highcharts({
            chart: {
                type: 'bar',
                height: high,
                // options3d: {
                //     enabled: true,
                //     alpha: 10,
                //     beta: 10,
                //     depth: 50,
                //     viewDistance: 25
                // }
            },
            credits: false,
            title: {
                text: 'Aktivitas Pengguna'
            },
            xAxis: {
                categories: nama
            },
            yAxis: {
                title: {
                    text: 'Jumlah Menit'
                }
            },
            series: [{
                name: 'Durasi aktivitas (dalam satuan menit)',
                data: value
            }]
        })
    });
});