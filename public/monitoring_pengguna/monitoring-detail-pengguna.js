   $(function() {
       function getBulan(id) {
           var bulan = '';
           switch (parseInt(id)) {
               case 1:
                   bulan = 'Januari';
                   break;
               case 2:
                   bulan = 'Februari';
                   break;
               case 3:
                   bulan = 'Maret';
                   break;
               case 4:
                   bulan = 'April';
                   break;
               case 5:
                   bulan = 'Mei';
                   break;
               case 6:
                   bulan = 'Juni';
                   break;
               case 7:
                   bulan = 'Juli';
                   break;
               case 8:
                   bulan = 'Agustus';
                   break;
               case 9:
                   bulan = 'September';
                   break;
               case 10:
                   bulan = 'Oktober';
                   break;
               case 11:
                   bulan = 'November';
                   break;
               case 12:
                   bulan = 'Desember';
                   break;
           }
           return bulan;
       }
       //select tahun
       $('select[name=tahun]').on('change', function(event) {
           var _val = $(this).val();
           $.get(base_url + '/monitoring-pengguna/user/hits2' + window.location.search + '&tahun=' + _val, function(data) {
               console.log(data);
               var _bulan = [];
               var _hits = [];
               _hits[0] = 0,
               _hits[1] = 0,
               _hits[2] = 0,
               _hits[3] = 0,
               _hits[4] = 0,
               _hits[5] = 0,
               _hits[6] = 0,
               _hits[7] = 0,
               _hits[8] = 0,
               _hits[9] = 0,
               _hits[10] = 0,
               _hits[11] = 0;
               if (data.hits.length > 0) {
                   // _hits[parseInt(data.hits[0].bulan) - 1] = parseInt(data.hits[data.hits.length - 1].total_durasi);
                   data.hits.forEach(function(e, i) {
                       _hits[parseInt(e.bulan) - 1] = parseInt(e.total_durasi);
                   });
                   $('.progress').hide();
                   $('#grf-aktivitas-tahun').highcharts({
                       credits: false,
                       chart: {
                           type: 'column'
                       },
                       title: {
                           text: 'Rata - rata aktivitas ' + data.hits[0].id_m_user + ' Per Tahun'
                       },
                       subtitle: {
                           // text: 'Source: WorldClimate.com'
                       },
                       xAxis: {
                           categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                           crosshair: true
                       },
                       yAxis: {
                           min: 0,
                           title: {
                               text: 'Durasi (menit)'
                           }
                       },
                       tooltip: {
                           headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                           pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} menit</b></td></tr>',
                           footerFormat: '</table>',
                           shared: true,
                           useHTML: true
                       },
                       plotOptions: {
                           column: {
                               pointPadding: 0.2,
                               borderWidth: 0
                           }
                       },
                       series: [{
                           name: data.hits[0].id_m_user,
                           data: _hits
                       }]
                   });
               } else {
                   alert('belum ada aktivitas pada tahun tersebut');
               }
           });
       });
       console.log("PATH TAHUN : " + base_url + '/monitoring-pengguna/user/hits2' + window.location.search + '&tahun=' + year);
       $.get(base_url + '/monitoring-pengguna/user/hits2' + window.location.search + '&tahun=' + year, function(data) {
           // console.log(data);
           var _bulan = [];
           var _hits = [];
           _hits[0] = 0,
           _hits[1] = 0,
           _hits[2] = 0,
           _hits[3] = 0,
           _hits[4] = 0,
           _hits[5] = 0,
           _hits[6] = 0,
           _hits[7] = 0,
           _hits[8] = 0,
           _hits[9] = 0,
           _hits[10] = 0,
           _hits[11] = 0;
           $('.progress').hide();
           data.hits.forEach(function(e, i) {
                console.log(e);
               _hits[parseInt(e.bulan) - 1] = parseInt(e.total_durasi);
           });
           console.log("GENERATE CHART TAHUN USER ACT");
           console.log(_hits);
           console.log(data);
           $('#grf-aktivitas-tahun').highcharts({
               credits: false,
               chart: {
                   type: 'column'
               },
               title: {
                   text: 'Rata - rata aktivitas ' + data.hits[0].id_m_user + ' Per Tahun'
               },
               subtitle: {
                   // text: 'Source: WorldClimate.com'
               },
               xAxis: {
                   categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                   crosshair: true
               },
               yAxis: {
                   min: 0,
                   title: {
                       text: 'Durasi (menit)'
                   }
               },
               tooltip: {
                   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                   pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} menit</b></td></tr>',
                   footerFormat: '</table>',
                   shared: true,
                   useHTML: true
               },
               plotOptions: {
                   column: {
                       pointPadding: 0.2,
                       borderWidth: 0
                   }
               },
               series: [{
                   name: data.hits[0].id_m_user,
                   data: _hits
               }]
           });
       });
       $('select[name=bulan]').on('change', function(event) {
           var _val = $(this).val();
           $.get(base_url + '/monitoring-pengguna/user/hits2' + window.location.search + "&bulan=" + _val, function(data) {
               console.log(data);
               var _jumlahHari = 30;
               var _hari = [];
               var _hariInit = [];
               for (var i = 0; i < 30; i++) {
                   _hari[i] = 0;
                   _hariInit[i] = i + 1;
               }
               if (data.hits.length > 0) {
                   if (data.hits[0].jumlahHari > _jumlahHari) {
                       _hari[30] = 0;
                       _hariInit[30] = 31;
                   }
                   if (data.hits[0].jumlahHari == 28) {
                       delete _hari[29];
                       delete _hari[28];
                       delete _hariInit[29];
                       delete _hariInit[28];
                   }
                   if (data.hits[0].jumlahHari == 29) {
                       delete _hari[29];
                       delete _hariInit[29];
                   }
                   data.hits.forEach(function(e, i) {
                       _hari[parseInt(e.tanggal) - 1] = e.total_durasi;
                   });
                   console.log(_hari);
                   $('#grf-aktivitas-bulan').highcharts({
                       chart: {
                           type: 'column',
                           // options3d: {
                           //     enabled: true,
                           //     alpha: 15,
                           //     beta: 15,
                           //     depth: 50,
                           //     viewDistance: 25
                           // }
                       },
                       title: {
                           text: 'Rata - Rata Waktu Aktif ' + data.hits[0].id_m_user + '  per Bulan'
                       },
                       credits: false,
                       subtitle: {},
                       xAxis: {
                           categories: _hariInit,
                           crosshair: true
                       },
                       yAxis: {
                           min: 0,
                           title: {
                               text: 'Waktu (menit)'
                           }
                       },
                       xAxis: {
                           title: {
                               text: 'Tanggal'
                           }
                       },
                       tooltip: {
                           headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                           pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} menit</b></td></tr>',
                           footerFormat: '</table>',
                           shared: true,
                           useHTML: true
                       },
                       plotOptions: {
                           column: {
                               pointPadding: 0.2,
                               borderWidth: 0
                           }
                       },
                       series: [{
                           name: data.hits[0].id_m_user,
                           data: _hari
                       }, ]
                   });
               } else {
                   alert('belum ada aktivitas pada belum tersebut')
               }
               $('.progress').hide();
           });
       });
       $.get(base_url + '/monitoring-pengguna/user/hits2' + window.location.search + "&bulan=" + month, function(data) {
           console.log(data);
           var _jumlahHari = 30;
           var _hari = [];
           var _hariInit = [];
           for (var i = 0; i < 30; i++) {
               _hari[i] = 0;
               _hariInit[i] = i + 1;
               _hari[i].toString();
               _hariInit[i].toString();
           }
           
           if (data.hits.length > 0) {
               if (data.hits[0].jumlahHari > _jumlahHari) {
                   _hari[30] = 0;
                   _hariInit[30] = 31;
                   _hari[30].toString();
                   _hari[30].toString();
               }
               if (data.hits[0].jumlahHari == 28) {
                   delete _hari[29];
                   delete _hari[28];
                   delete _hariInit[29];
                   delete _hariInit[28];
               }
               if (data.hits[0].jumlahHari == 29) {
                   delete _hari[29];
                   delete _hariInit[29];
               }
               data.hits.forEach(function(e, i) {
                   _hari[parseInt(e.tanggal) - 1] = e.total_durasi;
               });
               console.log("hari");
               console.log(_hari);
               console.log("hari init");
           console.log(_hariInit);
               $('#grf-aktivitas-bulan').highcharts({
                   chart: {
                       type: 'column',
                       // options3d: {
                       //     enabled: true,
                       //     alpha: 15,
                       //     beta: 15,
                       //     depth: 50,
                       //     viewDistance: 25
                       // }
                   },
                   title: {
                       text: 'Rata - Rata Waktu Aktif ' + data.hits[0].id_m_user + '  per Bulan'
                   },
                   credits: false,
                   subtitle: {},
                   xAxis: {
                       
                       categories: ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU'],
                       min: 0.5,
                       max: _hariInit.length - 1.5,
                       startOnTick: false,
                       endOnTick: false,
                       crosshair: true,
                       labels: {
                           step: 0
                       }
                   },
                   yAxis: {
                       min: 1,
                       title: {
                           text: 'Waktu (menit)'
                       }
                   },
                   xAxis: {
                       title: {
                           text: 'Tanggal'
                       }
                   },
                   tooltip: {
                       headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                       pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} menit</b></td></tr>',
                       footerFormat: '</table>',
                       shared: true,
                       useHTML: true
                   },
                   plotOptions: {
                       column: {
                           pointPadding: 0.2,
                           borderWidth: 0
                       }
                   },
                   series: [{
                       name: data.hits[0].id_m_user,
                       data: _hari
                   }, ]
               });
           } else {
               alert('belum ada aktivitas pada belum tersebut')
           }
           $('.progress').hide();
       });
   });