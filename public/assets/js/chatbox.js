function toggleChatBox() {
	if ($('.dockmodal').hasClass("minimized")) {
		$('#chatbox').dockmodal('restore');
	} else {
		$('#chatbox').dockmodal('minimize');
	}
}

$("#chatbox").dockmodal({
	minimizedWidth: 270,
	height: 400,
	title: "Hubungi Admin",
	gutter: 10,
	showClose: false,
	initialState: "minimized",
	restore: function(dialogue) {
		restoring = true;
		dialogue = dialogue.find('.dialogs');
		dialogue.slimScroll({ scrollTo : dialogue.prop('scrollHeight') });
		restoring = false;
		if (!is_chatbox_opened) {
			getMessage(null,msg_time_max,false,false,true);
			is_chatbox_opened = true;
			setInterval(function() {
				getMessage(msg_time_max,now(),false,false,false);
			},5000);
		}
	}
});

$('.dialogs').slimScroll({
	height: '320px'
}).bind('slimscroll', function(e, pos){
	console.log(pos);
	if (pos == "top" && !first && !restoring) {
		getMessage(null,msg_time_min,true,false,false);
	} else {
		
	}
});
var is_chatbox_opened = false;
var msg_time_min = now();
var msg_time_max = now();
var getting = false;
var first = false;
var restoring = false;

function now() {
	return (new Date()).getTime();
}
function eptotime(ep) {
	var d = new Date(parseInt(ep));
	return d.toDateString()+" "+d.toTimeString().substr(0,5);
}
function getMessage(time_min,time_max,top,selfsubmit,init) {
	if (getting) return;
	getting = true;
	data = {
		time_min: time_min,
		time_max: time_max,
	};
	var dialogue = $("#chatbox .dialogs");
	$.get(chat_url,data,function(res) {
		getting = false;
		if (res.length == 0 && top) first = true;
		if (res.length == 0) return;
		if (init) {
			msg_time_min = parseInt(res[res.length-1]['time']);
		}
		if (!top) {
			msg_time_max = parseInt(res[0]['time']);
			res.reverse();
		} else {
			msg_time_min = parseInt(res[res.length-1]['time']);
		}
		if (dialogue.prop('scrollHeight') == dialogue.scrollTop() + 320)
			bottom = true;
		else
			bottom = false;
		for(var i = 0; i < res.length; i++) {
			var el = 
'  <div class="itemdiv dialogdiv">\
    <div class="user">\
      <img src="'+asset_url+'assets/img/'+(res[i]['type']=='broadcast'?'megaphone':(res[i]['receiver_id']!="-1"?'user-blue':'user-green'))+'.png">\
    </div>\
    <div class="body">\
      <div class="time">\
        <i class="fa fa-time"></i>\
        <span class="green chat_time">'+eptotime(res[i]['time'])+'</span>\
      </div>\
\
      <div class="name chat_name">\
        '+res[i]['sender_name']+'\
      </div>\
      <div class="text chat_content">'+res[i]['content']+'</div>\
    </div>\
  </div>\
';
			if (top) {
				dialogue.prepend(el);
			} else {
				dialogue.append(el);
			}
		}
		if (selfsubmit || bottom) {
			dialogue.slimScroll({ scrollTo : dialogue.prop('scrollHeight') });
		} 
		if (top) {
			dialogue.slimScroll({ scrollTo : 10 });
		}
	});
}
sendMessage = function() {
	data = {
		content: $("#input_chat_msg").val()
	};
	if ($("#input_chat_msg").val() != "") {
		$("#input_chat_msg").val("");
		$.post(chat_url,data,function(res) {
			getMessage(msg_time_max,now(),false,true,false);
		});
	}
};
$("#submit_chat_msg").click(sendMessage);
$('#input_chat_msg').keydown(function (event) {
    var keypressed = event.keyCode || event.which;
    if (keypressed == 13) {
        sendMessage();
    }
});

