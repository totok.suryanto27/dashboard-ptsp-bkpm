$(function(){
	var filterTypeIzin = $('#filter-type-izin');
	var filterHarianIzin = $('#harian_izin');
    var filterBulananIzin = $('#bulanan_izin');
    var filterTahunanIzin = $('#tahunan_izin');
    var filterPeriodikIzin = $('#periodik_izin');

	$('#instansi').on('change',function(){
		var instansi = $('#instansi').val();
		 if (instansi != "") {
			change_judul_iz();
		 };
	});
	
    filterTypeIzin.select2();
    var selectIzin = $('#izin');
        $('#dp_izi_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
    
    $('#dp_izi_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_izi_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_izi_daily').on('change',function(){
        var tanggal = $('#dp_izi_daily').val();
         if (tanggal != "") {
            change_judul_iz();
         };
    });


    $('#dp_izi_periodik_to').on('change',function(){
        var tanggal = $('#dp_izi_periodik_from').val();
        var tanggal_sd = $('#dp_izi_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_iz();
         }; 
    });

    $('#dp_izi_periodik_from').on('change',function(){
        var tanggal = $('#dp_izi_periodik_from').val();
        var tanggal_sd = $('#dp_izi_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_iz();
         }; 
    });


    selectIzin.on('change',function(event){
        event.preventDefault();
        change_judul_iz();
    });


    filterTypeIzin.on('change', function(event){
        event.preventDefault();
        filterHarianIzin.hide();
        filterBulananIzin.hide();
        filterTahunanIzin.hide();
        filterPeriodikIzin.hide();

        switch(filterTypeIzin.val()){
            case '1' : filterHarianIzin.show();break;
            case '2' : filterBulananIzin.show();break;
            case '3' : filterTahunanIzin.show();break;
            case '4' : filterPeriodikIzin.show();break;
        }

        change_judul_iz();
    });

    $('#m_izin_bulanan').select2();
    $('#m_izin_bulanan').on('change',function(event){
        change_judul_iz();

    });

    $('#tahun_izin_bulan').select2();
	$('#tahun_izin_bulan').on('change',function(event){
        change_judul_iz();

    });

    $('#tahun_izin').select2();
    $('#tahun_izin').on('change',function(event){
        change_judul_iz();
    });
    

    change_judul_iz();

           // create_chart();
    

});

function get_izi(){
    var izin = " Seluruh Izin ";
    
    var filterType = $('#filter-type').val();

    if ($('#izin').val() != "-") {
        izin = "Izin "+$("#izin option:selected").text();
    };
    return izin;
}
 
function change_by_month_iz(){
    var url = base_url + '/rest/tabel-komparasi-izin-bulanan-ptsp?bulan='+$('#m_izin_bulanan').val()+'&tahun='+ $('#tahun_izin').val();
    if ($('#instansi').val() != "-") {
        url = url+'&id_instansi='+$('#instansi').val();
    };
    
    $.get(url, function(data) {
        populate_table_izin(data);
       
    });


}

function change_by_year_iz(){
    var url = base_url + '/rest/tabel-komparasi-izin-tahunan-ptsp?tahun='+$('#tahun_izin').val();
    if ($('#instansi').val() != "-") {
        url = url+'&id_instansi='+$('#instansi').val();
    };
    $.get(url, function(data) {
        populate_table_izin(data);
       
    });

}

function change_by_periodik_iz(){
    var url = base_url + '/rest/tabel-komparasi-grup-izin-periodik-ptsp?from_date='+$('#dp_izi_periodik_from').val()+'&to_date='+$('#dp_izi_periodik_to').val();
    
    if ($('#instansi').val() != "-") {
        url = url+'&id_instansi='+$('#instansi').val();
    };
    $.get(url, function(data) {
        populate_table_izin(data);
       
    });
}

function populate_table_izin(data){

    $('#tabel-izi').empty();
    var instansi = [];
    var value = [];
    var objeks = [];
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var ot = parseInt(obj.ontime);
		var dl = parseInt(obj.delay); 
        var persen = ot+dl;
        var performansi = 0;
        var performansi2 = 0;
        
        if (persen > 0 ) {
            performansi =  (ot/persen)*100;
            performansi = performansi.toFixed(0);
        };
         
        if (persen > 0 ) {
            performansi2 =  (dl/persen)*100;
            performansi2 = performansi2.toFixed(0);
        };
        var isi = "<tr><td>"+obj.nama_instansi+"</td><td>"+dl+"</td><td>"+ot+"</td><td>"+performansi2+" % </td><td>"+performansi+" % </td></tr>";
        $('#tabel-izi').append(isi);  
        
        var newObj = 
        {nama_instansi:obj.nama_instansi,performansis:parseInt(performansi),performansis2:parseInt(performansi2)};
        objeks.push(newObj);
         
    };
    buat_klasemen_izin(objeks);
    var x = $('#tabel_izi').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });

}

function change_judul_iz(){

    var type = $('#filter-type-izin').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_izi_daily').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_izi()+" <br> Tanggal : "+hari;
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        var instansi = $('#instansi').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_izi()+"<br> Bulan : "+$('#m_izin_bulanan option:selected').text()+" "+$('#tahun_izin_bulan').val()+ " <br> Instansi :  "+$('#instansi option:selected').text();
        change_by_month_iz();
    } else if (type == "3") {
        var tahun = $('#tahun_izin').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_izi()+" <br> Tahun : "+tahun+ " <br> Instansi :  "+$('#instansi option:selected').text();
        change_by_year_iz();
    } else if (type == "4") {
        var hari_1 = $('#dp_izi_periodik_from').val();
        var hari_2 = $('#dp_izi_periodik_to').val(); 
        types = "Komparasi Persentase Ketepatan Waktu "+get_izi()+" <br> Periode : "+hari_1+" s/d "+hari_2+ " <br> Instansi :  "+$('#instansi option:selected').text();
		change_by_periodik_iz();	
    };


    $('#judul_izin').hide('fast');
    $('#judul_izin').html(types);
    $('#judul_izin').show('slow');
}

function buat_klasemen_izin(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = []; 
    var value2 = [];
    var nama_instansi = []; 
  
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis);
        value2.push(o.performansis2);
        nama_instansi.push(o.nama_instansi);
    };

     
    $('#container-speed-izin').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Komparasi Persentase Ketepatan Waktu Layanan Perizinan'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Persen'
        },
        plotOptions: {
			series: {
                cursor: 'pointer',
                point: { 
                }
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: { 
            align: 'left',
			verticalAlign: 'top',
			y: 20,
			floating: true,
			borderWidth: 0
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'On Time',
            data: value
        },{
            name: 'Delay',
            data: value2
        }
        ]
    });
}
function create_chart(){
    
     /*$('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Komparasi Performansi',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}'+' %'+'</span><br/>' +
                       '<span style="font-size:12px;color:silver">Performa</span></div>'
            },
            
        }]

    }));*/
}
