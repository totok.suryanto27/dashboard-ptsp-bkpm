$(function(){
	var filterTypebidang = $('#filter-type-bidang');
	var filterHarianbidang = $('#harian_bidang');
    var filterBulananbidang = $('#bulanan_bidang');
    var filterTahunanbidang = $('#tahunan_bidang');
    var filterPeriodikbidang = $('#periodik_bidang');

    filterTypebidang.select2();
    var selectbidang = $('#bidang');
        $('#dp_kemen_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
    
    $('#dp_kemen_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false, 
        format: "dd-mm-yyyy"
    });
    $('#dp_kemen_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_kemen_daily').on('change',function(){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_judul_bdg();
         };
    });


    $('#dp_kemen_periodik_to').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_bdg();
         }; 
    });

    $('#dp_kemen_periodik_from').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_bdg();
         }; 
    });


  	$('#tbl_bln').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });


    filterTypebidang.on('change', function(event){
        event.preventDefault();
        filterHarianbidang.hide();
        filterBulananbidang.hide();
        filterTahunanbidang.hide();
        filterPeriodikbidang.hide();

        switch(filterTypebidang.val()){
            case '1' : filterHarianbidang.show();break;
            case '2' : filterBulananbidang.show();break;
            case '3' : filterTahunanbidang.show();break;
            case '4' : filterPeriodikbidang.show();break;
        }

        change_judul_bdg();
    });

    $('#m_bidang_bulanan').select2();
    $('#m_bidang_bulanan').on('change',function(event){
        change_judul_bdg();

    });

    $('#tahun_bidang_bulan').select2();
	$('#tahun_bidang_bulan').on('change',function(event){
        change_judul_bdg();

    });

    $('#tahun_bidang').select2();
    $('#tahun_bidang').on('change',function(event){
        change_judul_bdg();
    });


    change_judul_bdg();

});

function get_kemen(){
    var bidang = " Seluruh bidang ";
    return bidang;
}

function populate_table_b(data){
	$('#tabel-bidang').empty();
	var flow = [];
	var value_data = [];
	for (var i = 0; i < data.length; i++) {
		var obj = data[i];
		var ot = parseInt(obj.total_on_time);
		var dl = parseInt(obj.total_terlambat);
		var persen = ot+dl;
		var performansi = 0;
		if (persen > 0 ) {
			performansi = parseFloat((ot/persen)*100);
            //performansi = performansi.toFixed(0);
		};
		var isi = "<tr><td>"+obj.nama_flow+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td><td>"+performansi.toFixed(2)+" % </td></tr>";
		$('#tabel-bidang').append(isi);
		flow.push(obj.nama_flow);
		value_data.push(performansi);
	};

	buat_bar_char('grafik_bidang',flow,value_data);

}
/*function change_by_periodik_bdg(){
    var url = base_url + '/rest/dashboard-data-periodik?tanggal='+$('#dp_kemen_periodik_from').val()+'&tanggal_sd='+$('#dp_kemen_periodik_to').val();
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
        $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);
    });
}


 
function change_by_date_bdg(){
    var url = base_url + '/rest/dashboard-data-harian?tanggal='+$('#dp_kemen_daily').val();
    $.get(url, function(data) {
        data_bulanan = JSON.parse(data);
        var obj_data = data_bulanan[0];
         $('#total_on_time_bidang').html(obj_data.total_on_time);
        $('#total_terlambat_bidang').html(obj_data.total_terlambat);
        $('#total_selesai_bidang').html(obj_data.total_selesai);
        $('#total_pengajuan').html(obj_data.total_pengajuan);

    });
}*/

function change_by_month_bdg(){
    var url = base_url + '/rest/rekap-pengajuan-bidang-v2?bulan='+$('#m_bidang_bulanan').val()+'&tahun='+ $('#tahun_bidang_bulan').val();
    console.log(url);
    $.get(url, function(data) {
        populate_table_b(data);
    });
    

}

function change_by_year_bdg(){
    var url = base_url + '/rest/rekap-pengajuan-bidang-v1?tahun='+$('#tahun_bidang').val();
    console.log(url);

    $.get(url, function(data) {
        populate_table_b(data);
    });
    
}

function change_judul_bdg(){
    var type = $('#filter-type-bidang').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_kemen_daily').val();
        types = "Komparasi Layanan "+get_kemen()+" <br> Tanggal : "+hari;
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        types = "Komparasi Layanan "+get_kemen()+" <br> Bulan : "+$('#m_bidang_bulanan option:selected').text()+" "+$('#tahun_bidang_bulan').val();
        change_by_month_bdg();
        console.log("tes");
    } else if (type == "3") {
        var tahun = $('#tahun_bidang').val();
        types = "Komparasi Layanan "+get_kemen()+" <br> Tahun : "+tahun;
        change_by_year_bdg();
        console.log("tes 3");

    } else if (type == "4") {
        var hari_1 = $('#dp_kemen_periodik_from').val();
        var hari_2 = $('#dp_kemen_periodik_to').val();
        types = "Komparasi Layanan "+get_kemen()+" <br> Periode : "+hari_1+" s/d "+hari_2;
    };


    $('#judul_bidang').hide('fast');
    $('#judul_bidang').html(types);
    $('#judul_bidang').show('slow');

    }
  function buat_bar_char(id_bidang,flow,value_data){
  		 $('#'+id_bidang).highcharts({
        chart: {
            type: 'column'
        },
        credits:false,
        title: {
            text: 'Grafik Seluruh Bidang'
        },
        xAxis: {
            categories: flow,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Presentase On Time (%)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>', 
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Nilai Performansi',
            data: value_data

        }]
    });
  }