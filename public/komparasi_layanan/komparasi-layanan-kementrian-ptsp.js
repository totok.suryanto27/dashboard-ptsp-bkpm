var gaugeOptions = {

        chart: {
            type: 'bar'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false 
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 10,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
                    },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 10,
                    borderWidth: 3,
                    useHTML: true
                }
            }
        }
    };

$(function(){
	var filterTypeKementrian = $('#filter-type-kementrian');
	var filterHarianKementrian = $('#harian_kementrian');
    var filterBulananKementrian = $('#bulanan_kementrian');
    var filterTahunanKementrian = $('#tahunan_kementrian');
    var filterPeriodikKementrian = $('#periodik_kementrian');

    filterTypeKementrian.select2();
    var selectKementrian = $('#kementrian');
        $('#dp_kemen_daily').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    }); 
    
    $('#dp_kemen_periodik_from').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });
    $('#dp_kemen_periodik_to').datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy"
    });

    $('#dp_kemen_daily').on('change',function(){
        var tanggal = $('#dp_kemen_daily').val();
         if (tanggal != "") {
            change_judul_km();
         };
    });


    $('#dp_kemen_periodik_to').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
        
    });

    $('#dp_kemen_periodik_from').on('change',function(){
        var tanggal = $('#dp_kemen_periodik_from').val();
        var tanggal_sd = $('#dp_kemen_periodik_to').val();
         if (tanggal != "" && tanggal_sd != "") {
            change_judul_km();
         }; 
    });


    selectKementrian.on('change',function(event){
        event.preventDefault();
        change_judul_km();
    });


    filterTypeKementrian.on('change', function(event){
        event.preventDefault();
        filterHarianKementrian.hide();
        filterBulananKementrian.hide();
        filterTahunanKementrian.hide();
        filterPeriodikKementrian.hide();

        switch(filterTypeKementrian.val()){
            case '1' : filterHarianKementrian.show();break;
            case '2' : filterBulananKementrian.show();break;
            case '3' : filterTahunanKementrian.show();break;
            case '4' : filterPeriodikKementrian.show();break;
        }

        change_judul_km();
       
    });

    $('#m_kementrian_bulanan').select2();
    $('#m_kementrian_bulanan').on('change',function(event){
        change_judul_km();

    });

    $('#tahun_kementrian_bulan').select2();
	$('#tahun_kementrian_bulan').on('change',function(event){
        change_judul_km();

    });

    $('#tahun_kementrian').select2();
    $('#tahun_kementrian').on('change',function(event){
        change_judul_km();
    });
    

    change_judul_km();

           // create_chart();
    

});

function get_kemen(){
    var kementrian = " Seluruh PTSP ";
    
    var filterType = $('#filter-type').val();

    if ($('#kementrian').val() != "-") {
        kementrian = "Kementrian "+$("#kementrian option:selected").text();
    };
    return kementrian;
}

function change_by_month_km(){
    var url = base_url + '/rest/tabel-komparasi-bulanan-ptsp?bulan='+$('#m_kementrian_bulanan').val()+'&tahun='+ $('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    
    $.get(url, function(data) {
        populate_table_x2(data,$('#m_kementrian_bulanan').val(),$('#tahun_kementrian').val(),'1');
       
    });

	//e.preventDefault();
    //e.stopPropagation(); 
}

function change_by_year_km(){
    var url = base_url + '/rest/tabel-komparasi-tahunan-ptsp?tahun='+$('#tahun_kementrian').val();
    if ($('#kementrian').val() != "-") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        populate_table_x2(data,$('#tahun_kementrian').val(),'Year','2');
       
    }); 
	//e.preventDefault();
    //e.stopPropagation();
}

function change_by_periodik_km(){
    var url = base_url + '/rest/tabel-komparasi-periodik-ptsp?from_date='+$('#dp_kemen_periodik_from').val()+'&to_date='+ $('#dp_kemen_periodik_to').val();
    
	//$('#tabel-kemen').empty();
    if ($('#kementrian').val() != "") {
        url = url+'&kementrian='+$('#kementrian').val();
    };
    $.get(url, function(data) {
        populate_table_x2(data,$('#dp_kemen_periodik_from').val(),$('#dp_kemen_periodik_to').val()); 
    });  
}

function change_by_month_iz2(tgl_awal,tgl_akhir,instansi,status){
    var url = base_url + '/rest/tabel-komparasi-izin-bulanan-ptsp?bulan='+tgl_awal+'&tahun='+tgl_akhir+'&instansi='+instansi+'&status='+status
     
    $.get(url, function(data) {
        populate_table_izin2(data,status);
       
    });


}

function change_by_year_iz2(tgl_awal,instansi,status){
    var url = base_url + '/rest/tabel-komparasi-izin-tahunan-ptsp?tahun='+tgl_awal+'&instansi='+instansi+'&status='+status;
     
    $.get(url, function(data) {
        populate_table_izin2(data,status);
       
    });

}

function change_by_periodik_iz2(tgl_awal,tgl_akhir,instansi,status){
    var url = base_url + '/rest/tabel-komparasi-izin-periodik-ptsp?from_date='+tgl_awal+'&to_date='+tgl_akhir+'&instansi='+instansi+'&status='+status;
	  
    $.get(url, function(data) {
        populate_table_izin2(data,status);
       
    });
}

function populate_table_izin2(data,status){ 
    $('#tabel-izi').empty();
    var instansi = [];
    var value = [];
    var objeks = []; 
    var status = status;
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        
        var jml = parseInt(obj.jml);
		var	performansi = jml.toFixed(0); 
		
        //if(status=="Delay"){ 
			//var ot = parseInt(obj.jml);
			//var	performansi = ot.toFixed(0); 
		//}else{
			//var dl = parseInt(obj.jml);
			//var	performansi = dl.toFixed(0); 
		//}
        
        var isi = "<tr><td>"+obj.nama_jenis_perizinan+"</td><td>"+jml+"</td><td>"+performansi+" % </td></tr>";
        $('#tabel-izi2').append(isi);
        
        
        var newObj = {nama_jenis_perizinan:obj.nama_jenis_perizinan,performansis:parseInt(performansi)};
        objeks.push(newObj);
       
        
    };
    buat_klasemen_izin2(objeks);
    var x = $('#tabel_izi2').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
    
}

function buat_klasemen_izin2(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];
    var nama_jenis_perizinan = [];
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis);
        nama_jenis_perizinan.push(o.nama_jenis_perizinan);
    };
 
    $('#container-speed').highcharts({
        chart: {
            type: 'bar' 
        },
        title: {
            text: 'Jumlah Perizinan per PTSP Daerah'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_jenis_perizinan,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Izin'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Jumlah Izin',
            data: value
        }]
    });
}
 
function populate_table_x(data){ 
    
    var instansi = [];
    var value = [];
    var objeks = [];
	$('#tabel-kemen').empty();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var ot = parseInt(obj.total_on_time);
        var dl = parseInt(obj.total_terlambat);
        var persen = ot+dl;
        var performansi = 0;
        if (persen > 0 ) {
            performansi =  (ot/persen)*100;
            performansi = performansi.toFixed(0);
        };
         
        var isi = "<tr><td>"+obj.nama_instansi+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td><td>"+performansi+" % </td></tr>";
        $('#tabel-kemen').append(isi);
      
        var newObj = {nama_instansi:obj.nama_instansi,performansis:parseInt(performansi)};
        objeks.push(newObj);
        
    };
    buat_klasemen(objeks);
    var x = $('#tbl_kemen').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });

    
}

function populate_table_x2(data,tgl_from,tgl_to){
	
    //location.reload();
    var instansi = [];
    var value = [];
    var objeks = [];
	$('#tabel-kemen').empty();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        var ot = parseInt(obj.ontime);
		var dl = parseInt(obj.delay); 
        var persen = ot+dl;
        var performansi = 0;
        var performansi2 = 0;
        
        if (persen > 0 ) {
            performansi =  (ot/persen)*100;
            performansi = performansi.toFixed(0);
        };
         
        if (persen > 0 ) {
            performansi2 =  (dl/persen)*100;
            performansi2 = performansi2.toFixed(0);
        };
        var isi = "<tr><td>"+obj.nama_instansi+"</td><td>"+dl+"</td><td>"+ot+"</td><td>"+performansi2+" % </td><td>"+performansi+" % </td></tr>";
        $('#tabel-kemen').append(isi);  
        
        var newObj = 
        {nama_instansi:obj.nama_instansi,performansis:parseInt(performansi),performansis2:parseInt(performansi2)};
        objeks.push(newObj);
        
    };
    buat_klasemen2(objeks,tgl_from,tgl_to);
    var x = $('#tbl_kemen').DataTable({
        processing: true,
        destroy: true,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
	//location.reload();
}

function change_judul_km(){

    var type = $('#filter-type-kementrian').val();
    var types = "";
    var bidang = $("#bidang").val();
    if (type == "1") {
        var hari = $('#dp_kemen_daily').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_kemen()+" <br> Tanggal : "+hari;
    } else if (type == "2") {
        var bulan = $('#bulan').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_kemen()+" <br> Bulan : "+$('#m_kementrian_bulanan option:selected').text()+" "+$('#tahun_kementrian_bulan').val();
        change_by_month_km();
	 
    } else if (type == "3") {
        var tahun = $('#tahun_kementrian').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_kemen()+" <br> Tahun : "+tahun;
        change_by_year_km();
	 
    } else if (type == "4") {
        var hari_1 = $('#dp_kemen_periodik_from').val();
        var hari_2 = $('#dp_kemen_periodik_to').val();
        types = "Komparasi Persentase Ketepatan Waktu "+get_kemen()+" <br> Periode : "+hari_1+" s/d "+hari_2;
		change_by_periodik_km();
	 
    };


    $('#judul_kementrian').hide('fast');
    $('#judul_kementrian').html(types);
    $('#judul_kementrian').show('slow');

}

function buat_klasemen2(objeks,tgl_from,tgl_to){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = []; 
    var value2 = [];
    var nama_instansi = [];
    var tgl_awal=tgl_from;
    var tgl_akhir=tgl_to; 
  
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis);
        value2.push(o.performansis2);
        nama_instansi.push(o.nama_instansi);
    };

     
    $('#container-speed').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Komparasi Persentase Ketepatan Waktu Layanan Instansi'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Persen'
        },
        plotOptions: {
			series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () { 
							
                            $("#container-speed").html("");
                            
                            types = "Komparasi Persentase Ketepatan Waktu <br> Periode : "+tgl_awal+" s/d "+tgl_akhir+ " <br> Instansi :  "+this.category;  
							change_by_periodik_iz2(tgl_awal,tgl_akhir,this.category,this.series.name);
							
							//if (v_kondisi == "1") { //by Bulan
								//types = "Komparasi Persentase Ketepatan Waktu <br> Bulan  : "+tgl_awal+" - "+tgl_akhir+" <br> Instansi :  "+this.category;
								//change_by_month_iz2(tgl_awal,tgl_akhir,this.category,this.series.name);
							//} else if (v_kondisi == "2") { //by Tahun
								//types = "Komparasi Persentase Ketepatan Waktu <br> Tahun : "+tgl_awal+ " <br> Instansi :  "+this.category;
								//change_by_year_iz2(tgl_awal,this.category,this.series.name);
							//} else if (v_kondisi == "3") { //by Periodik
								//types = "Komparasi Persentase Ketepatan Waktu <br> Periode : "+tgl_awal+" s/d "+tgl_akhir+ " <br> Instansi :  "+this.category;  
								//change_by_periodik_iz2(tgl_awal,tgl_akhir,this.category,this.series.name);
							//} 
							
                            $('#judul_kementrian').hide('fast');
							$('#judul_kementrian').html(types);
							$('#judul_kementrian').show('slow');
								
                        }
                    }
                }
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: { 
            align: 'left',
			verticalAlign: 'top',
			y: 20,
			floating: true,
			borderWidth: 0
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'On Time',
            data: value
        },{
            name: 'Delay',
            data: value2
        }
        ]
    });
}

function buat_klasemen(objeks){
    objeks.sort(function(a, b){
     return b.performansis-a.performansis;
    })
    var value = [];  
    var nama_instansi = [];
    for (var i = 0; i < objeks.length; i++) {
        var o = objeks[i];
        value.push(o.performansis); 
        nama_instansi.push(o.nama_instansi);
    };
 
    $('#container-speed').highcharts({
        chart: {
            type: 'bar',
            height: 800
        },
        title: {
            text: 'Komparasi Persentase Ketepatan Waktu Layanan PTSP'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: nama_instansi,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performansi',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Persen'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
                 series: {
                stacking: 'normal'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Persentase',
            data: value}]
    });
}
function create_chart(){
    
     /*$('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Komparasi Performansi',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}'+' %'+'</span><br/>' +
                       '<span style="font-size:12px;color:silver">Performa</span></div>'
            },
            
        }]

    }));*/
}
