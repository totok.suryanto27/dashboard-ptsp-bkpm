$(function() {
    var tabelKementrianKomparasi = $('#tabel-kementrian-komparasi'),
        tabelBidangKomparasi = $('#tabel-bidang-komparasi');
    var dtTabelKementrianKomparasi = tabelKementrianKomparasi.DataTable(),
        dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable();
    var grfKomparasiKementrian = $('#grf-komparasi-kementrian');
    var filterKementrian = $('#filter-kementrian'),
        filterHarianKementrian = $('#filter-harian-kementrian'),
        filterBulanKementrian = $('#filter-bulan-kementrian'),
        filterTahunKementrian = $('#filter-tahun-kementrian'),
        filterT1Kementrian = $('#filter-t1-kementrian'),
        filterT2Kementrian = $('#filter-t2-kementrian'),
        filterHarianBidang = $('#filter-harian-bidang'),
        filterBulanBidang = $('#filter-bulan-bidang'),
        filterTahunBidang = $('#filter-tahun-bidang'),
        filterT1Bidang = $('#filter-t1-bidang'),
        filterT2Bidang = $('#filter-t2-bidang'),
        filterPeriode = $('.periode'),
        filterWaktuKementrian = $('#filter-waktu-kementrian'),
        filterWaktuBidang = $('#filter-waktu-bidang');
    var tanggalHead = $('#tanggal-head');
    var groupKementrianHarian = $('#group-kementrian-harian'),
        groupKementrianBulanan = $('#group-kementrian-bulanan'),
        groupKementrianTahunan = $('#group-kementrian-tahunan'),
        groupKementrianPeriodik = $('.group-kementrian-periodik');

    var groupBidangHarian = $('#group-bidang-harian'),
        groupBidangBulanan = $('#group-bidang-bulanan'),
        groupBidangTahunan = $('#group-bidang-tahunan'),
        groupBidangPeriodik = $('.group-bidang-periodik');
    //default filter kementrian
    groupKementrianHarian.fadeIn('fast');
    groupKementrianBulanan.fadeOut('fast');
    groupKementrianTahunan.fadeOut('fast');
    groupKementrianPeriodik.fadeOut('fast');

    groupBidangHarian.fadeIn('fast');
    groupBidangBulanan.fadeOut('fast');
    groupBidangTahunan.fadeOut('fast');
    groupBidangPeriodik.fadeOut('fast');

    filterWaktuBidang.on('change', function(event) {
        switch (parseInt(filterWaktuBidang.val())) {
            case 1:
                groupBidangHarian.fadeIn('fast');
                groupBidangBulanan.fadeOut('fast');
                groupBidangTahunan.fadeOut('fast');
                groupBidangPeriodik.fadeOut('fast');
                break;
            case 2:
                groupBidangHarian.fadeOut('fast');
                groupBidangBulanan.fadeIn('fast');
                groupBidangTahunan.fadeIn('fast');
                groupBidangPeriodik.fadeOut('fast');
                break;
            case 3:
                groupBidangHarian.fadeOut('fast');
                groupBidangBulanan.fadeOut('fast');
                groupBidangTahunan.fadeIn('fast');
                groupBidangPeriodik.fadeOut('fast');
                break;
            case 4:
                groupBidangHarian.fadeOut('fast');
                groupBidangBulanan.fadeOut('fast');
                groupBidangTahunan.fadeOut('fast');
                groupBidangPeriodik.fadeIn('fast');
                break;
        }
    });

    filterWaktuKementrian.on('change', function(event) {
        switch (parseInt(filterWaktuKementrian.val())) {
            case 1:
                groupKementrianHarian.fadeIn('fast');
                groupKementrianBulanan.fadeOut('fast');
                groupKementrianTahunan.fadeOut('fast');
                groupKementrianPeriodik.fadeOut('fast');
                break;
            case 2:
                groupKementrianHarian.fadeOut('fast');
                groupKementrianBulanan.fadeIn('fast');
                groupKementrianTahunan.fadeIn('fast');
                groupKementrianPeriodik.fadeOut('fast');
                break;
            case 3:
                groupKementrianHarian.fadeOut('fast');
                groupKementrianBulanan.fadeOut('fast');
                groupKementrianTahunan.fadeIn('fast');
                groupKementrianPeriodik.fadeOut('fast');
                break;
            case 4:
                groupKementrianHarian.fadeOut('fast');
                groupKementrianBulanan.fadeOut('fast');
                groupKementrianTahunan.fadeOut('fast');
                groupKementrianPeriodik.fadeIn('fast');
                break;
        }
    });
    filterKementrian.select2();
    filterBulanKementrian.select2();
    filterTahunKementrian.select2();
    filterBulanBidang.select2();
    filterTahunBidang.select2();
    filterHarianBidang.datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy",
        // startView: 'decade',
        // minViewMode: "year",
        todayHighlight: true,
        todayBtn: "linked"
    });
    filterHarianKementrian.datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy",
        // startView: 'decade',
        // minViewMode: "year",
        todayHighlight: true,
        todayBtn: "linked"
    });
    filterPeriode.datepicker({
        autoclose: true,
        showButtonPanel: true,
        changeMonth: true,
        changeYear: false,
        format: "dd-mm-yyyy",
        // startView: 'decade',
        // minViewMode: "year",
        todayHighlight: true,
        todayBtn: "linked"
    });
    /*
        BIDANG
    */
    //filter harian bidang
    filterHarianBidang.on('change', function(event) {
        $.get(base_url + '/rest/komparasi-layanan-bidang?tanggal=' + filterHarianBidang.val(), function(data) {
            generateBarChart('#grf-komparasi-bidang', {
                categories: (function() {
                    var _temp = [];
                    data.forEach(function(e, i) {
                        _temp.push(e.nama_flow)
                    })
                    return _temp;
                })(),
                seriesData: (function() {
                    var _temp = [];
                    data.forEach(function(e, i) {
                        console.log(e);
                        _temp.push(parseInt(e.performansi_));
                    })
                    return _temp;
                })(),
            }, {
                title: 'Semua Bidang',
                subTitle: filterHarianBidang.val()
            });
            dtTabelBidangKomparasi.destroy();
            dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                "aaData": data,
                "aoColumns": [{
                    "mDataProp": "no"
                }, {
                    "mDataProp": "nama_flow"
                }, {
                    "mDataProp": "total_pengajuan"
                }, {
                    "mDataProp": "total_selesai"
                }, {
                    "mDataProp": "performansi"
                }]
            });
        });
    });
    filterBulanBidang.on('change', function(event) {
        filterHarianBidang.val('');
        if (filterTahunBidang.val() == "") {
            $.get(base_url + '/rest/komparasi-layanan-bidang?bulan=' + filterBulanBidang.val(), function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Bulan ' + filterBulanBidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        } else {
            $.get(base_url + '/rest/komparasi-layanan-bidang?bulan=' + filterBulanBidang.val() + "&tahun=" + filterTahunBidang.val(), function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Bulan ' + filterBulanBidang.val() + ' Tahun ' + filterTahunBidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        }
    })
    filterTahunBidang.on('change', function(event) {
        filterHarianBidang.val('');
        if (filterBulanBidang.val() == "") {
            $.get(base_url + '/rest/komparasi-layanan-bidang?tahun=' + filterBulanBidang.val() + "&filter=2", function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Tahun ' + filterTahunBidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        } else {
            $.get(base_url + '/rest/komparasi-layanan-bidang?bulan=' + filterBulanBidang.val() + "&tahun=" + filterTahunBidang.val(), function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Bulan ' + filterBulanBidang.val() + ' Tahun ' + filterTahunBidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        }
    })
    filterT1Bidang.on('change', function(event) {
        filterBulanBidang.val('');
        filterTahunBidang.val('');
        filterHarianBidang.val('');
        if (filterT2Bidang.val() != "") {
            $.get(base_url + '/rest/komparasi-layanan-bidang?t1=' + filterT1Bidang.val() + '&t2=' + filterT2Bidang.val(), function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Dari tanggal ' + filterT1Bidang.val() + ' sampai ' + filterT2Bidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        }
    })
    filterT2Bidang.on('change', function(event) {
        filterBulanBidang.val('');
        filterTahunBidang.val('');
        filterHarianBidang.val('');
        if (filterT1Bidang.val() != "") {
            $.get(base_url + '/rest/komparasi-layanan-bidang?t1=' + filterT1Bidang.val() + '&t2=' + filterT2Bidang.val(), function(data) {
                generateBarChart('#grf-komparasi-bidang', {
                    categories: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            _temp.push(e.nama_flow)
                        })
                        return _temp;
                    })(),
                    seriesData: (function() {
                        var _temp = [];
                        data.forEach(function(e, i) {
                            console.log(e);
                            _temp.push(parseInt(e.performansi_));
                        })
                        return _temp;
                    })(),
                }, {
                    title: 'Semua Bidang',
                    subTitle: 'Dari tanggal ' + filterT1Bidang.val() + ' sampai ' + filterT2Bidang.val(),
                });
                dtTabelBidangKomparasi.destroy();
                dtTabelBidangKomparasi = tabelBidangKomparasi.DataTable({
                    "aaData": data,
                    "aoColumns": [{
                        "mDataProp": "no"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total_pengajuan"
                    }, {
                        "mDataProp": "total_selesai"
                    }, {
                        "mDataProp": "performansi"
                    }]
                });
            });
        } else {
            alert('pilih tanggal awal dahulu');
        }
    })
    $.get(base_url + '/rest/komparasi-layanan-bidang', function(data) {
        generateBarChart('#grf-komparasi-bidang', {
            categories: (function() {
                var _temp = [];
                data.forEach(function(e, i) {
                    _temp.push(e.nama_flow)
                })
                return _temp;
            })(),
            seriesData: (function() {
                var _temp = [];
                data.forEach(function(e, i) {
                    console.log(e);
                    _temp.push(parseInt(e.performansi_));
                })
                return _temp;
            })(),
        }, {
            title: 'Semua Bidang',
            subTitle: year
        });
    });

    function generateBarChart(element, data, opt) {
        console.log("data");
        console.log(data.seriesData);
        $(element).highcharts({
            credits: false,
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Komparasi Layanan Semua Bidang'
            },
            subtitle: {
                text: opt.subTitle
            },
            xAxis: {
                min: 0,
                categories: data.categories,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' persen'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Komparasi',
                data: data.seriesData
            }]
        });
    }
    //filter by kementrian
    filterKementrian.on('change', function(event) {
        filterHarianKementrian.val('');
        filterBulanKementrian.val('');
        filterT1Kementrian.val('');
        filterT2Kementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
        } else {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + $(this).val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                tanggalHead.text("Kementerian : " + data[data.length - 1].nama_instansi);
                generateHalfGaugeChart('#grf-komparasi-kementrian', {
                    seriesName: data[data.length - 1].nama_instansi,
                    seriesData: data[data.length - 1].persen_on_time
                }, {
                    text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                })
            });
        }
    });
    filterHarianKementrian.on('change', function(event) {
        filterBulanKementrian.val('');
        filterTahunKementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
            filterHarianKementrian.val('');
        } else {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&hari=" + filterHarianKementrian.val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                tanggalHead.text("Tanggal : " + data[data.length - 1].hari);
                if (data.length <= 0) {
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        }
    });
    filterBulanKementrian.on('change', function(event) {
        filterHarianKementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
            filterBulanKementrian.val('');
            filterTahunKementrian.val('');
        } else
        if (filterTahunKementrian.val() == "") {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&bulan=" + filterBulanKementrian.val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                if (data.length <= 0) {
                    // tanggalHead.text("Kementerian");
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    tanggalHead.text("Bulan : " + data[data.length - 1].bulan);
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        } else {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&bulan=" + filterBulanKementrian.val() + "&tahun=" + filterTahunKementrian.val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                if (data.length <= 0) {
                    tanggalHead.text("");
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    tanggalHead.text("Bulan : " + data[data.length - 1].bulan);
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        }
    })
    filterTahunKementrian.on('change', function(event) {
        filterHarianKementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
            filterBulanKementrian.val('');
            filterTahunKementrian.val('');
        }
        if (filterBulanKementrian.val() == "") {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&tahun=" + filterTahunKementrian.val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                if (data.length <= 0) {
                    tanggalHead.text("");
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    tanggalHead.text("Tahun : " + data[data.length - 1].tahun);
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        } else {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&bulan=" + filterBulanKementrian.val() + "&tahun=" + filterTahunKementrian.val(), function(data) {
                /*optional stuff to do after success */
                console.log(data);
                if (data.length <= 0) {
                    tanggalHead.text("");
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    tanggalHead.text("Tahun : " + data[data.length - 1].tahun);
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        }
    })
    filterT2Kementrian.on('change', function(event) {
        var t2 = filterT2Kementrian.val();
        var t1 = filterT1Kementrian.val();
        filterBulanKementrian.val('');
        filterTahunKementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
        }
        if (t1 == "") {
            alert('pilih tanggal awal dahulu')
        } else {
            $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&t1=" + t1 + "&t2=" + t2, function(data) {
                /*optional stuff to do after success */
                console.log(data);
                tanggalHead.text("Dari tanggal : " + t1 + " hingga " + t2);
                if (data.length <= 0) {
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: filterKementrian.val(),
                        seriesData: 0
                    }, {
                        text: 'Komparasi Layanan ' + filterKementrian.val()
                    })
                } else {
                    generateHalfGaugeChart('#grf-komparasi-kementrian', {
                        seriesName: data[data.length - 1].nama_instansi,
                        seriesData: data[data.length - 1].persen_on_time
                    }, {
                        text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                    })
                }
            });
        }
    })
    filterT1Kementrian.on('change', function(event) {
        var t2 = filterT2Kementrian.val();
        var t1 = filterT1Kementrian.val();
        filterBulanKementrian.val('');
        filterTahunKementrian.val('');
        if (filterKementrian.val() == "") {
            alert('pilih kementrian dahulu');
        }
        $.get(base_url + '/rest/komparasi-layanan-kementrian?kementrian=' + filterKementrian.val() + "&t1=" + t1 + "&t2=" + t2, function(data) {
            /*optional stuff to do after success */
            console.log(data);
            tanggalHead.text("Dari tanggal : " + t1 + " hingga " + t2);
            if (data.length <= 0) {
                generateHalfGaugeChart('#grf-komparasi-kementrian', {
                    seriesName: filterKementrian.val(),
                    seriesData: 0
                }, {
                    text: 'Komparasi Layanan ' + filterKementrian.val()
                })
            } else {
                generateHalfGaugeChart('#grf-komparasi-kementrian', {
                    seriesName: data[data.length - 1].nama_instansi,
                    seriesData: data[data.length - 1].persen_on_time
                }, {
                    text: 'Komparasi Layanan ' + data[data.length - 1].nama_instansi
                })
            }
        });
    });
    // filterTahunKementrian.on('change', function(event) {
    //     if (filterKementrian.val() == "") {
    //         alert('pilih kementrian dahulu')
    //     }
    // })
    //filter by hari
    //filter by bulan
    //filter by tahun
    //filter by periode
    //komparasi kementrian
    $.get(base_url + '/rest/komparasi-layanan-kementrian', function(data) {
        /*optional stuff to do after success */
        console.log("LAST");
        console.log(data[0]);
        tanggalHead.text("Kementrian : " + data[0].nama_instansi);
        generateHalfGaugeChart('#grf-komparasi-kementrian', {
            seriesName: data[0].nama_instansi,
            seriesData: data[0].persen_on_time
        }, {
            text: 'Komparasi Layanan ' + data[0].nama_instansi
        })
        // dtTabelKementrianKomparasi.destroy();
        // dtTabelKementrianKomparasi = tabelKementrianKomparasi.DataTable({
        //     "aaData": data,
        //     "aoColumns": [{
        //         "mDataProp": "no"
        //     }, {
        //         "mDataProp": "nama_instansi"
        //     }, {
        //         "mDataProp": "hitung_pengajuan"
        //     }, {
        //         "mDataProp": "hitung_on_time"
        //     }, {
        //         "mDataProp": "persen"
        //     }]
        // });
    });

    function generateHalfGaugeChart(element, data, opt) {
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#DF5353'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#55BF3B'] // red 
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };
        // The speed gauge
        $(element).highcharts(Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: opt.text
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: data.seriesName,
                data: [parseInt(data.seriesData)],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' + ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' + '<span style="font-size:12px;color:silver">% (persen)</span></div>'
                },
                tooltip: {
                    valueSuffix: ' % (persen)'
                }
            }]
        }));
    }
});