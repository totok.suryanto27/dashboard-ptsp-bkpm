$(function(){
	$('#tahunan_bidang').on('change',function(){
			change_judul();
	});
	change_judul();
});

function change_judul(){
	var dropdownKementrianThn = $('#tahunan_bidang');
	var textTitle = "Rekap Pengajuan Seluruh Bidang Tahun "+dropdownKementrianThn.val();
	var url = base_url+"/rest/rekap-pengajuan-bidang-v1?tahun="+dropdownKementrianThn.val();
	$.get(url, function(data) {
		console.log(data);
		populateTableBidang(data);
	});
	console.log(url);
}

function populateTableBidang(data){
	$('#tabel-bdg').empty();
	var total_delay = 0;
	var total_on_times = 0;
	for (var i = 0; i < data.length; i++) {
		var obj = data[i];
		var total = "<tr><td>"+obj.nama_flow+"</td><td>"+obj.total_pengajuan+"</td><td>"+obj.total_selesai+"</td><td>"+obj.total_terlambat+"</td><td>"+obj.total_on_time+"</td></tr>";
		$('#tabel-bdg').append(total);
		total_delay = total_delay + parseInt(obj.total_terlambat);
		total_on_times = total_on_times + parseInt(obj.total_on_time);
	};

	var seratus_persent = total_delay+total_on_times;
	if (seratus_persent != 0) {
		var presentase_delay = (total_delay/seratus_persent)*100;
		var presentase_ontime = (total_on_times/seratus_persent)*100;
		create_pie_bidang('bidang_pie_charts',presentase_ontime,presentase_delay,$('#tahunan_bidang').val());
	}else{

	};
}

function create_pie_bidang(id_div,total_on_time,total_terlambat,tahuns){

    $('#'+id_div).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: false,
            title: {
                text: 'Komparasi Semua Bidang Tahun '+tahuns
            },
            tooltip: {
                pointFormat: '{series.name}:{point.y} <b>({point.percentage:.1f}%)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}) %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true,
                    depth: 35
                }
            },
            series: [{
                name: "Nilai",
                colorByPoint: true,
                dataLabels: {
                            color:'white',
                            distance: -50,
                        },
                data: [{
                    name: "On-time",
                    y: parseInt(total_on_time)
                }, {
                    name: "Delay",
                    y: parseInt(total_terlambat),    
                    sliced: true,
                    selected: true
                }, ]
            }]
        });

}


