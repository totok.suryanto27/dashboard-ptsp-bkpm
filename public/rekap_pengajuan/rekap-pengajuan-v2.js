$(function(){
	var dropdownKementrian = $('#kementrian_bln');
	var dropdownKementrianThn = $('#tahunan_bln');
	var dropdownBidangThn = $('#tahunan_bidang');
	dropdownKementrian.select2();
	dropdownKementrianThn.select2();
	dropdownBidangThn.select2();
	dropdownKementrian.on('change',function(){
		judulKementrian();
	});
	dropdownKementrianThn.on('change',function(){
		judulKementrian();
	});
	dropdownBidangThn.on('change',function(){
		judulKementrian();
	});

	judulKementrian();


}); 


function judulKementrian(){
	var dropdownKementrian = $('#kementrian_bln');
	var dropdownKementrianThn = $('#tahunan_bln');
	var textTitle = "Rekap Pengajuan Seluruh kementrian Tahun "+dropdownKementrianThn.val();
	var url = base_url+"/rest/rekap-pengajuan-kementrian-v1?tahun="+dropdownKementrianThn.val();
	if (dropdownKementrian.val() != "-") {
		textTitle = "Rekap Pengajuan "+$('#kementrian_bln option:selected').text()+" Tahun "+dropdownKementrianThn.val();
		url = url+'&id_instansi='+dropdownKementrian.val();
	};

	console.log(url);

	 $.get(url, function(data) {
	 	var persen_on_time = 0;
	 	var persen_delay = 0;
        $('#tabel-bulanan').empty();
	 	populateTable(data);
          for (var i = data.length - 1; i >= 0; i--) {
          	var obj = data[i];
          	persen_on_time = persen_on_time +parseInt(obj.on_time);
          	persen_delay = persen_delay+parseInt(obj.terlambat);
          };

          var seratus_persen = persen_on_time + persen_delay;
          if (seratus_persen == 0) {
          	//
          	$('#pie_charts').html("<div align='center'>belum ada data pengajuan</div>");
          }else{
          	var presentase_on_time = (persen_on_time/seratus_persen)*100;
          	var presentase_on_delay = (persen_delay/seratus_persen)*100;
          	create_pie('pie_charts',presentase_on_time,presentase_on_delay)
          };
          	bar_chart(textTitle,data);


    });

}

function populateTable(data){
	$('#tabel-bulanan').empty();
	var ttl_pengajuan = 0;
	var ttl_selesai = 0;
	var ttl_terlambat = 0;
	var ttl_on_time = 0;
	for (var i = 0; i < data.length; i++) {
		var obj = data[i];
		ttl_pengajuan = ttl_pengajuan+parseInt(obj.total);
		ttl_selesai = ttl_selesai+parseInt(obj.selesai);
		ttl_terlambat = ttl_terlambat+parseInt(obj.terlambat);
		ttl_on_time = ttl_on_time+parseInt(obj.on_time);
		var total = "<tr><td>"+obj.nama_bulan+"</td><td>"+obj.total+"</td><td>"+obj.selesai+"</td><td>"+obj.terlambat+"</td><td>"+obj.on_time+"</td></tr>";
		$('#tabel-bulanan').append(total);
	};
	var total_s = "<tr style='font-weight:bolder;'><td>Total</td><td>"+ttl_pengajuan+"</td><td>"+ttl_selesai+"</td><td>"+ttl_terlambat+"</td><td>"+ttl_on_time+"</td></tr>";
	$('#tabel-bulanan').append(total_s);
    console.log("ping");
     $('#tbl_bln').DataTable({
        processing: true,
        destroy: true,
        "bSort" : false,
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
}

function judulBidang(){
	var dropdownBidangThn = $('#tahunan_bidang');
	var textTitle = "Rekap Pengajuan Seluruh Bidang Tahun "+dropdownKementrianThn.val();
	console.log(textTitle);

}

function table_kementrian(){

}

function table_bidang(){
	
}

function bar_chart(judul,data){
	$('#bar_charts').highcharts({
        credits: false,
        chart: {
            type: 'bar'
        },
        title: {
            text: judul
        },
        xAxis: {
            categories: ['januari', 'feburari', 'maret', 'april', 'mei','juni', 'juli', 'agustus', 'september', 'oktober','november', 'desember'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Pengajuan',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' total pengajuan'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Pengajuan',
            data: [
            	parseInt(data[0].total),
            	parseInt(data[1].total),
            	parseInt(data[2].total),
            	parseInt(data[3].total),
            	parseInt(data[4].total),
            	parseInt(data[5].total),
            	parseInt(data[6].total),
            	parseInt(data[7].total),
            	parseInt(data[8].total),
            	parseInt(data[9].total),
            	parseInt(data[10].total),
            	parseInt(data[11].total),
            ]
        }]
    });
}

function create_pie(id_div,total_on_time,total_terlambat){

    $('#'+id_div).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: false,
            title: {
                text: 'Komparasi'
            },
            tooltip: {
                pointFormat: '{series.name}:{point.y} <b>({point.percentage:.1f}%)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}) %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true,
                    depth: 35
                }
            },
            series: [{
                name: "Nilai",
                colorByPoint: true,
                dataLabels: {
                            color:'white',
                            distance: -50,
                        },
                data: [{
                    name: "On-time",
                    y: parseInt(total_on_time)
                }, {
                    name: "Delay",
                    y: parseInt(total_terlambat),    
                    sliced: true,
                    selected: true
                }, ]
            }]
        });

}
