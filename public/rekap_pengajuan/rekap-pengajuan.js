$(function() {
    var filterTahunKementrian = $('#filter-tahun-kementrian');
    var filterTahunBidang = $('#filter-tahun-bidang');
    var tabelKementrian = $('#table-kementrian'),
        tabelBidang = $('#tabel-bidang');
    var progress = $('.progress');
    var grfRekapPengajuanKementrian = $('#grf-rekap-pengajuan-kementrian'),
        grfKomparasiKementrian = $('#komparasi-kementrian'),
        grfRekapPengajuanBidang = $('#grf-rekap-pengajuan'),
        grfKomparasiBidang = $('#komparasi');
    var filterKementrian = $('#filter-kementrian');
    var dtTabelKementrian = tabelKementrian.DataTable({
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
    var dtTabelBidang = tabelBidang.DataTable({
        aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
    });
    filterTahunKementrian.select2();
    filterKementrian.select2();
    filterTahunBidang.select2();
    //init semua
    /*
        BIDANG
    */
    $.get(base_url + '/rest/rekap-pengajuan-bidang', function(data) {
        
        var _bulan = (function() {
            var _temp = [];
            data.rekap.forEach(function(e, i) {
                _temp.push(e.nama_flow);
            })
            return _temp;
        })();
        var _isi = (function() {
            var _temp = [];
            for (var i = 0; i < 12; i++) {
                _temp[i] = 0;
            }
            var _o = [];
            data.rekap.forEach(function(e, i) {
                // _temp.push(parseInt(e.total))
                
                
                
                
                // _temp[parseInt(e.bulan) - 1] = parseInt(1000);
                var _x = {
                    name: e.nama_flow,
                    data: (function() {
                        var x = [];
                        for (var i = 0; i < 12; i++) {
                            x[i] = 0;
                        }
                        x[parseInt(e.bulan) - 1] = parseInt(e.total);
                        return x;
                    })()
                }
                
                _o.push(_x);
            });
            return _o;
        })();
        
        
        
        generateBarChart('#grf-rekap-pengajuan', {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            series: _isi
        }, {
            title: 'Semua Bidang',
            tahun: data.rekap[data.rekap.length - 1].tahun,
            bulan: data.rekap[data.rekap.length - 1].nama_bulan,
        });
        
        generatePieChart('#komparasi', data.komparasi, {
            title: 'Semua Bidang',
            tahun: data.rekap[data.rekap.length - 1].tahun,
            bulan: data.rekap[data.rekap.length - 1].nama_bulan,
        }); 
        dtTabelBidang.destroy();
        dtTabelBidang = tabelBidang.DataTable({
            aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
            "aaData": data.rekap,
            "aoColumns": [{
                "mDataProp": "tahun"
            }, {
                "mDataProp": "nama_flow"
            }, {
                "mDataProp": "total"
            }, {
                "mDataProp": "selesai"
            }, {
                "mDataProp": "on_proses"
            }]
        });
        progress.hide();
    });
    // on change tahun bidang
    filterTahunBidang.on('change', function(event) {
        var tahun = $(this).val();
        progress.fadeIn('fast');
        $.get(base_url + '/rest/rekap-pengajuan-bidang-tahun?tahun=' + tahun, function(data) {
            if (data.rekap.length <= 0) {
                alert('Rekap belum tersedia');
            } else {
                
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_flow);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    for (var i = 0; i < 12; i++) {
                        _temp[i] = 0;
                    }
                    var _o = [];
                    data.rekap.forEach(function(e, i) {
                        // _temp.push(parseInt(e.total))
                        
                        
                        
                        
                        // _temp[parseInt(e.bulan) - 1] = parseInt(1000);
                        var _x = {
                            name: e.nama_flow,
                            data: (function() {
                                var x = [];
                                for (var i = 0; i < 12; i++) {
                                    x[i] = 0;
                                }
                                x[parseInt(e.bulan) - 1] = parseInt(e.total);
                                return x;
                            })()
                        }
                        
                        _o.push(_x);
                    });
                    return _o;
                })();
                
                
                
                generateBarChart('#grf-rekap-pengajuan', {
                    categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                    series: _isi
                }, {
                    title: 'Semua Bidang',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi', data.komparasi, {
                    title: 'Semua Bidang',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelBidang.destroy();
                dtTabelBidang = tabelBidang.DataTable({
                    "aaData": data.rekap,
                    "iDisplayLength": -1,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_flow"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }],
                });
            }
            progress.hide();
        });
    });
    /*
        KEMENTRIAN
    */
    $.get(base_url + '/rest/rekap-pengajuan-kementrian-v1', function(data) {
        var _bulan = (function() {
            var _temp = [];
            data.rekap.forEach(function(e, i) {
                _temp.push(e.nama_bulan);
            })
            return _temp;
        })();
        var _isi = (function() {
            var _temp = [];
            data.rekap.forEach(function(e, i) {
                _temp.push(parseInt(e.total))
            })
            return _temp;
        })();
        
        generateBarChart('#grf-rekap-pengajuan-kementrian', {
            categories: _bulan,
            seriesData: _isi,
        }, {
            title: filterKementrian.val() || ' Semua Kementrian ',
            tahun: data.rekap[data.rekap.length - 1].tahun,
            bulan: data.rekap[data.rekap.length - 1].nama_bulan,
        });
        
        generatePieChart('#komparasi-kementrian', data.komparasi, {
            title: filterKementrian.val() || ' Semua Kementrian ',
            tahun: data.rekap[data.rekap.length - 1].tahun,
            bulan: data.rekap[data.rekap.length - 1].nama_bulan,
        });
        progress.hide();
    });
    //on change kementrian
    filterKementrian.on('change', function(event) {
        var namaKementrian = $(this).val();
        var tahun = filterTahunKementrian.val();
        progress.fadeIn('fast');
        //berdasarkan nama
        if (tahun == "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-nama?kementrian=' + namaKementrian, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        //berdasarkan tahun
        if (namaKementrian == "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-tahun?tahun=' + tahun, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        //berdasarkan nama & tahun
        if (namaKementrian != "" && tahun != "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-nama-tahun?kementrian=' + namaKementrian + '&tahun=' + tahun, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        progress.hide();
    });
    // on change tahun
    filterTahunKementrian.on('change', function(event) {
        var namaKementrian = filterKementrian.val();
        var tahun = $(this).val();
        progress.fadeIn('fast');
        //berdasarkan nama
        if (tahun == "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-nama?kementrian=' + namaKementrian, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        //berdasarkan tahun
        if (namaKementrian == "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-tahun?tahun=' + tahun, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        //berdasarkan nama & tahun
        if (namaKementrian != "" && tahun != "") {
            $.get(base_url + '/rest/rekap-pengajuan-kementrian-nama-tahun?kementrian=' + namaKementrian + '&tahun=' + tahun, function(data) {
                /*optional stuff to do after success */
                var _bulan = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(e.nama_bulan);
                    })
                    return _temp;
                })();
                var _isi = (function() {
                    var _temp = [];
                    data.rekap.forEach(function(e, i) {
                        _temp.push(parseInt(e.total))
                    })
                    return _temp;
                })();
                
                generateBarChart('#grf-rekap-pengajuan-kementrian', {
                    categories: _bulan,
                    seriesData: _isi,
                }, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                
                generatePieChart('#komparasi-kementrian', data.komparasi, {
                    title: filterKementrian.val() || ' Semua Kementrian ',
                    tahun: data.rekap[data.rekap.length - 1].tahun,
                    bulan: data.rekap[data.rekap.length - 1].nama_bulan,
                });
                dtTabelKementrian.destroy();
                dtTabelKementrian = tabelKementrian.DataTable({
                    aLengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    iDisplayLength: -1,
                    paging: false,
                    "aaData": data.rekap,
                    "aoColumns": [{
                        "mDataProp": "tahun"
                    }, {
                        "mDataProp": "nama_bulan"
                    }, {
                        "mDataProp": "total"
                    }, {
                        "mDataProp": "selesai"
                    }, {
                        "mDataProp": "on_proses"
                    }]
                })
            });
        }
        progress.hide();
    });
    /*
        @ opt : object
    */


    function generateBarChart(element, data, opt) {
        var _series = [];
        if (data.series) {
            _series = data.series
        } else {
            _series = [{
                name: 'Jumlah Rekapan',
                data: data.seriesData
            }]
        }
        $(element).highcharts({
            credits: false,
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Grafik Rekap Pengajuan ' + opt.title + ' Tahun ' + opt.tahun
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                min: 0,
                categories: data.categories,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' persen'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: _series
        });
    }

    function generatePieChart(element, data, opt) {
        $(element).highcharts({
            credits: false,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Komparasi ' + opt.title + ' Tahun ' + opt.tahun
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white',
                            fontSize: "15px"
                        },
                        distance: -60
                    },
                    showInLegend: true,
                }
            },
            series: [{
                name: "Komparasi",
                colorByPoint: true,
                data: [{
                    name: "On Time",
                    y: parseInt(data.persen_on_time)
                }, {
                    name: "Delay",
                    y: parseInt(data.persen_delay)
                }, ]
            }]
        });
    }
});