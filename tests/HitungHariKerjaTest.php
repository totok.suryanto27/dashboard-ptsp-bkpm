<?php
use App\Services\HitungHariKerja;
class HitungHariKerjaTest extends TestCase {

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $test =new HitungHariKerja();
        $this->assertEquals($test->count('2015-07-15','2015-07-24'),4);        
    }

    public function testBasic2Example()
    {
        $test =new HitungHariKerja();
        $this->assertEquals($test->count('2015-07-15','2015-07-17'),1);        
    }

    public function testBasic3Example()
    {
        $test =new HitungHariKerja();
        $this->assertEquals($test->count('2015-07-16','2015-07-18'),0);        
    }

}
