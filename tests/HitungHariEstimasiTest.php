<?php

use App\Services\HitungHariEstimasi;
class HitungHariEstimasiTest extends TestCase {



    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $test =new HitungHariEstimasi();
        $this->assertEquals($test->count('2015-07-15', 4), '2015-07-27');        
    }

    public function testBasic2Example()
    {
        $test =new HitungHariEstimasi();
        $this->assertEquals($test->count('2015-07-15', 1), '2015-07-22');        
    }

    public function testBasic3Example()
    {
        $test =new HitungHariEstimasi();
        $this->assertEquals($test->count('2015-07-17', 0), '2015-07-17');        
    }


}
