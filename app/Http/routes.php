<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('test',function() {
	$x = DB::table('hitung_perizinan_harian')->whereNull('id_m_instansi')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('hari',date('Y-m-d',strtotime('16-08-2015')))->first();
	return response()->json($x);
});
Route::get('/', array('as'=>'welcome','uses'=>'PageController@index'));
Route::get('perizinan-kl', array('as'=>'perizinan-kl','uses'=>'PageController@perizinanKl'));
Route::get('perizinan-kl/{id_m_instansi}', array('as'=>'perizinan-kl-list','uses'=>'PageController@perizinanKlList'));
Route::get('perizinan-kl/{id_m_instansi}/{id_m_sop_deskripsi}', array('as'=>'perizinan-kl-detail','uses'=>'PageController@perizinanKlDetail'));
Route::get('page/{page}', array('as'=>'page','uses'=>'PageController@index'));
Route::get('setlang/{lang}', array('as'=>'setlang','uses'=>'PageController@setLang'));

// Open Pindahan dari aplikasi Dashboard //
Route::get('dashboardbaru', 'NewDashboardController@index');
Route::group(['prefix' => 'monitoring-pengguna'], function(){
	Route::get('/', 'MonitoringPenggunaController@index');
	Route::get('/{id}', 'MonitoringPenggunaController@detail');
	Route::get('/save/log', 'MonitoringPenggunaController@create');
	Route::get('/user/hits2', 'MonitoringPenggunaController@getUserHits3');
});
Route::get('/rekap-pengajuan', 'NewRekapPengajuanController@index');
// Open Dashboard KL //
Route::get('/monitoring-pengajuan', 'MonitoringPengajuanController@index');
Route::get('/komparasi-layanan', 'NewKomparasiLayanan@index');
Route::get('/jumlah-pengajuan', 'MonitoringJumlahPengajuanController@index');
Route::get('/monitoring-performansi', 'MonitoringPerformansiController@index');
// Close Dashboard KL //

// Open Dashboard PTSP //
Route::get('/monitoring-pengajuan-ptsp', 'PtspMonitoringPengajuanController@index'); // Tambahan Controller utk PTSP
Route::get('/komparasi-layanan-ptsp', 'PtspNewKomparasiLayanan@index'); // Tambahan Controller utk PTSP
Route::get('/jumlah-pengajuan-ptsp', 'PtspMonitoringJumlahPengajuanController@index'); // Tambahan Controller utk PTSP
Route::get('/monitoring-performansi-ptsp', 'PtspMonitoringPerformansiController@index'); // Tambahan Controller utk PTSP
// Close Dashboard PTSP //
// Close Pindahan dari aplikasi Dashboard //
	
Route::get('home', 'HomeController@index');
Route::any('change-my-detail', array('middleware'=>'auth','as'=>'change-my-detail','uses'=>'HomeController@changeMyDetail'));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Open For Data KL //
Route::group(['prefix' => 'trackingregister'], function()
{
	Route::get('/', 'TrackingRegisterController@index');
	Route::get('/cari/{status?}', 'TrackingRegisterController@cari');
	Route::get('/search', 'TrackingRegisterController@search');
	Route::get('/export', 'TrackingRegisterController@export');
	Route::get('/edit/{id}', 'TrackingRegisterController@edit');
	Route::post('/update/{id}', 'TrackingRegisterController@update');
	Route::get('/delete/{id}', 'TrackingRegisterController@delete');
	Route::get('/listizin/{id}', 'TrackingRegisterController@listizin');
});
Route::group(['prefix' => 'konsultasi'], function()
{
	Route::get('/', 'KonsultasiController@index');
	Route::get('/create', 'KonsultasiController@create');
	Route::get('/getData', 'KonsultasiController@getData');
	Route::get('/get-kategori', 'KonsultasiController@getKategori');
	Route::get('/search', 'KonsultasiController@search');
	Route::get('/export', 'KonsultasiController@export');
	Route::post('/store', 'KonsultasiController@store');
	Route::get('/read/{id}', 'KonsultasiController@show');
	Route::get('/edit/{id}', 'KonsultasiController@edit');
	Route::post('/update/{id}', 'KonsultasiController@update');
	Route::get('/delete/{id}','KonsultasiController@destroy');

	Route::get('/lXOqwe3jmlxz01okad', 'KonsultasiController@index_iframe');
	Route::get('/cpw23ME923pdksaopd', 'KonsultasiController@list_iframe');
	Route::get('/SOIAJ32423kmcsxpal/{id}', 'KonsultasiController@show_iframe');
	Route::get('/co32i4uPSkdxcgh43H/{id}', 'KonsultasiController@edit_iframe');
	Route::post('/LKcdmskl32l4jozckk/{id}', 'KonsultasiController@update_iframe');
	Route::get('/COijdiowe23okxzzls/{id}','KonsultasiController@destroy_iframe');
	Route::get('/FNudifer932jeknfxa', 'KonsultasiController@export_iframe');
	Route::get('/KHVydg2iyer9kjhsac', 'KonsultasiController@search_iframe');

});
// Close For Data KL //

// Open For Data PTSP //
Route::group(['prefix' => 'trackingregisterptsp'], function()
{
	Route::get('/', 'PtspTrackingRegisterController@index');
	Route::get('/cari/{status?}', 'PtspTrackingRegisterController@cari');
	Route::get('/search', 'PtspTrackingRegisterController@search');
	Route::get('/export', 'PtspTrackingRegisterController@export');
	Route::get('/edit/{id}', 'PtspTrackingRegisterController@edit');
	Route::post('/update/{id}', 'PtspTrackingRegisterController@update');
	Route::get('/delete/{id}', 'PtspTrackingRegisterController@delete');
}); 
// Close For Data PTSP //

Route::group(['prefix' => 'tracking'], function()
{
	Route::get('/', 'TrackingController@index');
	Route::get('debug', 'TrackingRegisterController@debug');
	Route::get('perusahaan', 'TrackingController@findPerusahaan');
	Route::get('search-perusahaan', 'TrackingController@searchPerusahaan');
	Route::get('search-npwp', 'TrackingController@searchNpwp');
	Route::get('jenis-perizinan', 'TrackingController@getJenisPerizinan');
	Route::get('hitung-estimasi', 'TrackingController@hitungEstimasi');
	Route::get('detail', 'TrackingController@getDetail');
	Route::get('lembaran', 'TrackingController@getLembaran');
	Route::post('buat-perusahaan', 'TrackingController@buatPerusahaan');
	Route::post('check-in', 'TrackingController@checkIn');
	Route::post('check-out', 'TrackingController@checkOut');
	Route::post('ambil-izin', 'TrackingController@ambilIzin');
	Route::post('izin-stop', 'TrackingController@izinStop');
	Route::post('izin-tunda', 'TrackingController@izinTunda');
	Route::post('izin-lanjut', 'TrackingController@izinLanjut');
	Route::post('konsultasi', 'TrackingController@createKonsultasi');
});

Route::group(['prefix' => 'investortracking'], function()
{
	Route::get('/', 'InvestorTrackingController@index');
	Route::get('perusahaan', 'InvestorTrackingController@search');
	Route::get('detail', 'InvestorTrackingController@searchDetailIzin');
	Route::get('table', 'InvestorTrackingController@getDataTable');
});

Route::group(['prefix' => 'user'], function()
{
	Route::get('/', 'UserController@index');
	Route::get('/create', 'UserController@create');
	Route::get('/getData', 'UserController@getData');
	Route::get('/search', 'UserController@search');
	Route::post('/store', 'UserController@store');
	Route::get('/read/{id}', 'UserController@show');
	Route::get('/edit/{id}', 'UserController@edit');
	Route::post('/update/{id}', 'UserController@update');
	Route::get('/delete/{id}','UserController@destroy');
});

Route::group(['prefix' => 'konsultasi-kategori'], function()
{
	Route::get('/', 'KonsultasiKategoriController@index');
	Route::get('/create', 'KonsultasiKategoriController@create');
	Route::get('/getData', 'KonsultasiKategoriController@getData');
	Route::get('/search', 'KonsultasiKategoriController@search');
	Route::post('/store', 'KonsultasiKategoriController@store');
	Route::get('/read/{id}', 'KonsultasiKategoriController@show');
	Route::get('/edit/{id}', 'KonsultasiKategoriController@edit');
	Route::post('/update/{id}', 'KonsultasiKategoriController@update');
	Route::get('/delete/{id}','KonsultasiKategoriController@destroy');
});

Route::group(['prefix' => 'instansi'], function()
{
	Route::get('/', 'InstansiController@index');
	Route::get('/create', 'InstansiController@create');
	Route::get('/getData', 'InstansiController@getData');
	Route::get('/download/{id}', 'InstansiController@getDownload');
	Route::get('/downloadeng/{id}', 'InstansiController@getDownloadEng');
	Route::get('/search', 'InstansiController@search');
	Route::post('/store', 'InstansiController@store');
	Route::get('/read/{id}', 'InstansiController@show');
	Route::get('/edit/{id}', 'InstansiController@edit');
	Route::post('/update/{id}', 'InstansiController@update');
	Route::get('/delete/{id}','InstansiController@destroy');
});

Route::group(['prefix' => 'jenisperizinan'], function()
{
	Route::get('/', 'JenisPerizinanController@index');
	Route::get('/create', 'JenisPerizinanController@create');
	Route::get('/getData', 'JenisPerizinanController@getData');
	Route::get('/search', 'JenisPerizinanController@search');
	Route::post('/store', 'JenisPerizinanController@store');
	Route::get('/read/{id}', 'JenisPerizinanController@show');
	Route::get('/edit/{id}', 'JenisPerizinanController@edit');
	Route::post('/update/{id}', 'JenisPerizinanController@update');
	Route::get('/delete/{id}','JenisPerizinanController@destroy');
});

Route::group(['prefix' => 'sop-deskripsi'], function()
{
	Route::get('/', 'SopDeskripsiController@index');
	Route::get('/create', 'SopDeskripsiController@create');
	Route::get('/getData', 'SopDeskripsiController@getData');
	Route::get('/search', 'SopDeskripsiController@search');
	Route::post('/store', 'SopDeskripsiController@store');
	Route::get('/read/{id}', 'SopDeskripsiController@show');
	Route::get('/edit/{id}', 'SopDeskripsiController@edit');
	Route::post('/update/{id}', 'SopDeskripsiController@update');
	Route::get('/delete/{id}','SopDeskripsiController@destroy');
});

Route::group(['prefix' => 'flowizinterintegrasi'], function()
{
	Route::get('/', 'FlowIzinTerintegrasiController@index');
	Route::get('/create', 'FlowIzinTerintegrasiController@create');
	Route::get('/getData', 'FlowIzinTerintegrasiController@getData');
	Route::get('/search', 'FlowIzinTerintegrasiController@search');
	Route::post('/store', 'FlowIzinTerintegrasiController@store');
	Route::get('/read/{id}', 'FlowIzinTerintegrasiController@show');
	Route::get('/edit/{id}', 'FlowIzinTerintegrasiController@edit');
	Route::post('/update/{id}', 'FlowIzinTerintegrasiController@update');
	Route::get('/editdetail/{id}', 'FlowIzinTerintegrasiController@editDetail');
	Route::post('/updatedetail/{id}', 'FlowIzinTerintegrasiController@updateDetail');
	Route::get('/delete/{id}','FlowIzinTerintegrasiController@destroy');
});

Route::group(['prefix' => 'logs'], function()
{
	Route::get('/', 'LogsController@index');
	Route::get('/getData', 'LogsController@getData');
	
});

Route::group(['prefix' => 'gallery'], function()
{
	Route::get('/', ['as' => 'gallery', 'uses' => 'GalleryController@index']);
	Route::post('/upload', 'GalleryController@uploadFiles');
	Route::get('/image-list', 'GalleryController@imageList');
	Route::get('/delete/{id}','GalleryController@destroy');
});

Route::group(['prefix' => 'libur'], function()
{
	Route::get('/', 'HariLiburController@index');
	Route::get('/getData', 'HariLiburController@getData');
	Route::get('/create', 'HariLiburController@create');
	Route::post('/store', 'HariLiburController@store');
	Route::get('/read/{id}', 'HariLiburController@show');
	Route::get('/edit/{id}', 'HariLiburController@edit');
	Route::post('/update/{id}', 'HariLiburController@update');
	Route::get('/delete/{id}','HariLiburController@destroy');
	
});

Route::group(['prefix' => 'pengajuanizin'], function()
{
	Route::get('/', 'PengajuanIzinController@index');
	Route::get('/cari', 'PengajuanIzinController@cari');
	Route::get('/search', 'PengajuanIzinController@search');
	Route::get('/read/{id}', 'PengajuanIzinController@read');
});

Route::group(['prefix'=>'rest'], function(){
	
	Route::get('/data-rekap-pengajuan', 'RekapPengajuanController@dataRekap');
	Route::get('/rekap-pengajuan-bulanan', 'RekapPengajuanController@rekapPengajuanBulanan');
	Route::get('/komparasi-pengajuan', 'RekapPengajuanController@komparasiPengajuan');
	Route::get('/presentase-ontime', 'KomparasiLayananController@getPresentasiOnTime');
	Route::get('/presentase-ontime-bidang', 'KomparasiLayananController@getPresentasiOnTimeBidang');
	Route::get('/hits', 'MonitoringPenggunaController@getUserHits');
	
	Route::get('/dashboard-kementrian', 'DashboardController@mainKementrian');
	Route::get('/hitung_durasi', 'DurationController@index');
	Route::get('/list-instansi', function(){
		return DB::table(TABLESPACE.'M_INSTANSI')->get();
	});

	Route::get('/test-db', 'DashboardController@test');
	Route::get('/rekap-pengajuan-kementrian', 'RekapPengajuanController@restRekapPengajuanKementrian');
	Route::get('/rekap-pengajuan-kementrian-nama', 'RekapPengajuanController@restRekapPengajuanKementrianByNama');
	Route::get('/rekap-pengajuan-kementrian-tahun', 'RekapPengajuanController@restRekapPengajuanKementrianByTahun');
	Route::get('/rekap-pengajuan-kementrian-nama-tahun', 'RekapPengajuanController@restRekapPengajuanKementrianByNamaTahun');
	Route::get('/rekap-pengajuan-kementrian-v1', 'NewRekapPengajuanController@newRekapKementrian');
	Route::get('/rekap-pengajuan-bidang-v1', 'NewRekapPengajuanController@newRekapBidang');
	Route::get('/rekap-pengajuan-bidang-v2', 'NewRekapPengajuanController@newRekapBidangBln');
	
	Route::get('/data-performansi-bulanan','MonitoringPerformansiController@get_monthly_performance');
	Route::get('/data-performansi-tahunan','MonitoringPerformansiController@get_yearsly_performance');
	Route::get('/tabel-performansi-bulanan','MonitoringPerformansiController@get_monthly_table');
	Route::get('/tabel-performansi-tahunan','MonitoringPerformansiController@get_yearsly_table');
	Route::get('/tabel-komparasi-tahunan','NewKomparasiLayanan@get_years');
	Route::get('/tabel-komparasi-bulanan','NewKomparasiLayanan@get_monthly');
	Route::get('/tabel-komparasi-periodik','NewKomparasiLayanan@get_periodic'); //tambahan September 2016 
	Route::get('/tabel-komparasi-izin-tahunan','NewKomparasiLayanan@get_izin_years');
	Route::get('/tabel-komparasi-izin-bulanan','NewKomparasiLayanan@get_izin_monthly');
	Route::get('/tabel-komparasi-izin-periodik','NewKomparasiLayanan@get_izin_periodik'); //tambahan September 2016
	Route::get('/tabel-komparasi-grup-izin-periodik','NewKomparasiLayanan@get_grupizin_periodik'); //tambahan September 2016
	Route::get('/monitoring-pengajuan-data-ptsp', 'PtspMonitoringPengajuanController@get_data_ptsp'); //tambahan September 2016
	Route::get('/monitoring-pengajuan-ptsp-periodik', 'PtspMonitoringPengajuanController@periodicData'); //tambahan Oktober 2016
	Route::get('/monitoring-izin-ptsp-periodik', 'PtspMonitoringPengajuanController@periodicIzin'); //tambahan Oktober 2016
	Route::get('/tabel-komparasi-tahunan-ptsp','PtspNewKomparasiLayanan@get_years'); //tambahan September 2016 
	Route::get('/tabel-komparasi-bulanan-ptsp','PtspNewKomparasiLayanan@get_monthly'); //tambahan September 2016 
	Route::get('/tabel-komparasi-periodik-ptsp','PtspNewKomparasiLayanan@get_periodic'); //tambahan September 2016 
	Route::get('/tabel-komparasi-izin-tahunan-ptsp','PtspNewKomparasiLayanan@get_izin_years'); //tambahan September 2016 
	Route::get('/tabel-komparasi-izin-bulanan-ptsp','PtspNewKomparasiLayanan@get_izin_monthly'); //tambahan September 2016 
	Route::get('/tabel-komparasi-izin-periodik-ptsp','PtspNewKomparasiLayanan@get_izin_periodik'); //tambahan September 2016
	Route::get('/tabel-komparasi-grup-izin-periodik-ptsp','PtspNewKomparasiLayanan@get_grupizin_periodik'); //tambahan September 2016
	
	Route::get('/rekap-pengajuan-bidang', 'RekapPengajuanController@restRekapPengajuanBidang');
	Route::get('/rekap-pengajuan-bidang-tahun', 'RekapPengajuanController@restRekapPengajuanBidangTahun');

	Route::get('/komparasi-layanan-kementrian', 'KomparasiLayananController@restKomparasiLayananKementrian');
	Route::get('/komparasi-layanan-bidang', 'KomparasiLayananController@restKomparasiLayananBidang');


	Route::get('/dashboard-bidang', 'DashboardController@main');
	Route::get('/dashboard-kementrian', 'DashboardController@mainKementrian');
	Route::get('/dashboard-bidang-v2', 'DashboardController@dashboardBidang');
	Route::get('/monitoring-pengajuan-harian', 'MonitoringPengajuanController@initData');
	Route::get('/monitoring-pengajuan-bulanan', 'MonitoringPengajuanController@monthlyData');
	Route::get('/monitoring-pengajuan-tahunan', 'MonitoringPengajuanController@yearlyData');
	Route::get('/monitoring-pengajuan-periodik', 'MonitoringPengajuanController@periodicData');
	Route::get('/monitoring-pengajuan-harian-km', 'MonitoringPengajuanController@initDataKmntr');
	Route::get('/monitoring-pengajuan-bulanan-km', 'MonitoringPengajuanController@monthlyDataKmntr');
	Route::get('/monitoring-pengajuan-tahunan-km', 'MonitoringPengajuanController@yearlyDataKmntr');
	Route::get('/monitoring-pengajuan-periodik-km', 'MonitoringPengajuanController@periodicDataKmntr');
 

	Route::get('/dashboard-data-harian', 'NewDashboardController@initDataKmntr');
	Route::get('/dashboard-data-bulanan', 'NewDashboardController@monthlyDataKmntr');
	Route::get('/dashboard-data-tahunan', 'NewDashboardController@yearlyDataKmntr');
	Route::get('/dashboard-data-periodik', 'NewDashboardController@periodicDataKmntr');
	Route::get('/dashboard-on-proses', 'NewDashboardController@initDataOnProses');
	Route::get('/dashboard-drilldown', 'NewDashboardController@initDataDrilldown');

	// /getGeneralData
	//monitoring pengguna
	Route::get('/durasi-pengguna', 'MonitoringPenggunaController@getGeneralData');
	Route::get('/durasi-pengguna-bulanan', 'MonitoringPenggunaController@getBulanan');
	Route::get('/durasi-pengguna-tahunan', 'MonitoringPenggunaController@getTahunan');
 

	Route::get('/hitung-perizinan-bulanan/{jenis}', function($jenis){
		if($jenis == 1){
			//kementrian
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')
			->join('M_DIREKTORAT', 'HITUNG_PERIZINAN_BULANAN.id_m_flow_terintegrasi', '=', 'M_DIREKTORAT.id_m_direktorat')
			->get();
		}else{
			//bidang
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')
			->join('M_INSTANSI', 'HITUNG_PERIZINAN_BULANAN.id_m_instansi', '=', 'M_INSTANSI.id_m_instansi')
			->get();
		}
		
	});

	Route::get('/hitung-perizinan-harian/{jenis}', function($jenis){
		if($jenis == 1){
			//kementrian
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')
			->join('M_DIREKTORAT', 'HITUNG_PERIZINAN_HARIAN.id_m_flow_terintegrasi', '=', 'M_DIREKTORAT.id_m_direktorat')
			->get();
		}else{
			//bidang
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')
			->join('M_INSTANSI', 'HITUNG_PERIZINAN_HARIAN.id_m_instansi', '=', 'M_INSTANSI.id_m_instansi')
			->get();
		}
		
	});

	Route::get('/hitung-perizinan-tahunan/{jenis}', function($jenis){
		if($jenis == 1){
			//kementrian
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_TAHUNAN')
			->join('M_DIREKTORAT', 'HITUNG_PERIZINAN_TAHUNAN.id_m_flow_terintegrasi', '=', 'M_DIREKTORAT.id_m_direktorat')
			->get();
		}else{
			//bidang
			return DB::table(TABLESPACE.'HITUNG_PERIZINAN_TAHUNAN')
			->join('M_INSTANSI', 'HITUNG_PERIZINAN_TAHUNAN.id_m_instansi', '=', 'M_INSTANSI.id_m_instansi')
			->get();
		}
		
	});

	Route::get('/hitung-perizinan-proses', function(){
		return DB::table(TABLESPACE.'HITUNG_PERIZINAN_ON_PROSES')
			->join('M_DIREKTORAT', 'HITUNG_PERIZINAN_ON_PROSES.id_m_instansi', '=', 'M_DIREKTORAT.id_m_instansi')
			->get();
	});
});
