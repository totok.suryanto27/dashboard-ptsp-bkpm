<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits; 

class NewDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
			// 'Admin1' <= getUserId()
            $hits = new UserHits;
            $hits->page = 'Dashboard';
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month; 
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 
    
    public function index(Request $request) {
        Navigator::setActive(url('/'));
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year; 
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;
        
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;
        
        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );
        
        return view('pages.dashboard-v2', compact('list_bulan','current_month','current_year' ,'list_instansi','list_kementrian','current_month','current_year','date_current'));
    }
    public function initDataDrilldown(){
        $QUERY = "SELECT  NAMA_FLOW,ID_M_FLOW_TERINTEGRASI as id_proses,
        NVL(SUM(TOTAL_IZIN),0)  AS izin_on_process,
        NVL(SUM(DELAY),0) AS on_process_telat
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES
        WHERE ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NOT NULL 
        AND ID_M_FLOW_TERINTEGRASI != 0
        GROUP BY NAMA_FLOW,ID_M_FLOW_TERINTEGRASI";
        $data_onproses = DB::select(DB::raw($QUERY));

        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;

        $QUERY_FLOW = "SELECT 
        NAMA_FLOW,ID_M_FLOW_TERINTEGRASI as id_selesai,
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE BULAN=".$current_month." AND TAHUN = ".$current_year." 
        AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NOT NULL 
        AND ID_M_FLOW_TERINTEGRASI != 0
        GROUP BY NAMA_FLOW,ID_M_FLOW_TERINTEGRASI";
        $data_selesai = DB::select(DB::raw($QUERY_FLOW)); 

        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));

        $data = array();
        foreach ($list_instansi as $key) {
            $nama_flow = $key->nama_flow;
            $id = $key->id_m_flow_izin_terintegrasi;
            $izin_on_process = 0;
            $on_process_telat = 0;
            $on_process = 0;
            foreach ($data_onproses as $p) {
                if ($p->id_proses == $id) {
                    $izin_on_process = $p->izin_on_process;
                    $on_process_telat = $p->on_process_telat;
                    $on_process = $izin_on_process - $on_process_telat;

                }
            }

            $total_pengajuan = 0;
            $total_selesai = 0;
            $total_terlambat = 0;
            $total_on_time = 0;

            foreach ($data_selesai as $s) {
                if ($s->id_selesai == $id) {
                    $total_pengajuan = $s->total_pengajuan;
                    $total_selesai = $s->total_selesai;
                    $total_terlambat = $s->total_terlambat;
                    $total_on_time = $s->total_on_time;

                }
            }

            $obj = array('nama_flow' =>$nama_flow ,
            'id_bidang' =>$id,
            'izin_on_process' => $izin_on_process,
            'on_process_telat' => $on_process_telat,
            'on_process' => $on_process,
            'total_pengajuan'=> $total_pengajuan,
            'total_selesai'=>$total_selesai,
            'total_terlambat'=>$total_terlambat,
            'total_on_time'=>$total_on_time );

            array_push($data, $obj);

        }

        print json_encode($data);


    }
    public function initDataOnProses(Request $request){

        $QUERY = "SELECT 
        NVL(SUM(TOTAL_IZIN),0)  AS izin_on_process,
        NVL(SUM(DELAY),0) AS on_process_telat
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES";
        if ($request->input('kementrian')) {
            $QUERY = $QUERY." WHERE ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." WHERE ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }

        $data = DB::select(DB::raw($QUERY));
        echo json_encode($data);

    }


    public function initDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        $current_date = $current_day."-".$current_month."-".$current_year;
        if ($request->input('tanggal')) {
            $current_date =  $request->input('tanggal');
        }
 
        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE TRUNC(HARI) = TO_DATE('".$current_date."', 'DD-MM-YYYY')";
        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));


        echo json_encode($data);
    }

    public function monthlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        if ($request->input('bulan')) {
            $current_month = $request->input('bulan');
        }
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE BULAN=".$current_month." AND TAHUN = ".$current_year;

        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }

    
    public function yearlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN WHERE  TAHUN = ".$current_year;

        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL" ;
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }
    
    public function periodicDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        $to_date = $current_day."-".$current_month."-".$current_year;
        $from_date = $current_day."-".$current_month."-".$current_year;
        if ($request->input('tanggal')) {
            $from_date =  $request->input('tanggal');
            $to_date =  $request->input('tanggal_sd');
        }
        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE TRUNC(HARI) BETWEEN TO_DATE('".$from_date."', 'DD-MM-YYYY')
         AND TO_DATE('".$to_date."','DD-MM-YYYY')";
        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));


        echo json_encode($data);
    }
}
