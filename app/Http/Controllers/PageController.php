<?php namespace App\Http\Controllers;

use App\Model\Instansi;
use App\Model\SopDeskripsi;
use DB;

class PageController extends Controller {

	public function __construct()
	{
		\App::setLocale(getLang());
		//$this->middleware('cache_page');
	}

	public function index($page='index')
	{
		$lang = getLang();
		$view = "pages_$lang/$page";
		if (view()->exists($view))
			return view($view);
		$view = "pages_id/$page";
		if (view()->exists($view))
			return view($view);
		return redirect('/');
	}
	
	public function setLang($lang='id')
	{
		if (!in_array($lang,['id','en'])) {
			return redirect()->back()->message("Bahasa $lang tidak dikenali oleh sistem");
		}
		setLang($lang);
		return redirect()->back();
	}
	
	public function perizinanKl()
	{
		$instansi = Instansi::where('isbko','T')->get();
		return view('dashboard.perizinan-kl',compact('instansi'));
	}
	
	public function perizinanKlList($id_m_instansi)
	{
		$instansi = Instansi::find($id_m_instansi);
		if (!$instansi) return redirect()->back()->message("Instansi tidak ditemukan");
		$lang = getLang();
		$izin_kl = SopDeskripsi::with('children')->where('bahasa',$lang)->where('id_m_instansi',$id_m_instansi)->whereNull('id_parent')->orderBy('id_m_sop_deskripsi')->get();
		$izin_kl_group = $izin_kl->sortBy('id_m_direktorat')->groupBy('id_m_direktorat');
		return view('dashboard.perizinan-kl-list',compact('izin_kl_group','instansi'));
	}
	
	public function perizinanKlDetail($id_m_instansi,$id_m_sop_deskripsi)
	{
		$instansi = Instansi::find($id_m_instansi);
		if (!$instansi) return redirect()->back()->message("Instansi tidak ditemukan");
		$lang = getLang();
		$sop_deskripsi = SopDeskripsi::where('bahasa',$lang)->where('id_m_instansi',$id_m_instansi)->where('id_m_sop_deskripsi',$id_m_sop_deskripsi)->first();
		return view('dashboard.perizinan-kl-detail',compact('sop_deskripsi','instansi'));
	}

}
