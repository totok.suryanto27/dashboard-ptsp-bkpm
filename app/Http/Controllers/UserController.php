<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    private $columns = [
    'NAMA_USER',
    'PASSWORD',
    'ID_M_INSTANSI',
    //'ID_M_USER_ROLES',
    'ID_M_DIREKTORAT',
    'JABATAN',
    'TELP',
    'EMAIL']
    ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('user.index');
    }

    public function getData(){
        $user = User::all();
        return Datatables::of($user)
        ->addColumn('actions','
                        <a href="{{ url( \'user\read\',$id_m_user )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a><br>
                        <a href="{{ url( \'user\edit\',$id_m_user )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a><br>
                        <a href="{{ url( \'user\delete\',$id_m_user ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $user = New User;
        $user->id_m_user = $req->id_m_user;
        $user->nama_user = $req->nama_user;
        $user->password = md5($req->password);
        $user->id_m_instansi = $req->id_m_instansi;
        $user->id_m_direktorat = $req->id_m_direktorat;
        $user->telp = $req->telp;
        $user->email = $req->email;
        $user->jabatan = $req->jabatan;
        if ($req->npwp)
            $user->npwp = $req->npwp;
        else
            $user->npwp = null;
        $user->roles()->attach(getRoleIds(getRoleSet()[$req->role]));
        $user->save();

        //return redirect('user/');
        return view('user.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $user = User::find($id);
        $user->id_m_user = $req->id_m_user;
        $user->nama_user = $req->nama_user;
        if($req->password != ''){
            $user->password = md5($req->password);
        }
        $user->id_m_instansi = $req->id_m_instansi;
        $user->id_m_direktorat = $req->id_m_direktorat;
        $user->telp = $req->telp;
        $user->email = $req->email;
        $user->nip = $req->nip;
        $user->jabatan = $req->jabatan;
        if ($req->npwp)
            $user->npwp = $req->npwp;
        else
            $user->npwp = null;
        $user->roles()->detach();
        $user->roles()->attach(getRoleIds(getRoleSet()[$req->role]));
        $user->save();


        return view('user.edit', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       User::find($id)->delete();
        return redirect()->back();
    }
}
