<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HitungPerizinanBulanan;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits;

class RekapPengajuanController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    // public function __construct()
    // {
    //     \App::setLocale(getLang());
    //     $this->middleware('guest');
    // }
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Rekap Pengajuan';
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 
    
    public function index(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $currentYear = \Carbon\Carbon::now()->year;
        $bulan = array(instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear),);
        
        $bulan2 = array(instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear), instanceRekap($currentYear),);
        
        $bulan[0]->bulan = '1';
        $bulan[0]->nama_bulan = 'Januari';
        
        $bulan[1]->bulan = '2';
        $bulan[1]->nama_bulan = 'Februari';
        
        $bulan[2]->bulan = '3';
        $bulan[2]->nama_bulan = 'Maret';
        
        $bulan[3]->bulan = '4';
        $bulan[3]->nama_bulan = 'April';
        
        $bulan[4]->bulan = '5';
        $bulan[4]->nama_bulan = 'Mei';
        
        $bulan[5]->bulan = '6';
        $bulan[5]->nama_bulan = 'Juni';
        
        $bulan[6]->bulan = '7';
        $bulan[6]->nama_bulan = 'Juli';
        
        $bulan[7]->bulan = '8';
        $bulan[7]->nama_bulan = 'Agustus';
        
        $bulan[8]->bulan = '9';
        $bulan[8]->nama_bulan = 'September';
        
        $bulan[9]->bulan = '10';
        $bulan[9]->nama_bulan = 'Oktober';
        
        $bulan[10]->bulan = '11';
        $bulan[10]->nama_bulan = 'November';
        
        $bulan[11]->bulan = '12';
        $bulan[11]->nama_bulan = 'Desember';
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan total,HITUNG_PERIZINAN_BULANAN.hitung_selesai selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NOT NULL AND id_m_jenis_perizinan IS NULL "));
        
        // echo "belum masuk kondisi <br>";
        
        // echo "masuk bidang belum kondisi <br>";
        $rekapPengajuanBulananBidang = DB::select(DB::raw('select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan total,HITUNG_PERIZINAN_BULANAN.hitung_selesai selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses on_proses from '.TABLESPACE.'.HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NULL AND id_m_flow_terintegrasi IS NOT NULL'));
        
        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            $bulan[$val->bulan - 1] = $val;
        }
        
        foreach ($rekapPengajuanBulananBidang as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            
            $bulan2[$val->bulan - 1] = $val;
        }
        
        $i = 1;
        foreach ($bulan2 as $val) {
            if (empty($val->bulan)) {
                $val->nama_bulan = getNamaBulan($i);
                $val->bulan = intval($i);
            }
            $i++;
        }
        
        return view('pages.rekap-pengajuan', compact('dataBulanan', 'list_instansi', 'list_kementrian', 'title', 'years', 'bulan', 'bulan2','currentYear'));
    }

    public function newRekapKementrian(Request $request){
        $time = \Carbon\Carbon::now();
        $currentYear = $time->year;
        $id_m_instansi = "IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";

        if ($request->input('tahun')) {
            $currentYear = $request->input('tahun');
        }

        if($request->input('id_instansi')){
            $id_m_instansi = " = ".$request->input('id_instansi')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }

        $query = "SELECT bulan,tahun,
        HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,
        HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,
        HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,
        HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,
        HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses 
        from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE ID_M_INSTANSI ".$id_m_instansi."  
        AND tahun = ".$currentYear;

        $bulan[0]['bulan'] = '1';
        $bulan[0]['nama_bulan'] = 'Januari';
        $bulan[0]['kosong'] ="1";
        
        $bulan[1]['bulan'] = '2';
        $bulan[1]['nama_bulan'] = 'Februari';
        $bulan[1]['kosong'] ="1";
        
        $bulan[2]['bulan'] = '3';
        $bulan[2]['nama_bulan'] = 'Maret';
        $bulan[2]['kosong'] ="1";
        
        $bulan[3]['bulan'] = '4';
        $bulan[3]['nama_bulan'] = 'April';
        $bulan[3]['kosong'] ="1";
        
        $bulan[4]['bulan'] = '5';
        $bulan[4]['nama_bulan'] = 'Mei';
        $bulan[4]['kosong'] ="1";
        
        $bulan[5]['bulan'] = '6';
        $bulan[5]['nama_bulan'] = 'Juni';
        $bulan[5]['kosong'] ="1";
        
        $bulan[6]['bulan'] = '7';
        $bulan[6]['nama_bulan'] = 'Juli';
        $bulan[6]['kosong'] ="1";
        
        $bulan[7]['bulan'] = '8';
        $bulan[7]['nama_bulan'] = 'Agustus';
        $bulan[7]['kosong'] ="1";
        
        $bulan[8]['bulan'] = '9';
        $bulan[8]['nama_bulan'] = 'September';
        $bulan[8]['kosong'] ="1";
        
        $bulan[9]['bulan'] = '10';
        $bulan[9]['nama_bulan'] = 'Oktober';
        $bulan[9]['kosong'] ="1";
        
        $bulan[10]['bulan'] = '11';
        $bulan[10]['nama_bulan'] = 'November';
        $bulan[10]['kosong'] ="1";
        
        $bulan[11]['bulan'] = '12';
        $bulan[11]['nama_bulan'] = 'Desember';
        $bulan[11]['kosong'] ="1";

        $rekaps = array();
        

        $rekap = DB::select(DB::raw($query));
        $temps = array();
        $i = 0; 
        foreach ($rekap as $val) {
            $bln = $val->bulan;
            $data['bulan'] = $bln;
            $data['tahun'] = $currentYear;
            $data['total'] = $val->total;
            $data['selesai'] = $val->selesai;
            $data['terlambat'] = $val->terlambat;
            $data['on_time'] = $val->on_time;
            $data['on_proses'] = $val->on_proses;
            $data['nama_bulan'] = $bulan[$bln-1]['nama_bulan'];
            $data['persenOnTime'] = 0;
            $data['persenDelay'] = 0;
            $bulan[$bln-1]['kosong'] = "0";
            array_push($temps, $data);
            $i++;
        }
        $x = 0;
        for ($j=0; $j < 12 ; $j++) { 
            if ($bulan[$j]['kosong'] == "1") {
                $data['bulan'] = $j+1;
                $data['tahun'] = $currentYear;
                $data['total'] = 0;
                $data['selesai'] = 0;
                $data['terlambat'] = 0;
                $data['on_time'] = 0;
                $data['on_proses'] = 0;
                $data['nama_bulan'] = $bulan[$j]['nama_bulan'];
                $data['persenOnTime'] = 0;
                $data['persenDelay'] = 0;
                $bulan[$bln-1]['kosong'] = "0";
                array_push($rekaps, $data);
            }else{
                $data = $temps[$x];
                array_push($rekaps, $data);
                $x++;

            }
        }
        $komparasi = array('persen_on_time' =>0 , 'persen_delay'=>0);
        $json['rekap'] = $rekaps;
        $json['komparasi'] = $komparasi;    
        return($json);
    }
    
    public function restRekapPengajuanKementrian(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $bulan = array(instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(),);
        
        
        
        $bulan[0]->bulan = '1';
        $bulan[0]->nama_bulan = 'Januari';
        $bulan[0]->kosong =true;
        
        $bulan[1]->bulan = '2';
        $bulan[1]->nama_bulan = 'Februari';
        $bulan[1]->kosong =true;
        
        $bulan[2]->bulan = '3';
        $bulan[2]->nama_bulan = 'Maret';
        $bulan[2]->kosong =true;
        
        $bulan[3]->bulan = '4';
        $bulan[3]->nama_bulan = 'April';
        $bulan[3]->kosong =true;
        
        $bulan[4]->bulan = '5';
        $bulan[4]->nama_bulan = 'Mei';
        $bulan[4]->kosong =true;
        
        $bulan[5]->bulan = '6';
        $bulan[5]->nama_bulan = 'Juni';
        $bulan[5]->kosong =true;
        
        $bulan[6]->bulan = '7';
        $bulan[6]->nama_bulan = 'Juli';
        $bulan[6]->kosong =true;
        
        $bulan[7]->bulan = '8';
        $bulan[7]->nama_bulan = 'Agustus';
        $bulan[7]->kosong =true;
        
        $bulan[8]->bulan = '9';
        $bulan[8]->nama_bulan = 'September';
        $bulan[8]->kosong =true;
        
        $bulan[9]->bulan = '10';
        $bulan[9]->nama_bulan = 'Oktober';
        $bulan[9]->kosong =true;
        
        $bulan[10]->bulan = '11';
        $bulan[10]->nama_bulan = 'November';
        $bulan[10]->kosong =true;
        
        $bulan[11]->bulan = '12';
        $bulan[11]->nama_bulan = 'Desember';
        $bulan[11]->kosong =true;
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NOT NULL AND tahun = '" . \Carbon\Carbon::now()->year . "'"));
        
        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            $bulan[$val->bulan - 1] = $val;
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
    }
    
    public function restRekapPengajuanKementrianByNama(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        $kementrian = $request->input('kementrian');
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $bulan = array(instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(),);
        
        
        
        $bulan[0]->bulan = '1';
        $bulan[0]->nama_bulan = 'Januari';
        
        $bulan[1]->bulan = '2';
        $bulan[1]->nama_bulan = 'Februari';
        
        $bulan[2]->bulan = '3';
        $bulan[2]->nama_bulan = 'Maret';
        
        $bulan[3]->bulan = '4';
        $bulan[3]->nama_bulan = 'April';
        
        $bulan[4]->bulan = '5';
        $bulan[4]->nama_bulan = 'Mei';
        
        $bulan[5]->bulan = '6';
        $bulan[5]->nama_bulan = 'Juni';
        
        $bulan[6]->bulan = '7';
        $bulan[6]->nama_bulan = 'Juli';
        
        $bulan[7]->bulan = '8';
        $bulan[7]->nama_bulan = 'Agustus';
        
        $bulan[8]->bulan = '9';
        $bulan[8]->nama_bulan = 'September';
        
        $bulan[9]->bulan = '10';
        $bulan[9]->nama_bulan = 'Oktober';
        
        $bulan[10]->bulan = '11';
        $bulan[10]->nama_bulan = 'November';
        
        $bulan[11]->bulan = '12';
        $bulan[11]->nama_bulan = 'Desember';
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE nama_instansi = '" . $kementrian . "'"));
        
        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;

            $bulan[$val->bulan - 1] = $val;
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
        // echo "select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NOT NULL AND id_m_flow_terintegrasi IS NULL AND id_m_jenis_perizinan IS NULL AND nama_instansi = '" . $kementrian . "'";
    }
    
    public function restRekapPengajuanKementrianByTahun(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        $tahun = $request->input('tahun');
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $bulan = array(instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(),);
        
        if ($tahun) {
            $bulan = array(instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun),);
        }
        
        
        
        $bulan[0]->bulan = '1';
        $bulan[0]->nama_bulan = 'Januari';
        
        $bulan[1]->bulan = '2';
        $bulan[1]->nama_bulan = 'Februari';
        
        $bulan[2]->bulan = '3';
        $bulan[2]->nama_bulan = 'Maret';
        
        $bulan[3]->bulan = '4';
        $bulan[3]->nama_bulan = 'April';
        
        $bulan[4]->bulan = '5';
        $bulan[4]->nama_bulan = 'Mei';
        
        $bulan[5]->bulan = '6';
        $bulan[5]->nama_bulan = 'Juni';
        
        $bulan[6]->bulan = '7';
        $bulan[6]->nama_bulan = 'Juli';
        
        $bulan[7]->bulan = '8';
        $bulan[7]->nama_bulan = 'Agustus';
        
        $bulan[8]->bulan = '9';
        $bulan[8]->nama_bulan = 'September';
        
        $bulan[9]->bulan = '10';
        $bulan[9]->nama_bulan = 'Oktober';
        
        $bulan[10]->bulan = '11';
        $bulan[10]->nama_bulan = 'November';
        
        $bulan[11]->bulan = '12';
        $bulan[11]->nama_bulan = 'Desember';
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NOT NULL AND id_m_flow_terintegrasi IS NULL AND tahun = '" . $tahun . "'"));
        
        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            $bulan[$val->bulan - 1] = $val;
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
    }
    
    public function restRekapPengajuanKementrianByNamaTahun(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        $kementrian = $request->input('kementrian');
        $tahun = $request->input('tahun');
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $bulan = array(instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(),);
        
        if ($tahun) {
            $bulan = array(instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun), instanceRekap($tahun),);
        }
        
        
        
        $bulan[0]->bulan = '1';
        $bulan[0]->nama_bulan = 'Januari';
        
        $bulan[1]->bulan = '2';
        $bulan[1]->nama_bulan = 'Februari';
        
        $bulan[2]->bulan = '3';
        $bulan[2]->nama_bulan = 'Maret';
        
        $bulan[3]->bulan = '4';
        $bulan[3]->nama_bulan = 'April';
        
        $bulan[4]->bulan = '5';
        $bulan[4]->nama_bulan = 'Mei';
        
        $bulan[5]->bulan = '6';
        $bulan[5]->nama_bulan = 'Juni';
        
        $bulan[6]->bulan = '7';
        $bulan[6]->nama_bulan = 'Juli';
        
        $bulan[7]->bulan = '8';
        $bulan[7]->nama_bulan = 'Agustus';
        
        $bulan[8]->bulan = '9';
        $bulan[8]->nama_bulan = 'September';
        
        $bulan[9]->bulan = '10';
        $bulan[9]->nama_bulan = 'Oktober';
        
        $bulan[10]->bulan = '11';
        $bulan[10]->nama_bulan = 'November';
        
        $bulan[11]->bulan = '12';
        $bulan[11]->nama_bulan = 'Desember';
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun,HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NOT NULL AND id_m_flow_terintegrasi IS NULL AND tahun = '" . \Carbon\Carbon::now()->year . "' AND nama_instansi = '" . $kementrian . "'"));
        
        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            $bulan[$val->bulan - 1] = $val;
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
    }

    public function restRekapPengajuanBidang(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $bulan = array(instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(), instanceRekap(),);
        
        
        
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun, nama_flow, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai,SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat,SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time,SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_proses) as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NULL AND id_m_flow_terintegrasi IS NOT NULL AND id_m_jenis_perizinan IS NULL AND tahun = '".\Carbon\Carbon::now()->year."' AND id_m_flow_terintegrasi != 0 GROUP BY (nama_flow, bulan, tahun)"));
        $bulan = [];

        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            
            array_push($bulan, $val);
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
        // return $rekapPengajuanBulananKementrian;
    }

    public function restRekapPengajuanBidangTahun(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        $tahun = $request->input('tahun');
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        
        
        $dataBulanan = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $list_instansi = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        $rekapPengajuanBulananKementrian = DB::select(DB::raw("select bulan, tahun, nama_flow, HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_instansi IS NULL AND id_m_flow_terintegrasi IS NOT NULL AND id_m_jenis_perizinan IS NULL AND tahun = '".$tahun."'"));
        $bulan = [];

        foreach ($rekapPengajuanBulananKementrian as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
            $val->bulan = intval($val->bulan);
            $val->persenOnTime = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
            $val->persenDelay = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            
            array_push($bulan, $val);
        }
        $i = 0;
        $total_on_time = 0;
        $total_delay = 0;
        $total_selesai = 0;
        foreach ($bulan as $val) {
            if (!empty($bulan[$i + 1])) {
                $total_on_time+= ($val->on_time + $bulan[$i + 1]->on_time);
                $total_delay+= ($val->terlambat + $bulan[$i + 1]->terlambat);
                $total_selesai+= ($val->selesai + $bulan[$i + 1]->selesai);
            }
            
            $i++;
        }
        $persen_on_time = ($total_selesai == 0) ? 0 : ($total_on_time / $total_selesai) * 100;
        $persen_delay = ($total_selesai == 0) ? 0 : ($total_delay / $total_selesai) * 100;
        return array('rekap' => $bulan, 'komparasi' => array('persen_on_time' => $persen_on_time, 'persen_delay' => $persen_delay));
        // return $rekapPengajuanBulananKementrian;
    }

    
    
    public function komparasiPengajuan(Request $request) {
        if ($request->input('bidang')) {
            $rekapPengajuanBulanan = DB::select(DB::raw("select SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN, ".TABLESPACE.".M_FLOW_IZIN_TERINTEGRASI WHERE M_FLOW_IZIN_TERINTEGRASI.id_m_flow_izin_terintegrasi = HITUNG_PERIZINAN_BULANAN.id_m_flow_terintegrasi group by HITUNG_PERIZINAN_BULANAN.tahun, M_FLOW_IZIN_TERINTEGRASI.nama_flow HAVING HITUNG_PERIZINAN_BULANAN.tahun = '" . \Carbon\Carbon::now()->year . "' AND M_FLOW_IZIN_TERINTEGRASI.nama_flow = '" . $request->input('bidang') . "'"));
        } 
        else {
            $rekapPengajuanBulanan = DB::select(DB::raw("select SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN, ".TABLESPACE.".M_FLOW_IZIN_TERINTEGRASI WHERE M_FLOW_IZIN_TERINTEGRASI.id_m_flow_izin_terintegrasi = HITUNG_PERIZINAN_BULANAN.id_m_flow_terintegrasi group by HITUNG_PERIZINAN_BULANAN.tahun HAVING HITUNG_PERIZINAN_BULANAN.tahun = '" . \Carbon\Carbon::now()->year . "'"));
        }
        
        foreach ($rekapPengajuanBulanan as $val) {
            $val->persen_selesai = ($val->total == 0) ? 0 : ($val->selesai / $val->total) * 100;
            $val->persen_terlambat = ($val->total == 0) ? 0 : ($val->terlambat / $val->total) * 100;
            $val->persen_on_time = ($val->total == 0) ? 0 : ($val->on_time / $val->total) * 100;
        }
        return $rekapPengajuanBulanan;
    }
    
    public function dataRekap() {
        return HitungPerizinanBulanan::all();
    }
    
    public function rekapPengajuanBulanan(Request $request) {
        
        //nantinya pake parameter id bidang
        $data = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $tahun = ($request->input('tahunbidang')) ? $request->input('tahunbidang') : \Carbon\Carbon::now()->year;
        if ($request->input('bidang')) {
            $rekapPengajuanBulanan = DB::select(DB::raw("select nama_flow, bulan, tahun, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total_pengajuan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time, SUM(hitung_on_proses) as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN group by HITUNG_PERIZINAN_BULANAN.bulan, HITUNG_PERIZINAN_BULANAN.tahun, nama_flow HAVING nama_flow = '" . $request->input('bidang') . "' AND tahun = '" . $tahun . "'"));
        } 
        else {
            $rekapPengajuanBulanan = DB::select(DB::raw("select nama_flow, bulan, tahun, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total_pengajuan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time, SUM(hitung_on_proses) as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE id_m_jenis_perizinan IS NULL AND id_m_instansi IS NULL AND id_m_flow_terintegrasi BETWEEN 1 AND 5 group by HITUNG_PERIZINAN_BULANAN.bulan, HITUNG_PERIZINAN_BULANAN.tahun, nama_flow HAVING tahun = '" . $tahun . "'"));
        }
        foreach ($rekapPengajuanBulanan as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
        }
        
        $list_bidang = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $result = array('rekap' => $rekapPengajuanBulanan, 'bidang' => $list_bidang);
        return $result;
    }
    
    public function rekapPengajuanKementrian(Request $request) {
        $data = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->get();
        $tahun = $request->input('tahun');
        $title = "Dirjen Pajak";
        
        $query = "";
        if ($request->input('kementrian')) {
            $title = $request->input('kementrian');
            $query = "select nama_instansi, bulan, tahun, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total_pengajuan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_proses) as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN ";
            if (empty($tahun)) {
                $query.= " group by HITUNG_PERIZINAN_BULANAN.bulan, HITUNG_PERIZINAN_BULANAN.tahun, nama_instansi HAVING nama_instansi = '" . $request->input('kementrian') . "'";
            } 
            else {
                $query.= " AND HITUNG_PERIZINAN_BULANAN.tahun = '" . $tahun . "' group by HITUNG_PERIZINAN_BULANAN.bulan, HITUNG_PERIZINAN_BULANAN.tahun, nama_instansi HAVING nama_instansi = '" . $request->input('kementrian') . "'";
            }
            $rekapPengajuanBulanan = DB::select(DB::raw($query));
        } 
        else {
            $query = "select nama_instansi as nama_direktorat, bulan, tahun, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total_pengajuan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_selesai) as selesai, SUM(HITUNG_PERIZINAN_BULANAN.hitung_terlambat) as terlambat, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as on_time, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_proses) as on_proses from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where id_m_instansi IS NOT NULL AND id_m_jenis_perizinan IS NULL and id_m_flow_terintegrasi IS NULL AND nama_instansi = '" . $title . "' group by HITUNG_PERIZINAN_BULANAN.bulan, HITUNG_PERIZINAN_BULANAN.tahun, nama_instansi ";
            if (!empty($tahun)) {
                $query.= " HAVING tahun = '" . $tahun . "'";
            }
            $rekapPengajuanBulanan = DB::select(DB::raw($query));
        }
        foreach ($rekapPengajuanBulanan as $val) {
            $val->nama_bulan = getNamaBulan($val->bulan);
        }
        
        $list_kementrian = DB::table(TABLESPACE.'M_INSTANSI')->get();
        $result = array('rekap' => $rekapPengajuanBulanan, 'kementrian' => $list_kementrian);
        
        return $result;
    }
}
