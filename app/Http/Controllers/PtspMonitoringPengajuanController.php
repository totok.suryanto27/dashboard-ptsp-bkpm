<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Navigator;
use DB; 

use App\UserLog;
use App\UserHits;

class PtspMonitoringPengajuanController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */  

    public function __construct()
    {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();

        if(\Auth::user()){
			// 'Admin1' <= getUserId()
            $hits = new UserHits;
            $hits->page = 'Monitoring Pengajuan'; 
            // $hits->id_user_log = getUserId();
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();

            UserLog::where('id_m_user', getUserId())
                        ->where('id', UserLog::orderBy('id', 'desc')->first()->id)
                        ->update([
                            'hits'=>UserHits::where(['id_user_log' => getUserId(), 'status'=>1])->count()
                        ]);
        }
    }
    public function index(Request $request)
    {
        Navigator::setActive(url('monitoring-pengajuan-ptsp'));
        // return \Auth::user();
        $query_perizinan_harian = DB::raw("select NVL(sum(HITUNG_SELESAI),0) as total_selesai, NVL(sum(HITUNG_PENGAJUAN),0) as total_pengajuan from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI='".\Carbon\Carbon::now()."'");
        $result_total_harian = DB::select($query_perizinan_harian);
        $total_selesai = $result_total_harian[count($result_total_harian)-1]->total_selesai;
        $total_izin_masuk = $result_total_harian[count($result_total_harian)-1]->total_pengajuan;
        //all 
       // $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW'));
       // $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_DIREKTORAT ORDER BY NAMA_DIREKTORAT'));
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;
        
        $time = \Carbon\Carbon::now();
        
        $list_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(singkatan)'));
        //if (getInstansi()) {
            //$list_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp where id_ptsp = '.getInstansi().'ORDER BY UPPER(NAMA_PTSP)'));
        //} else {
            //$list_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(NAMA_PTSP)'));
        //}
        
        //all
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));
        
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        
        //request harian ~ default
        $query_perizinan_harian = DB::raw("select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') group by rollup (hari) ");
        $result_total_harian = DB::select($query_perizinan_harian);
        if (count($result_total_harian) > 0) {
            $total_selesai_ = $result_total_harian[count($result_total_harian) - 1]->total_selesai;
        } 
        else {
            $total_selesai_ = 0;
        }
        
        //request perizinan on proses
        $query_perizinan_on_proses = DB::raw("select sum(total_izin) as total_izin from ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES group by rollup (total_izin) ");
        $result_total_on_proses = DB::select($query_perizinan_on_proses);
        if(count($result_total_on_proses)){
            $total_izin_on_proses = $result_total_on_proses[count($result_total_on_proses) - 1]->total_izin;    
        }else{
            $total_izin_on_proses = 0;
        }
        
        
        //request perizinan harian on time
        $query_perizinan_harian_on_time = DB::raw("select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') group by rollup (hari) ");
        $result_total_harian_on_time = DB::select($query_perizinan_harian_on_time);
        if(count($result_total_harian_on_time)){
            $total_on_time = $result_total_harian_on_time[count($result_total_harian_on_time) - 1]->total_on_time;    
        }else{
            $total_on_time = 0;
        }
        
        
        //request perizinan harian delay
        $query_perizinan_harian_delay = DB::raw("select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') group by rollup (hari) ");
        $result_total_harian_terlambat = DB::select($query_perizinan_harian_delay);
        if(count($result_total_harian_terlambat) > 0){
            $total_terlambat = $result_total_harian_terlambat[count($result_total_harian_terlambat) - 1]->total_terlambat;    
        }else{
            $total_terlambat = 0;
        }
        
        
        //request perizinan on proses delay
        $query_perizinan_on_proses_delay = DB::raw("select sum(delay) as delay from ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES group by rollup (delay) ");
        $result_total_on_proses_delay = DB::select($query_perizinan_on_proses_delay);

        if(count($result_total_on_proses_delay) > 0){
            $total_izin_proses_delay = $result_total_on_proses_delay[count($result_total_on_proses_delay) - 1]->delay;    
        }else{
            $total_izin_proses_delay = 0;
        }

        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;
        
        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );

       // var_dump($result_total_harian);
        return view('pages.monitoring-pengajuan-ptsp',compact('current_month','current_year','list_bulan','date_current','list_instansi', 'list_kementrian','list_ptsp', 'total_selesai','total_izin_masuk', 'total_selesai_', 'total_izin_proses_delay', 'total_terlambat', 'total_on_time', 'total_izin_on_proses', 'years', 'time'));
    }
    
     public function get_data_ptsp(Request $request){
        
        if ($request->input('id_ptsp') == '-') {
            $filer="";
        }else{
			$filer = "where id_ptsp=".$request->input('id_ptsp');
		}
		
		//$kementrian = " AND a.ID_M_INSTANSI IS NOT NULL"; 

        //if (getInstansi()) {
            //$list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        //} else {
            //$list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        //}

        $query = "select id_ptsp, singkatan, sum(total_izin) as total
					from (
					select p.id_ptsp, p.singkatan, count(distinct pm.id_permohonan) as total_izin 
					from ppm.t_ppm_permohonan pm, spiparam.tp_ptsp p
					where p.id_ptsp = pm.id_ptsp
					and p.id_ptsp not in (1, 99999)
					and no_ppm not like '  %' and no_ppm is not null
					group by p.id_ptsp, p.singkatan
					union
					select p.id_ptsp, p.singkatan, count(distinct ip.id_permohonan) as total_izin 
					from ipspipise.t_ip_permohonan ip, spiparam.tp_ptsp p
					where p.id_ptsp = ip.id_ptsp
					and p.id_ptsp not in (1, 99999)
					and no_ip not like '  %' and no_ip is not null
					group by p.id_ptsp, p.singkatan
					union
					select p.id_ptsp, p.singkatan, count(distinct iu.id_permohonan) as total_izin 
					from iutspipise.t_iut_permohonan iu, spiparam.tp_ptsp p
					where p.id_ptsp = iu.id_ptsp
					and p.id_ptsp not in (1, 99999)
					and no_iut not like '  %' and no_iut is not null
					group by p.id_ptsp, p.singkatan
					union 
					select p.id_ptsp, p.singkatan, count(distinct pab.id_permohonan) as total_izin 
					from pabean.t_pab_permohonan pab, spiparam.tp_ptsp p
					where p.id_ptsp = pab.id_ptsp
					and p.id_ptsp not in (1, 99999)
					and no_pabean not like '  %' and no_pabean is not null
					group by p.id_ptsp, p.singkatan
					)
					".$filer."
					group by id_ptsp, singkatan
					order by id_ptsp,total desc";
		 
          $data_pengajuan = DB::select(DB::raw($query));
          $data = array();

          foreach ($data_pengajuan as $k) { 
		     $x['id_ptsp']= $k->id_ptsp;
             $x['singkatan']= $k->singkatan;
             $x['total']= $k->total; 
			array_push($data, $x);
          };
		  
          return($data);
    }
  
    public function initData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        $current_date = $current_day."-".$current_month."-".$current_year;
        if ($request->input('tanggal')) {
            $current_date =  $request->input('tanggal');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE TRUNC(HARI) = TO_DATE('".$current_date."', 'DD-MM-YYYY')";
        if ($request->input('bidang')) {
            $QUERY = $QUERY." AND ID_M_FLOW_TERINTEGRASI = ".$request->input('bidang');
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));


        echo json_encode($data);
    }
    
    public function monthlyData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        if ($request->input('bulan')) {
            $current_month = $request->input('bulan');
        }

        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE BULAN=".$current_month." AND TAHUN = ".$current_year;

        if ($request->input('bidang')) {
            $QUERY = $QUERY." AND ID_M_FLOW_TERINTEGRASI = ".$request->input('bidang');
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }

    
    public function yearlyData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN WHERE  TAHUN = ".$current_year;

        if ($request->input('bidang')) {
            $QUERY = $QUERY." AND ID_M_FLOW_TERINTEGRASI = ".$request->input('bidang');
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }
    
    public function periodicData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        
        $to_date = $current_day."-".$current_month."-".$current_year;
        $from_date = $current_day."-".$current_month."-".$current_year;
        
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date'); 
        }
        
        if($request->input('to_date')){
			$to_date = $request->input('to_date');
		}

        $query = "select ID_INSTANSI,INSTANSI, count(NO_IZIN) jml
						from SPIPISE.MV_UTAMA_SPM_PTSP
					   where tgl_pengajuan  > to_date('".$from_date."','DD-MM-YYYY')
						 AND tgl_selesai < to_date('".$to_date."','DD-MM-YYYY')
					GROUP by ID_INSTANSI,INSTANSI";
        
         $data_performansi = DB::select(DB::raw($query));
         $data = array();
		 
          foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->id_instansi;
             $x['nama_instansi']= $k->instansi;
             $x['total_pengajuan']= $k->jml;  
             
			array_push($data, $x);
          };
		  
          return($data);
    }

    
    public function periodicIzin(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        
        $to_date = $current_day."-".$current_month."-".$current_year;
        $from_date = $current_day."-".$current_month."-".$current_year;
        
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date'); 
        }
        
        if($request->input('to_date')){
			$to_date = $request->input('to_date');
		}
		
		$filter = "";
		
        if($request->input('instansi')){ 
			$id_instansi = DB::select(DB::raw('select ID_PTSP as ID from SPIPARAM.TP_PTSP WHERE UPPER(singkatan) like upper(\'%'.$request->input('instansi').'%\')')); 
			$filter = "AND ID_INSTANSI = ".$id_instansi[0]->id;
		}
		
        if($request->input('id_instansi')){  
			$filter = "AND ID_INSTANSI = ".$request->input('id_instansi');
		}
 
        $query = "SELECT ID_INSTANSI,
						  INSTANSI,
						  kode_jenis,
						  kode_jenis||' - '||nama_jenis nama_jenis,
						  COUNT(no_izin) jml
					FROM SPIPISE.MV_UTAMA_SPM_PTSP
			       WHERE tgl_pengajuan > to_date('".$from_date."','DD-MM-YYYY')
					 AND tgl_selesai   < to_date('".$to_date."','DD-MM-YYYY')
					 ".$filter."
				GROUP BY ID_INSTANSI, INSTANSI,  kode_jenis,nama_jenis";
        
         $data_performansi = DB::select(DB::raw($query));
         $data = array();
		 
          foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->kode_jenis;
             $x['nama_instansi']= $k->nama_jenis;
             $x['total_pengajuan']= $k->jml;  
             
			array_push($data, $x);
          };
		  
          return($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function initDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        $current_date = $current_day."-".$current_month."-".$current_year;
        if ($request->input('tanggal')) {
            $current_date =  $request->input('tanggal');
        }
 
        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE TRUNC(HARI) = TO_DATE('".$current_date."', 'DD-MM-YYYY')";
        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));


        echo json_encode($data);
    }
    
    public function monthlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        if ($request->input('bulan')) {
            $current_month = $request->input('bulan');
        }
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE BULAN=".$current_month." AND TAHUN = ".$current_year;

        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }

    
    public function yearlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN WHERE  TAHUN = ".$current_year;

        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL" ;
        }
        $data = DB::select(DB::raw($QUERY));

        echo json_encode($data);
    }
    
    public function periodicDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $current_day = \Carbon\Carbon::now()->day;
        $to_date = $current_day."-".$current_month."-".$current_year;
        $from_date = $current_day."-".$current_month."-".$current_year;
        if ($request->input('tanggal')) {
            $from_date =  $request->input('tanggal');
            $to_date =  $request->input('tanggal_sd');
        }
        $QUERY = "SELECT 
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE TRUNC(HARI) BETWEEN TO_DATE('".$from_date."', 'DD-MM-YYYY')
         AND TO_DATE('".$to_date."','DD-MM-YYYY')";
        if ($request->input('kementrian')) {
            $QUERY = $QUERY." AND ID_M_INSTANSI = ".$request->input('kementrian')." AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }else {
            $QUERY = $QUERY." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";
        }
        $data = DB::select(DB::raw($QUERY));


        echo json_encode($data);
    }

}
