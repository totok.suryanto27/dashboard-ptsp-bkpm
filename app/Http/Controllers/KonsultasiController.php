<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use Excel;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Instansi;
use App\Model\Konsultasi;
use App\Model\KonsultasiKategori;

class KonsultasiController extends Controller
{
    private $columns = ['id_konsultasi','id_m_instansi','id_m_direktorat','nama_instansi','nama_direktorat','nama_tamu','nama_perusahaan','alamat','telepon','email', 'judul_konsultasi','id_konsul_cat', 'pertanyaan', 'jawaban', 'nama_petugas','create_tms'];
    private $columns_searchable = ['nama_instansi','nama_direktorat','nama_tamu','nama_perusahaan','alamat','telepon','email', 'judul_konsultasi', 'pertanyaan', 'jawaban', 'nama_petugas'];

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index_iframe','list_iframe','show_iframe','edit_iframe','update_iframe','destroy_iframe','export_iframe','search_iframe']]);
        $this->middleware('role:konsultasi,monitoring,kl',['except'=>['index_iframe','list_iframe','show_iframe','edit_iframe','update_iframe','destroy_iframe','export_iframe','search_iframe']]);
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('konsultasi.index');
    }
    
    public function index_iframe()
    {
        return view('konsultasi.index_iframe');
    }
    
    public function list_iframe()
    {
        global $headerless;
        $headerless = true;
        return $this->index();
    }
    
    public function show_iframe($id)
    {
        global $headerless;
        $headerless = true;
        return $this->show($id);
    }
    
    public function edit_iframe($id)
    {
        global $headerless;
        $headerless = true;
        return $this->edit($id);
    }
    
    public function update_iframe(Request $request, $id)
    {
        global $headerless;
        $headerless = true;
        return $this->update($request, $id);
    }
    
    public function destroy_iframe($id)
    {
        global $headerless;
        $headerless = true;
        return $this->destroy($request, $id);
    }
    
    public function export_iframe(Request $request)
    {
        global $headerless;
        $headerless = true;
        return $this->export($request);
    }
    
    public function search_iframe(Request $request)
    {
        global $headerless;
        $headerless = true;
        return $this->search($request);
    }

    public function search(Request $request) {
        global $headerless;
        $params = $request->get('params',[]);
        $search = $request->get('search',null);
        $search_value = isset($search['value'])?$search['value']:false;
        $query = Konsultasi::with('kategori')->select($this->columns);
        foreach ($params as $param => $search) {
            if ($search == '') continue;
            switch($param) {
                case 'tanggal_mulai':
                    $query = $query->where("create_tms",'>=',$search);
                    break;
                case 'tanggal_selesai':
                    $query = $query->where("create_tms",'<=',date('Y-m-d',strtotime("+1 day",strtotime($search))));
                    break;
                case 'biodata':
                    $biodata = Konsultasi::where('description','like',"%$search%")->lists('id_konsultasi');
                    $query = $query->whereIn("id_konsultasi",$biodata);
                    break;
                case 'biodata_content':
                    $biodata = Konsultasi::whereRaw("match(content) against ('$search' in boolean mode)")->lists('id_konsultasi');
                    $query = $query->whereIn("id_konsultasi",$biodata);
                    break;
                case 'id_m_instansi':
                    $query = $query->where($param,$search);
                    break;
                default:
                    $query = $query->where("upper($param)",'like',strtoupper("%$search%"));
                    break;
            }
        }
        if ($search_value) {
            $searchable = $this->columns_searchable;
            $query = $query->where(function($q) use ($searchable,$search_value) {
                foreach ($searchable as $col) {
                    $q = $q->orWhere("upper($col)",'like',strtoupper("%$search_value%"));
                }
            });
        }
        $konsul = $query;
        return Datatables::of($konsul)
            ->addColumn('actions',isset($headerless)?'
                        <a href="{{ url( \'konsultasi/SOIAJ32423kmcsxpal\',$id_konsultasi )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a><br>
                        <a href="{{ url( \'konsultasi/co32i4uPSkdxcgh43H\',$id_konsultasi )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a><br>
                ':'
                        <a href="{{ url( \'konsultasi/read\',$id_konsultasi )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a><br>
                        <a href="{{ url( \'konsultasi/edit\',$id_konsultasi )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a><br>
                        <a href="{{ url( \'konsultasi/delete\',$id_konsultasi ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a><br>
                        ')
            ->addColumn('biodata', '
<span class="align-keterangan1">Instansi</span>: <span class="align-isi1">{{$nama_instansi}}</span><br>'.
//<span class="align-keterangan1">Direktorat</span>: <span class="align-isi1">{{$nama_direktorat}}</span><br>
'<span class="align-keterangan1">Nama Tamu</span>: <span class="align-isi1">{{$nama_tamu}}</span><br>
<span class="align-keterangan1">Perusahaan</span>: <span class="align-isi1">{{$nama_perusahaan}}</span>')
            ->addColumn('contact', '
<span class="align-keterangan2">Alamat</span>: <span class="align-isi2">{{$alamat}}</span><br>
<span class="align-keterangan2">Telepon</span>: <span class="align-isi2">{{$telepon}}</span><br>
<span class="align-keterangan2">Email</span>: <span class="align-isi2">{{$email}}</span>')
            ->addColumn('konsultasi', '
<span class="align-keterangan3">Kategori</span>: <span class="align-isi3">{{$kategori->nama_konsul_cat}}</span><br>
<span class="align-keterangan3">Judul</span>: <span class="align-isi3">{{$judul_konsultasi}}</span><br>
<span class="align-keterangan3">Pertanyaan</span>: <span class="align-isi3">{{$pertanyaan}}</span><br>
<span class="align-keterangan3">Jawaban</span>: <span class="align-isi3">{{$jawaban}}</span><br>
<span class="align-keterangan3">Petugas</span>: <span class="align-isi3">{{$nama_petugas}}</span>')
            ->removeColumn('nama_instansi')
            ->removeColumn('nama_direktorat')
            ->removeColumn('nama_sender')
            ->removeColumn('nama_tamu')
            ->removeColumn('nama_perusahaan')
            ->removeColumn('alamat')
            ->removeColumn('telepon')
            ->removeColumn('email')
            ->removeColumn('id_konsul_cat')
            ->removeColumn('kategori')
            ->removeColumn('judul_konsultasi')
            ->removeColumn('pertanyaan')
            ->removeColumn('jawaban')
            ->removeColumn('nama_petugas')
            ->make(true);
    }

    public function export(Request $request) {
        global $headerless;
        $params = $request->all();
        $search = $request->get('search',null);
        $search_value = isset($search['value'])?$search['value']:false;
        $query = Konsultasi::with('kategori')->select($this->columns)->orderBy('create_tms');
        foreach ($params as $param => $search) {
            if ($search == '') continue;
            switch($param) {
                case 'tanggal_mulai':
                    $query = $query->where("create_tms",'>=',$search);
                    break;
                case 'tanggal_selesai':
                    $query = $query->where("create_tms",'<=',date('Y-m-d',strtotime("+1 day",strtotime($search))));
                    break;
                case 'biodata':
                    $biodata = Konsultasi::where('description','like',"%$search%")->lists('id_konsultasi');
                    $query = $query->whereIn("id_konsultasi",$biodata);
                    break;
                case 'biodata_content':
                    $biodata = Konsultasi::whereRaw("match(content) against ('$search' in boolean mode)")->lists('id_konsultasi');
                    $query = $query->whereIn("id_konsultasi",$biodata);
                    break;
                default:
                    $query = $query->where("upper($param)",'like',strtoupper("%$search%"));
                    break;
            }
        }
        if ($search_value) {
            $searchable = $this->columns_searchable;
            $query = $query->where(function($q) use ($searchable,$search_value) {
                foreach ($searchable as $col) {
                    $q = $q->orWhere("upper($col)",'like',strtoupper("%$search_value%"));
                }
            });
        }
        Excel::create("rekap konsultasi", function($excel) use ($query) {
            $excel->sheet('Konsultasi', function($sheet) use ($query) {
                $sheet->setFontFamily('Tahoma');
                $sheet->setFontSize(12);
                $sheet->setWidth(array(
                    'A'     =>  6.86  * 1,
                    'B'     =>  16 * 1,
                    'C'     =>  16 * 1,
                    'D'     =>  16 * 1,
                    'E'     =>  16 * 1,
                    'F'     =>  16 * 1,
                    'G'     =>  16 * 1,
                    'H'     =>  16 * 1,
                    'I'     =>  16 * 1,
                    'J'     =>  16 * 1,
                    'K'     =>  16 * 1,
                    'L'     =>  16 * 1,
                    'M'     =>  16 * 1,
                    'N'     =>  16 * 1,
                ));
                $sheet->mergeCells('A1:N1');
                $sheet->cell('A1', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(18);
                });
                $sheet->row(1, ['Konsultasi PTSP Pusat']);
                $sheet->row(4, [
                    'NO',
                    'TANGGAL',
                    'WAKTU',
                    'INSTANSI',
                    'KATEGORI',
                    'NAMA TAMU',
                    'PERUSAHAAN',
                    'ALAMAT',
                    'TELEPON',
                    'EMAIL',
                    'JUDUL',
                    'PERTANYAAN',
                    'JAWABAN',
                    'PETUGAS'
                ]);

                $sheet->cells('A4:N4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setValignment('center');
                });
                
                $rownum = 5;
                $i = 1;
                foreach ($query->get() as $konsultasi) {
                    $sheet->row($rownum,[
                        $i,
                        str_replace("&nbsp;"," ",getFullDate($konsultasi->create_tms)),
                        date("H:i \W\I\B",strtotime($konsultasi->create_tms)),
                        $konsultasi->nama_instansi,
                        $konsultasi->kategori->nama_konsul_cat,
                        $konsultasi->nama_tamu,
                        $konsultasi->nama_perusahaan,
                        $konsultasi->alamat,
                        $konsultasi->telepon,
                        $konsultasi->email,
                        $konsultasi->judul_konsultasi,
                        $konsultasi->pertanyaan,
                        $konsultasi->jawaban,
                        $konsultasi->nama_petugas
                    ]);
                    $sheet->setHeight($rownum, -1);
                    $rownum++;
                    $i++;
                }
                $sheet->cells('A5:N'. $sheet->getHighestRow(), function($cells) {
                    $cells->setValignment('top');
                });
                $sheet->getStyle('A5:N' . $sheet->getHighestRow())
                    ->getAlignment()->setWrapText(true);
                $sheet->setBorder('A4:N'.($rownum-1), 'thin');
                $sheet->setFreeze('A5');
                $sheet->setPageMargin(0.25);
                $sheet->getSheetView()->setZoomScale(80);
                $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');
    }
    
    public function getData(){
        $konsultasi = Konsultasi::all();
        
        return Datatables::of($konsultasi)
        ->addColumn('actions','
                        <a href="{{ url( \'konsultasi\read\',$id_konsultasi )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a>
                        <a href="{{ url( \'konsultasi\edit\',$id_konsultasi )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a>
                        <a href="{{ url( \'konsultasi\delete\',$id_konsultasi ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('konsultasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $konsultasi = New Konsultasi;
        $instansi = Instansi::find($req->id_m_instansi);
        $nama_instansi = $instansi?$instansi->nama_instansi:'';
        $konsultasi->id_m_instansi = $req->id_m_instansi;
        $konsultasi->nama_instansi = $nama_instansi;
        $konsultasi->nama_tamu = $req->nama_tamu;
        $konsultasi->nama_perusahaan = $req->nama_perusahaan;
        $konsultasi->alamat = $req->alamat;
        $konsultasi->telepon = $req->telepon;
        $konsultasi->email = $req->email;
        $konsultasi->judul_konsultasi = $req->judul_konsultasi;
        $konsultasi->pertanyaan = $req->pertanyaan;
        $konsultasi->jawaban = $req->jawaban;
        $konsultasi->nama_petugas = $req->get('nama_petugas',getUserName());
        $konsultasi->save();

        return redirect('konsultasi/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        global $headerless;
        $konsultasi = Konsultasi::find($id);
        return view('konsultasi.view', compact('konsultasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        global $headerless;
        $konsultasi = Konsultasi::find($id);
        return view('konsultasi.edit', compact('konsultasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        global $headerless;
        $konsultasi = Konsultasi::find($id);
        $konsultasi->id_konsul_cat = $req->id_konsul_cat;
        //if (preg_match('/^[0-9][0-9]{0,11}$/',$konsultasi->id_konsul_cat)) {
        //    if (KonsultasiKategori::find($konsultasi->id_konsul_cat)) {
        //        $newkategori = false;
        //    } else {
        //        $newkategori = true;
        //        $namakategori= $konsultasi->id_konsul_cat;
        //    }
        //} else {
        //    $newkategori = true;
        //    $namakategori= $konsultasi->id_konsul_cat;
        //}
        //if ($newkategori) {
        //    $kat = KonsultasiKategori::where('upper(nama_konsul_cat)',strtoupper($namakategori))->first();
        //    if ($kat) {
        //        $konsultasi->id_konsul_cat = $kat->id_konsul_cat;
        //    } else {
        //        $kategori = KonsultasiKategori::create([
        //            'nama_konsul_cat' => $namakategori
        //        ]);
        //        $konsultasi->id_konsul_cat = $kategori->id_konsul_cat;
        //    }
        //}
        $instansi = Instansi::find($req->id_m_instansi);
        $nama_instansi = $instansi?$instansi->nama_instansi:'';
        $konsultasi->id_m_instansi = $req->id_m_instansi;
        $konsultasi->nama_instansi = $nama_instansi;
        $konsultasi->nama_tamu = $req->nama_tamu;
        $konsultasi->nama_perusahaan = $req->nama_perusahaan;
        $konsultasi->alamat = $req->alamat;
        $konsultasi->telepon = $req->telepon;
        $konsultasi->email = $req->email;
        $konsultasi->judul_konsultasi = $req->judul_konsultasi;
        $konsultasi->pertanyaan = $req->pertanyaan;
        $konsultasi->jawaban = $req->jawaban;
        $konsultasi->save();

        return isset($headerless)?redirect('konsultasi/cpw23ME923pdksaopd?id_m_instansi='.$req->id_m_instansi):redirect('konsultasi/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        global $headerless;
        Konsultasi::find($id)->delete();
        return redirect()->back();
    }
    
    public function getKategori(Request $req)
    {
        $data = [
            'results'=>[]
        ];
        $results = KonsultasiKategori::where('upper(nama_konsul_cat)','like',"%".strtoupper($req->get('term'))."%")
                        ->skip(($req->get('page',1)-1)*10)->take(10)->get(['nama_konsul_cat','id_konsul_cat']);
        foreach ($results as $res) {
            $data['results'][] = [
                'id' => $res->id_konsul_cat,
                'text' => $res->nama_konsul_cat,
            ];
        }
        return response()->json($data);
    }
}
