<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HitungPerizinanBulanan;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits; 

class MonitoringPengajuanController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */ 
    

    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Monitoring Pengajuan';
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month; 
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 
    
    public function index(Request $request) {
        Navigator::setActive(url('monitoring-pengajuan'));
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year; 
        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;        
        
        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );


        
        return view('pages.monitoring-pengajuan', compact('list_izin', 'list_bulan', 'list_instansi','list_kementrian','current_month','current_year','date_current'));
    }
 
    public function yearlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
		
		$kementrian = " AND a.ID_M_INSTANSI IS NOT NULL"; 

        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }

        $query = "select a.ID_M_INSTANSI,b.NAMA_INSTANSI, NVL(sum(a.HITUNG_PENGAJUAN),0) as total_pengajuan 
				  from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN a,
					   ".TABLESPACE.".M_INSTANSI b
				  where a.ID_M_INSTANSI=b.ID_M_INSTANSI  
				    and a.TAHUN=".$current_year." ".$kementrian."
				  group by a.ID_M_INSTANSI,b.NAMA_INSTANSI
				  order by b.NAMA_INSTANSI";
		 
          $data_performansi_kementrian = DB::select(DB::raw($query));
          $data = array();

          foreach ($list_kementrian as $k) { 
		     $x['id_instansi']= $k->id_m_instansi;
             $x['nama_instansi']= $k->nama_instansi;
             $x['total_pengajuan']= 0; 
              foreach ($data_performansi_kementrian as $d) {
                  if ($k->nama_instansi == $d->nama_instansi) {
                      $x['total_pengajuan']= $d->total_pengajuan; 
                  }
              }
			array_push($data, $x);
          };
		  
          return($data);
    }


    public function monthlyDataKmntr(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }
        
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        
        $kementrian = " AND a.ID_M_INSTANSI IS NOT NULL"; 

        $query = "select a.ID_M_INSTANSI,b.NAMA_INSTANSI, NVL(sum(a.HITUNG_PENGAJUAN),0) as total_pengajuan 
				  from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN a,
					   ".TABLESPACE.".M_INSTANSI b
				  where a.ID_M_INSTANSI=b.ID_M_INSTANSI 
                    and a.TAHUN = ".$current_year." AND a.BULAN = ".$current_month." ".$kementrian."
				  group by a.ID_M_INSTANSI,b.NAMA_INSTANSI
				  order by b.NAMA_INSTANSI";
          
          $data_performansi_kementrian = DB::select(DB::raw($query));
           $data = array();
          foreach ($list_kementrian as $k) {
			 $x['id_instansi']= $k->id_m_instansi;
             $x['nama_instansi']= $k->nama_instansi;
             $x['total_pengajuan']= 0; 
              foreach ($data_performansi_kementrian as $d) {
                  if ($k->nama_instansi == $d->nama_instansi) {
                      $x['total_pengajuan']= $d->total_pengajuan; 
                  }
              }
              array_push($data, $x);
          }
          return($data);
    }
   

    public function periodicDataKmntr(Request $request){
		$date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;   
        
        $from_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];
        $to_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];

        //$kementrian = " AND b.NAMA_INSTANSI IS NOT NULL";
     
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date');
            //$kementrian .= " AND a.HARI > to_date('".$request->input('from_date')."','DD-MM-YYYY')";
        }

        if ($request->input('to_date')) {
            $to_date1 =  $request->input('to_date');
            //$kementrian .= " AND a.HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
        
        //if (getInstansi()) {
            //$list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        //} else {
            //$list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        //}
         
		$query = "select ID_INSTANSI,INSTANSI, count(NO_IZIN) jml
						from SPIPISE.MV_UTAMA_SPM_KL
					   where tgl_pengajuan  > to_date('".$from_date."','DD-MM-YYYY')
						 AND tgl_selesai < to_date('".$to_date."','DD-MM-YYYY')
					GROUP by ID_INSTANSI,INSTANSI";
        
         $data_performansi = DB::select(DB::raw($query));
         $data = array();
		 
          foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->id_instansi;
             $x['nama_instansi']= $k->instansi;
             $x['total_pengajuan']= $k->jml;  
             
			array_push($data, $x);
          };
		  
          return($data);
    }
   
    public function yearlyData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
        
		$izin = " AND a.ID_M_JENIS_PERIZINAN IS NOT NULL";
        if ($request->input('id_instansi') != '') {
          $izin .= " AND a.ID_M_INSTANSI = ".$request->input('id_instansi');
        }

        if ($request->input('instansi') != '') {
			$list_kementrian = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$request->input('instansi').'%\')')); 
 
			$izin .= " AND a.ID_M_INSTANSI = ".$list_kementrian[0]->id;
        }

        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));

        $query = "SELECT a.ID_M_JENIS_PERIZINAN,
						 a.ID_M_INSTANSI,
						 b.nama_jenis_perizinan,
						 a.HITUNG_PENGAJUAN total_pengajuan
				  FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN a,
					   ".TABLESPACE.".m_jenis_perizinan b 
                  WHERE a.ID_M_JENIS_PERIZINAN=b.ID_M_JENIS_PERIZINAN  
                  AND a.HITUNG_PENGAJUAN > 0 AND TAHUN = ".$current_year." ".$izin." order by a.HITUNG_PENGAJUAN desc";
      
          $data_performansi_izin = DB::select(DB::raw($query));
            
          $data = array();
           
          
          foreach ($data_performansi_izin as $d) { 
			  $x['id_instansi']= $d->id_m_instansi;
			  $x['nama_jenis_perizinan']= $d->nama_jenis_perizinan;
			  $x['total_pengajuan']= $d->total_pengajuan;  
			  
			  array_push($data, $x);
		  }
          return($data);
    }


    public function monthlyData(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }
        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));
        
        $izin = " AND a.ID_M_JENIS_PERIZINAN IS NOT NULL";
        if ($request->input('id_instansi') != '') {
          $izin .= " AND a.ID_M_INSTANSI = ".$request->input('id_instansi');
        }

        if ($request->input('instansi') != '') {
			$list_kementrian = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$request->input('instansi').'%\')')); 
 
			$izin .= " AND a.ID_M_INSTANSI = ".$list_kementrian[0]->id;
        }

        $query = "SELECT a.ID_M_JENIS_PERIZINAN,
						 a.ID_M_INSTANSI,
						 b.nama_jenis_perizinan,
						 a.HITUNG_PENGAJUAN total_pengajuan
				  FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN a,
					   ".TABLESPACE.".m_jenis_perizinan b  
                  WHERE a.ID_M_JENIS_PERIZINAN=b.ID_M_JENIS_PERIZINAN 
                  AND a.HITUNG_PENGAJUAN > 0 
                  AND TAHUN = ".$current_year." AND BULAN = ".$current_month." ".$izin;
          $data_performansi_izin = DB::select(DB::raw($query));
           $data = array();
   
           foreach ($data_performansi_izin as $d) { 
			  $x['id_instansi']= $d->id_m_instansi;
			  $x['nama_jenis_perizinan']= $d->nama_jenis_perizinan;
			  $x['total_pengajuan']= $d->total_pengajuan;  
			  
			  array_push($data, $x);
		  }
          
          //foreach ($list_izin as $k) {
             //if ($recent_name == $k->nama_jenis_perizinan) continue;
             //$recent_name = $k->nama_jenis_perizinan;
             //$x['nama_jenis_perizinan']= $k->nama_jenis_perizinan;
             //$x['total_pengajuan']= 0; 
              //foreach ($data_performansi_izin as $d) {
                  //if ($k->id_m_jenis_perizinan == $d->id_m_jenis_perizinan) {
                      //$x['total_pengajuan']= $d->total_pengajuan; 
                      //break;
                  //}
              //}
              //array_push($data, $x);
          //}
          
          return($data);
    }
    
    public function periodicData(Request $request){
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;   
        
        $from_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];
        $to_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];

		$izin = " ";
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date');
            //$izin .= " AND a.HARI > to_date('".$request->input('from_date')."','DD-MM-YYYY')";
        }

        if ($request->input('to_date')) {
            $to_date =  $request->input('to_date');
            //$izin .= " AND a.HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
         
		$filter = "";
		
        if($request->input('instansi')){  
			$inst=$request->input('instansi');
			if($inst=='BKPM PUSAT'){
				$id_inst = 1;
			}else{ 
				$id_instansi = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$inst.'%\')')); 
				$id_inst = $id_instansi[0]->id;
				}
			$filter = "AND ID_INSTANSI = ".$id_inst;
		}
		
        if($request->input('id_instansi')){
			$inst=$request->input('id_instansi');
			if($inst==20){
				$inst=1;
			}
			$filter = "AND ID_INSTANSI = ".$inst;
		}
 
        $query = "SELECT ID_INSTANSI,
						  INSTANSI,
						  kode_jenis,
						  kode_jenis||' - '||nama_jenis nama_jenis,
						  COUNT(no_izin) jml
					FROM SPIPISE.MV_UTAMA_SPM_KL
			       WHERE tgl_pengajuan > to_date('".$from_date."','DD-MM-YYYY')
					 AND tgl_selesai   < to_date('".$to_date."','DD-MM-YYYY')
					 ".$filter."
				GROUP BY ID_INSTANSI, INSTANSI,kode_jenis,nama_jenis";
        
         $data_performansi = DB::select(DB::raw($query));
         $data = array();
		 //echo $query;
		 //exit;
          foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->kode_jenis;
             $x['nama_instansi']= $k->nama_jenis;
             $x['total_pengajuan']= $k->jml;  
             
			array_push($data, $x);
          };
		  
          return($data); 
    }
   
}
