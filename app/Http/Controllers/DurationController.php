<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Navigator;
use DB;

use App\UserLog; 
use App\UserHits;

class DurationController extends Controller
{


function index(){
		$user = \Auth::user();
        $userId = $user->id_m_user;
        $userLog = new UserLog;
        $userLog = UserLog::where('id_m_user', '=', $userId)->orderBy('created_at', 'desc')->first();
        $durasi = hitung_durasi($userLog->jam_login,$userLog->updated_at);
        $userLog->hitung_menit =  $durasi['menit'];
        $userLog->hitung_jam =  $durasi['jam'];
        $userLog->hitung_detik =  $durasi['detik'];
        $userLog->update();

        return json_encode($userLog);
}

}

 ?>