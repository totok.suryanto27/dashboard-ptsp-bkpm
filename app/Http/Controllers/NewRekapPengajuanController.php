<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HitungPerizinanBulanan;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits;

class NewRekapPengajuanController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    // public function __construct()
    // {
    //     \App::setLocale(getLang());
    //     $this->middleware('guest');
    // }
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Rekap Pengajuan';
            $hits->id_user_log = getUserId();
            // $hits->id_user_log = 'Admin1';
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 
    
    public function index(Request $request) {
        Navigator::setActive(url('rekap-pengajuan'));
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        
        return view('pages.rekap-pengajuan-v2', compact( 'list_kementrian','current_month','current_year'));
    }

    public function newRekapKementrian(Request $request){
        $time = \Carbon\Carbon::now();
        $currentYear = $time->year;
        $id_m_instansi = "IS NULL AND NAMA_FLOW IS NULL";

        if ($request->input('tahun')) {
            $currentYear = $request->input('tahun');
        }

        if($request->input('id_instansi')){
            $id_m_instansi = " = ".$request->input('id_instansi')." AND NAMA_FLOW IS NULL";
        }

        $query = "SELECT bulan,tahun,
        HITUNG_PERIZINAN_BULANAN.hitung_pengajuan as total,
        HITUNG_PERIZINAN_BULANAN.hitung_selesai as selesai,
        HITUNG_PERIZINAN_BULANAN.hitung_terlambat as terlambat,
        HITUNG_PERIZINAN_BULANAN.hitung_on_time as on_time,
        HITUNG_PERIZINAN_BULANAN.hitung_on_proses as on_proses 
        from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE ID_M_INSTANSI ".$id_m_instansi."  
        AND tahun = ".$currentYear;



        $bulan[0]['bulan'] = '1';
        $bulan[0]['nama_bulan'] = 'Januari';
        $bulan[0]['kosong'] ="1";
        
        $bulan[1]['bulan'] = '2';
        $bulan[1]['nama_bulan'] = 'Februari';
        $bulan[1]['kosong'] ="1";
        
        $bulan[2]['bulan'] = '3';
        $bulan[2]['nama_bulan'] = 'Maret';
        $bulan[2]['kosong'] ="1";
        
        $bulan[3]['bulan'] = '4';
        $bulan[3]['nama_bulan'] = 'April';
        $bulan[3]['kosong'] ="1";
        
        $bulan[4]['bulan'] = '5';
        $bulan[4]['nama_bulan'] = 'Mei';
        $bulan[4]['kosong'] ="1";
        
        $bulan[5]['bulan'] = '6';
        $bulan[5]['nama_bulan'] = 'Juni';
        $bulan[5]['kosong'] ="1";
        
        $bulan[6]['bulan'] = '7';
        $bulan[6]['nama_bulan'] = 'Juli';
        $bulan[6]['kosong'] ="1";
        
        $bulan[7]['bulan'] = '8';
        $bulan[7]['nama_bulan'] = 'Agustus';
        $bulan[7]['kosong'] ="1";
        
        $bulan[8]['bulan'] = '9';
        $bulan[8]['nama_bulan'] = 'September';
        $bulan[8]['kosong'] ="1";
        
        $bulan[9]['bulan'] = '10';
        $bulan[9]['nama_bulan'] = 'Oktober';
        $bulan[9]['kosong'] ="1";
        
        $bulan[10]['bulan'] = '11';
        $bulan[10]['nama_bulan'] = 'November';
        $bulan[10]['kosong'] ="1";
        
        $bulan[11]['bulan'] = '12';
        $bulan[11]['nama_bulan'] = 'Desember';
        $bulan[11]['kosong'] ="1";

        $rekaps = array();
        

        $rekap = DB::select(DB::raw($query));
        $temps = array();
        $i = 0; 
        foreach ($rekap as $val) {
            $bln = $val->bulan;
            $data['bulan'] = $bln;
            $data['tahun'] = $currentYear;
            $data['total'] = $val->total;
            $data['selesai'] = $val->selesai;
            $data['terlambat'] = $val->terlambat;
            $data['on_time'] = $val->on_time;
            $data['on_proses'] = $val->on_proses;
            $data['nama_bulan'] = $bulan[$bln-1]['nama_bulan'];
            $data['persenOnTime'] = 0;
            $data['persenDelay'] = 0;
            $bulan[$bln-1]['kosong'] = "0";
            array_push($temps, $data);
            $i++;
        }
        $x = 0;
        for ($j=0; $j < 12 ; $j++) { 
            if ($bulan[$j]['kosong'] == "1") {
                $data['bulan'] = $j+1;
                $data['tahun'] = $currentYear;
                $data['total'] = 0;
                $data['selesai'] = 0;
                $data['terlambat'] = 0;
                $data['on_time'] = 0;
                $data['on_proses'] = 0;
                $data['nama_bulan'] = $bulan[$j]['nama_bulan'];
                $data['persenOnTime'] = 0;
                $data['persenDelay'] = 0;
                $bulan[$j]['kosong'] = "0";
                array_push($rekaps, $data);
            }else{
                $data = $temps[$x];
                array_push($rekaps, $data);
                $x++;

            }
        }
        return($rekaps);
    }

    public function newRekapBidang(Request $request){
        
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        $QUERY_FLOW = "SELECT 
        NAMA_FLOW,ID_M_FLOW_TERINTEGRASI as id_selesai,
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN WHERE TAHUN = ".$current_year." 
        AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NOT NULL 
        AND ID_M_FLOW_TERINTEGRASI != 0
        GROUP BY NAMA_FLOW,ID_M_FLOW_TERINTEGRASI";
        $data_selesai = DB::select(DB::raw($QUERY_FLOW)); 

        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));

        $data = array();
        foreach ($list_instansi as $key) {
            $nama_flow = $key->nama_flow;
            $id = $key->id_m_flow_izin_terintegrasi;
            $total_pengajuan = 0;
            $total_selesai = 0;
            $total_terlambat = 0;
            $total_on_time = 0;

            foreach ($data_selesai as $s) {
                if ($s->id_selesai == $id) {
                    $total_pengajuan = $s->total_pengajuan;
                    $total_selesai = $s->total_selesai;
                    $total_terlambat = $s->total_terlambat;
                    $total_on_time = $s->total_on_time;

                }
            }

            $obj = array(
            'nama_flow' =>$nama_flow ,
            'id_bidang' =>$id,
            'total_pengajuan'=> $total_pengajuan,
            'total_selesai'=>$total_selesai,
            'total_terlambat'=>$total_terlambat,
            'total_on_time'=>$total_on_time );

            array_push($data, $obj);

        }

        return($data);
    }
 

    public function newRekapBidangBln(Request $request){
        
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }

        $QUERY_FLOW = "SELECT 
        NAMA_FLOW,ID_M_FLOW_TERINTEGRASI as id_selesai,
        NVL(SUM(HITUNG_PENGAJUAN),0)  AS total_pengajuan,
        NVL(SUM(HITUNG_SELESAI),0) AS total_selesai,
        NVL(SUM(HITUNG_TERLAMBAT),0) AS total_terlambat,
        NVL(SUM(HITUNG_ON_TIME),0) AS total_on_time
        FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE TAHUN = ".$current_year." AND BULAN = ".$current_month." 
        AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NOT NULL 
        AND ID_M_FLOW_TERINTEGRASI != 0
        GROUP BY NAMA_FLOW,ID_M_FLOW_TERINTEGRASI";
        $data_selesai = DB::select(DB::raw($QUERY_FLOW)); 

        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));

        $data = array();
        foreach ($list_instansi as $key) {
            $nama_flow = $key->nama_flow;
            $id = $key->id_m_flow_izin_terintegrasi;
            $total_pengajuan = 0;
            $total_selesai = 0;
            $total_terlambat = 0;
            $total_on_time = 0;

            foreach ($data_selesai as $s) {
                if ($s->id_selesai == $id) {
                    $total_pengajuan = $s->total_pengajuan;
                    $total_selesai = $s->total_selesai;
                    $total_terlambat = $s->total_terlambat;
                    $total_on_time = $s->total_on_time;

                }
            }

            $obj = array(
            'nama_flow' =>$nama_flow ,
            'id_bidang' =>$id,
            'total_pengajuan'=> $total_pengajuan,
            'total_selesai'=>$total_selesai,
            'total_terlambat'=>$total_terlambat,
            'total_on_time'=>$total_on_time );

            array_push($data, $obj);

        }

        return($data);
    }



    
   
}
