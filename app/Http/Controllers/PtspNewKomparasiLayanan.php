<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HitungPerizinanBulanan;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits; 

class PtspNewKomparasiLayanan extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */ 
    

    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Rekap Pengajuan PTSP';
            $hits->id_user_log = getUserId();
            // $hits->id_user_log = 'Admin1';
            $hits->bulan = \Carbon\Carbon::now()->month; 
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 
    
    public function index(Request $request) {
        Navigator::setActive(url('komparasi-layanan-ptsp'));
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year; 
        $list_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(singkatan)'));
        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));
        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI ORDER BY NAMA_FLOW '));
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;        
        
        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );


        
        return view('pages.komparasi-layanan-ptsp', compact('list_izin', 'list_ptsp', 'list_bulan', 'list_instansi','list_kementrian','current_month','current_year','date_current'));
    }

    public function get_monthly_table(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }

        $kementrian = " AND NAMA_PTSP IS NOT NULL";
        if ($request->input('id_instansi')) {
          $kementrian = " AND ID_PTSP = ".$request->input('id_instansi');
        }

        $query = "SELECT 
                  NAMA_INSTANSI,
                  HITUNG_PENGAJUAN as total_pengajuan,
                  HITUNG_SELESAI as total_selesai,
                  HITUNG_TERLAMBAT as total_terlambat,
                  HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN_PTSP 
                  WHERE TAHUN = ".$current_year." AND BULAN = ".$current_month." ".$kementrian;
          $data_performansi_kementrian = DB::select(DB::raw($query));
          echo json_encode($data_performansi_kementrian);
    }

    public function get_yearsly_performance(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
        
          $query = "SELECT 
                    HITUNG_PENGAJUAN as total_pengajuan,
                    HITUNG_SELESAI as total_selesai,
                    HITUNG_TERLAMBAT as total_terlambat,
                    HITUNG_ON_TIME as total_on_time
                    FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN_PTSP 
                    WHERE TAHUN = ".$current_year." AND ID_PTSP IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";

        if ($request->input('id_instansi')) {
            $id_instansi = $request->input('id_instansi');
            $query = " ";

        }
        $data_performansi_kementrian = DB::select(DB::raw($query));
        echo json_encode($data_performansi_kementrian);
    }
 
    public function get_years(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
		
		$ptsp = " ";
        if ($request->input('id_instansi')) {
          $ptsp = " AND ID_PTSP = ".$request->input('id_instansi');
        }
		 
		$listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(SINGKATAN)'));
        //if (getInstansi()) {
            //$listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        //} else {
            //$listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(SINGKATAN)'));
        //}

        $query = "SELECT 
                  nama_ptsp,
                  HITUNG_PENGAJUAN as total_pengajuan,
                  HITUNG_SELESAI as total_selesai,
                  HITUNG_TERLAMBAT as total_terlambat,
                  HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN_PTSP 
                  WHERE TAHUN = ".$current_year." ".$ptsp;
          $data_performansi_kementrian = DB::select(DB::raw($query));
          $data = array();
		
		foreach ($data_performansi_kementrian as $d) { 
			  $x['nama_instansi']= $d->nama_ptsp;
			  $x['total_pengajuan']= $d->total_pengajuan;
			  $x['total_selesai']= $d->total_selesai;
			  $x['total_terlambat']= $d->total_terlambat;
			  $x['total_on_time']= $d->total_on_time; 
		  array_push($data, $x);
		  }
          //foreach ($listp_ptsp as $k) {
             //$x['total_pengajuan']= 0;
             //$x['total_selesai']= 0;
             //$x['total_terlambat']= 0;
             //$x['total_on_time']= 0;
              //foreach ($data_performansi_kementrian as $d) {
                  //if ($k->singkatan == $d->nama_ptsp) {
					  //$x['nama_instansi']= $k->singkatan;
                      //$x['total_pengajuan']= $d->total_pengajuan;
                      //$x['total_selesai']= $d->total_selesai;
                      //$x['total_terlambat']= $d->total_terlambat;
                      //$x['total_on_time']= $d->total_on_time;
                      
                  //}
              //}
              //array_push($data, $x);
          //}
          return($data);
    }


    public function get_monthly(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }
        $listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(SINGKATAN)'));
        //if (getInstansi()) {
            //$listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        //} else {
            //$listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(SINGKATAN)'));
        //}
        $kementrian = " AND NAMA_PTSP IS NOT NULL";
        if ($request->input('id_instansi')) {
          $kementrian = " AND ID_PTSP = ".$request->input('id_instansi');
        }

        $query = "SELECT 
                  nama_ptsp,
                  HITUNG_PENGAJUAN as total_pengajuan,
                  HITUNG_SELESAI as total_selesai,
                  HITUNG_TERLAMBAT as total_terlambat,
                  HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN_PTSP 
                  WHERE TAHUN = ".$current_year." AND BULAN = ".$current_month." ".$kementrian;
          $data_performansi_kementrian = DB::select(DB::raw($query));
          
           $data = array();
           foreach ($data_performansi_kementrian as $d) { 
			  $x['nama_instansi']= $d->nama_ptsp;
			  $x['total_pengajuan']= $d->total_pengajuan;
			  $x['total_selesai']= $d->total_selesai;
			  $x['total_terlambat']= $d->total_terlambat;
			  $x['total_on_time']= $d->total_on_time; 
		  array_push($data, $x);
		  }
          //foreach ($listp_ptsp as $k) {
             //$x['nama_instansi']= $k->singkatan;
             //$x['total_pengajuan']= 0;
             //$x['total_selesai']= 0;
             //$x['total_terlambat']= 0;
             //$x['total_on_time']= 0;
              //foreach ($data_performansi_kementrian as $d) {
                  //if ($k->singkatan == $d->singkatan) {
                      //$x['total_pengajuan']= $d->total_pengajuan;
                      //$x['total_selesai']= $d->total_selesai;
                      //$x['total_terlambat']= $d->total_terlambat;
                      //$x['total_on_time']= $d->total_on_time;
                      
                  //}
              //}
              //array_push($data, $x);
          //}
          return($data);
    }
   

    public function get_periodic(Request $request){
		$date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;   
        
        $from_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];
        $to_date= $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];

        $kementrian = " AND NAMA_PTSP IS NOT NULL";
     
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date');
            //$kementrian .= " AND HARI > to_date('".$request->input('from_date')."','DD-MM-YYYY')";
        }

        if ($request->input('to_date')) {
            $to_date =  $request->input('to_date');
            //$kementrian .= " AND HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
        $listp_ptsp = DB::select(DB::raw('select * from SPIPARAM.tp_ptsp ORDER BY UPPER(SINGKATAN)'));
       
        $filter="";
        if ($request->input('id_instansi')) { 
			$filter = " WHERE x.ID_INSTANSI=".$request->input('id_instansi');
			if($request->input('ket')){
				$filter .= " AND x.KET='".$request->input('ket')."'";
			}
        }
         
		$query = "SELECT ID_INSTANSI,
						   INSTANSI, 
						   spipise.F_DURASI_PELAYANAN_PTSP_ONTIME(ID_INSTANSI,to_date('".$from_date."','DD-MM-YYYY'),to_date('".$to_date."','DD-MM-YYYY')) ontime,
						   spipise.F_DURASI_PELAYANAN_PTSP_DELAY(ID_INSTANSI,to_date('".$from_date."','DD-MM-YYYY'),to_date('".$to_date."','DD-MM-YYYY')) delay
						FROM SPIPISE.MV_UTAMA_SPM_PTSP
						WHERE tgl_pengajuan > to_date('".$from_date."','DD-MM-YYYY')
						AND tgl_selesai  < to_date('".$to_date."','DD-MM-YYYY') 
						GROUP BY ID_INSTANSI, INSTANSI".$filter; 
						
			$data_performansi = DB::select(DB::raw($query));
            $data = array();
           
          foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->id_instansi;
             $x['nama_instansi']= $k->instansi;
		     $x['ontime']= $k->ontime;
             $x['delay']= $k->delay; 
             
			array_push($data, $x);
          };
          
          return($data);
    }
   
    public function get_izin_years(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        $izin = " AND a.ID_M_JENIS_PERIZINAN IS NOT NULL";
        if ($request->input('id_instansi') != '') {
          $izin .= " AND a.ID_PTSP = ".$request->input('id_instansi');
        }

        if ($request->input('instansi') != '') {
			//$list_kementrian = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$request->input('instansi').'%\')')); 
			$listp_ptsp = DB::select(DB::raw('select ID_PTSP as ID from SPIPARAM.tp_ptsp WHERE UPPER(SINGKATAN) like upper(\'%'.$request->input('instansi').'%\')')); 
 
			$izin .= " AND a.ID_PTSP = ".$listp_ptsp[0]->id;
        }

        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));

        $query = "SELECT 
                  a.ID_M_JENIS_PERIZINAN,
				  b.nama_jenis_perizinan,
                  a.HITUNG_PENGAJUAN as total_pengajuan,
                  a.HITUNG_SELESAI as total_selesai,
                  a.HITUNG_TERLAMBAT as total_terlambat,
                  a.HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN_PTSP a,
					   ".TABLESPACE.".m_jenis_perizinan b 
                  WHERE a.ID_M_JENIS_PERIZINAN=b.ID_M_JENIS_PERIZINAN  AND a.TAHUN = ".$current_year." ".$izin;
          $data_performansi_izin = DB::select(DB::raw($query));
          $data = array();
            
           //echo $query;
		   //exit;
          $recent_name = "";
		  
		   if(count($data_performansi_izin)==0){
				  $x['nama_jenis_perizinan']= '';
				  $x['total_pengajuan']= 0;
				  $x['total_selesai']= 0;
				  $x['total_terlambat']= 0;
				  $x['total_on_time']= 0;
				  
		  }else{
			  foreach ($data_performansi_izin as $d) { 
				  $x['nama_jenis_perizinan']= $d->nama_jenis_perizinan;
				  $x['total_pengajuan']= $d->total_pengajuan;
				  $x['total_selesai']= $d->total_selesai;
				  $x['total_terlambat']= $d->total_terlambat;
				  $x['total_on_time']= $d->total_on_time;
				  break; 
			  }
			  array_push($data, $x); 
		  }
		   
          return($data);
    }


    public function get_izin_monthly(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }
        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));
        $izin = " AND a.ID_M_JENIS_PERIZINAN IS NOT NULL";
        if ($request->input('id_instansi') != '') {
          $izin .= " AND a.ID_PTSP = ".$request->input('id_instansi');
        }

        if ($request->input('instansi') != '') {
			//$list_kementrian = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$request->input('instansi').'%\')')); 
			$listp_ptsp = DB::select(DB::raw('select ID_PTSP as ID from SPIPARAM.tp_ptsp WHERE UPPER(SINGKATAN) like upper(\'%'.$request->input('instansi').'%\')')); 
 
			$izin .= " AND a.ID_PTSP = ".$listp_ptsp[0]->id;
        }

        $query = "SELECT 
                  a.ID_M_JENIS_PERIZINAN,
				  b.nama_jenis_perizinan,
                  a.HITUNG_PENGAJUAN as total_pengajuan,
                  a.HITUNG_SELESAI as total_selesai,
                  a.HITUNG_TERLAMBAT as total_terlambat,
                  a.HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN_PTSP a,
					   ".TABLESPACE.".m_jenis_perizinan b 
                  WHERE a.ID_M_JENIS_PERIZINAN=b.ID_M_JENIS_PERIZINAN  AND a.TAHUN = ".$current_year." AND a.BULAN = ".$current_month." ".$izin;
          $data_performansi_izin = DB::select(DB::raw($query));
           $data = array();
          
           
           if(count($data_performansi_izin)==0){
				  $x['nama_jenis_perizinan']= '';
				  $x['total_pengajuan']= 0;
				  $x['total_selesai']= 0;
				  $x['total_terlambat']= 0;
				  $x['total_on_time']= 0;
				  
		  }else{
			  foreach ($data_performansi_izin as $d) { 
				  $x['nama_jenis_perizinan']= $d->nama_jenis_perizinan;
				  $x['total_pengajuan']= $d->total_pengajuan;
				  $x['total_selesai']= $d->total_selesai;
				  $x['total_terlambat']= $d->total_terlambat;
				  $x['total_on_time']= $d->total_on_time;
				  break; 
			  }
			  array_push($data, $x); 
		  }
            
          return($data);
    }
    
    public function get_izin_periodik(Request $request){
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;   
        
        $from_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];
        $to_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];

		$izin = " ";
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date');
            $izin .= " AND a.HARI > to_date('".$request->input('from_date')."','DD-MM-YYYY')";
        }

        if ($request->input('to_date')) {
            $to_date =  $request->input('to_date');
            $izin .= " AND a.HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
        $list_izin = DB::select(DB::raw('select * from '.TABLESPACE.'.M_jenis_perizinan ORDER BY UPPER(NAMA_jenis_perizinan)'));
        
        if ($request->input('id_instansi') != '') {
          $izin .= " AND a.ID_PTSP = ".$request->input('id_instansi');
        }

        $filter="";
        if ($request->input('instansi')) {
			//if($request->input('instansi')=='BKPM PUSAT'){
				//$id_insta = 1;
			//}
			//else{
				////$list_kementrian = DB::select(DB::raw('select ID_M_INSTANSI as ID from '.TABLESPACE.'.M_INSTANSI WHERE UPPER(NAMA_INSTANSI) like upper(\'%'.$request->input('instansi').'%\')')); 
				//$listp_ptsp = DB::select(DB::raw('select ID_PTSP as ID from SPIPARAM.tp_ptsp WHERE UPPER(SINGKATAN) like upper(\'%'.$request->input('instansi').'%\')')); 
				//$id_insta = $listp_ptsp[0]->id;
				//} 
				$listp_ptsp = DB::select(DB::raw('select ID_PTSP as ID from SPIPARAM.tp_ptsp WHERE UPPER(SINGKATAN) like upper(\'%'.$request->input('instansi').'%\')')); 
				$id_insta = $listp_ptsp[0]->id;
			$filter = " WHERE x.ID_INSTANSI=".$id_insta;
				if($request->input('status')){
					$filter .= " AND x.KET='".$request->input('status')."'";
				}
        }

        //if ($request->input('status') != '') { 
			//$status = $request->input('status'); 
        //}

        $query = "select * from (select ID_INSTANSI,INSTANSI,kode_jenis,kode_jenis||'-'||nama_jenis nama_jenis,
										COUNT(no_izin) jml,'Delay' ket
									FROM SPIPISE.MV_UTAMA_SPM_PTSP
								   where to_char(SPIPISE.ADDDAYS_SKIP_HOLIDAYS(tgl_pengajuan,(tgl_selesai-tgl_pengajuan))-tgl_selesai) > sop
									 and tgl_pengajuan  > to_date('".$from_date."','DD-MM-YYYY')
									 AND tgl_selesai < to_date('".$to_date."','DD-MM-YYYY')
								group by ID_INSTANSI,INSTANSI,kode_jenis,nama_jenis
								UNION
								 select ID_INSTANSI,INSTANSI,kode_jenis,kode_jenis||'-'||nama_jenis nama_jenis,
										COUNT(no_izin) jml,'On Time' ket
									FROM SPIPISE.MV_UTAMA_SPM_PTSP
								   where to_char(SPIPISE.ADDDAYS_SKIP_HOLIDAYS(tgl_pengajuan,(tgl_selesai-tgl_pengajuan))-tgl_selesai) < sop
									 and tgl_pengajuan  > to_date('".$from_date."','DD-MM-YYYY')
									 AND tgl_selesai < to_date('".$to_date."','DD-MM-YYYY')
								group by ID_INSTANSI,INSTANSI,kode_jenis,nama_jenis)x ".$filter;
                  
          $data_performansi_izin = DB::select(DB::raw($query));
          $data = array();
          
          //echo $query;
          //exit;
          
          foreach ($data_performansi_izin as $k) { 
			 $x['nama_jenis_perizinan']= $k->nama_jenis;
		     $x['id_instansi']= $k->id_instansi;
             $x['nama_instansi']= $k->instansi;
             $x['ket']= $k->ket;
             $x['jml']= $k->jml;  
             
			array_push($data, $x);
          };
          
          //if(count($data_performansi_izin)==0){
				  //$x['nama_jenis_perizinan']= '';
				  //$x['total_pengajuan']= 0;
				  //$x['total_selesai']= 0;
				  //$x['total_terlambat']= 0;
				  //$x['total_on_time']= 0;
				  
		  //}else{
			  //foreach ($data_performansi_izin as $d) { 
				  //$x['nama_jenis_perizinan']= $d->nama_jenis_perizinan;
				  //$x['total_pengajuan']= $d->total_pengajuan;
				  //$x['total_selesai']= $d->total_selesai;
				  //$x['total_terlambat']= $d->total_terlambat;
				  //$x['total_on_time']= $d->total_on_time;
				  //break; 
			  //}
			  //array_push($data, $x); 
		  //}
           
          return($data);
    }
    
    public function get_grupizin_periodik(Request $request){
		$date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;   
        
        $from_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];
        $to_date = $date_current['current_day']."-".$date_current['current_month']."-".$date_current['current_year'];

        $kementrian = " AND NAMA_INSTANSI IS NOT NULL";
     
        if ($request->input('from_date')) {
            $from_date =  $request->input('from_date');
            //$kementrian .= " AND HARI > to_date('".$request->input('from_date')."','DD-MM-YYYY')";
        }

        if ($request->input('to_date')) {
            $to_date1 =  $request->input('to_date');
            //$kementrian .= " AND HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
		
		$filter="";
        if ($request->input('id_instansi')) {
            $id_instansi =  $request->input('id_instansi');
            $filter = " AND ID_INSTANSI=".$id_instansi;
            //$kementrian .= " AND HARI < to_date('".$request->input('to_date')."','DD-MM-YYYY')";
        }
        
           
		$query = "SELECT ID_INSTANSI,INSTANSI,kode_jenis,kode_jenis||'-'||nama_jenis nama_jenis, 
						   spipise.F_DURASI_LAYANAN_PTSP_IZIN_OT(ID_INSTANSI,kode_jenis,to_date('".$from_date."','DD-MM-YYYY'),to_date('".$to_date."','DD-MM-YYYY')) ontime,
						   spipise.F_DURASI_LAYANAN_PTSP_IZIN_DLY(ID_INSTANSI,kode_jenis,to_date('".$from_date."','DD-MM-YYYY'),to_date('".$to_date."','DD-MM-YYYY')) delay
						FROM SPIPISE.MV_UTAMA_SPM_PTSP
						WHERE tgl_pengajuan > to_date('".$from_date."','DD-MM-YYYY')
						AND tgl_selesai  < to_date('".$to_date."','DD-MM-YYYY')".$filter."
						GROUP BY ID_INSTANSI, INSTANSI, kode_jenis, nama_jenis"; 
					
           $data_performansi = DB::select(DB::raw($query));
           $data = array();
            
           foreach ($data_performansi as $k) { 
		     $x['id_instansi']= $k->kode_jenis;
             $x['nama_instansi']= $k->nama_jenis;
		     $x['ontime']= $k->ontime;
             $x['delay']= $k->delay; 
             
			array_push($data, $x);
          };
		  
          return($data);
           
    }
   
}
