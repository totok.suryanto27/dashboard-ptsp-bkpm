<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Navigator;

use DB;

use App\UserLog;
use App\UserHits;

class KomparasiLayananController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Komparasi Layanan';
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    }
    
    public function index() { 
        Navigator::setActive(url('komparasi-layanan'));
        $list_izin = DB::table(TABLESPACE.'.M_jenis_perizinan')->get();
        $list_instansi = DB::table(TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI')->get();
        if (getInstansi()) {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->where('id_m_instansi',getInstansi())->orderBy('nama_instansi', 'asc')->get();
        } else {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        }
        $time = \Carbon\Carbon::now();
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        
        // $presentasiOnTime = $this->getPresentasiOnTime();
        // $presentaseOnTimeBidang = $this->getPresentasiOnTimeBidang();
        
        //harian
        $presentasiOnTimeIzin = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->distinct()
            ->select(
                DB::raw('SUM(hitung_pengajuan) as hitung_pengajuan'), 
                DB::raw('SUM(hitung_selesai) as hitung_selesai'),
                DB::raw('SUM(hitung_terlambat) as hitung_terlambat'),
                DB::raw('SUM(hitung_on_time) as hitung_on_time'),
                DB::raw('SUM(hitung_on_proses) as hitung_on_proses'),
                'id_m_jenis_perizinan'
                )
            ->whereNull('id_m_flow_terintegrasi')->whereNotNull('id_m_jenis_perizinan')
            ->whereNull('id_m_instansi')

            ->groupBy('id_m_jenis_perizinan')
            ->orderBy('id_m_jenis_perizinan', 'asc')
            ->get();


        $presentasiOnTimeKementrian = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->distinct()
            ->select(
                DB::raw('SUM(hitung_pengajuan) as hitung_pengajuan'), 
                DB::raw('SUM(hitung_selesai) as hitung_selesai'),
                DB::raw('SUM(hitung_terlambat) as hitung_terlambat'),
                DB::raw('SUM(hitung_on_time) as hitung_on_time'),
                DB::raw('SUM(hitung_on_proses) as hitung_on_proses'),
                'nama_instansi'
                )
            ->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_instansi')

            ->groupBy('nama_instansi')
            ->orderBy('nama_instansi', 'asc')
            ->get();


        $komparasiLayananBidang = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('id_m_flow_terintegrasi', '!=', 0)
            ->groupBy('nama_flow')

            ->get();
        // return $this->getPresentasiOnTime();
        $obj = instanceStdClass();
        $result = [];
        
        foreach ($presentasiOnTimeIzin as $val) {
            $izin = DB::table(TABLESPACE.'.M_jenis_perizinan')->where('id_m_jenis_perizinan',$val->id_m_jenis_perizinan)->first();
            $val->nama_jenis_perizinan = $izin ? $izin->nama_jenis_perizinan : '-';
            $val->persen_on_time = ($val->hitung_on_time == 0) ? 0 : ($val->hitung_on_time / $val->hitung_selesai) * 100;
            $val->persen_delay = ($val->hitung_terlambat == 0) ? 0 : ($val->hitung_terlambat / $val->hitung_selesai) * 100;
        }
        
        foreach ($presentasiOnTimeKementrian as $val) {
            $val->nama_direktorat = $val->nama_instansi;
            $val->persen_on_time = ($val->hitung_on_time == 0) ? 0 : ($val->hitung_on_time / $val->hitung_selesai) * 100;
            $val->persen_delay = ($val->hitung_terlambat == 0) ? 0 : ($val->hitung_terlambat / $val->hitung_selesai) * 100;
        }

        $i = 1;
        foreach($komparasiLayananBidang as $val){
            $val->no = $i;
            $val->performansi = intval(($val->total_selesai == 0) ? 0 : ($val->total_on_time / $val->total_selesai) * 100) . "%";
            $val->performansi_ = ($val->total_selesai == 0) ? 0 : ($val->total_on_time / $val->total_selesai) * 100;
            $i++;
        }
        // DB::enableQueryLog();
        // return $presentasiOnTimeKementrian;
        return view('pages.komparasi-layanan', compact('list_instansi', 'presentasiOnTimeKementrian', 'list_kementrian', 'years', 'komparasiLayananBidang', 'list_izin', 'presentasiOnTimeIzin'));
    }
    
    public function restKomparasiLayananKementrian(Request $request) {
        Navigator::setActive(url('komparasi-layanan'));
        $list_instansi = DB::table(TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI')->get();
        if (getInstansi()) {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->where('id_m_instansi',getInstansi())->orderBy('nama_instansi', 'asc')->get();
        } else {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        }
        $idInstansi = 'Kementerian Pertanian';
        $hari = \Carbon\Carbon::now();
        
        
        //default
        $presentasiOnTimeKementrian = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->distinct()
            ->select(
                DB::raw('SUM(hitung_pengajuan) as hitung_pengajuan'), 
                DB::raw('SUM(hitung_selesai) as hitung_selesai'),
                DB::raw('SUM(hitung_terlambat) as hitung_terlambat'),
                DB::raw('SUM(hitung_on_time) as hitung_on_time'),
                DB::raw('SUM(hitung_on_proses) as hitung_on_proses'),
                'nama_instansi'
                )
            ->whereNotNull('id_m_instansi')
            ->whereNull('id_m_flow_terintegrasi')
            ->whereNull('id_m_jenis_perizinan')

            ->groupBy('nama_instansi')
            ->orderBy('nama_instansi', 'asc')
            // ->having('nama_instansi', '=', $idInstansi)
            ->get();
        if ($request->input('kementrian')) {
            $idInstansi = $request->input('kementrian');
            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->distinct()
            ->select(
                DB::raw('SUM(hitung_pengajuan) as hitung_pengajuan'), 
                DB::raw('SUM(hitung_selesai) as hitung_selesai'),
                DB::raw('SUM(hitung_terlambat) as hitung_terlambat'),
                DB::raw('SUM(hitung_on_time) as hitung_on_time'),
                DB::raw('SUM(hitung_on_proses) as hitung_on_proses'),
                'nama_instansi'
                )
            ->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_instansi')

            ->groupBy('nama_instansi')
            ->orderBy('nama_instansi', 'asc')
            ->having('nama_instansi', '=', $idInstansi)
            ->get();
        }
        
        //harian
        if ($request->input('kementrian') && $request->input('hari')) {
            $hari = $request->input('hari');
            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('nama_instansi', '=', $idInstansi)->where('hari', '=', date('Y-m-d', strtotime($hari)))->get();
        }

        //bulan
        if ($request->input('kementrian') && $request->input('bulan')) {
            $bulan = $request->input('bulan');

            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('nama_instansi', '=', $idInstansi)->where('bulan', '=', $bulan)->get();
        }

        //bulan + tahun
        if ($request->input('kementrian') && $request->input('bulan') && $request->input('tahun')) {
            $bulan = $request->input('bulan');
            $tahun = $request->input('tahun');
            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('nama_instansi', '=', $idInstansi)->where('bulan', '=', $bulan)->where('tahun', '=', $tahun)->get();
        }

        //tahun
        if ($request->input('kementrian') && $request->input('tahun')) {
            $tahun = $request->input('tahun');
            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'HITUNG_PERIZINAN_TAHUNAN')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('nama_instansi', '=', $idInstansi)->where('tahun', '=', $tahun)->get();
        }

        //periode 
        if($request->input('kementrian') && $request->input('t1') && $request->input('t2')){
            $t1 = $request->input('t1');
            $t2 = $request->input('t2');
            $presentasiOnTimeKementrian = DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('nama_instansi', '=', $idInstansi)
             ->whereBetween('hari', [date('Y-m-d', strtotime($t1)), date('Y-m-d', strtotime($t2))])->get();
        }
        
        // return $this->getPresentasiOnTime();
        $obj = instanceStdClass();
        $result = [];
        
        $i = 1;
        foreach ($presentasiOnTimeKementrian as $val) {
            $val->nama_direktorat = $val->nama_instansi;
            $val->persen_on_time = ($val->hitung_on_time == 0) ? 0 : ($val->hitung_on_time / $val->hitung_selesai) * 100;
            $val->persen_delay = ($val->hitung_terlambat == 0) ? 0 : ($val->hitung_terlambat / $val->hitung_selesai) * 100;
            $val->no = $i;
            $val->persen = intval(($val->hitung_on_time == 0) ? 0 : ($val->hitung_on_time / $val->hitung_selesai) * 100) . " %"; 
            $i++;
        }
        
        DB::enableQueryLog();

        return $presentasiOnTimeKementrian;
    }

    public function restKomparasiLayananBidang(Request $request) {
        Navigator::setActive(url('komparasi-layanan'));
        
        //default
        $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('id_m_flow_terintegrasi', '!=', 0)
            ->groupBy('nama_flow')
            ->get();
        if($request->input('tanggal')){
            $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('hari', '=', date('Y-m-d', strtotime($request->input('tanggal'))))
            ->groupBy('nama_flow')
            ->get();
        }

        if($request->input('bulan')){
            $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('bulan', '=', $request->input('bulan'))
            ->groupBy('nama_flow')
            ->get();
        }

        if($request->input('bulan') && $request->input('tahun')){
            $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_BULANAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('bulan', '=', $request->input('bulan'))
            ->where('tahun', '=', $request->input('tahun'))
            ->groupBy('nama_flow')
            ->get();
        }

        if($request->input('filter') && $request->input('tahun')){
            $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_TAHUNAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->where('tahun', '=', $request->input('tahun'))
            ->groupBy('nama_flow')
            ->get();
        }

        if($request->input('t1') && $request->input('t2')){
            $komparasiLayananBidang = DB::table(TABLESPACE.'HITUNG_PERIZINAN_HARIAN')
            ->select('nama_flow',DB::raw('SUM(hitung_pengajuan) as total_pengajuan'),
                DB::raw('SUM(hitung_on_time) as total_on_time'),
                DB::raw('SUM(hitung_terlambat) as total_terlambat'),
                DB::raw('SUM(hitung_selesai) as total_selesai'),
                DB::raw('SUM(hitung_on_proses) as total_on_proses'))
            ->whereNull('id_m_instansi')
            ->whereNull('id_m_jenis_perizinan')
            ->whereNotNull('id_m_flow_terintegrasi')
            ->whereBetween('hari', [date('Y-m-d', strtotime($request->input('t1'))), date('Y-m-d', strtotime($request->input('t2')))])
            ->groupBy('nama_flow')
            ->get();
        }

        $i = 1;
        foreach($komparasiLayananBidang as $val){
            $val->no = $i;
            $val->performansi = intval(($val->total_selesai == 0) ? 0 : ($val->total_on_time / $val->total_selesai) * 100) . "%";
            $val->performansi_ = ($val->total_selesai == 0) ? 0 : ($val->total_on_time / $val->total_selesai) * 100;
            $i++;
        }
        
        return $komparasiLayananBidang;
    }
    
    public function getPresentasiOnTime() {
        $query = "

        ";
        $prs = DB::select(DB::raw($query));
        
        return $prs;
    }
    
    public function getPresentasiOnTimeBidang() {
        $query = "SELECT DISTINCT bulan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as total_ontime, SUM(HITUNG_PERIZINAN_BULANAN.hitung_pengajuan) as total_pengajuan,  M_FLOW_IZIN_TERINTEGRASI.nama_flow FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN, ".TABLESPACE.".M_FLOW_IZIN_TERINTEGRASI WHERE M_FLOW_IZIN_TERINTEGRASI.id_m_flow_izin_terintegrasi = HITUNG_PERIZINAN_BULANAN.id_m_flow_terintegrasi GROUP BY HITUNG_PERIZINAN_BULANAN.id_m_instansi, M_FLOW_IZIN_TERINTEGRASI.nama_flow, HITUNG_PERIZINAN_BULANAN.tahun, HITUNG_PERIZINAN_BULANAN.bulan HAVING HITUNG_PERIZINAN_BULANAN.tahun = '" . \Carbon\Carbon::now()->year . "'";
        $list_bidang = DB::table(TABLESPACE.'M_FLOW_IZIN_TERINTEGRASI')->get();
        $prs = DB::select(DB::raw($query));
        
        $result = array('bidang' => $list_bidang, 'prs' => $prs);
        
        return $result;
    }
    
    // public function getPresentasiOnTime(){
    //     $query = "SELECT DISTINCT bulan, SUM(HITUNG_PERIZINAN_BULANAN.hitung_on_time) as total_ontime,  M_INSTANSI.nama_instansi FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN, ".TABLESPACE.".M_INSTANSI WHERE M_INSTANSI.id_m_instansi = HITUNG_PERIZINAN_BULANAN.id_m_instansi GROUP BY HITUNG_PERIZINAN_BULANAN.id_m_instansi, M_INSTANSI.nama_instansi, HITUNG_PERIZINAN_BULANAN.tahun, HITUNG_PERIZINAN_BULANAN.bulan HAVING HITUNG_PERIZINAN_BULANAN.tahun = '".\Carbon\Carbon::now()->year."'";
    //     $prs = DB::select(
    //         DB::raw($query)
    //     );
    
    //     $januari = array();
    //     $februari = array();
    //     $maret = array();
    //     $april = array();
    //     $mei = array();
    //     $juni = array();
    //     $juli = array();
    //     $agustus = array();
    //     $september = array();
    //     $oktober = array();
    //     $november = array();
    //     $desember = array();
    
    //     foreach ($prs as $val) {
    //         switch ($val->bulan) {
    //             case 1:
    //                 array_push($januari, $val);
    //                 break;
    //             case 2:
    //                 array_push($februari, $val);
    //                 break;
    //             case 3:
    //                 array_push($maret, $val);
    //                 break;
    //             case 4:
    //                 array_push($april, $val);
    //                 break;
    //             case 5:
    //                 array_push($mei, $val);
    //                 break;
    //             case 6:
    //                 array_push($juni, $val);
    //                 break;
    //             case 7:
    //                 array_push($juli, $val);
    //                 break;
    //             case 8:
    //                 array_push($agustus, $val);
    //                 break;
    //             case 9:
    //                 array_push($september, $val);
    //                 break;
    //             case 10:
    //                 array_push($oktober, $val);
    //                 break;
    //             case 11:
    //                 array_push($november, $val);
    //                 break;
    //             case 12:
    //                 array_push($desember, $val);
    //                 break;
    //         }
    //     }
    
    //     $series = array(
    //         'januari' => $januari,
    //         'februari' => $februari,
    //         'maret' => $maret,
    //         'april' => $april,
    //         'mei' => $mei,
    //         'juni' => $juni,
    //         'juli' => $juli,
    //         'agustus' => $agustus,
    //         'september' => $september,
    //         'oktober' => $oktober,
    //         'november' => $november,
    //         'desember' => $desember,
    //     );
    
    //     return $series;
    
    //     // return $prs;
    // }
    
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
        //
        
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
        //
        
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
        //
        
        
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        //
        
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        //
        
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        //
        
        
    }
}
