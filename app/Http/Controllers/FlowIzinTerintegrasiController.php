<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\FlowIzinTerintegrasi;
use App\Model\FlowDetailIzin;
use App\Model\JenisPerizinan;
use App\Model\TrackingRegisterDetail;

class FlowIzinTerintegrasiController extends Controller
{
    private $columns = [
    'nama_flowizinterintegrasi',
    'abbrv',
    'isdelated',
    'isbko',
    'filename',
    'filename_eng',
    'nama_flowizinterintegrasi_eng'
    ]
    ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('flowizinterintegrasi.index');
    }


    public function getData(){
        $flowizinterintegrasi = FlowIzinTerintegrasi::all();
        
        return Datatables::of($flowizinterintegrasi)
        ->addColumn('actions','
                        <a href="{{ url( \'flowizinterintegrasi\read\',$id_m_flow_izin_terintegrasi )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a>
                        <a href="{{ url( \'flowizinterintegrasi\edit\',$id_m_flow_izin_terintegrasi )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a>
                        <a href="{{ url( \'flowizinterintegrasi\editdetail\',$id_m_flow_izin_terintegrasi )}}"><i class="fa fa-edit"></i>&nbsp;Ubah&nbsp;detail</a>
                        <a href="{{ url( \'flowizinterintegrasi\delete\',$id_m_flow_izin_terintegrasi ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('flowizinterintegrasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        
        $flowizinterintegrasi = New FlowIzinTerintegrasi;
        $flowizinterintegrasi->no_sop = $req->no_sop;
        $flowizinterintegrasi->nama_flow = $req->nama_flow;
        $flowizinterintegrasi->keterangan = $req->keterangan;
        $flowizinterintegrasi->id_m_bidang_usaha = $req->nama_bidang_usaha;
        $flowizinterintegrasi->save();
        $id = $flowizinterintegrasi->id_m_flow_izin_terintegrasi;
        $i = 1;
        foreach($req->all() as $key=>$value){
              if("jenis_perizinan" == substr($key,0,15)){
                    $izin = new FlowDetailIzin;
                    $izin->id_m_flow_izin_terintegrasi = $id;
                    $izin->id_m_jenis_perizinan = $value;
                    $jenis_perizinan = JenisPerizinan::where('id_m_jenis_perizinan',$value)->first();
                    $izin->sla_izin = $jenis_perizinan->sop;
                    $izin->urutan = $i++;
                    $izin->save();
              }
        }
        

        return redirect('flowizinterintegrasi/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $flow_izin_terintegrasi = FlowIzinTerintegrasi::find($id);
        return view('flowizinterintegrasi.view', compact('flow_izin_terintegrasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $flow_izin_terintegrasi = FlowIzinTerintegrasi::find($id);
        return view('flowizinterintegrasi.edit', compact('flow_izin_terintegrasi'));
    }

    public function editDetail($id)
    {
        $flowdetailizin = FlowDetailIzin::where('id_m_flow_izin_terintegrasi',$id)->orderBy('urutan', 'ASC')->get(['id_m_jenis_perizinan','urutan','id_m_flow_izin_terintegrasi']);
        return view('flowizinterintegrasi.editdetail', compact('flowdetailizin','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $flowizinterintegrasi = FlowIzinTerintegrasi::find($id);
        $flowizinterintegrasi->no_sop = $req->no_sop;
        $flowizinterintegrasi->nama_flow = $req->nama_flow;
        $flowizinterintegrasi->keterangan = $req->keterangan;
        $flowizinterintegrasi->id_m_bidang_usaha = $req->nama_bidang_usaha;
        $flowizinterintegrasi->save();


        return redirect('flowizinterintegrasi/');
    }

    public function updateDetail(Request $req, $id)
    {   
        $lastId=0;
        foreach($req->all() as $key=>$value){
            if("jenis_perizinan" == substr($key,0,15)){
                $key = substr($key,16,19);
                $izin =  FlowDetailIzin::where('id_m_flow_izin_terintegrasi',$id)->where('urutan',$key)->first();
                if(!isset($izin)){
                    $izin = new FlowDetailIzin;
                    $izin->id_m_flow_izin_terintegrasi = $id;
                    $izin->urutan = $key;
                }
                $izin->id_m_jenis_perizinan = $value;
                $jenis_perizinan = JenisPerizinan::where('id_m_jenis_perizinan',$value)->first();
                $izin->sla_izin = $jenis_perizinan->sop;
                $lastId = $key;
                $izin->save();
            }
        }
        $izin_hapus = FlowDetailIzin::where('id_m_flow_izin_terintegrasi',$id)->where('urutan','>',$lastId)->get();
        foreach($izin_hapus as $izin) {
            $izin->delete();
        }

        return redirect('flowizinterintegrasi/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {   
        if (TrackingRegisterDetail::where('id_m_flow_izin_terintegrasi',$id)->count() > 0) {
            return redirect()->back()->with(array('message_type'=>'error','message_title'=>'Delete gagal','message'=>'Ada registrasi dengan izin ini'));
        }
        FlowIzinTerintegrasi::find($id)->delete();
        $izin_hapus =  FlowDetailIzin::where('id_m_flow_izin_terintegrasi',$id)->get();
        foreach($izin_hapus as $izin) {
            $izin->delete();
        }
        return redirect()->back();
    }
}
