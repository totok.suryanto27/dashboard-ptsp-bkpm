<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Navigator;
use DB;

use App\UserLog; 
use App\UserHits;

class PtspMonitoringPerformansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response 
     */
    public function __construct()
    {
        $this->middleware('auth');
        \App::setLocale(getLang());
        createSidebar();

        if(\Auth::user()){
            $hits = new UserHits;
            $hits->page = 'Monitoring Performansi PTSP';
            // $hits->id_user_log = getUserId();
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
			// getUserId() => 'Admin1'
            UserLog::where('id_m_user', getUserId())
                        ->where('id', UserLog::orderBy('id', 'desc')->first()->id)
                        ->update([
                            'hits'=>UserHits::where(['id_user_log' => getUserId() , 'status'=>1])->count()
                        ]);

        }
    }
    public function index()
    {
        Navigator::setActive(url('monitoring-performansi-ptsp'));
        // return \Auth::user();
        
      
        $current_year = \Carbon\Carbon::now()->year;
          //all
        $current_month = \Carbon\Carbon::now()->month;
        
        
        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );

        if (getInstansi()) {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI where id_m_instansi = '.getInstansi().'ORDER BY UPPER(NAMA_INSTANSI)'));
        } else {
            $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_INSTANSI ORDER BY UPPER(NAMA_INSTANSI)'));
        }
        
        $current_month_name = $list_bulan[$current_month-1]['bulan'];
        
        return view('pages.monitoring-performansi-ptsp',compact('list_bulan', 'list_kementrian','current_month','current_month_name','current_year'));
    }

    public function get_monthly_performance(Request $request){
        $current_month = \Carbon\Carbon::now()->month;
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }

        echo $current_month;
    }

    public function get_yearsly_table(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        $kementrian = " AND NAMA_INSTANSI IS NOT NULL";
        if ($request->input('id_instansi')) {
          $kementrian = " AND ID_M_INSTANSI = ".$request->input('id_instansi');
        }


        $query = "SELECT 
                  NAMA_INSTANSI,
                  HITUNG_PENGAJUAN as total_pengajuan,
                  HITUNG_SELESAI as total_selesai,
                  HITUNG_TERLAMBAT as total_terlambat,
                  HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN 
                  WHERE TAHUN = ".$current_year." ".$kementrian;
          $data_performansi_kementrian = DB::select(DB::raw($query));
          echo json_encode($data_performansi_kementrian);
    }


    public function get_monthly_table(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }

        if ($request->input('bulan')) {
            $current_month =  $request->input('bulan');
        }

        $kementrian = " AND NAMA_INSTANSI IS NOT NULL";
        if ($request->input('id_instansi')) {
          $kementrian = " AND ID_M_INSTANSI = ".$request->input('id_instansi');
        }

        $query = "SELECT 
                  NAMA_INSTANSI,
                  HITUNG_PENGAJUAN as total_pengajuan,
                  HITUNG_SELESAI as total_selesai,
                  HITUNG_TERLAMBAT as total_terlambat,
                  HITUNG_ON_TIME as total_on_time
                  FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN 
                  WHERE TAHUN = ".$current_year." AND BULAN = ".$current_month." ".$kementrian;
          $data_performansi_kementrian = DB::select(DB::raw($query));
          echo json_encode($data_performansi_kementrian);
    }

    public function get_yearsly_performance(Request $request){
        $current_year = \Carbon\Carbon::now()->year;

        if ($request->input('tahun')) {
            $current_year =  $request->input('tahun');
        }
        
          $query = "SELECT 
                    HITUNG_PENGAJUAN as total_pengajuan,
                    HITUNG_SELESAI as total_selesai,
                    HITUNG_TERLAMBAT as total_terlambat,
                    HITUNG_ON_TIME as total_on_time
                    FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN 
                    WHERE TAHUN = ".$current_year." AND ID_M_INSTANSI IS NULL AND ID_M_FLOW_TERINTEGRASI IS NULL AND ID_M_JENIS_PERIZINAN IS NULL";

        if ($request->input('id_instansi')) {
            $id_instansi = $request->input('id_instansi');
            $query = " ";

        }
        $data_performansi_kementrian = DB::select(DB::raw($query));
        echo json_encode($data_performansi_kementrian);
    }
    // public function detail($id){
    //     $id = $id;
    //     return view('monitoring-performansi.detail', compact('id'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
