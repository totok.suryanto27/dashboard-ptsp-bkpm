<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\SopDeskripsi;
use App\Model\Instansi;
use DB;

class SopDeskripsiController extends Controller
{
    private $columns = [
    'id_m_sop_deskripsi',
    'nama_sop',
    'id_parent',
    'id_m_instansi'
    ]
    ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin,executive');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('sop_deskripsi.index');
    }


    public function getData(){
        if (getInstansi()) {
            $sop_deskripsi = SopDeskripsi::with('instansi')->where('bahasa','id')->where('id_m_instansi',getInstansi())->get($this->columns);
        } else {
            $sop_deskripsi = SopDeskripsi::with('instansi')->where('bahasa','id')->get($this->columns);
        }

        return Datatables::of($sop_deskripsi)
        //->addColumn('status','
        //                {{is_null($persyaratan)?\'<i class="fa fa-check green"></i>\':\'<i class="fa fa-times red"></i>\'}} Persyaratan<br>
        //                {{is_null($sla)?\'<i class="fa fa-check green"></i>\':\'<i class="fa fa-times red"></i>\'}} SLA
        //                {{is_null($prosedur)?\'<i class="fa fa-check green"></i>\':\'<i class="fa fa-times red"></i>\'}} Prosedur
        //                {{is_null($dasar_hukum)?\'<i class="fa fa-check green"></i>\':\'<i class="fa fa-times red"></i>\'}} Dasar Hukum<br>
        //                ')
        //->removeColumn('persyaratan')->removeColumn('sla')->removeColumn('prosedur')->removeColumn('dasar_hukum')->removeColumn('deleted_at')
        ->addColumn('actions','
                        <a href="{{ url( \'perizinan-kl/\'.$id_m_instansi,$id_m_sop_deskripsi )}}" target="_blank"><i class="fa fa-search"></i>&nbsp;read</a><br/>
                        <a href="{{ url( \'sop-deskripsi/edit\',$id_m_sop_deskripsi )}}"><i class="fa fa-edit"></i>&nbsp;edit</a><br/>
                        <a href="{{ url( \'sop-deskripsi/delete\',$id_m_sop_deskripsi ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;delete</a><br/>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('sop_deskripsi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $langs = ['id','en'];
        $fields = ['persyaratan','nama_sop','sla','prosedur','dasar_hukum'];
        foreach ($langs as $lang) {
            $sop_deskripsi = New SopDeskripsi;
            $id_m_sop_deskripsi = DB::table(TABLESPACE . ".M_SOP_DESKRIPSI")->max('id_m_sop_deskripsi') + 1;
            $sop_deskripsi->id_m_sop_deskripsi = $id_m_sop_deskripsi;
            $sop_deskripsi->id_parent = $req->get('id_parent',null);
            $sop_deskripsi->id_m_instansi = $req->get('id_m_instansi');
            $sop_deskripsi->id_m_direktorat = $req->get('id_m_direktorat');
            $sop_deskripsi->id_m_jenis_perizinan = $req->get('id_m_jenis_perizinan');
            $sop_deskripsi->bahasa = $lang;
            foreach ($fields as $field) {
                $sop_deskripsi->$field = $req->get($field)[$lang];
            }
            $sop_deskripsi->save();
        }

        return redirect('sop-deskripsi')->with(array('message'=>'Deskripsi SOP berhasil ditambahkan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $sop_deskripsis = [];
        $sop_deskripsis['id'] = SopDeskripsi::where('id_m_sop_deskripsi',$id)->where('bahasa','id')->first();
        $sop_deskripsis['en'] = SopDeskripsi::where('id_m_sop_deskripsi',$id)->where('bahasa','en')->first();
        if (!$sop_deskripsis['id']) return redirect()->back()->with(array('message'=>'Tidak ditemukan','message_type'=>'error'));
        if (!$sop_deskripsis['en']) $sop_deskripsis['en'] = new SopDeskripsi;
        return view('sop_deskripsi.edit', compact('sop_deskripsis','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $langs = ['id','en'];
        $fields = ['persyaratan','nama_sop','sla','prosedur','dasar_hukum'];
        foreach ($langs as $lang) {
            $sop_deskripsi = SopDeskripsi::where('id_m_sop_deskripsi',$id)->where('bahasa',$lang)->first();
            if (!$sop_deskripsi) $sop_deskripsi = new SopDeskripsi;
            $sop_deskripsi->id_m_sop_deskripsi = $req->get('id_m_sop_deskripsi');
            $sop_deskripsi->id_parent = $req->get('id_parent',null);
            $sop_deskripsi->id_m_instansi = $req->get('id_m_instansi');
            $sop_deskripsi->id_m_direktorat = $req->get('id_m_direktorat');
            $sop_deskripsi->id_m_jenis_perizinan = $req->get('id_m_jenis_perizinan');
            $sop_deskripsi->bahasa = $lang;
            foreach ($fields as $field) {
                $sop_deskripsi->$field = $req->get($field)[$lang];
            }
            $sop_deskripsi->save();
        }

        return redirect('sop-deskripsi')->with(array('message'=>'Deskripsi SOP berhasil diubah'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        SopDeskripsi::where('id_m_sop_deskripsi',$id)->delete();
        return redirect()->back()->with(array('message'=>'Delete berhasil!'));
    }
}
