<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Navigator;
use DB;

use App\UserLog;
use App\UserHits;

class DashboardController extends Controller
{
    
    private $time; 
    
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Dashboard';
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    }
    
    public function dashboardBidang(Request $request) {
        $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->select('nama_flow', DB::raw('SUM(hitung_pengajuan) as total_pengajuan'), DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_terlambat) as total_terlambat'), DB::raw('SUM(hitung_on_time) as total_on_time'))->whereRaw('id_m_flow_terintegrasi IS NOT NULL')->groupBy('nama_flow')->get();
        foreach ($query as $val) {
            $val->persen_on_time = toPersen($val->total_on_time, ($val->total_on_time + $val->total_selesai));
            $val->persen_delay = toPersen($val->total_terlambat, ($val->total_terlambat + $val->total_selesai));
            $val->persen_selesai = toPersen($val->total_selesai, $val->total_selesai);
        }
        
        return $query;
    }
    
    public function dashboard(Request $request) {
        Navigator::setActive(url('/'));
        $time = \Carbon\Carbon::now();
        
        //all
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI '));
        
        if (getInstansi()) {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->where('id_m_instansi',getInstansi())->orderBy('nama_instansi', 'asc')->get();
        } else {
            $list_kementrian = DB::table(TABLESPACE.'.M_INSTANSI')->orderBy('nama_instansi', 'asc')->get();
        }
        
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $tanggal = \Carbon\Carbon::now();
        $query_harian_all = "SELECT hitung_terlambat as total_terlambat, hitung_selesai as total_selesai, hitung_pengajuan as total_pengajuan, hitung_on_time as total_on_time, hitung_on_proses as total_on_proses FROM ".TABLESPACE.".hitung_perizinan_harian WHERE id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND id_m_flow_terintegrasi IS NULL";
        
        // return $query_harian_all;
        $harian_all = DB::select($query_harian_all);
        
        $query_harian_proses_all = "SELECT total_izin as total_izin, delay as total_delay FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES WHERE id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND create_tms = to_date('".date('Y-m-d', strtotime($tanggal))."')";
        $harian_proses_all = DB::select(DB::raw($query_harian_proses_all));
        
        $total_selesai = 0;
        $total_on_time = 0;
        $total_pengajuan = 0;
        $total_terlambat = 0;
        
        $total_izin_proses_delay = 0;
        $total_izin_on_proses = 0;
        $total_sum_proses = 0;
        
        if (!empty($harian_all)) {
            $total_selesai = $harian_all[count($harian_all)-1]->total_selesai;
            $total_on_time = $harian_all[count($harian_all)-1]->total_on_time;
            $total_pengajuan = $harian_all[count($harian_all)-1]->total_pengajuan;
            $total_terlambat = $harian_all[count($harian_all)-1]->total_terlambat;
        }
        
        if (!empty($harian_proses_all)) {
            $total_izin_proses_delay = $harian_proses_all[count($harian_proses_all)-1]->total_delay;
            $total_izin_on_proses = $harian_proses_all[count($harian_proses_all)-1]->total_izin;
            $total_sum_proses = $total_izin_proses_delay + $total_izin_on_proses;
        }
        
        // return array(
        //     'total selesai ' => $total_selesai,
        //     'total_on_time' => $total_on_time,
        //     'total_pengajuan' => $total_pengajuan,
        //     'total_terlambat' => $total_terlambat,
        //     'total_izin_proses_delay' => $total_izin_proses_delay,
        //     'total_izin_on_proses' => $total_izin_on_proses,
        //     'total_sum_proses' => $total_sum_proses
        
        // );
        
        // return $harian_all;
        // return $query_harian_proses_all;
        return view('pages.dashboard', compact('list_instansi', 'years', 'list_kementrian', 'total_selesai', 'total_izin_proses_delay', 'total_izin_on_proses', 'total_on_time', 'total_terlambat', 'total_izin_proses_delay', 'time', 'total_sum_proses', 'total_pengajuan'));
    }
    
    public function restDashboard() {
        $data = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_BULANAN')->get();
        return $data;
    }
    
    private function stringQueryBidang($request) {
        
        if ($request->input('idinstansi')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        }
        
        if ($request->input('idinstansi') && $request->input('tanggal1')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        } 
        else if ($request->input('idinstansi') && $request->input('tanggal1') && $request->input('tanggal2')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        } 
        else if ($request->input('idinstansi') && $request->input('bulan')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (bulan) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (bulan) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (bulan)";
        } 
        else if ($request->input('idinstansi') && $request->input('tahun')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (tahun) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (tahun) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_flow_terintegrasi = '" . $request->input('idinstansi') . "' group by rollup (tahun)";
        }
        
        if ($request->input('tanggal1')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari)";
        } 
        else if ($request->input('tanggal1') && $request->input('tanggal2')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari)";
        } 
        else if ($request->input('bulan')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan)";
        } 
        else if ($request->input('tahun')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun)";
        }
        
        return array('query_perizinan_harian' => $query_perizinan_harian, 'query_perizinan_harian_on_time' => $query_perizinan_harian_on_time, 'query_perizinan_harian_delay' => $query_perizinan_harian_delay);
    }
    
    private function stringQueryKementrian($request) {
        $query_perizinan_harian = "";
        $query_perizinan_harian_on_time = "";
        $query_perizinan_harian_delay = "";
        if ($request->input('idinstansi')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between to_date('{$this->time->startOfMonth() }') and to_date('{$this->time->endOfMonth() }') and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        }
        
        if ($request->input('idinstansi') && $request->input('tanggal1')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        } 
        else if ($request->input('idinstansi') && $request->input('tanggal1') && $request->input('tanggal2')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (hari)";
        } 
        else if ($request->input('idinstansi') && $request->input('bulan')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (bulan) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (bulan) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (bulan)";
        } 
        else if ($request->input('idinstansi') && $request->input('tahun')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (tahun) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (tahun) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' and id_m_instansi = '" . $request->input('idinstansi') . "' group by rollup (tahun)";
        }
        
        if ($request->input('tanggal1')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI = '" . $request->input('tanggal1') . "' group by rollup (hari)";
        } 
        else if ($request->input('tanggal1') && $request->input('tanggal2')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN where HARI between '" . $request->input('tanggal1') . "' and '" . $request->input('tanggal2') . "' group by rollup (hari)";
        } 
        else if ($request->input('bulan')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN where BULAN = '" . $request->input('bulan') . "' group by rollup (bulan)";
        } 
        else if ($request->input('tahun')) {
            $query_perizinan_harian = "select sum(hitung_selesai) as total_selesai from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun) ";
            $query_perizinan_harian_on_time = "select sum(hitung_on_time) as total_on_time from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun) ";
            $query_perizinan_harian_delay = "select sum(hitung_terlambat) as total_terlambat from ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN where tahun = '" . $request->input('tahun') . "' group by rollup (tahun)";
        }
        
        return array('query_perizinan_harian' => $query_perizinan_harian, 'query_perizinan_harian_on_time' => $query_perizinan_harian_on_time, 'query_perizinan_harian_delay' => $query_perizinan_harian_delay);
    }
    
    public function main(Request $request) {
        //default
        $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->select('nama_flow', DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_on_time) as total_on_time'), DB::raw('SUM(hitung_terlambat) as total_terlambat'))->whereNull('id_m_instansi')->whereNull('id_m_jenis_perizinan')->where('id_m_flow_terintegrasi', '!=', 0)->groupBy('hari', 'nama_flow')->having('hari', '=', date('Y-m-d', strtotime(\Carbon\Carbon::now())))->get();
        //harian
        if($request->input('tanggal')){
            $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_HARIAN')->select('nama_flow', DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_on_time) as total_on_time'), DB::raw('SUM(hitung_terlambat) as total_terlambat'))->whereNull('id_m_instansi')->whereNull('id_m_jenis_perizinan')->where('id_m_flow_terintegrasi', '!=', 0)->whereRaw('id_m_flow_terintegrasi BETWEEN 1 AND 5')->groupBy('hari', 'nama_flow')->having('hari', '=', date('Y-m-d', strtotime(\Carbon\Carbon::now())))->get();    
        }
        
        //bulanan
        if($request->input('bulan')){
            $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_BULANAN')->select('nama_flow', DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_on_time) as total_on_time'), DB::raw('SUM(hitung_terlambat) as total_terlambat'))->whereNull('id_m_instansi')->whereNull('id_m_jenis_perizinan')->where('id_m_flow_terintegrasi', '!=', 0)->whereRaw('id_m_flow_terintegrasi BETWEEN 1 AND 5')->groupBy('hari', 'nama_flow')->having('hari', '=', date('Y-m-d', strtotime(\Carbon\Carbon::now())))->get();
        }
        //bulanan + tahun
        if($request->input('bulan') && $request->input('tahun')){
            $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_BULANAN')->select('nama_flow', DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_on_time) as total_on_time'), DB::raw('SUM(hitung_terlambat) as total_terlambat'))->whereNull('id_m_instansi')->whereNull('id_m_jenis_perizinan')->where('id_m_flow_terintegrasi', '!=', 0)->whereRaw('id_m_flow_terintegrasi BETWEEN 1 AND 5')->groupBy('hari', 'nama_flow')->having('hari', '=', date('Y-m-d', strtotime(\Carbon\Carbon::now())))->get();
        }
        //tahunan
        if($request->input('tahun') && $request->input('filter')){
            $query = DB::table(TABLESPACE.'.HITUNG_PERIZINAN_TAHUNAN')->select('nama_flow', DB::raw('SUM(hitung_selesai) as total_selesai'), DB::raw('SUM(hitung_on_time) as total_on_time'), DB::raw('SUM(hitung_terlambat) as total_terlambat'))->whereNull('id_m_instansi')->whereNull('id_m_jenis_perizinan')->where('id_m_flow_terintegrasi', '!=', 0)->whereRaw('id_m_flow_terintegrasi BETWEEN 1 AND 5')->groupBy('hari', 'nama_flow')->having('hari', '=', date('Y-m-d', strtotime(\Carbon\Carbon::now())))->get();
        }

        foreach ($query as $val) {
            $val->persen_on_time = ($val->total_selesai == 0) ? 0 : ($val->total_on_time / ($val->total_on_time + $val->total_terlambat)) * 100;
            $val->persen_terlambat = ($val->total_selesai == 0) ? 0 : ($val->total_terlambat / ($val->total_terlambat + $val->total_on_time)) * 100;
        }
        return $query;
    }
    
    public function test(Request $request) {
        $tanggal = $request->input('tanggal');
        
        // $query_all = "SELECT SUM(hitung_selesai) as total_selesai, SUM(hitung_terlambat) as total_terlambat,SUM(hitung_on_time) as total_on_time, SUM(hitung_pengajuan) as total_pengajuan, SUM(IZININT_HITUNG_ON_TIME) as total_IZININT_ontime, SUM(IZININT_HITUNG_PENGAJUAN) as total_IZININT_pengajuan, SUM(IZININT_HITUNG_SELESAI) as total_IZININT_selesai, SUM(IZININT_HITUNG_TERLAMBAT) as total_IZININT_terlambat, SUM(HITUNG_ON_PROSES) as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE id_m_instansi IS NULL AND id_m_flow_terintegrasi IS NULL AND id_m_jenis_perizinan IS NULL  HAVING hari = to_date('" . $tanggal . "')";
        // $tanggal_t = date('Y-m-d', strtotime($tanggal));
        // DB::table(TABLESPACE.'.hitung_perizinan_harian')
        //     ->whereNull('id_m_instansi')
        //     ->whereNull('id_m_jenis_perizinan')
        //     ->whereNull('id_m_flow_terintegrasi')
        //     ->where("hari", $tanggal_t)
        //     ->first();
        // DB::enableQueryLog();
        
        // return dd(DB::getQueryLog());
        $x = DB::table(TABLESPACE.'.hitung_perizinan_harian')->where('id_m_instansi', '=', '14')->whereNull('id_m_flow_terintegrasi')->whereNull('id_m_jenis_perizinan')->where('hari', date('Y-m-d', strtotime('18-08-2015')))->first();
        return response()->json($x);
    }
    
    public function mainKementrian(Request $request) {
        
        $idinstansi = $request->input('id');
        $filterWaktu = $request->input('filter');
        $tanggal = $request->input('tanggal');
        
        // $tanggal1 = $request->input('tanggal1');
        // $tanggal2 = $request->input('tanggal2');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $time = \Carbon\Carbon::now();
        
        // $listQueryKementrian = $this->stringQueryKementrian($request);
        
        //all
        $list_instansi = DB::select(DB::raw('select * from '.TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI '));
        
        $list_kementrian = DB::select(DB::raw('select * from '.TABLESPACE.'.M_DIREKTORAT'));
        
        $years = [($time->year), ($time->year + 1), ($time->year + 2), ($time->year + 3) ];
        $query_all = "";
        $query_harian_proses_all = "SELECT total_izin as total_izin, delay as total_delay FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES WHERE ROWNUM = 1";
        $case = 0;
        $message = "";
        
        switch ($filterWaktu) {
            case 1:
                $case = 1;
                if ($idinstansi) {
                    $message = 'masuk filter 1 dengan id instansi';
                    $query_all = "SELECT nama_instansi, hitung_selesai as total_selesai, hitung_terlambat as total_terlambat, hitung_on_time as total_on_time, hitung_pengajuan as total_pengajuan, IZININT_HITUNG_ON_TIME as total_IZININT_ontime, IZININT_HITUNG_PENGAJUAN as total_IZININT_pengajuan, IZININT_HITUNG_SELESAI as total_IZININT_selesai, IZININT_HITUNG_TERLAMBAT as total_IZININT_terlambat, HITUNG_ON_PROSES as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_HARIAN WHERE id_m_instansi = '" . $idinstansi . "' AND hari = '" . date('Y-m-d', strtotime($tanggal)) . "' AND ROWNUM = 1";
                    $query_harian_proses_all = "SELECT total_izin as total_izin, delay as total_delay FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES WHERE ROWNUM = 1 AND id_m_instansi = '" . $idinstansi . "' AND TRUNC(create_tms) = to_timestamp('" . $tanggal . "', 'DD-MM-YYYY')";
                } 
                else {
                    $message = 'masuk filter 1 tanpa id instansi';
                    
                    $query_all = "SELECT nama_instansi, hitung_terlambat as total_terlambat, hitung_selesai as total_selesai, hitung_pengajuan as total_pengajuan, hitung_on_time as total_on_time, hitung_on_proses as total_on_proses FROM ".TABLESPACE.".hitung_perizinan_harian WHERE ROWNUM = 1 AND id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND id_m_flow_terintegrasi IS NULL AND hari = '" . date('Y-m-d', strtotime($tanggal)) . "' AND ROWNUM = 1";
                    $query_harian_proses_all = "SELECT total_izin as total_izin, delay as total_delay FROM ".TABLESPACE.".HITUNG_PERIZINAN_ON_PROSES WHERE id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND TRUNC(create_tms) = to_timestamp('" . $tanggal . "', 'DD-MM-YYYY') AND ROWNUM = 1";
                }
                
                break;

            case 2:
                $case = 2;
                if ($idinstansi) {
                    $message = 'masuk filter 2 dengan id instansi';
                    $query_all = "SELECT bulan, tahun, nama_instansi, hitung_selesai as total_selesai, hitung_terlambat as total_terlambat,hitung_on_time as total_on_time, hitung_pengajuan as total_pengajuan, IZININT_HITUNG_ON_TIME as total_IZININT_ontime, IZININT_HITUNG_PENGAJUAN as total_IZININT_pengajuan, IZININT_HITUNG_SELESAI as total_IZININT_selesai, IZININT_HITUNG_TERLAMBAT as total_IZININT_terlambat, HITUNG_ON_PROSES as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE  ROWNUM = 1 AND id_m_instansi = '" . $idinstansi . "' AND bulan = '" . $bulan . "' ";
                    if($request->input('tahun')){
                        $query_all = "SELECT bulan, tahun, nama_instansi, hitung_selesai as total_selesai, hitung_terlambat as total_terlambat,hitung_on_time as total_on_time, hitung_pengajuan as total_pengajuan, IZININT_HITUNG_ON_TIME as total_IZININT_ontime, IZININT_HITUNG_PENGAJUAN as total_IZININT_pengajuan, IZININT_HITUNG_SELESAI as total_IZININT_selesai, IZININT_HITUNG_TERLAMBAT as total_IZININT_terlambat, HITUNG_ON_PROSES as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE  ROWNUM = 1 AND id_m_instansi = '" . $idinstansi . "' AND bulan = '" . $bulan . "' AND tahun = '".$request->input('tahun')."' ";
                    }
                } 
                else {
                    $message = 'masuk filter 2 tanpa id instansi';
                    $query_all = "SELECT bulan, tahun, nama_instansi, hitung_terlambat as total_terlambat, hitung_selesai as total_selesai, hitung_pengajuan as total_pengajuan, hitung_on_time as total_on_time, hitung_on_proses as total_on_proses FROM ".TABLESPACE.".hitung_perizinan_bulanan WHERE ROWNUM = 1 AND id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND id_m_flow_terintegrasi IS NULL AND bulan = '" . $bulan . "' AND ROWNUM = 1";
                    if($request->input('tahun')){
                        $query_all = "SELECT bulan, tahun, nama_instansi, hitung_selesai as total_selesai, hitung_terlambat as total_terlambat,hitung_on_time as total_on_time, hitung_pengajuan as total_pengajuan, IZININT_HITUNG_ON_TIME as total_IZININT_ontime, IZININT_HITUNG_PENGAJUAN as total_IZININT_pengajuan, IZININT_HITUNG_SELESAI as total_IZININT_selesai, IZININT_HITUNG_TERLAMBAT as total_IZININT_terlambat, HITUNG_ON_PROSES as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_BULANAN WHERE bulan = '" . $bulan . "' AND tahun = '".$request->input('tahun')."' AND ROWNUM = 1 ";
                    }
                }
                
                break;

            case 3:
                $case = 3;
                if ($idinstansi) {
                    $message = 'masuk filter 3 dengan id instansi';
                    $query_all = "SELECT nama_instansi, hitung_selesai as total_selesai, hitung_terlambat as total_terlambat,hitung_on_time as total_on_time, hitung_pengajuan as total_pengajuan, IZININT_HITUNG_ON_TIME as total_IZININT_ontime, IZININT_HITUNG_PENGAJUAN as total_IZININT_pengajuan, IZININT_HITUNG_SELESAI as total_IZININT_selesai, IZININT_HITUNG_TERLAMBAT as total_IZININT_terlambat,HITUNG_ON_PROSES as total_on_proses FROM ".TABLESPACE.".HITUNG_PERIZINAN_TAHUNAN WHERE ROWNUM = 1 AND id_m_instansi = '" . $idinstansi . "' AND tahun = '" . $tahun . "'";
                } 
                else {
                    $message = 'masuk filter 3 tanpa id instansi';
                    $query_all = "SELECT nama_instansi, hitung_terlambat as total_terlambat, hitung_selesai as total_selesai, hitung_pengajuan as total_pengajuan, hitung_on_time as total_on_time, hitung_on_proses as total_on_proses FROM ".TABLESPACE.".hitung_perizinan_tahunan WHERE id_m_instansi IS NULL AND id_m_jenis_perizinan IS NULL AND id_m_flow_terintegrasi IS NULL AND tahun = '" . $tahun . "' AND ROWNUM = 1";
                }
                
                break;
        }
        
        // echo "query sebelum tampil";
        // echo $query_all;
        // return $query_all;
        DB::enableQueryLog();
        $result_all = DB::select($query_all);
        $harian_proses_all = DB::select(DB::raw($query_harian_proses_all));
        
        // return $query_harian_proses_all;
        // return $result_all;
        
        $total_selesai = 0;
        $total_on_time = 0;
        $total_pengajuan = 0;
        $total_terlambat = 0;
        $total_on_proses = 0;
        
        $total_izin_proses_delay = 0;
        $total_izin_on_proses = 0;
        $total_sum_proses = 0;
        
        if (!empty($result_all)) {
            
            // return $result_all;
            // return $case . " <> " . $message . " <> " . $query_all;
            // return $query_all;
            
            // return $case . "-" . $message . " _ " .$query_all;
            
            $total_selesai = $result_all[0]->total_selesai;
            $total_on_time = $result_all[0]->total_on_time;
            $total_pengajuan = $result_all[0]->total_pengajuan;
            $total_terlambat = $result_all[0]->total_terlambat;
            $total_on_proses = $result_all[0]->total_on_proses;
        }
        
        if (!empty($harian_proses_all)) {
            
            $total_izin_proses_delay = $harian_proses_all[0]->total_delay;
            $total_izin_on_proses = $harian_proses_all[0]->total_izin;
            $total_sum_proses = $total_izin_proses_delay + $total_izin_on_proses;
        }
        
        // return DB::getQueryLog();
        
        // echo $query_all;
        // return $result_all;
        
        // echo "<br />";
        // echo $query_harian_proses_all;
        // return $query_harian_proses_all;
        return array('list_instansi' => $list_instansi, 'years' => $years, 'list_kementrian' => $list_kementrian, 'total_selesai' => $total_selesai, 'total_izin_proses_delay' => $total_izin_proses_delay, 'total_izin_on_proses' => $total_izin_on_proses, 'total_on_time' => $total_on_time, 'total_terlambat' => $total_terlambat, 'total_on_proses' => $total_on_proses, 'total_sum_proses' => $total_sum_proses, 'query all' => $query_all, 'query proses' => $query_harian_proses_all, 'case' => $case, 'message' => $message, 'total_pengajuan' => $total_pengajuan, 'result_all'=>$result_all);
    }
}
