<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IzinPrinsipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    public function index()
    {
        return view('izin_prinsip.index');
    }
    
    public function create()
    {
        return view('izin_prinsip.create');
    }
    
    public function store()
    {
        return redirect()->back();
    }
    
    public function show($id)
    {
        return view('izin_prinsip.view');
    }
    
    public function edit($id)
    {
        return view('izin_prinsip.edit');
    }
    
    public function update($id)
    {
        return redirect()->back();
    }
    
    public function destroy($id)
    {
        return redirect()->back();
    }
}
