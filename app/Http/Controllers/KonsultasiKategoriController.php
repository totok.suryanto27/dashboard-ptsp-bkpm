<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\KonsultasiKategori;

class KonsultasiKategoriController extends Controller
{
    private $columns = [
        'ID_KONSUL_CAT',
        'NAMA_KONSUL_CAT',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('konsultasi_kategori.index');
    }

    public function getData(){
        $konsultasi_kategori = KonsultasiKategori::all();
        return Datatables::of($konsultasi_kategori)
        ->addColumn('actions','
                        <a href="{{ url( \'konsultasi-kategori\read\',$id_konsul_cat )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a><br>
                        <a href="{{ url( \'konsultasi-kategori\edit\',$id_konsul_cat )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a><br>
                        <a href="{{ url( \'konsultasi-kategori\delete\',$id_konsul_cat ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('konsultasi_kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $konsultasi_kategori = New KonsultasiKategori;
        $konsultasi_kategori->id_konsul_cat = $req->id_konsul_cat;
        $konsultasi_kategori->nama_konsul_cat = $req->nama_konsul_cat;
        $konsultasi_kategori->save();

        return redirect('konsultasi-kategori/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $konsultasi_kategori = KonsultasiKategori::find($id);
        return view('konsultasi_kategori.view', compact('konsultasi_kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $konsultasi_kategori = KonsultasiKategori::find($id);
        return view('konsultasi_kategori.edit', compact('konsultasi_kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $konsultasi_kategori = KonsultasiKategori::find($id);
        $konsultasi_kategori->id_konsul_cat = $req->id_konsul_cat;
        $konsultasi_kategori->nama_konsul_cat = $req->nama_konsul_cat;
        $konsultasi_kategori->save();


        return redirect('konsultasi-kategori/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       KonsultasiKategori::find($id)->delete();
        return redirect()->back();
    }
}
