<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Controllers\Controller;
use App\Model\TrackingRegisterDetail;
use App\Model\Libur;
use Auth;
use Faker\Factory as Faker;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use App\Services\HitungHariLibur;

class PengajuanIzinController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
        \App::setLocale(getLang());
        createSidebar();
    }

    public function index()
    {
        if (isRole('investor'))
            return view('pengajuan_izin.index_investor');
        return view('pengajuan_izin.index');
    }

    public function cari()
    {
        return view('pengajuan_izin.cari');
    }
    
    public function debug()
    {
        return view('pengajuan_izin.debug');
    }
    
    public function search(Request $request) 
    {
        $params = $request->get('params',false);
        if (isRole('investor')) 
            $params['npwp'] = Auth::user()->npwp;
        $table = new Collection;
        if (!$params) {
            $trackings = TrackingRegisterDetail::get();
        } else {
            $query = TrackingRegisterDetail::whereRaw("1=1");
            foreach ($params as $key => $search) {
                switch($key) {
                    case 'qrcode':
                        $query = $query->where('upper(ID_QRCODE)','=',strtoupper($search));
                        break;
                    case 'performance':
                        if (intval($search) > 0)
                            $query = $query->where($key,'=',$search);
                        else if ($search == 'on_going') 
                            $query = $query->whereNull('tanggal_selesai')->where('tanggal_threshold','>',date('Y-m-d'));
                        else if ($search == 'on_going_delay') 
                            $query = $query->whereNull('tanggal_selesai')->where('tanggal_threshold','<',date('Y-m-d'));
                        break;
                    case 'id_m_jenis_perizinan':
                    case 'id_m_instansi':
                    case 'id_m_direktorat':
                    case 'npwp':
                        $query = $query->where($key,'=',$search);
                        break;
                    case 'nama_perusahaan':
                        continue;
                        break;
                    default:
                        $query = $query->where($key,'like',"%$search%");
                        break;
                }
            }
            $trackings = $query->get();
        }
        foreach($trackings as $tracking) {
            $table->push([
                'id_qrcode'         =>  $tracking->id_qrcode,
                'nama_instansi'     => $tracking->nama_jenis_perizinan,
                'tanggal_masuk'     => '<span class="hidden">'.$tracking->tanggal_pengajuan.'</span>'.getFullDate($tracking->tanggal_pengajuan),
                'tanggal_selesai'   => '<span class="hidden">'.($tracking->tanggal_selesai?$tracking->tanggal_selesai:'0000-00-00').'</span>'.getFullDate($tracking->tanggal_selesai),
                'tanggal_estimasi'  => '<span class="hidden">'.$tracking->tanggal_selesai_estimasi.'</span>'.getFullDate($tracking->tanggal_selesai_estimasi),                
                'sla' => str_pad($tracking->sop,4,' ',STR_PAD_LEFT)." Hari",
                'status' => $tracking->getStatus(),
                'read' => '<a href="'.url('pengajuanizin/read/'.$tracking->id_tracking_register_detail).'"><i class="fa fa-edit"></i></a>',
            ]);
        }
        //tgl keluar kalau ga ada belum selesai status
        //tgl keluar kalau uda ada selesai statusnya
        //tgl estimasi - tgl masuk = hari normal- hari libur liat weekend
        
        return Datatables::of($table)->make(true);
    }

    public function searchFaker(Request $request) 
    {
        $params = $request->get('params',[]);
        $users = new Collection;
        $faker = Faker::create();

        for ($i = 0; $i < 100; $i++) {
            $users->push([
                'instansi'         =>  $params['qrcode']+ 1,
                'status'       => $faker->name,
                'tanggal_masuk' => $faker->time($format = 'H:i:s', $max = 'now') ,
                'tanggal_keluar' => $faker->time($format = 'H:i:s', $max = 'now') 
            ]);
        }

        return Datatables::of($users)->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function read($id)
    {
        $tracking = TrackingRegisterDetail::find($id); 
        if (!$tracking)
        return redirect('pengajuanizin')->with('message','Data tidak ditemukan')->with('message_type','error');
        //restrict read only user's tracking
        if ($tracking->npwp != Auth::user()->npwp)
        return redirect('pengajuanizin')->with('message','Data tidak ditemukan')->with('message_type','error');
        $tracking->status = $tracking->getStatus();
        $tracking->sla = str_pad($tracking->sop,4,' ',STR_PAD_LEFT)." Hari";
        $tracking->tanggal_pengajuan = getFullDate($tracking->tanggal_pengajuan);
        $tracking->tanggal_selesai   = getFullDate($tracking->tanggal_selesai);
        $tracking->tanggal_selesai_estimasi = getFullDate($tracking->tanggal_selesai_estimasi);               
        return view('pengajuan_izin.read', compact('tracking'));
    }

}
