<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\JenisPerizinan;
use App\Model\TrackingRegisterDetail;

class JenisPerizinanController extends Controller
{
    private $columns = [
    'nama_jenis_perizinan',
    'abbrv',
    'id_m_direktorat',
    'sop',
    'threshold',
    'kategori',
    'm_id_bidang_usaha',
    'id_m_instansi'
    ]
    ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('jenisperizinan.index');
    }


    public function getData(){
        $jenisperizinan = JenisPerizinan::with('instansi')->get();

        return Datatables::of($jenisperizinan)
        ->addColumn('actions','
                        <a href="{{ url( \'jenisperizinan\read\',$id_m_jenis_perizinan )}}" ><i class="fa fa-search"></i>&nbsp;read</a><br/>
                        <a href="{{ url( \'jenisperizinan\edit\',$id_m_jenis_perizinan )}}"><i class="fa fa-edit"></i>&nbsp;edit</a><br/>
                        <a href="{{ url( \'jenisperizinan\delete\',$id_m_jenis_perizinan ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;delete</a><br/>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('jenisperizinan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $jenisperizinan = New JenisPerizinan;
        $jenisperizinan->nama_jenis_perizinan = $req->nama_jenis_perizinan;
        $jenisperizinan->abbrv = $req->abbrv;
        $jenisperizinan->id_m_direktorat = $req->nama_direktorat;
        $jenisperizinan->sop = $req->sop;
        $jenisperizinan->threshold = $req->threshold;
        $jenisperizinan->m_id_bidang_usaha = $req->nama_bidang_usaha;
        $jenisperizinan->id_m_instansi = $req->nama_instansi;
        $jenisperizinan->save();

        return redirect('jenisperizinan/')->with(array('message'=>'Penyimpanan berhasil!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $jenis_perizinan = JenisPerizinan::find($id);
        return view('jenisperizinan.view', compact('jenis_perizinan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $jenis_perizinan = JenisPerizinan::find($id);
        return view('jenisperizinan.edit', compact('jenis_perizinan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $jenisperizinan = JenisPerizinan::find($id);
        $jenisperizinan->nama_jenis_perizinan = $req->nama_jenis_perizinan;
        $jenisperizinan->abbrv = $req->abbrv;
        $jenisperizinan->id_m_direktorat = $req->nama_direktorat;
        $jenisperizinan->sop = $req->sop;
        $jenisperizinan->threshold = $req->threshold;
        $jenisperizinan->m_id_bidang_usaha = $req->nama_bidang_usaha;
        $jenisperizinan->id_m_instansi = $req->nama_instansi;
        $jenisperizinan->save()->with(array('message'=>'Update berhasil!'));


        return redirect('jenisperizinan/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (TrackingRegisterDetail::where('id_m_jenis_perizinan',$id)->count() > 0) {
            return redirect()->back()->with(array('message_type'=>'error','message_title'=>'Delete gagal','message'=>'Ada registrasi dengan izin ini'));
        }
        JenisPerizinan::find($id)->delete();
        return redirect()->back()->with(array('message'=>'Delete berhasil!'));
    }
}
