<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HitungPerizinanBulanan;

use App\Http\Requests; 
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Navigator;
use DB;

use App\UserLog;
use App\UserHits; 

/**
* 
*/
class PtspMonitoringJumlahPengajuanController extends Controller
{
	
	public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        $this->time = Carbon::now();
        createSidebar();
        
        if (\Auth::user()) {
            $hits = new UserHits;
            $hits->page = 'Monitoring Pelayanan';
            // $hits->id_user_log = getUserId();
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month; 
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    } 

    public function index(){
        Navigator::setActive(url('jumlah-pengajuan-ptsp'));
        $date_current['current_year'] = \Carbon\Carbon::now()->year;
        $date_current['current_month'] = \Carbon\Carbon::now()->month;
        $date_current['current_day'] = \Carbon\Carbon::now()->day;
    	return view('pages.jumlah-pengajuan-ptsp',compact('date_current'));

    }
}

