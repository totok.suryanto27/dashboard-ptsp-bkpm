<?php

namespace App\Http\Controllers;

use Request;
use DB;
use Auth;

use App\Model\RegPerusahaan;
use App\Model\Perusahaan;
use App\Model\JenisPerizinan;
use App\Model\FlowIzinTerintegrasi;
use App\Model\FlowDetail;
use App\Model\Direktorat;
use App\Model\BidangUsaha;
use App\Model\IzinPrinsip;
use App\Model\IzinUsaha;

use App\Model\FlowDetailIzin;
use App\Model\TrackingRegisterDetail;
use App\Model\TrackingRegisterHeader;
use App\Model\Instansi;

use App\Services\HitungHariKerja;
use App\Services\HitungHariEstimasi;

class InvestorTrackingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin,kl,monitoring,ptsp,investor,investor_tracking,check_in');
        \App::setLocale(getLang());
        createSidebar();
    }

    public function index()
    {
        if (isRole('investor'))
            return view('investor.index_investor');
        else
            return view('investor.index');
    }

    public function search()
    {
        $id = strtoupper(Request::get('id',''));
        if (strpos($id,"-, NPWP: ") === 0) {
            return $this->findPerusahaanByNpwp(substr($id,9));
        }
        if (isRole('investor') && $id == '') {
            return $this->findPerusahaanByNpwp(Auth::user()->npwp);
        }
        $izin_prinsip = IzinPrinsip::with('perusahaan','reg_perusahaan')->where('upper(id_permohonan)',$id)->orWhere('upper(no_ip)',$id)->first();
        $data = $izin_prinsip->perusahaan;
        if (!$data) {
            $data = RegPerusahaan::where('upper(id_qrcode)',$id)->orWhere('upper(no_ip)',$id)->first();
        } else {
            $data->npwp = $data->npwp_perusahaan;
            $data->alamat = $data->alamat_prshn;
            $provinsi = $data->provinsi()->first();
            $data->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
            $kabkot = $data->kabkot()->first();
            $data->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
            $data->telepon = $data->no_telepon_prshn;
            $data->pimpinan = '';
        }
        if ($data) {
            $data->izin_terintegrasi = FlowIzinTerintegrasi::getByQRCode($id);
            $data->izin_tunggal = TrackingRegisterDetail::getIzinTunggal($id);
        }
        return response()->json($data);
    }
    
    private function findPerusahaanByNpwp($id)
    {
        $data = RegPerusahaan::where('upper(npwp)',$id)->first();
        if (!$data) {
            $data = Perusahaan::where('upper(npwp_perusahaan)',$id)->first();
            if (!$data) {
                return response()->json($data);
            }
            $data->npwp = $data->npwp_perusahaan;
            $data->alamat = $data->alamat_prshn;
            $provinsi = $data->provinsi()->first();
            $data->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
            $kabkot = $data->kabkot()->first();
            $data->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
            $data->telepon = $data->no_telepon_prshn;
        }
        if ($data) {
            $data->izin_terintegrasi = [];
            $data->izin_tunggal = TrackingRegisterDetail::getIzinTunggalNpwp($id);
        }
        return response()->json($data);
    }

    public function searchDetailIzin()
    {
        $id_jenis = strtoupper(Request::get('id_m_jenis_perizinan',''));
        $id = strtoupper(Request::get('id_qrcode',''));
        if (strpos($id,"-, NPWP: ") === 0) {
            $npwp = substr($id,9);
        }
        if (isRole('investor') && $id == '') {
            $npwp = Auth::user()->npwp;
        }
        $id_m_flow_izin_terintegrasi = substr(Request::get('id_m_flow_izin_terintegrasi',''),2);
        if ($id_m_flow_izin_terintegrasi == '') $id_m_flow_izin_terintegrasi = null;
        if (strpos($id_jenis,"IZIN_PRINSIP_") === 0) {
            $data2 = JenisPerizinan::where('id_m_jenis_perizinan',"1")->first();
            $izin_prinsip = IzinPrinsip::where('id_permohonan',substr($id_jenis,13))->first();
            if ($izin_prinsip) {
                $hhe = new HitungHariEstimasi;
                $hhk = new HitungHariKerja;
                $estimasi = $hhe->count($izin_prinsip->tgl_pengajuan,$data2->sop);
                $data2->status = $izin_prinsip->tgl_disetujui?
                    ($data2->sop?(substr($izin_prinsip->tgl_disetujui,0,10) > $estimasi?TrackingRegisterDetail::DELAY:TrackingRegisterDetail::ON_TIME)
                    :TrackingRegisterDetail::ON_TIME)
                    :TrackingRegisterDetail::ON_GOING_BIASA;
                $data2->tanggal_pengajuan = getFullDate($izin_prinsip->tgl_pengajuan);
                $data2->tanggal_selesai_estimasi = $data2->sop?getFullDate($estimasi):"-";
                $data2->tanggal_selesai = getFullDate($izin_prinsip->tgl_disetujui);
                $data2->sop = $data2->sop?$data2->sop." Hari":"-";
                $data2->lama_proses = $izin_prinsip->tgl_disetujui?$hhk->count($izin_prinsip->tgl_pengajuan,$izin_prinsip->tgl_disetujui)." Hari":"-";
            } else {
                $data2->status = "BELUM DIAJUKAN";
                $data2->tanggal_pengajuan = "-";
                $data2->tanggal_selesai_estimasi = "-";
                $data2->tanggal_selesai = "-";
                $data2->sop = "-";
                $data2->lama_proses = "-";
            }
        } else if (strpos($id_jenis,"IZIN_USAHA_") === 0) {
            $izin_usahas = IzinUsaha::where('id_permohonan_parent',$id)->get();
            $data2 = JenisPerizinan::where('id_m_jenis_perizinan',"165")->first();
            $data2->sop = $data2->sop?$data2->sop." Hari":"-";
            if (!$izin_usahas->isEmpty()) {
                $iu = [];
                foreach ($izin_usahas as $izin_usaha) {
                    $data3 = new \stdClass;
                    $hhe = new HitungHariEstimasi;
                    $estimasi = $hhe->count($izin_usaha->tgl_pengajuan,$data2->sop);
                    $data3->id_permohonan = $izin_usaha->id_permohonan;
                    $data3->tanggal_pengajuan = getFullDate($izin_usaha->tgl_pengajuan);
                    $data3->tanggal_selesai_estimasi = $data2->sop?getFullDate($estimasi):"-";
                    $data3->tanggal_selesai = getFullDate($izin_usaha->tgl_disetujui);
                    if ($izin_usaha->tgl_disetujui) {
                        $data3->status = $izin_usaha->tgl_disetujui?
                            ($data2->sop?(substr($izin_usaha->tgl_disetujui,0,10) > $estimasi?TrackingRegisterDetail::DELAY:TrackingRegisterDetail::ON_TIME)
                            :TrackingRegisterDetail::ON_TIME)
                            :TrackingRegisterDetail::ON_GOING_BIASA;
                        $hhk = new HitungHariKerja;
                        $data3->lama_proses = $hhk->count($izin_usaha->tgl_pengajuan,$izin_usaha->tgl_disetujui)." Hari";
                    } else {
                        $data3->status = TrackingRegisterDetail::ON_GOING_BIASA;
                        $data3->lama_proses = "-";
                    }
                    $iu[] = $data3;
                }
                if (count($iu) == 1) {
                    $data2->tanggal_pengajuan = $iu[0]->tanggal_pengajuan;
                    $data2->tanggal_selesai_estimasi = $iu[0]->tanggal_selesai_estimasi;
                    $data2->tanggal_selesai = $iu[0]->tanggal_selesai;
                    $data2->status = $iu[0]->status;
                    $data2->lama_proses = $iu[0]->lama_proses;
                } else {
                    for ($i = 0; $i < count($iu); $i++) {
                        $data2->tanggal_pengajuan .= $iu[$i]->id_permohonan.": ".$iu[$i]->tanggal_pengajuan."<br>";
                        $data2->tanggal_selesai_estimasi .= $iu[$i]->id_permohonan.": ".$iu[$i]->tanggal_selesai_estimasi."<br>";
                        $data2->tanggal_selesai .= $iu[$i]->id_permohonan.": ".$iu[$i]->tanggal_selesai."<br>";
                        $data2->status .= $iu[$i]->id_permohonan.": ".$iu[$i]->status."<br>";
                        $data2->lama_proses .= $iu[$i]->id_permohonan.": ".$iu[$i]->lama_proses."<br>";
                    }
                    $data2->tanggal_pengajuan = substr($data2->tanggal_pengajuan,0,-4);
                    $data2->tanggal_selesai_estimasi = substr($data2->tanggal_selesai_estimasi,0,-4);
                    $data2->tanggal_selesai = substr($data2->tanggal_selesai,0,-4);
                    $data2->status = substr($data2->status,0,-4);
                    $data2->lama_proses = substr($data2->lama_proses,0,-4);
                }
            } else {
                $data2->status = "BELUM DIAJUKAN";
                $data2->tanggal_pengajuan = "-";
                $data2->tanggal_selesai_estimasi = "-";
                $data2->tanggal_selesai = "-";
                $data2->sop = "-";
                $data2->lama_proses = "-";
            }
        } else {
            $data2 = JenisPerizinan::where('id_m_jenis_perizinan',$id_jenis)->first();

            if (isset($npwp)) {
                $data = TrackingRegisterDetail::where('upper(npwp)',$npwp)->where('upper(id_m_jenis_perizinan)',$id_jenis)->first();
            } else {
                $data = TrackingRegisterDetail::where('upper(id_qrcode)',$id)->where('upper(id_m_jenis_perizinan)',$id_jenis)->where('id_m_flow_izin_terintegrasi',$id_m_flow_izin_terintegrasi)->first();
            }
            if($data) {
                $data2->status = $data->getStatus();
                $data2->tanggal_pengajuan = getFullDate($data->tanggal_pengajuan);
                $data2->tanggal_selesai_estimasi = getFullDate($data->tanggal_selesai_estimasi);
                $data2->nama_instansi = $data->nama_instansi;
                $data2->nama_direktorat = $data->nama_direktorat;
                $data2->nama_bidang_usaha = $data->nama_bidang_usaha;
                $data2->tanggal_selesai = getFullDate($data->tanggal_selesai);
                $data2->sop = $data->sop?$data->sop." Hari":"-";
                $data2->lama_proses = $data->lama_proses?$data->lama_proses." Hari":"-";
            }else {
                $data2->status = "BELUM DIAJUKAN";
                $data2->tanggal_pengajuan = "-";
                $data2->tanggal_selesai_estimasi = "-";
                $data2->tanggal_selesai = "-";
                $data2->sop = "-";
                $data2->lama_proses = "-";
            }
        }
        return response()->json($data2);
    }

    public function getDataTable()
    {
        $id = strtoupper(Request::get('id_qrcode',''));
        $id_m_flow_izin_terintegrasi = strtoupper(Request::get('id_m_flow_izin_terintegrasi',''));
        $array = array();
        
        $izin_prinsip = IzinPrinsip::where('id_permohonan',$id)->first();
        if ($izin_prinsip) {
            $hhe = new HitungHariEstimasi;
            $data2 = JenisPerizinan::where('id_m_jenis_perizinan',"1")->first();
            $estimasi = $hhe->count($izin_prinsip->tgl_pengajuan,$data2->sop);
            $status = $izin_prinsip->tgl_disetujui?
                ($data2->sop?(substr($izin_prinsip->tgl_disetujui,0,10) > $estimasi?TrackingRegisterDetail::DELAY:TrackingRegisterDetail::ON_TIME)
                :TrackingRegisterDetail::ON_TIME)
                :TrackingRegisterDetail::ON_GOING_BIASA;
        }
        $array['Badan Koordinasi Penanaman Modal  '][] = [
            "status" => $izin_prinsip?($izin_prinsip->tgl_disetujui?$status:TrackingRegisterDetail::ON_GOING_BIASA):"BELUM DIAJUKAN",
            "izin" => "Izin Prinsip",
            "id" => "izin_prinsip_{$id}"
        ];
        $detail_izin = FlowDetailIzin::where('id_m_flow_izin_terintegrasi', $id_m_flow_izin_terintegrasi)->with('jenis_perizinan.direktorat.instansi')->get();
        foreach($detail_izin as $izin) {
            $result = TrackingRegisterDetail::where('upper(id_qrcode)',$id)
                ->where('upper(id_m_jenis_perizinan)',$izin->id_m_jenis_perizinan)
                ->where('upper(id_m_flow_izin_terintegrasi)',$id_m_flow_izin_terintegrasi)->first();  
            if($result) {
                ($izin->jenis_perizinan?'':dd($izin));
                $resultArray = array( "status"=>$result->getStatus(),"izin"=>$izin->jenis_perizinan->nama_jenis_perizinan,"id"=>$izin->jenis_perizinan->id_m_jenis_perizinan);
                $array[$izin->jenis_perizinan->direktorat->instansi->nama_instansi][] = $resultArray;
            }else {
                ($izin->jenis_perizinan?'':dd($izin));
                $resultArray = array( "status"=>"Belum Diajukan","izin"=>$izin->jenis_perizinan->nama_jenis_perizinan,"id"=>$izin->jenis_perizinan->id_m_jenis_perizinan);
                $array[$izin->jenis_perizinan->direktorat->instansi->nama_instansi][] = $resultArray;
            }
        }
        $izin_usahas = IzinUsaha::where('id_permohonan_parent',$id)->get();
        if (!$izin_usahas->isEmpty()) {
            $status = TrackingRegisterDetail::ON_GOING_BIASA;
            foreach ($izin_usahas as $izin_usaha) {
                if ($izin_usaha->tgl_disetujui) {
                    $status = TrackingRegisterDetail::ON_TIME;
                    break;
                }
            }
        } else {
            $status = "Belum Diajukan";
        }
        $array['Badan Koordinasi Penanaman Modal '][] = [
            "status" => $status,
            "izin" => "Izin Usaha",
            "id" => "izin_usaha_{$id}"
        ];
        return response()->json($array);
    }



}