<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Excel;

use App\Http\Controllers\Controller;
use App\Model\TrackingRegisterDetail;
use App\Model\JenisPerizinan;
use App\Model\Libur;
use Faker\Factory as Faker;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use App\Services\HitungHariLibur;
use App\Services\HitungHariKerja;
use App\Services\HitungHariEstimasi;

class PtspTrackingRegisterController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin,monitoring,ptsp,search_check,edit_check',['except'=>['debug','search','export']]);
        $this->middleware('role:admin,monitoring,ptsp,search_check,edit_check,check_in,check_out',['only'=>['debug','search','export']]);
        \App::setLocale(getLang());
        createSidebar();
    }

    public function index()
    {
        return view('tracking_register_ptsp.index');
    }

    public function cari($status = null)
    {
        return view('tracking_register_ptsp.cari',compact('status'));
    }
    
    public function debug()
    {
        return view('tracking_register_ptsp.debug');
    }
    
    public function search(Request $request) 
    {
        $params = $request->get('params',false);
        $table = new Collection;
        if (!$params) {
            $trackings = TrackingRegisterDetail::get();
        } else {
            $query = TrackingRegisterDetail::orderBy('tanggal_pengajuan');
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    case 'qrcode':
                        $query = $query->where('upper(ID_QRCODE)','=',strtoupper($search));
                        break;
                    case 'performance':
                        if ($search == 'O')
                            $query = $query->where(function($q) {
                                $q = $q->where('performance','I')->orWhere('performance','O');
                            });
                        else if ($search == 'D') 
                            $query = $query->where($key,'=',$search);
                        else if ($search == 'on_going') 
                            $query = $query->whereNull('tanggal_selesai')->where('tanggal_threshold','>',date('Y-m-d H:i:s'));
                        else if ($search == 'on_going_delay') 
                            $query = $query->whereNull('tanggal_selesai')->where('tanggal_threshold','<',date('Y-m-d H:i:s'));
                        else if ($search == 'on_going_warning') 
                            $query = $query->whereNull('tanggal_selesai')->where('tanggal_warning','<',date('Y-m-d H:i:s'))->where('tanggal_threshold','>',date('Y-m-d H:i:s'));
                        break;
                    case 'tanggal_pengajuan_dari':
                    case 'tanggal_selesai_dari':
                    case 'tanggal_selesai_estimasi_dari':
                        $query = $query->where(substr($key,0,-5),'>=',$search);
                        break;
                    case 'tanggal_pengajuan_sampai':
                    case 'tanggal_selesai_sampai':
                    case 'tanggal_selesai_estimasi_sampai':
                        $query = $query->where(substr($key,0,-7),'<=',date('Y-m-d',strtotime("+1 day",strtotime($search))));
                        break;
                    case 'id_m_jenis_perizinan':
                    case 'id_m_instansi':
                    case 'id_m_direktorat':
                        $query = $query->where($key,'=',$search);
                        break;
                    case 'nama_perusahaan':
                        continue;
                        break;
                    default:
                        $query = $query->where($key,'like',"%$search%");
                        break;
                }
            }
            $trackings = $query->get();
        }
        foreach($trackings as $tracking) {
            $table->push([
                'id_qrcode'         =>  $tracking->id_qrcode,
                'nama_jenis_perizinan'=> $tracking->nama_jenis_perizinan,
                'nama_instansi'     => $tracking->nama_instansi,
                'nama_perusahaan'   => $tracking->nama_perusahaan,
                'tanggal_masuk'     => '<span class="hidden">'.$tracking->tanggal_pengajuan.'</span>'.getFullDate($tracking->tanggal_pengajuan),
                'tanggal_selesai'   => '<span class="hidden">'.($tracking->tanggal_selesai?$tracking->tanggal_selesai:'0000-00-00').'</span>'.getFullDate($tracking->tanggal_selesai),
                'tanggal_estimasi'  => '<span class="hidden">'.$tracking->tanggal_selesai_estimasi.'</span>'.getFullDate($tracking->tanggal_selesai_estimasi),                
                'sla' => $tracking->sop?str_pad($tracking->sop,4,' ',STR_PAD_LEFT)." Hari":"-",
                'status' => $tracking->getStatus(),
                'edit' => '<a href="'.url('tracking/lembaran?id_tracking_register_detail='.$tracking->id_tracking_register_detail).'"><i class="fa fa-download"></i>&nbsp;file</a><br>'.(isRole('edit_check')?
                           '<a href="'.url('trackingregister/edit/'.$tracking->id_tracking_register_detail).'"><i class="fa fa-edit"></i>&nbsp;edit</a><br>
                           <a href="'.url('trackingregister/delete/'.$tracking->id_tracking_register_detail).'" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;delete</a>':''),
            ]);
        }
        //tgl keluar kalau ga ada belum selesai status
        //tgl keluar kalau uda ada selesai statusnya
        //tgl estimasi - tgl masuk = hari normal- hari libur liat weekend
        
        return Datatables::of($table)->make(true);
    }
    
    public function export(Request $request) 
    {
        $params = $request->all();
        if (!$params) {
            $trackings = TrackingRegisterDetail::get();
        } else {
            $query = TrackingRegisterDetail::orderBy('tanggal_pengajuan');
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    case 'qrcode':
                        $query = $query->where('upper(ID_QRCODE)','=',strtoupper($search));
                        break;
                    case 'performance':
                        if ($search == 'O')
                            $query = $query->where(function($q) {
                                $q = $q->where('performance','I')->orWhere('performance','O');
                            });
                        else if ($search == 'D') 
                            $query = $query->where($key,'=',$search);
                        else if ($search == 'on_going') 
                            $query = $query->whereNull('performance')->where('tanggal_threshold','>',date('Y-m-d'));
                        else if ($search == 'on_going_delay') 
                            $query = $query->whereNull('performance')->where('tanggal_threshold','<',date('Y-m-d'));
                        else if ($search == 'on_going_warning') 
                            $query = $query->whereNull('performance')->where('tanggal_warning','<',date('Y-m-d'))->where('tanggal_threshold','>',date('Y-m-d H:i:s'));
                        break;
                    case 'tanggal_pengajuan_dari':
                    case 'tanggal_selesai_dari':
                    case 'tanggal_selesai_estimasi_dari':
                        $query = $query->where(substr($key,0,-5),'>=',$search);
                        break;
                    case 'tanggal_pengajuan_sampai':
                    case 'tanggal_selesai_sampai':
                    case 'tanggal_selesai_estimasi_sampai':
                        $query = $query->where(substr($key,0,-7),'<=',date('Y-m-d',strtotime("+1 day",strtotime($search))));
                        break;
                    case 'id_m_jenis_perizinan':
                    case 'id_m_instansi':
                    case 'id_m_direktorat':
                        $query = $query->where($key,'=',$search);
                        break;
                    case 'nama_perusahaan':
                        continue;
                        break;
                    default:
                        $query = $query->where($key,'like',"%$search%");
                        break;
                }
            }
            $trackings = $query->get();
        }
        Excel::create("rekap pengajuan", function($excel) use ($trackings) {
            $excel->sheet('Daftar Pengajuan', function($sheet) use ($trackings) {
                $sheet->setFontFamily('Tahoma');
                $sheet->setFontSize(12);
                $sheet->setWidth(array(
                    'A'     =>  6.86  * 1,
                    'B'     =>  20 * 1,
                    'C'     =>  20 * 1,
                    'D'     =>  20 * 1,
                    'E'     =>  20 * 1,
                    'F'     =>  20 * 1,
                    'G'     =>  20 * 1,
                    'H'     =>  20 * 1,
                    'I'     =>  20 * 1,
                ));
                $sheet->mergeCells('A1:I1');
                $sheet->cell('A1', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(18);
                });
                $sheet->row(1, ['Daftar Pengajuan PTSP Pusat']);
                $sheet->row(4, [
                    'NO',
                    'NAMA PERIZINAN',
                    'INSTANSI',
                    'PERUSAHAAN',
                    'TANGGAL MULAI',
                    'TANGGAL SELESAI',
                    'TANGGAL ESTIMASI',
                    'SLA',
                    'STATUS',
                ]);

                $sheet->cells('A4:I4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setValignment('center');
                });
                
                $rownum = 5;
                $i = 1;
                foreach ($trackings as $tracking) {
                    $sheet->row($rownum,[
                        $i,
                        $tracking->nama_jenis_perizinan,
                        $tracking->nama_instansi,
                        $tracking->nama_perusahaan,
                        str_replace("&nbsp;"," ",getFullDate($tracking->tanggal_pengajuan)),
                        str_replace("&nbsp;"," ",getFullDate($tracking->tanggal_selesai)),
                        str_replace("&nbsp;"," ",getFullDate($tracking->tanggal_selesai_estimasi)),
                        $tracking->sop?$tracking->sop." Hari":"-",
                        $tracking->getStatus()
                    ]);
                    $sheet->setHeight($rownum, -1);
                    $rownum++;
                    $i++;
                }
                $sheet->cells('A5:I'. $sheet->getHighestRow(), function($cells) {
                    $cells->setValignment('top');
                });
                $sheet->getStyle('A5:I' . $sheet->getHighestRow())
                    ->getAlignment()->setWrapText(true);
                $sheet->setBorder('A4:I'.($rownum-1), 'thin');
                $sheet->setFreeze('A5');
                $sheet->setPageMargin(0.25);
                $sheet->getSheetView()->setZoomScale(80);
                $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');
    }

    public function searchFaker(Request $request) 
    {
        $params = $request->get('params',[]);
        $users = new Collection;
        $faker = Faker::create();

        for ($i = 0; $i < 100; $i++) {
            $users->push([
                'instansi'         =>  $params['qrcode']+ 1,
                'status'       => $faker->name,
                'tanggal_masuk' => $faker->time($format = 'H:i:s', $max = 'now') ,
                'tanggal_keluar' => $faker->time($format = 'H:i:s', $max = 'now') 
            ]);
        }

        return Datatables::of($users)->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $tracking = TrackingRegisterDetail::find($id);
        return view('tracking_register_ptsp.edit', compact('tracking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $tracking = TrackingRegisterDetail::find($id);
        $tracking->tanggal_pengajuan = $req->tanggal_pengajuan;
        $tracking->tanggal_selesai = $req->tanggal_selesai == "1970-01-01" || !$req->tanggal_selesai || $req->tanggal_selesai == ""
             ? null : $req->tanggal_selesai;
        
        $jenis_izin = JenisPerizinan::find($tracking->id_m_jenis_perizinan);
        if (!$jenis_izin) {
            return response()->json(['success'=>false,'msg'=>'Jenis izin tidak ditemukan']);
        }
        $hhe = new HitungHariEstimasi;
        $estimasi = $jenis_izin->sop=="0"?null:$hhe->count($tracking->tanggal_pengajuan,$jenis_izin->sop);
        $dwarning = $jenis_izin->sop=="0"?null:$hhe->count($tracking->tanggal_pengajuan,floor($jenis_izin->sop*0.7));
        $tracking->tanggal_selesai_estimasi = $estimasi;
        $tracking->tanggal_threshold = $estimasi;
        $tracking->tanggal_warning = $dwarning;
        if ($tracking->tanggal_selesai) {
            if (date("Y-m-d",strtotime($tracking->tanggal_selesai)) <= date("Y-m-d",strtotime($tracking->tanggal_threshold))) {
                $tracking->performance = "O";
            } else {
                $tracking->performance = "D";
            }
            $hhk = new HitungHariKerja;
            $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$id)->orderBy('tanggal_stop','desc')->first();
            if ($trds) {
                $sop_used = $tracking->lama_proses?$tracking->lama_proses:0;
                $sop_used+= $hhk->count($trds->tanggal_restart,date('Y-m-d H:i:s'));
            } else {
                $sop_used = $hhk->count($tracking->tanggal_pengajuan,date('Y-m-d H:i:s'));
            }
            $tracking->lama_proses = $sop_used;
        } else {
            $tracking->performance = null;
        }
        if ($req->komentar_check_in) {
            $tracking->komentar_check_in = $req->komentar_check_in;
        }
        if ($req->komentar_check_out) {
            $tracking->komentar_check_out = $req->komentar_check_out;
        }
        if ($req->komentar_stop) {
            $stop = $tracking->trd_stop()->first();
            $stop->alasan_stop = $req->komentar_stop;
            $stop->save();
        }
        $tracking->save();

        return redirect()->back()->with('message','Edit data Tracking Register berhasil');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete(Request $req, $id)
    {
        $tracking = TrackingRegisterDetail::find($id);
        if (!$tracking) {
            return response()->json(['success'=>false,'msg'=>'Tracking tidak ditemukan']);
        }
        $tracking->delete();

        return redirect()->back()->with('message','Hapus Tracking Register berhasil');
    }

}
