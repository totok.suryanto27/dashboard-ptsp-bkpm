<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Libur;

class HariLiburController extends Controller
{
   

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('libur.index');
    }

    public function getData(){
        $libur = Libur::all();
        return Datatables::of($libur)
        ->editColumn('tanggal', function ($libur) {
                return getFullDate($libur->tanggal);
            })
        ->addColumn('actions','
                        <a href="{{ url( \'libur\read\',$tanggal )}}"><i class="fa fa-search"></i>&nbsp;Lihat</a>
                        <a href="{{ url( \'libur\edit\',$tanggal )}}"><i class="fa fa-edit"></i>&nbsp;Ubah</a>
                        <a href="{{ url( \'libur\delete\',$tanggal ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('libur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $libur = New Libur;
        $libur->tahun = $req->tahun;
        $libur->tanggal = $req->tanggal;
        $libur->deskripsi = $req->deskripsi;
        $libur->save();

        return redirect('libur/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $libur = Libur::find($id);
        return view('libur.view', compact('libur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $libur = Libur::find($id);
        return view('libur.edit', compact('libur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $libur = Libur::find($id);
        $libur->tahun = $req->tahun;
        $libur->deskripsi = $req->deskripsi;
        $libur->save();


        return redirect('libur/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       Libur::find($id)->delete();
        return redirect()->back();
    }
}
