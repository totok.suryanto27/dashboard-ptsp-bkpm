<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Navigator;
use App\UserLog;
use App\UserHits;

use DB;

class MonitoringPenggunaController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function __construct() {
        $this->middleware('auth');
        \App::setLocale(getLang());
        createSidebar();
        
        if (\Auth::user()) {
            
            $hits = new UserHits;
            $hits->page = 'Monitoring Pengguna';
            // $hits->id_user_log = getUserId();
            $hits->id_user_log = getUserId();
            $hits->bulan = \Carbon\Carbon::now()->month;
            $hits->tahun = \Carbon\Carbon::now()->year;
            $hits->status = 1;
            $hits->save();
            
            UserLog::where('id_m_user', getUserId())->where('id', UserLog::orderBy('id', 'desc')->first()->id)->update(['hits' => UserHits::where(['id_user_log' => getUserId(), 'status' => 1])->count() ]);
        }
    }
    public function index() {
        Navigator::setActive(url('monitoring-pengguna'));
        $auth = \Auth::user();
        $query = "SELECT ID_M_USER,sum(HITUNG_MENIT) as Menit,sum(HITUNG_DETIK) as Detik,sum(HITUNG_JAM) as Jam
                    FROM ".TABLESPACE.".USER_LOGS
                    GROUP BY ID_M_USER";
        $data = DB::select(DB::raw($query));
        $list_durasi = array();
        foreach ($data as $general) {
            $detik = $general->detik;
            $detik_ke_menit = $detik/60;
            $sisa_detik = $detik%60;
          //  echo $detik." detik = ".floor($detik_ke_menit)." menit : ".$sisa_detik." detik<Br>";
            $menit = $general->menit+floor($detik_ke_menit);
            $menit_ke_jam = $menit/60;
            $sisa_menit = $menit%60;

            $total_menit = ( $general->jam*60 ) + $menit ;
            $jams = $general->jam + floor($menit_ke_jam);
            $s = array('nama' => $general->id_m_user,
            'waktu_aktif' =>$jams." jam  ".$sisa_menit." menit ".$sisa_detik." detik",
            'total_menit' => $total_menit
             );
            array_push($list_durasi, $s);
        }

        return view('pages.monitoring-pengguna', compact('list_durasi'));
        //return $data;
    }

    public function getGeneralData(){
        $query = "SELECT ID_M_USER,sum(HITUNG_MENIT) as Menit,sum(HITUNG_DETIK) as Detik,sum(HITUNG_JAM) as Jam
                    FROM ".TABLESPACE.".USER_LOGS
                    GROUP BY ID_M_USER";
        $data = DB::select(DB::raw($query));
        $x = array();
        foreach ($data as $general) {
            $detik = $general->detik;
            $detik_ke_menit = $detik/60;
            $sisa_detik = $detik%60;
          //  echo $detik." detik = ".floor($detik_ke_menit)." menit : ".$sisa_detik." detik<Br>";
            $menit = $general->menit+floor($detik_ke_menit);
            $menit_ke_jam = $menit/60;
            $sisa_menit = $menit%60;

            $total_menit = ( $general->jam*60 ) + $menit ;
            $jams = $general->jam + floor($menit_ke_jam);
            $s = array('nama' => $general->id_m_user,
            'waktu aktif' =>$jams." jam  ".$sisa_menit." menit ".$sisa_detik." detik",
            'total_menit' => $total_menit
             );
            array_push($x, $s);
        }

        $haha = array('data' => $x,'total'=>sizeof($x));
        return $haha;            
    }

    public function getBulanan(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $current_month = \Carbon\Carbon::now()->month;
        $id_m_user = '-';
        if ($request->input('bulan')) {
            $current_month = $request->input('bulan');
        }
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        if ($request->input('user')) {
            $id_m_user = $request->input('user');
        }
        $sql ="SELECT TRUNC(created_at,'DD') as tes,
        sum(HITUNG_MENIT) as Menit,
        sum(HITUNG_DETIK) as Detik,sum(HITUNG_JAM) as Jam
        FROM ".TABLESPACE.".USER_LOGS where TAHUN = ".$current_year." and ID_M_USER = '".$id_m_user."' AND BULAN = ".$current_month."
        GROUP BY TRUNC(created_at,'DD')";
        $data = DB::select(DB::raw($sql));

        $jml_february = 28;
        if ($current_year % 4 == 0) {
            $jml_february = 29;
        }

        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari','total_hari'=>31),
            array('no'=>2,'bulan' => 'Februari','total_hari'=>$jml_february),
            array('no'=>3,'bulan' => 'Maret','total_hari'=>31),
            array('no'=>4,'bulan' => 'April','total_hari'=>30),
            array('no'=>5,'bulan' => 'Mei','total_hari'=>31),
            array('no'=>6,'bulan' => 'Juni','total_hari'=>30),
            array('no'=>7,'bulan' => 'Juli','total_hari'=>31),
            array('no'=>8,'bulan' => 'Agustus','total_hari'=>31),
            array('no'=>9,'bulan' => 'September','total_hari'=>30),
            array('no'=>10,'bulan' => 'Oktober','total_hari'=>31),
            array('no'=>11,'bulan' => 'November','total_hari'=>30),
            array('no'=>12,'bulan' => 'Desember','total_hari'=>31)
         );
       // echo $current_month;
        $jml_hari = $list_bulan[$current_month-1]['total_hari'];
        $return_value = array();
         for ($i=1; $i <= $jml_hari ; $i++) { 
            $value = 0;
             foreach ($data as $d) {
                $tanggal = substr($d->tes,8,2);
                $tanggal_int = (int)$tanggal;
                if ($i == $tanggal_int) {
                    $detik = $d->detik;
                    $detik_ke_menit = $detik/60;
                    $sisa_detik = $detik%60;
                    $menit = $d->menit+floor($detik_ke_menit);
                    $value = ( $d->jam*60 ) + $menit ;
                }
            }

            $obj = array('hari' =>$i,'total_menit'=>$value);
            array_push($return_value,$obj);
         }
        
         echo json_encode($return_value);

    }


    public function getTahunan(Request $request){
        $current_year = \Carbon\Carbon::now()->year;
        $id_m_user = '-';
        if ($request->input('tahun')) {
            $current_year = $request->input('tahun');
        }

        if ($request->input('user')) {
            $id_m_user = $request->input('user');
        }

        $list_bulan = array(
            array('no'=>1,'bulan' => 'Januari'),
            array('no'=>2,'bulan' => 'Februari'),
            array('no'=>3,'bulan' => 'Maret'),
            array('no'=>4,'bulan' => 'April'),
            array('no'=>5,'bulan' => 'Mei'),
            array('no'=>6,'bulan' => 'Juni'),
            array('no'=>7,'bulan' => 'Juli'),
            array('no'=>8,'bulan' => 'Agustus'),
            array('no'=>9,'bulan' => 'September'),
            array('no'=>10,'bulan' => 'Oktober'),
            array('no'=>11,'bulan' => 'November'),
            array('no'=>12,'bulan' => 'Desember')
         );

        $query = "SELECT BULAN,sum(HITUNG_MENIT) as Menit,sum(HITUNG_DETIK) as Detik,sum(HITUNG_JAM) as Jam
                FROM ".TABLESPACE.".USER_LOGS where TAHUN = ".$current_year." and ID_M_USER = '".$id_m_user."'
                GROUP BY BULAN";
        $data = DB::select(DB::raw($query));
        $return_value = [];
        foreach ($list_bulan as $key) {
            $no_bln =  $key['no'];
            $value = 0;
            foreach ($data as $d) {
                if ($no_bln == $d->bulan) {
                    $detik = $d->detik;
                    $detik_ke_menit = $detik/60;
                    $sisa_detik = $detik%60;
                    $menit = $d->menit+floor($detik_ke_menit);
                    $value = ( $d->jam*60 ) + $menit ;
                }
            }

            $rows = array('bulan' =>  $key['bulan'],'total_menit'=>$value);
            array_push($return_value,$rows);
        }
        echo json_encode($return_value);
    }
     
    public function getUserHits3(Request $request) {
        $auth = \Auth::user();
        //by user
        $allDuration = DB::table(TABLESPACE.'USER_LOGS')->select(DB::raw('SUM(hitung_jam) as total_jam'), DB::raw('SUM(hitung_menit) as total_menit'), DB::raw('SUM(hitung_detik) as total_detik'), 'id_m_user')->where('status', '=', 0)->groupBy('id_m_user')->having('id_m_user', '=', $request->input('user'))->get();
        $getDurations = $this->getDurations();
        $i = 0;
        foreach ($allDuration as $val) {
            $val->durasi = toMenit($val->total_jam, $val->total_menit, $val->total_detik);
            $val->formatDurasi = formatDurasi($val->total_jam, $val->total_menit, $val->total_detik);
            if ($getDurations[$i]->id_m_user == $val->id_m_user) {
                $val->jam_login = $getDurations[$i]->jam_login;
                $val->jam_logout = $getDurations[$i]->jam_logout;
                $val->hits = $getDurations[$i]->total_hits;
            }
            $i++;
        }
        
        return $allDuration;
    }
    
    public function detail($id) {
        
        //query database for the id;
        Navigator::setActive(url('monitoring-pengguna'));
        $year = \Carbon\Carbon::now()->year;
        $month = \Carbon\Carbon::now()->month;
        
        // return $month;
        return view('monitoring-pengguna.detail', compact('id', 'year', 'month'));
    }
    
    public function getUserHits2(Request $request) {
        $query = "SELECT USER_LOGS.id_m_user, SUM(USER_LOGS.hits) as total_hits, bulan, SUM(USER_LOGS.hitung_jam) as total_jam, SUM(USER_LOGS.hitung_menit) as total_menit, SUM(USER_LOGS.hitung_detik) as total_detik FROM ".TABLESPACE.".USER_LOGS, M_USER WHERE USER_LOGS.id_m_user = M_USER.id_m_user GROUP BY USER_LOGS.id_m_user, USER_LOGS.bulan, USER_LOGS.tahun HAVING USER_LOGS.tahun = '" . \Carbon\Carbon::now()->year . "'";
        if ($request->input('user')) {
            $query = "SELECT USER_LOGS.id_m_user, SUM(USER_LOGS.hits) as total_hits, bulan, SUM(USER_LOGS.hitung_jam) as total_jam, SUM(USER_LOGS.hitung_menit) as total_menit, SUM(USER_LOGS.hitung_detik) as total_detik FROM ".TABLESPACE.".USER_LOGS, M_USER WHERE USER_LOGS.id_m_user = M_USER.id_m_user GROUP BY USER_LOGS.id_m_user, USER_LOGS.bulan, USER_LOGS.tahun HAVING USER_LOGS.tahun = '" . \Carbon\Carbon::now()->year . "' AND USER_LOGS.id_m_user = '" . $request->input('user') . "'";
        }
        
        if ($request->input('user') && $request->input('bulan')) {
            $query = "SELECT bulan, tahun, USER_LOGS.id_m_user, USER_LOGS.jam_logout, USER_LOGS.ts, SUM(USER_LOGS.hits) as total_hits, bulan, SUM(USER_LOGS.hitung_jam) as total_jam, SUM(USER_LOGS.hitung_menit) as total_menit, SUM(USER_LOGS.hitung_detik) as total_detik FROM ".TABLESPACE.".USER_LOGS, M_USER WHERE USER_LOGS.id_m_user = M_USER.id_m_user GROUP BY USER_LOGS.id_m_user, USER_LOGS.jam_logout, USER_LOGS.bulan, USER_LOGS.tahun, USER_LOGS.ts HAVING USER_LOGS.tahun = '" . \Carbon\Carbon::now()->year . "' AND USER_LOGS.id_m_user = '" . $request->input('user') . "' AND USER_LOGS.bulan = '" . $request->input('bulan') . "'";
        }
        
        if ($request->input('user') && $request->input('tahun')) {
            $query = "SELECT USER_LOGS.id_m_user, SUM(USER_LOGS.hits) as total_hits, bulan, SUM(USER_LOGS.hitung_jam) as total_jam, SUM(USER_LOGS.hitung_menit) as total_menit, SUM(USER_LOGS.hitung_detik) as total_detik FROM ".TABLESPACE.".USER_LOGS, M_USER WHERE USER_LOGS.id_m_user = M_USER.id_m_user AND tahun = '" . $request->input('tahun') . "' AND USER_LOGS.id_m_user = '" . $request->input('user') . "' GROUP BY bulan, USER_LOGS.id_m_user";
        }
        
        $list_user = DB::table(TABLESPACE.'M_USER')->get();
        $hits = DB::select(DB::raw($query));
        
        foreach ($hits as $val) {
            $val->total_durasi = ($val->total_jam * 60) + $val->total_menit + ($val->total_detik / 60);
            if ($request->input('user') && $request->input('bulan')) {
                $val->tanggal = date('j', strtotime($val->jam_logout));
                
                $val->jumlahHari = cal_days_in_month(CAL_GREGORIAN, $val->bulan, $val->tahun);
            }
        }
        
        $result = array('hits' => $hits, 'users' => $list_user);
        return $result;
    }
    
    public function getUserHits(Request $request) {
        // $list_user = DB::table(TABLESPACE.'M_USER')->get();
        // $hits = $this->getDurations();
        // $result = array('hits' => $hits, 'users' => $list_user);
        // return $result;
        $auth = \Auth::user();
        //by user
        $allDuration = DB::table(TABLESPACE.'USER_LOGS')->select(DB::raw('SUM(hitung_jam) as total_jam'), DB::raw('SUM(hitung_menit) as total_menit'), DB::raw('SUM(hitung_detik) as total_detik'), 'id_m_user')->where('status', '=', 0)->groupBy('id_m_user')->get();
        $getDurations = $this->getDurations();
        $i = 0;
        foreach ($allDuration as $val) {
            $val->durasi = toMenit($val->total_jam, $val->total_menit, $val->total_detik);
            $val->formatDurasi = formatDurasi($val->total_jam, $val->total_menit, $val->total_detik);
            if ($getDurations[$i]->id_m_user == $val->id_m_user) {
                $val->jam_login = $getDurations[$i]->jam_login;
                $val->jam_logout = $getDurations[$i]->jam_logout;
                $val->hits = $getDurations[$i]->total_hits;
            }
            $i++;
        }
        return $allDuration;
    }
    
    public function create() {
        
        return $this->getDurations();
    }
    
    private function getDurations() {
        
        // $logs = DB::select(DB::raw("SELECT USER_LOGS.id_m_user, bulan, SUM(hits) as total_hits, SUM(USER_LOGS.ts) AS total_act, jam_login FROM ".TABLESPACE.".USER_LOGS  GROUP BY USER_LOGS.id_m_user, bulan, tahun, hits, jam_login HAVING tahun = '" . \Carbon\Carbon::now()->year . "' AND SUM(ts) > 0 "));
        
        $logs = DB::table(TABLESPACE.'USER_LOGS')->select('M_USER.id_m_user', DB::raw('SUM(USER_LOGS.ts) as total_act'), DB::raw('SUM(USER_LOGS.hits) as total_hits'))->join('M_USER', 'M_USER.id_m_user', '=', 'USER_LOGS.id_m_user')->where('tahun', '=', \Carbon\Carbon::now()->year)->where('USER_LOGS.status', '=', 0)->groupBy('M_USER.id_m_user')->get();
        
        foreach ($logs as $val) {
            $val->jam = date('H', $val->total_act);
            $val->menit = date('i', $val->total_act);
            $val->detik = date('s', $val->total_act);
        }
        
        $last_login = DB::table(TABLESPACE.'USER_LOGS')->select('M_USER.id_m_user', 'jam_logout', 'jam_login', DB::raw('SUM(USER_LOGS.ts) as total_act'), DB::raw('SUM(USER_LOGS.hits) as total_hits'))->join('M_USER', 'M_USER.id_m_user', '=', 'USER_LOGS.id_m_user')->where('tahun', '=', \Carbon\Carbon::now()->year)->where('USER_LOGS.status', '=', 0)->groupBy('M_USER.id_m_user', 'jam_logout', 'jam_login')->get();
        
        foreach ($last_login as $val_last) {
            foreach ($logs as $val_logs) {
                if ($val_last->id_m_user == $val_logs->id_m_user) {
                    $val_logs->jam_login = $val_last->jam_login;
                    $val_logs->jam_logout = $val_last->jam_logout;
                    $val_logs->durasi = hitung_durasi($val_last->jam_login, $val_last->jam_logout);
                }
            }
        }
        
        return $logs;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
        //
        
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
        //
        
        
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        //
        
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        //
        
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        //
        
        
    }
}
