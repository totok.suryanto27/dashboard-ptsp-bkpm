<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use File;
use App\Http\Requests;
use Response;
use App\Http\Controllers\Controller;
use App\Model\Instansi;

class InstansiController extends Controller
{
    private $columns = [
    'nama_instansi',
    'abbrv',
    'isdelated',
    'isbko',
    'filename',
    'filename_eng',
    'nama_instansi_eng'
    ]
    ;
    private $destination_id = 'sop_bahasa';
    private $destination_en = 'sop_english';
    private $destination_logo = 'images/logokem';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('instansi.index');
    }


    public function getData(){
        $instansi = Instansi::all();
        
        return Datatables::of($instansi)
        ->addColumn('actions','
                        <a href="{{ url( \'instansi\read\',$id_m_instansi )}}"><i class="fa fa-search"></i>&nbsp;read</a><br/>
                        <a href="{{ url( \'instansi\edit\',$id_m_instansi )}}"><i class="fa fa-edit"></i>&nbsp;edit</a><br/>
                        <a href="{{ url( \'instansi\delete\',$id_m_instansi ) }}" onclick="notifyConfirm(event)"><i class="fa fa-trash"></i>&nbsp;delete</a><br/>
                        ')
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('instansi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $instansi = New Instansi;
        $instansi->nama_instansi = $req->nama_instansi;
        $instansi->abbrv = $req->abbrv;
        $fileName = $req->file('filename');
        $fileName->move(public_path($this->destination_id), $fileName->getClientOriginalName() );
        $instansi->filename = $fileName->getClientOriginalName();
        $fileName_eng = $req->file('filename_eng');
        $fileName_eng->move(public_path($this->destination_en), $fileName_eng->getClientOriginalName() );
        $instansi->filename_eng = $fileName_eng->getClientOriginalName();
        $filename_logo = $req->file('filename_logo');
        $filename_logo->move(public_path($this->destination_logo), $filename_logo->getClientOriginalName() );
        $instansi->filename_logo = $filename_logo->getClientOriginalName();
        $instansi->nama_instansi_eng = $req->nama_instansi_eng;
        $instansi->dasar_hukum = $req->dasar_hukum;
        $instansi->isbko = $req->isbko;
        $instansi->save();

        return redirect('instansi/')->with(array('message'=>'Penyimpanan berhasil!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $instansi = Instansi::find($id);
        return view('instansi.view', compact('instansi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $instansi = Instansi::find($id);
        return view('instansi.edit', compact('instansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $instansi = Instansi::find($id);
        $instansi->nama_instansi = $req->nama_instansi;
        $instansi->abbrv = $req->abbrv;
        if($req->file('filename')) {
            File::delete(public_path($this->destination_id."/".$instansi->filename));
            $filename = $req->file('filename');
            if (!$instansi->filename || $instansi->filename == '')
                $instansi->filename = $filename->getClientOriginalName();
            $filename->move(public_path($this->destination_id),$instansi->filename );
        }
        if($req->file('filename_eng')) {
            File::delete(public_path($this->destination_en."/".$instansi->filename_eng));
            $filename_eng = $req->file('filename_eng');
            if (!$instansi->filename_eng || $instansi->filename_eng == '')
                $instansi->filename_eng = $filename_eng->getClientOriginalName();
            $filename_eng->move(public_path($this->destination_en),$instansi->filename_eng );
        }
        if($req->file('filename_logo')) {
            File::delete(public_path($this->destination_en."/".$instansi->filename_logo));
            $filename_logo = $req->file('filename_logo');
            if (!$instansi->filename_logo || $instansi->filename_logo == '')
                $instansi->filename_logo = $filename_logo->getClientOriginalName();
            $filename_logo->move(public_path($this->destination_logo),$instansi->filename_logo );
        }
        
        $instansi->nama_instansi_eng = $req->nama_instansi_eng;
        $instansi->dasar_hukum = $req->dasar_hukum;
        $instansi->isbko = $req->isbko;
        $instansi->save();


        return redirect('instansi/')->with(array('message'=>'Update berhasil!'));
    }

    public function getDownload($id) 
    {
        $instansi = Instansi::find($id);
        $file= public_path($this->destination_id."/".$instansi->filename);
        $headers = array( 'Content-Type: application/octet-stream',
            );
        return Response::download($file, $instansi->filename, $headers);
    }

    public function getDownloadEng($id) 
    {
        $instansi = Instansi::find($id);
        $file= public_path($this->destination_en."/".$instansi->filename_eng);
        $headers = array( 'Content-Type: application/octet-stream',
            );
        return Response::download($file, $instansi->filename_eng, $headers);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       Instansi::find($id)->delete();
        return redirect()->back()->with(array('message'=>'Delete berhasil!'));
    }
}
