<?php namespace App\Http\Controllers;

use Auth;
use Navigator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Gallery;
use Validator;
use Request;
use view;
use DB;
use Redirect;
use File;
use Input;
use Response;

class GalleryController extends Controller {

	public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('role:admin');
        \App::setLocale(getLang());
        createSidebar();
	}

	public function index()
	{
		$img=Gallery::all();
        return view('gallery.index',compact('img'));
	}

    public function imageList() {
        $imgList = array();
        $files = File::allfiles(public_path().'\uploads');

        foreach ($files as $file) {
            $name = $file->getFilename();
            array_push($imgList, array("title"=>str_replace( './', '',$name), "value"=>asset('uploads/'.str_replace( './', '',$name))));
        }
        return json_encode($imgList);
    } 

	public function uploadFiles() {

        $input = Input::all();

        $rules = array(
            'file' => 'image|max:3000',
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }

        $destinationPath = 'uploads'; // upload path
        $filename = Input::file('file')->getClientOriginalName();
        $extension_pos = strrpos($filename, '.');
        $fileName = substr($filename, 0, $extension_pos) ."-". str_random(10) . substr($filename, $extension_pos);
        $upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path

        if ($upload_success) {
            $img = new Gallery;
			$img->img_location=$fileName;
			$img->save();
			return Redirect::back()->with('message','Operation Successful !');
        } else {
            return Response::json('error', 400);
        }
    }
    
    public function destroy($id)
    {
        $img = Gallery::find($id);
        unlink(public_path('uploads/'.$img->img_location));
        $img->delete();
        return redirect()->back()->with(array('message'=>'Delete berhasil!'));
    }

}