<?php

namespace App\Http\Controllers;

use Request;
use DB;
use Auth;
use File;
use QrCode;

use App\Model\RegPerusahaan;
use App\Model\Perusahaan;
use App\Model\JenisPerizinan;
use App\Model\FlowIzinTerintegrasi;
use App\Model\FlowDetail;
use App\Model\FlowDetailIzin;
use App\Model\IzinPrinsip;
use App\Model\TrackingRegisterDetail;
use App\Model\TrackingRegisterDetailStop;
use App\Model\TrackingRegisterHeader;
use App\Model\Instansi;
use App\Model\Konsultasi;
use App\Model\KonsultasiKategori;
use App\Model\Kabkot;
use App\Model\Provinsi;

use App\Services\HitungHariEstimasi;
use App\Services\HitungHariKerja;

class TrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['createKonsultasi','searchNpwp','findPerusahaan','searchPerusahaan']]);
        //$this->middleware('role:check_in,check_out,konsultasi',['only'=>['index','findPerusahaan','searchPerusahaan',
        //    'getJenisPerizinan','hitungEstimasi','buatPerusahaan','searchNpwp',]]);
        $this->middleware('role:check_in,check_out,konsultasi',['only'=>['index',
            'getJenisPerizinan','hitungEstimasi','buatPerusahaan']]);
        $this->middleware('role:check_in',['only'=>['checkIn','getLembaran']]);
        $this->middleware('role:check_out',['only'=>['checkOut','ambilIzin','izinStop','izinTunda','izinLanjut','getDetail']]);
        //$this->middleware('role:konsultasi',['only'=>['createKonsultasi']]);
        \App::setLocale(getLang());
        createSidebar();
    }
    
    public function index()
    {
        return view('tracking.index');
    }
    
    public function findPerusahaan()
    {
        $id = strtoupper(Request::get('id',''));
        if (strpos($id,"-, NPWP: ") === 0) {
            return $this->findPerusahaanByNpwp(substr($id,9));
        }
        $izin_prinsip = IzinPrinsip::with('perusahaan','reg_perusahaan')->where('upper(id_permohonan)',$id)->orWhere('upper(no_ip)',$id)->first();
        if ($izin_prinsip) {
            $data = $izin_prinsip->reg_perusahaan;
            if (!$data) {
                $data = $izin_prinsip->perusahaan;
                $data->npwp = $data->npwp_perusahaan;
                $data->alamat = $data->alamat_prshn;
                $provinsi = $data->provinsi()->first();
                $data->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
                $kabkot = $data->kabkot()->first();
                $data->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
                $data->telepon = $data->no_telepon_prshn;
            }
            if ($data) {
                $data->izin_terintegrasi = FlowIzinTerintegrasi::getByQRCode($id);
                foreach ($data->izin_terintegrasi as $row) {
                    $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                }
                $data->izin_open = TrackingRegisterDetail::getOpenIzin($id);
                foreach ($data->izin_open as $row) {
                    $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                    $row->tanggal_threshold = getFullDate($row->tanggal_threshold);
                }
                $data->izin_selesai = TrackingRegisterDetail::getIzinSelesai($id);
                foreach ($data->izin_selesai as $row) {
                    $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                    $row->tanggal_selesai = getFullDate($row->tanggal_selesai);
                }
                //$data->izin_tunda = TrackingRegisterDetail::getIzinTunda($id);
                //foreach ($data->izin_tunda as $row) {
                //    $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                //    $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$row->id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
                //    $row->tanggal_threshold = getFullDate($trds->tanggal_stop);
                //}
            }
        } else {
            $data = [];
        }
        return response()->json($data);
    }
    
    private function findPerusahaanByNpwp($id)
    {
        $data = RegPerusahaan::where('upper(npwp)',$id)->first();
        if (!$data) {
            $data = Perusahaan::where('upper(npwp_perusahaan)',$id)->first();
            $data->npwp = $data->npwp_perusahaan;
            $data->alamat = $data->alamat_prshn;
            $provinsi = $data->provinsi()->first();
            $data->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
            $kabkot = $data->kabkot()->first();
            $data->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
            $data->telepon = $data->no_telepon_prshn;
        }
        if ($data) {
            $data->izin_terintegrasi = [];
            $data->izin_open = TrackingRegisterDetail::getOpenIzinNpwp($id);
            foreach ($data->izin_open as $row) {
                $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                $row->tanggal_threshold = getFullDate($row->tanggal_threshold);
            }
            $data->izin_selesai = TrackingRegisterDetail::getIzinSelesaiNpwp($id);
            foreach ($data->izin_selesai as $row) {
                $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
                $row->tanggal_selesai = getFullDate($row->tanggal_selesai);
            }
            //$data->izin_tunda = TrackingRegisterDetail::getIzinTundaNpwp($id);
            //foreach ($data->izin_tunda as $row) {
            //    $row->tanggal_pengajuan = getFullDate($row->tanggal_pengajuan);
            //    $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$row->id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
            //    $row->tanggal_threshold = getFullDate($trds->tanggal_stop);
            //}
        }
        return response()->json($data);
    }
    
    public function searchPerusahaan()
    {
        $q = strtoupper(Request::get('q',''));
        $result = RegPerusahaan::where('upper(no_perusahaan)','like',"%$q%")
            ->orWhere('upper(nama_perusahaan)','like',"%$q%")->take(10)
            ->get(['nama_perusahaan','no_perusahaan','npwp']);
        $result2 = Perusahaan::where('upper(no_perusahaan)','like',"%$q%")
            ->orWhere('upper(nama_perusahaan)','like',"%$q%")->take(10)
            ->get(['nama_perusahaan','id_perusahaan','npwp_perusahaan']);
        $data = [];
        foreach ($result as $row) {
            $ips = IzinPrinsip::where('id_perusahaan',$row->no_perusahaan)->lists('id_permohonan','id_permohonan');
            $data[] = ['value'=>"$row->nama_perusahaan",'data'=>$ips,'npwp'=>$row->npwp];
        }
        foreach ($result2 as $row) {
            $ips = IzinPrinsip::where('id_perusahaan',$row->id_perusahaan)->lists('id_permohonan','id_permohonan');
            $data[] = ['value'=>"$row->nama_perusahaan",'data'=>$ips,'npwp'=>$row->npwp_perusahaan];
        }
        return response()->json(['query'=>$q,'suggestions'=>$data]);
    }
    
    public function searchNpwp()
    {
        $q = strtoupper(Request::get('q',''));
        $result = RegPerusahaan::where('upper(npwp)','like',"%$q%")->take(10)
            ->get(['nama_perusahaan','no_perusahaan','npwp']);
        $result2 = Perusahaan::where('upper(npwp_perusahaan)','like',"%$q%")->take(10)
            ->get(['nama_perusahaan','id_perusahaan','npwp_perusahaan']);
        $data = [];
        foreach ($result as $row) {
            $ips = IzinPrinsip::where('id_perusahaan',$row->no_perusahaan)->lists('id_permohonan','id_permohonan');
            $data[] = ['value'=>"$row->npwp",'data'=>$ips,'npwp'=>$row->npwp,'nama_perusahaan'=>$row->nama_perusahaan];
        }
        foreach ($result2 as $row) {
            $ips = IzinPrinsip::where('id_perusahaan',$row->id_perusahaan)->lists('id_permohonan','id_permohonan');
            $data[] = ['value'=>"$row->npwp_perusahaan $row->nama_perusahaan",'data'=>$ips,'npwp'=>$row->npwp_perusahaan,'nama_perusahaan'=>$row->nama_perusahaan];
        }
        return response()->json(['query'=>$q,'suggestions'=>$data]);
    }
    
    public function getJenisPerizinan()
    {
        $id = Request::get('id_m_jenis_perizinan','');
        $data = JenisPerizinan::find($id);
        return response()->json($data);
    }
    
    public function hitungEstimasi()
    {
        $id = Request::get('id_m_jenis_perizinan','');
        $model = JenisPerizinan::find($id);
        if ($model) {
            $today = date('Y-m-d H:i:s');
            if ($model->sop == "0") {
                return response()->json("Tidak terdefinisi");
            } else {
                $hhe = new HitungHariEstimasi;
                $estimasi = $hhe->count(date('Y-m-d H:i:s'),$model->sop);
                return response()->json(getFullDate($estimasi));
            }
        } else {
            return response()->json("Tidak terdefinisi");
        }
    }
    
    public function checkIn()
    {
        $id_qrcode = strtoupper(Request::get('fci_id_qrcode',''));
        if (strpos($id_qrcode,"-, NPWP: ") === 0) {
            $model = RegPerusahaan::where('upper(npwp)',substr($id_qrcode,9))->first();
            if (!$model) {
                $model = Perusahaan::where('upper(npwp_perusahaan)',substr($id_qrcode,9))->first();
                $model->npwp = $model->npwp_perusahaan;
                $model->no_perusahaan = $model->id_perusahaan;
            }
            if ($model)
                $id_qrcode = $model->id_permohonan;
        } else {
            $izin_prinsip = IzinPrinsip::with('perusahaan','reg_perusahaan')->where('upper(id_permohonan)',$id_qrcode)->orWhere('upper(no_ip)',$id_qrcode)->first();
            if ($izin_prinsip) {
                $model = $izin_prinsip->reg_perusahaan;
                if (!$model) {
                    $model = $izin_prinsip->perusahaan;
                    $model->npwp = $model->npwp_perusahaan;
                    $model->no_perusahaan = $model->id_perusahaan;
                }
                $id_qrcode = $izin_prinsip->id_permohonan;
            }
        }
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'IP Tidak ditemukan']);
        }
        $id_m_jenis_perizinan = Request::get('fci_id_m_jenis_perizinan','');
        $jenis_izin = JenisPerizinan::find($id_m_jenis_perizinan);
        //dd(DB::getQueryLog());
        if (!$jenis_izin) {
            return response()->json(['success'=>false,'msg'=>'Jenis izin tidak ditemukan']);
        }
        $hhe = new HitungHariEstimasi;
        $estimasi = $jenis_izin->sop=="0"?null:$hhe->count(date('Y-m-d H:i:s'),$jenis_izin->sop);
        $dwarning = $jenis_izin->sop=="0"?null:$hhe->count(date('Y-m-d H:i:s'),floor($jenis_izin->sop*0.7));
        $instansi = Instansi::find($jenis_izin->id_m_instansi);
        $nama_instansi = $instansi?$instansi->nama_instansi:'';
        
        $data = [
            'id_qrcode' => $id_qrcode,
            'id_m_jenis_perizinan' => $id_m_jenis_perizinan,
            'nama_jenis_perizinan' => $jenis_izin->nama_jenis_perizinan,
            'tanggal_pengajuan' => date('Y-m-d H:i:s'),
            'no_perusahaan' => $model->no_perusahaan,
            'npwp' => $model->npwp,
            'id_m_instansi' => $jenis_izin->id_m_instansi,
            'nama_instansi' => $nama_instansi,
            'nama_perusahaan' => $model->nama_perusahaan,
            'tanggal_selesai_estimasi' => $estimasi,
            'tanggal_threshold' => $estimasi,
            'tanggal_warning' => $dwarning,
            'sop' => $jenis_izin->sop,
            'threshold' => $jenis_izin->threshold,
            'komentar_check_in' => getUserName().": ".Request::get('fci_komentar_check_in',''),
        ];
        $data2 = [
            'id_qrcode' => $id_qrcode,
            'tanggal_ippengajuan' => date('Y-m-d H:i:s'),
            'no_perusahaan' => $model->no_perusahaan,
            'nama_perusahaan' => $model->nama_perusahaan
        ];
        $mode = Request::get('fci_id_m_flow_terintegrasi','');
        if ($mode == 'new_terintegrasi') {
            $data['id_m_flow_izin_terintegrasi'] = Request::get('fci_id_m_flow_terintegrasi_choice','');
            $has_fit = DB::table(TABLESPACE.'.T_PT_FLOW_IZIN_TERINTEGRASI')
                ->where('id_qrcode',$id_qrcode)
                ->where('id_m_flow_izin_terintegrasi',$data['id_m_flow_izin_terintegrasi'])
                ->count();
            if ($has_fit) {
                return response()->json(['success'=>false,'msg'=>'Izin terintegrasi sudah dibuat']);
            }
            $data['nama_bidang_usaha'] = FlowIzinTerintegrasi::find($data['id_m_flow_izin_terintegrasi'])->nama_flow;
            $trh = TrackingRegisterHeader::find($id_qrcode);
            if (!$trh) {
                $trh = new TrackingRegisterHeader;
                $trh->fill($data2);
                $trh->save();
            }
            DB::table(TABLESPACE.'.T_PT_FLOW_IZIN_TERINTEGRASI')->insert([
                'id_qrcode' => $id_qrcode,
                'id_m_flow_izin_terintegrasi' => $data['id_m_flow_izin_terintegrasi'],
                'tanggal_pengajuan' => $data['tanggal_pengajuan']
            ]);
        } else if ($mode == 'new_tunggal') {
            $ctrd = TrackingRegisterDetail::where('id_qrcode',$id_qrcode)->where('id_m_jenis_perizinan',$id_m_jenis_perizinan)->whereNull('id_m_flow_izin_terintegrasi')->first();
            if ($ctrd) {
                return response()->json(['success'=>false,'msg'=>'Izin sudah pernah diregistrasi pada '.$ctrd->tanggal_pengajuan]);
            }
        } else {
            $ctrd = TrackingRegisterDetail::where('id_qrcode',$id_qrcode)->where('id_m_jenis_perizinan',$id_m_jenis_perizinan)->where('id_m_flow_izin_terintegrasi',Request::get('fci_id_m_flow_terintegrasi',''))->first();
            if ($ctrd) {
                return response()->json(['success'=>false,'msg'=>'Izin sudah pernah diregistrasi pada '.$ctrd->tanggal_pengajuan]);
            }
            $data['id_m_flow_izin_terintegrasi'] = Request::get('fci_id_m_flow_terintegrasi','');
            $data['nama_bidang_usaha'] = FlowIzinTerintegrasi::find($data['id_m_flow_izin_terintegrasi'])->nama_flow;
        }
        $trd = new TrackingRegisterDetail;
        $trd->fill($data);
        $trd->save();
        return response()->json(['success'=>true,'msg'=>'Registrasi izin berhasil disimpan','id_tracking_register_detail'=>$trd->id_tracking_register_detail]);
    }
    
    public function checkOut()
    {
        $id_tracking_register_detail = Request::get('fco_id_tracking_register_detail','');
        $model = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak ditemukan']);
        }
        $model->tanggal_selesai = date("Y-m-d H:i:s");
        $hhk = new HitungHariKerja;
        $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
        if ($trds) {
            $sop_used = $model->lama_proses?$model->lama_proses:0;
            $sop_used+= $hhk->count($trds->tanggal_restart,date('Y-m-d H:i:s'));
        } else {
            $sop_used = $hhk->count($model->tanggal_pengajuan,date('Y-m-d H:i:s'));
        }
        $model->lama_proses = $sop_used;
        if (date("Y-m-d",strtotime($model->tanggal_selesai)) <= date("Y-m-d",strtotime($model->tanggal_threshold))) {
            $model->performance = "O";
        } else {
            $model->performance = "D";
        }
        DB::table(TABLESPACE.'.tracking_register_detail')->where('id_tracking_register_detail',$id_tracking_register_detail)->update([
            'tanggal_selesai' => $model->tanggal_selesai,
            'lama_proses' => $model->lama_proses,
            'performance' => $model->performance,
            'update_user' => Auth::user()?Auth::user()->id_m_user:Request::ip(),
            'update_tms' => $model->tanggal_selesai,
            'komentar_check_out' => getUserName().": ".Request::get('fco_komentar_check_out',''),
        ]);
        return response()->json(['success'=>true,'msg'=>'Update status izin selesai berhasil disimpan']);
    }
    
    public function ambilIzin()
    {
        $id_tracking_register_detail = Request::get('fai_id_tracking_register_detail','');
        $model = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak ditemukan']);
        }
        $model->tanggal_ambil = date("Y-m-d H:i:s");
        $model->save();
        return response()->json(['success'=>true,'msg'=>'Update status izin diambil berhasil disimpan']);
    }
    
    public function izinStop()
    {
        $id_tracking_register_detail = Request::get('fis_id_tracking_register_detail','');
        $model = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak ditemukan']);
        }
        $hhk = new HitungHariKerja;
        $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
        if ($trds) {
            $sop_used = $model->lama_proses?$model->lama_proses:0;
            $sop_used+= $hhk->count($trds->tanggal_restart,date('Y-m-d H:i:s'));
        } else {
            $sop_used = $hhk->count($model->tanggal_pengajuan,date('Y-m-d H:i:s'));
        }
        $model->lama_proses = $sop_used;
        $model->performance = null;
        $model->save();
        $trds = TrackingRegisterDetailStop::create([
            'id_qrcode' => $model->id_qrcode,
            'id_tracking_register_detail' => $model->id_tracking_register_detail,
            'tanggal_stop' => date("Y-m-d H:i:s"),
            'sop_used' => $sop_used,
            'alasan_stop' => getUserName().": ".Request::get('fis_alasan_stop',''),
            'last_warning_date' => $model->tanggal_warning,
            'last_threshold_date' => $model->tanggal_threshold,
            'last_estimate_date' => $model->tanggal_selesai_estimasi,
        ]);
        return response()->json(['success'=>true,'msg'=>'Update status stop izin berhasil disimpan']);
    }
    
    public function izinTunda()
    {
        $id_tracking_register_detail = Request::get('fit_id_tracking_register_detail','');
        $model = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak ditemukan']);
        }
        $hhk = new HitungHariKerja;
        $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
        if ($trds) {
            $sop_used = $model->lama_proses?$model->lama_proses:0;
            $sop_used+= $hhk->count($trds->tanggal_restart,date('Y-m-d H:i:s'));
        } else {
            $sop_used = $hhk->count($model->tanggal_pengajuan,date('Y-m-d H:i:s'));
        }
        $model->lama_proses = $sop_used;
        $model->jenis_status_selesai = "C";
        $model->save();
        $trds = TrackingRegisterDetailStop::create([
            'id_qrcode' => $model->id_qrcode,
            'id_tracking_register_detail' => $model->id_tracking_register_detail,
            'tanggal_stop' => date("Y-m-d H:i:s"),
            'sop_used' => $sop_used,
            'alasan_stop' => Request::get('fit_alasan_stop',''),
            'last_warning_date' => $model->tanggal_warning,
            'last_threshold_date' => $model->tanggal_threshold,
            'last_estimate_date' => $model->tanggal_selesai_estimasi,
        ]);
        return response()->json(['success'=>true,'msg'=>'Update status tunda izin berhasil disimpan']);
    }
    
    public function izinLanjut()
    {
        $id_tracking_register_detail = Request::get('fil_id_tracking_register_detail','');
        $model = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$model) {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak ditemukan']);
        }
        $trds = TrackingRegisterDetailStop::where('id_tracking_register_detail',$id_tracking_register_detail)->orderBy('tanggal_stop','desc')->first();
        if ($trds && $model->performance == "C") {
            $trds->tanggal_restart = date('Y-m-d H:i:s');
            $trds->save();
        } else {
            return response()->json(['success'=>false,'msg'=>'TRD Tidak valid untuk dilanjutkan']);
        }
        $hhe = new HitungHariEstimasi;
        $sop_used = $model->lama_proses;
        if (intval($sop_used) > intval($model->sop)) {
            
        } else {
            $sop_sisa = intval($model->sop) - intval($sop_used);
            $model->tanggal_warning = (floor($model->sop*0.7) - $sop_sisa > 0) ? $hhe->count(date('Y-m-d H:i:s'),floor($model->sop*0.7) - $sop_sisa) : date('Y-m-d H:i:s');
            $model->tanggal_threshold = $hhe->count(date('Y-m-d H:i:s'),$sop_sisa);
            $model->tanggal_selesai_estimasi = $hhe->count(date('Y-m-d H:i:s'),$sop_sisa);
        }
        $model->performance = null;
        $model->save();
        return response()->json(['success'=>true,'msg'=>'Update status tunda izin berhasil disimpan']);
    }
    
    public function createKonsultasi()
    {
        $request = Request::except('_token');
        $data = [];
        foreach ($request as $k=>$v) {
            $data[substr($k, 3)] = $v;
        }
        $instansi = Instansi::find($data['id_m_instansi']);
        $nama_instansi = $instansi?$instansi->nama_instansi:'';
        $data['nama_instansi'] = $nama_instansi;
        if (!Request::has('fk_nama_petugas')) {
            $data['nama_petugas'] = getUserName();
        }
        $model = new Konsultasi;
        $model->fill($data);
        if (preg_match('/^[0-9][0-9]{0,11}$/',$data['id_konsul_cat'])) {
            if (KonsultasiKategori::find($data['id_konsul_cat'])) {
                $newkategori = false;
            } else {
                $newkategori = true;
                $namakategori= $data['id_konsul_cat'];
            }
        } else {
            $newkategori = true;
            $namakategori= $data['id_konsul_cat'];
        }
        if ($newkategori) {
            $kat = KonsultasiKategori::where('upper(nama_konsul_cat)',strtoupper($namakategori))->first();
            if ($kat) {
                $model->id_konsul_cat = $kat->id_konsul_cat;
            } else {
                $kategori = KonsultasiKategori::create([
                    'nama_konsul_cat' => $namakategori
                ]);
                $model->id_konsul_cat = $kategori->id_konsul_cat;
            }
        }
        $model->save();
        return response()->json(['success'=>true,'msg'=>'Konsultasi berhasil disimpan']);
    }
    
    public function buatPerusahaan()
    {
        $request = Request::except('_token');
        $data = [];
        foreach ($request as $k=>$v) {
            $data[substr($k, 3)] = $v;
        }
        $perusahaan = RegPerusahaan::where('npwp',$data['npwp'])->first();
        if ($perusahaan) return response()->json(['success'=>false,'msg'=>'NPWP Perusahaan telah terdaftar pada perusahaan '.$perusahaan->nama_perusahaan]);
        $perusahaan = Perusahaan::where('npwp_perusahaan',$data['npwp'])->first();
        if ($perusahaan) return response()->json(['success'=>false,'msg'=>'NPWP Perusahaan telah terdaftar pada perusahaan '.$perusahaan->nama_perusahaan]);
        $model = new RegPerusahaan;
        $model->fill($data);
        $kabkot = Kabkot::find($model->id_kabkot);
        $model->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'';
        $provinsi = Provinsi::find($model->id_provinsi);
        $model->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'';
        //$model->no_ip = strtoupper("IP".rand(10000,99999).str_random(3));
        //$model->id_qrcode = $model->no_ip;
        $model->tanggal_pengajuan = date("Y-m-d H:i:s");
        $model->save();
        return response()->json(['success'=>true,'msg'=>'Perusahaan berhasil disimpan','id_qrcode'=>$model->id_qrcode,'npwp'=>$model->npwp]);
    }
    
    public function getDetail()
    {
        $id_tracking_register_detail = strtoupper(Request::get('id_tracking_register_detail',''));
        $id = strtoupper(Request::get('id',''));

        $data = TrackingRegisterDetail::where('id_tracking_register_detail',$id_tracking_register_detail)->first();
        $data2 = JenisPerizinan::where('id_m_jenis_perizinan',$data->id_m_jenis_perizinan)->first();
        if($data) {
            $data2->status = $data->getStatus();
            $data2->tanggal_pengajuan = getFullDate($data->tanggal_pengajuan);
            $data2->tanggal_selesai_estimasi = getFullDate($data->tanggal_selesai_estimasi);
            $data2->nama_instansi = $data->nama_instansi;
            $data2->nama_direktorat = $data->nama_direktorat;
            $data2->nama_bidang_usaha = $data->nama_bidang_usaha;
            $izin_terintegrasi = $data->id_m_flow_izin_terintegrasi?FlowIzinTerintegrasi::find($data->id_m_flow_izin_terintegrasi):false;
            if ($izin_terintegrasi) {
                $data2->nama_izin_terintegrasi = $izin_terintegrasi->nama_flow;
            } else {
                $data2->nama_izin_terintegrasi = "Izin Tunggal";
            }
        }else {
            $data2->status = "BELUM DIAJUKAN";
            $data2->tanggal_pengajuan = "-";
            $data2->tanggal_selesai_estimasi = "-";
        }
        return response()->json($data2);
    }
    
    public function getLembaran()
    {
        $view = Request::get('view',false);
        $id_tracking_register_detail = Request::get('id_tracking_register_detail','');
        $trd = TrackingRegisterDetail::find($id_tracking_register_detail);
        if (!$trd) return "Tracking izin tidak ditemukan";
        if (file_exists(base_path("resources/tracking/$id_tracking_register_detail.pdf"))) {
            if (strtotime($trd->update_tms) < File::lastModified(base_path("resources/tracking/$id_tracking_register_detail.pdf"))) {
                if ($view) {
                    return \Response::make(file_get_contents(base_path("resources/tracking/$id_tracking_register_detail.pdf")),200)->header('Content-Type', mime_content_type(base_path("resources/tracking/$id_tracking_register_detail.pdf")));
                } else {
                    return response()->download(base_path("resources/tracking/$id_tracking_register_detail.pdf"), "Tracking PTSP $id_tracking_register_detail.pdf");
                }
            }
        }
        if ($trd->id_qrcode == '' || !$trd->id_qrcode) {
            $trd->id_qrcode = '-, NPWP: '.$trd->npwp;
        }
        QrCode::format('png')->size(200)->margin(0)->errorCorrection('H')->merge("/public/images/logo-symbol.png", .25)->generate($trd->id_qrcode,base_path(str_replace(":","_","resources/tracking/{$trd->id_qrcode}.png")));
        $data = [];
        $data['tanggal_pengajuan'] = str_replace("&nbsp;"," ",getFullDate($trd->tanggal_pengajuan));
        $data['tanggal_selesai_estimasi'] = str_replace("&nbsp;"," ",getFullDate($trd->tanggal_selesai_estimasi));
        $data['nama_perusahaan'] = $trd->nama_perusahaan;
        $data['nama_jenis_perizinan'] = $trd->nama_jenis_perizinan;
        $data['nama_bidang_usaha'] = $trd->nama_bidang_usaha?$trd->nama_bidang_usaha:'-';
        $data['nama_instansi'] = $trd->nama_instansi?$trd->nama_instansi:'-';
        $data['sop'] = $trd->sop?$trd->sop." Hari":'-';
        $data['id_qrcode'] = $trd->id_qrcode?$trd->id_qrcode:'-';
        $data['npwp'] = $trd->npwp?$trd->npwp:'-';
        $data['no_perusahaan'] = $trd->no_perusahaan?$trd->no_perusahaan:'-';

        $izin_prinsip = IzinPrinsip::with('perusahaan','reg_perusahaan')->where('upper(id_permohonan)',$trd->id_qrcode)->first();
        if ($izin_prinsip) {
            $perusahaan = $izin_prinsip->reg_perusahaan;
            if (!$perusahaan) {
                $perusahaan = $izin_prinsip->perusahaan;
                $perusahaan->npwp = $perusahaan->npwp_perusahaan;
                $perusahaan->alamat = $perusahaan->alamat_prshn;
                $provinsi = $perusahaan->provinsi()->first();
                $perusahaan->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
                $kabkot = $perusahaan->kabkot()->first();
                $perusahaan->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
                $perusahaan->telepon = $perusahaan->no_telepon_prshn;
                $perusahaan->fax = $perusahaan->no_faximil;
            }
            $no_ip = $izin_prinsip->no_ip;
        } else {
            $perusahaan = RegPerusahaan::where('npwp',$trd->npwp)->first();
            if (!$perusahaan) {
                $perusahaan = Perusahaan::where('npwp_perusahaan',$trd->npwp)->first();
                $perusahaan->npwp = $perusahaan->npwp_perusahaan;
                $perusahaan->alamat = $perusahaan->alamat_prshn;
                $provinsi = $perusahaan->provinsi()->first();
                $perusahaan->nama_provinsi = $provinsi?$provinsi->nama_provinsi:'-';
                $kabkot = $perusahaan->kabkot()->first();
                $perusahaan->nama_kabkot = $kabkot?$kabkot->nama_kabkot:'-';
                $perusahaan->telepon = $perusahaan->no_telepon_prshn;
                $perusahaan->fax = $perusahaan->no_faximil;
            }
            $no_ip = '-';
        }
        $data['no_ip'] = $no_ip;
        $data['alamat_perusahaan'] = $perusahaan->alamat_perusahaan?$perusahaan->alamat_perusahaan:'-';
        $data['nama_kabkot'] = $perusahaan->nama_kabkot;
        $data['nama_provinsi'] = $perusahaan->nama_provinsi;
        $data['no_telepon'] = $perusahaan->telepon?$perusahaan->telepon:'-';
        $data['no_fax'] = $perusahaan->fax?$perusahaan->fax:'-';
        $data['email'] = $perusahaan->email?$perusahaan->email:'-';
        $data['nama_petugas'] = getUserName();
        $data['site_url'] = url('/');
        $data['image_qrcode'] = base_path(str_replace(":","_","resources/tracking/{$trd->id_qrcode}.png"));
        //return view('tracking.lembaran', $data);
        $pdf = \PDF::loadView('tracking.lembaran', $data);
        $pdf->save(base_path("resources/tracking/$id_tracking_register_detail.pdf"));
        if ($view) {
            return $pdf->stream("Tracking PTSP $id_tracking_register_detail.pdf");
        } else {
            return $pdf->download("Tracking PTSP $id_tracking_register_detail.pdf");
        }
        $templateProcessor->setImageValue('image1.png',base_path(str_replace(":","_","resources/tracking/{$trd->id_qrcode}.png")));
        unlink(base_path(str_replace(":","_","resources/tracking/{$trd->id_qrcode}.png")));
    }
}
