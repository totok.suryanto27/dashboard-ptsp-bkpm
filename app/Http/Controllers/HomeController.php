<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\TrackingRegisterDetail;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		\App::setLocale(getLang());
		createSidebar();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (isRole('executive') || isRole('admin')|| isRole('monitoring')|| isRole('ptsp')|| isRole('kl')) {
			$statuses = ['O','D','on_going','on_going_delay','on_going_warning'];
			$counter = ['semua'=>0];
			foreach ($statuses as $search) {
                if ($search == 'O')
                    $query = TrackingRegisterDetail::where(function($q) {
                        $q = $q->where('performance','I')->orWhere('performance','O');
                    });
                else if ($search == 'D') 
                    $query = TrackingRegisterDetail::where('performance','=',$search);
                else if ($search == 'on_going') 
                    $query = TrackingRegisterDetail::whereNull('tanggal_selesai')->where('tanggal_threshold','>',date('Y-m-d H:i:s'));
                else if ($search == 'on_going_delay') 
                    $query = TrackingRegisterDetail::whereNull('tanggal_selesai')->where('tanggal_threshold','<',date('Y-m-d H:i:s'));
                else if ($search == 'on_going_warning') 
                    $query = TrackingRegisterDetail::whereNull('tanggal_selesai')->where('tanggal_warning','<',date('Y-m-d H:i:s'))->where('tanggal_threshold','>',date('Y-m-d H:i:s'));
                if (getInstansi()) {
                    $query = $query->where('id_m_instansi',getInstansi());
                }
                $counter[$search] = $query->count();
                $counter['semua'] += $counter[$search];
            }
			return view('dashboard.executive',compact('counter'));
		}
		return view('home');
	}
	
	public function changeMyDetail(Request $request)
	{
		$id = Auth::user()->id_m_user;
		$model = User::find($id);
		if (!$model) {
			return redirect()->back()->with(array('message_type'=>'error','message'=>'Ada yang hilang'));
		}
		if ($request->isMethod('post')) {
			$this->validate($request, [
				'nama_user' => 'required|max:255',
				'email' => 'required|email|max:255',
				'jabatan' => 'max:255',
				'telp' => 'max:255',
				'nip' => 'max:100',
				'foto'=>'max:1024|mimes:jpg,jpeg',
			]);
			$model->nama_user = $request->get('nama_user',$model->nama_user);
			$model->email = $request->get('email',$model->email);
			$model->jabatan = $request->get('jabatan');
			$model->telp = $request->get('telp');
			$model->nip = $request->get('nip');
			if(!empty($request->file('foto'))){
				$destination = public_path('user/photos');
				$name = $model->foto;
				if (!$name || $name == "" || $name == "unknown.jpg") $name = str_random(32) .".". $request->file('foto')->getClientOriginalExtension();
				$request->file('foto')->move($destination,$name);
				$model->foto = $name;
			}
			$password = $request->get('password',"");
			$password_ulangi = $request->get('password_ulangi',"");
			if ($password != "") {
				if ($password != $password_ulangi) {
					return redirect()->back()
								->withInput($request->except('password','password_ulangi'))
								->withErrors([
									'password' => 'Isian password ulang tidak sama',
								]);
				}
				$model->password = md5($password);
			}
			$model->save();
			return redirect()->back()->with(array('message'=>'Profil berhasil diupdate'));
		}
		return view('auth.change-my-detail');
	}

}
