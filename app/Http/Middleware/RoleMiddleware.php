<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arg_list = func_get_args();
        for ($i = 2; $i < func_num_args(); $i++) {
            if (isRole($arg_list[$i])) {
                return $next($request);
            }
        }

        return redirect('home');
    }

}
