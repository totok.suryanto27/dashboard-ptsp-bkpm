<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Cache;

class CachePageMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = 'route_'.getLang()."_".str_replace( $request->root(), '', $request->fullUrl() );
        //dd($key);

        if (Cache::has($key) && Auth::guest()) {
            $content=Cache::get($key);
            return response($content);
        }

        $response = $next($request);
        //Cache::put($key,$response,60);

        if (!Cache::has($key) && Auth::guest()) {
            Cache::put($key, $response->getContent(), 600);
        }
        //dd($response);
        return $response;
    }

}
