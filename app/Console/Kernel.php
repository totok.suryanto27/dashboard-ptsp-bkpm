<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\HitungHarian',
		'App\Console\Commands\HitungHariIni',
		'App\Console\Commands\HitungBulanIni',
		'App\Console\Commands\HitungTahunIni',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('hitungharian')->everyFiveMinutes();
		$schedule->command('hitunghariini')->everyFiveMinutes();
		$schedule->command('hitungbulanini')->dailyAt('02:00');
		$schedule->command('hitungtahunini')->dailyAt('02:00');
	}

}
