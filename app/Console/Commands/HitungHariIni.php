<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Model\FlowIzinTerintegrasi;
use App\Model\Instansi;
use App\Model\JenisPerizinan;
use App\Model\TrackingRegisterDetail;
use DB;
//CREATE_TMS
//CREATE_USER
//HARI
//
//ID_M_INSTANSI
//ID_M_JENIS_PERIZINAN
//ID_M_FLOW_TERINTEGRASI
//NAMA_INSTANSI
//NAMA_FLOW
//
//HITUNG_PENGAJUAN
//HITUNG_SELESAI
//HITUNG_TERLAMBAT
//HITUNG_ON_TIME
//HITUNG_ON_PROSES
//
//IZININT_HITUNG_PENGAJUAN
//IZININT_HITUNG_SELESAI
//IZININT_HITUNG_TERLAMBAT
//IZININT_HITUNG_ON_TIME
//IZININT_HITUNG_ON_PROSES
class HitungHariIni extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hitunghariini';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hitung untuk hari ini';
	
	private $query_hitung_pengajuan;
	private $query_hitung_selesai;
	private $query_hitung_on_proses;
	
	private $query_izint_hitung_pengajuan;
	private $query_izint_hitung_selesai;
	private $query_izint_hitung_on_proses;
	
	private $datas;
	
	public function init($_data)
	{
		$this->query_hitung_pengajuan = TrackingRegisterDetail::where('tanggal_pengajuan','like',$_data['hari']."%");
		$this->query_hitung_selesai = TrackingRegisterDetail::where('tanggal_selesai','like',$_data['hari']."%");
		$this->query_hitung_on_proses = TrackingRegisterDetail::whereNull('tanggal_selesai');

		$this->query_izint_hitung_pengajuan = DB::table(TABLESPACE.'.t_pt_flow_izin_terintegrasi')->where('tanggal_pengajuan','like',$_data['hari']."%");
		$this->query_izint_hitung_selesai = DB::table(TABLESPACE.'.t_pt_flow_izin_terintegrasi')->where('tanggal_selesai','like',$_data['hari']."%");
		$this->query_izint_hitung_on_proses = DB::table(TABLESPACE.'.t_pt_flow_izin_terintegrasi')->whereNull('tanggal_selesai');
		
		$ftis = FlowIzinTerintegrasi::get();
		$ftis->push(0);
		$ftis->push(null);
		foreach ($ftis as $fti) {
			$data = $_data;
			if ($fti instanceof FlowIzinTerintegrasi) {
				$data['id_m_flow_izin_terintegrasi'] = $fti->id_m_flow_izin_terintegrasi;
				$data['nama_flow'] = $fti->nama_flow;
				$data['id_m_instansi'] = null;
				$data['id_m_jenis_perizinan'] = null;
				$data['nama_instansi'] = null;
				$this->datas[] = $data;
			} else if ($fti === 0) {
				$data['id_m_flow_izin_terintegrasi'] = 0;
				$data['nama_flow'] = 'Izin Tunggal';
				$data['id_m_instansi'] = null;
				$data['id_m_jenis_perizinan'] = null;
				$data['nama_instansi'] = null;
				$this->datas[] = $data;
			} else if ($fti === null) {
				$data['id_m_flow_izin_terintegrasi'] = null;
				$data['nama_flow'] = null;
				$instansis = Instansi::get();
				$instansis->push(null);
				foreach ($instansis as $instansi) {
					if ($instansi === 0) {
						$data['id_m_instansi'] = 0;
						$data['nama_instansi'] = 'Lain-lain';
						$data['id_m_jenis_perizinan'] = null;
						$this->datas[] = $data;
					} else if ($instansi === null) {
						$data['id_m_instansi'] = null;
						$data['nama_instansi'] = null;
						$izins = JenisPerizinan::get();
						$izins->push(null);
						foreach ($izins as $izin) {
							if ($izin === null) {
								$data['id_m_jenis_perizinan'] = null;
								$this->datas[] = $data;
							} else {
								$data['id_m_jenis_perizinan'] = $izin->id_m_jenis_perizinan;
								$this->datas[] = $data;
							}
						}
					} else {
						$data['id_m_instansi'] = $instansi->id_m_instansi;
						$data['nama_instansi'] = $instansi->nama_instansi;
						$data['id_m_jenis_perizinan'] = null;
						$this->datas[] = $data;
					}
				}
			}
		}
	}
	
	public function addCount($trd,$col)
	{
		foreach ($this->datas as &$data) {
			if ($data['id_m_instansi'] == $trd->id_m_instansi && $trd->id_m_instansi) {
				$data[$col]++;
			}
			if ($data['id_m_jenis_perizinan'] == $trd->id_m_jenis_perizinan && $trd->id_m_jenis_perizinan) {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] == $trd->id_m_flow_izin_terintegrasi && $trd->id_m_flow_izin_terintegrasi) {
				$data[$col]++;
			}
			if ($trd->id_m_flow_izin_terintegrasi === null && $data['id_m_flow_izin_terintegrasi'] === 0) {
				$data[$col]++;
			}
			if (($trd->id_m_instansi === null || $trd->id_m_instansi === "0") && $data['id_m_instansi'] === "0") {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] === null && $data['id_m_instansi'] === null && $data['id_m_jenis_perizinan'] === null) {
				$data[$col]++;
			}
		}
	}
	
	public function addCountIzint($trd,$col)
	{
		foreach ($this->datas as &$data) {
			if ($data['id_m_flow_izin_terintegrasi'] == $trd->id_m_flow_izin_terintegrasi && $trd->id_m_flow_izin_terintegrasi) {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] === null && $data['id_m_instansi'] === null) {
				$data[$col]++;
			}
		}
	}
	
	public function loadData()
	{
		$pengajuans = $this->query_hitung_pengajuan->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCount($pengajuan,'hitung_pengajuan');
		}
		$pengajuans = $this->query_hitung_selesai->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCount($pengajuan,'hitung_selesai');
			switch($pengajuan->performance) {
				case "I":
				case "O":
					$this->addCount($pengajuan,'hitung_on_time');
					break;
				case "D":
					$this->addCount($pengajuan,'hitung_terlambat');
					break;
			}
		}
		$pengajuans = $this->query_hitung_on_proses->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCount($pengajuan,'hitung_on_proses');
		}
		$pengajuans = $this->query_izint_hitung_pengajuan->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCountIzint($pengajuan,'izinint_hitung_pengajuan');
		}
		$pengajuans = $this->query_izint_hitung_on_proses->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCountIzint($pengajuan,'izinint_hitung_on_proses');
		}
	}
	
	public function insert($table)
	{
		foreach ($this->datas as $data) {
			$data['id_m_flow_terintegrasi'] = $data['id_m_flow_izin_terintegrasi'];
			unset($data['id_m_flow_izin_terintegrasi']);
			DB::table($table)->insert($data);
		}
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'create_tms' => date("Y-m-d H:i:s"),
			'create_user' => 'cronjob',
			'hari' => date("Y-m-d"),
			'id_m_jenis_perizinan' => null,
			'hitung_pengajuan' => 0,
			'hitung_selesai' => 0,
			'hitung_terlambat' => 0,
			'hitung_on_time' => 0,
			'hitung_on_proses' => 0,
			'izinint_hitung_pengajuan' => 0,
			'izinint_hitung_selesai' => 0,
			'izinint_hitung_terlambat' => 0,
			'izinint_hitung_on_time' => 0,
			'izinint_hitung_on_proses' => 0,
		];
		DB::table(TABLESPACE.'.hitung_perizinan_harian')->where('hari',$data['hari'])->delete();
		$this->init($data);
		$this->loadData();
		$this->insert(TABLESPACE.'.hitung_perizinan_harian');
		
		//$this->info("Data hari ini berhasil diupdate!");
	}

}
