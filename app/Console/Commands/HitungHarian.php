<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Model\FlowIzinTerintegrasi;
use App\Model\Instansi;
use App\Model\JenisPerizinan;
use App\Model\TrackingRegisterDetail;
use DB;

class HitungHarian extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hitungharian';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hitung untuk harian on proses';
	
	private $query_hitung_on_proses;
	private $query_izint_hitung_on_proses;
	
	private $datas;
	
	public function init($_data)
	{
		$this->query_hitung_on_proses = TrackingRegisterDetail::whereNull('tanggal_selesai');
		$this->query_izint_hitung_on_proses = DB::table(TABLESPACE.'.t_pt_flow_izin_terintegrasi')->whereNull('tanggal_selesai');
		
		$ftis = FlowIzinTerintegrasi::get();
		$ftis->push(0);
		$ftis->push(null);
		foreach ($ftis as $fti) {
			$data = $_data;
			if ($fti instanceof FlowIzinTerintegrasi) {
				$data['id_m_flow_izin_terintegrasi'] = $fti->id_m_flow_izin_terintegrasi;
				$data['nama_flow'] = $fti->nama_flow;
				$data['id_m_instansi'] = null;
				$data['nama_instansi'] = null;
				$this->datas[] = $data;
			} else if ($fti === 0) {
				$data['id_m_flow_izin_terintegrasi'] = 0;
				$data['nama_flow'] = 'Izin Tunggal';
				$data['id_m_instansi'] = null;
				$data['nama_instansi'] = null;
				$this->datas[] = $data;
			} else if ($fti === null) {
				$data['id_m_flow_izin_terintegrasi'] = null;
				$data['nama_flow'] = null;
				$instansis = Instansi::get();
				$instansis->push(null);
				foreach ($instansis as $instansi) {
					if ($instansi === 0) {
						$data['id_m_instansi'] = 0;
						$data['nama_instansi'] = 'Lain-lain';
					} else if ($instansi === null) {
						$data['id_m_instansi'] = null;
						$data['nama_instansi'] = null;
					} else {
						$data['id_m_instansi'] = $instansi->id_m_instansi;
						$data['nama_instansi'] = $instansi->nama_instansi;
					}
					$this->datas[] = $data;
				}
			}
		}
	}
	
	public function addCount($trd,$col)
	{
		foreach ($this->datas as &$data) {
			if ($data['id_m_instansi'] == $trd->id_m_instansi && $trd->id_m_instansi) {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] == $trd->id_m_flow_izin_terintegrasi && $trd->id_m_flow_izin_terintegrasi) {
				$data[$col]++;
			}
			if ($trd->id_m_flow_izin_terintegrasi === null && $data['id_m_flow_izin_terintegrasi'] === 0) {
				$data[$col]++;
			}
			if (($trd->id_m_instansi === null || $trd->id_m_instansi === "0") && $data['id_m_instansi'] === "0") {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] === null && $data['id_m_instansi'] === null) {
				$data[$col]++;
			}
		}
	}
	
	public function addCountIzint($trd,$col)
	{
		foreach ($this->datas as &$data) {
			if ($data['id_m_flow_izin_terintegrasi'] == $trd->id_m_flow_izin_terintegrasi && $trd->id_m_flow_izin_terintegrasi) {
				$data[$col]++;
			}
			if ($data['id_m_flow_izin_terintegrasi'] === null && $data['id_m_instansi'] === null) {
				$data[$col]++;
			}
		}
	}
	
	public function loadData()
	{
		$pengajuans = $this->query_hitung_on_proses->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCount($pengajuan,'total_izin');
			if (date("Y-m-d H:i:s") >= $pengajuan->tanggal_threshold && $pengajuan->tanggal_threshold != null) {
				$this->addCount($pengajuan,'delay');
			}
		}
		$pengajuans = $this->query_izint_hitung_on_proses->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCountIzint($pengajuan,'izinint_total_izin');
		}
	}
	
	public function insert($table)
	{
		foreach ($this->datas as $data) {
			$data['id_m_flow_terintegrasi'] = $data['id_m_flow_izin_terintegrasi'];
			unset($data['id_m_flow_izin_terintegrasi']);
			DB::table($table)->insert($data);
		}
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'create_tms' => date("Y-m-d H:i:s"),
			'create_user' => 'cronjob',
			'id_m_jenis_perizinan' => null,
			'total_izin' => 0,
			'delay' => 0,
			'izinint_total_izin' => 0,
			'izinint_delay' => 0,
		];
		DB::table(TABLESPACE.'.hitung_perizinan_on_proses')->truncate();
		$this->init($data);
		$this->loadData();
		$this->insert(TABLESPACE.'.hitung_perizinan_on_proses');
		
		//$this->info("Data hari ini berhasil diupdate!");
	}

}
