<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Model\FlowIzinTerintegrasi;
use App\Model\Instansi;
use App\Model\JenisPerizinan;
use App\Model\TrackingRegisterDetail;
use DB;

class HitungBulanIni extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hitungbulanini';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hitung untuk bulan ini';
	
	private $tahun;
	private $bulan;
	
	private $datas;
	
	public function init($_data)
	{
		$this->tahun = $_data['tahun'];
		$this->bulan = $_data['bulan'];
		$ftis = FlowIzinTerintegrasi::get();
		$ftis->push(0);
		$ftis->push(null);
		foreach ($ftis as $fti) {
			$data = $_data;
			if ($fti instanceof FlowIzinTerintegrasi) {
				$data['id_m_flow_terintegrasi'] = $fti->id_m_flow_izin_terintegrasi;
				$data['nama_flow'] = $fti->nama_flow;
				$data['id_m_instansi'] = null;
				$data['nama_instansi'] = null;
				$data['id_m_jenis_perizinan'] = null;
				$this->datas[] = $data;
			} else if ($fti === 0) {
				$data['id_m_flow_terintegrasi'] = 0;
				$data['nama_flow'] = 'Izin Tunggal';
				$data['id_m_instansi'] = null;
				$data['nama_instansi'] = null;
				$data['id_m_jenis_perizinan'] = null;
				$this->datas[] = $data;
			} else if ($fti === null) {
				$data['id_m_flow_terintegrasi'] = null;
				$data['nama_flow'] = null;
				$instansis = Instansi::get();
				$instansis->push(null);
				foreach ($instansis as $instansi) {
					if ($instansi === 0) {
						$data['id_m_instansi'] = 0;
						$data['nama_instansi'] = 'Lain-lain';
						$data['id_m_jenis_perizinan'] = null;
						$this->datas[] = $data;
					} else if ($instansi === null) {
						$data['id_m_instansi'] = null;
						$data['nama_instansi'] = null;
						$izins = JenisPerizinan::get();
						$izins->push(null);
						foreach ($izins as $izin) {
							if ($izin === null) {
								$data['id_m_jenis_perizinan'] = null;
								$this->datas[] = $data;
							} else {
								$data['id_m_jenis_perizinan'] = $izin->id_m_jenis_perizinan;
								$this->datas[] = $data;
							}
						}
					} else {
						$data['id_m_instansi'] = $instansi->id_m_instansi;
						$data['nama_instansi'] = $instansi->nama_instansi;
						$data['id_m_jenis_perizinan'] = null;
						$this->datas[] = $data;
					}
				}
			}
		}
	}
	
	public function addCount($trd,$cols)
	{
		foreach ($this->datas as &$data) {
			if ($data['id_m_flow_terintegrasi'] === $trd->id_m_flow_terintegrasi && $data['id_m_instansi'] === $trd->id_m_instansi && $data['id_m_jenis_perizinan'] === $trd->id_m_jenis_perizinan) {
				foreach ($cols as $col) {
					$data[$col]+=$trd->$col;
				}
				continue;
			}
		}
	}
	
	public function loadData()
	{
		$pengajuans = DB::table(TABLESPACE.'.hitung_perizinan_harian')->where('hari','like',$this->tahun."-".$this->bulan."%")->get();
		foreach ($pengajuans as $pengajuan) {
			$this->addCount($pengajuan,[
				'hitung_pengajuan',
				'hitung_selesai',
				'hitung_on_time',
				'hitung_terlambat',
				'hitung_on_proses',
				'izinint_hitung_pengajuan',
				'izinint_hitung_on_proses',
			]);
		}
	}
	
	public function insert($table)
	{
		foreach ($this->datas as $data) {
			DB::table($table)->insert($data);
		}
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = [
			'create_tms' => date("Y-m-d H:i:s"),
			'create_user' => 'cronjob',
			'bulan' => date("m",strtotime('yesterday')),
			'tahun' => date("Y",strtotime('yesterday')),
			'id_m_jenis_perizinan' => null,
			'hitung_pengajuan' => 0,
			'hitung_selesai' => 0,
			'hitung_terlambat' => 0,
			'hitung_on_time' => 0,
			'hitung_on_proses' => 0,
			'izinint_hitung_pengajuan' => 0,
			'izinint_hitung_selesai' => 0,
			'izinint_hitung_terlambat' => 0,
			'izinint_hitung_on_time' => 0,
			'izinint_hitung_on_proses' => 0,
		];
		DB::table(TABLESPACE.'.hitung_perizinan_bulanan')->where('bulan',$data['bulan'])->where('tahun',$data['tahun'])->delete();
		$this->init($data);
		$this->loadData();
		$this->insert(TABLESPACE.'.hitung_perizinan_bulanan');
		
		//$this->info("Data bulan ini berhasil diupdate!");
	}

}
