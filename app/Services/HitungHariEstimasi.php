<?php 
namespace App\Services;
use DB;
use App\Model\Libur;

class HitungHariEstimasi {
    private $liburs = [];
    
    public function __construct() {
        $this->init();
    }
    
    public function count($mulai, $sla) {
        $start = new \DateTime($mulai);

        $t = $start->getTimeStamp();
        for($i=0; $i<$sla; $i++){

            $addDay = 86400;

            $nextDay = date('w', ($t+$addDay));
            $nextDayFormat = date('Y-m-d 00:00:00', ($t+$addDay));

            if($nextDay == 0 || $nextDay == 6 || in_array($nextDayFormat, $this->liburs)) {
                $i--;
            }

            $t = $t+$addDay;
        }

        $start->setTimestamp($t);

        return $start->format( 'Y-m-d' );
    }

    protected function init() {
       $this->liburs = Libur::lists('tanggal')->toArray();
    }

    public function fakeInit() {
        return $this->liburs = array(0=>'2015-07-11',1=>'2015-07-14');
    }
}

