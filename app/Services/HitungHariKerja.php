<?php 
namespace App\Services;
use DB;
use App\Model\Libur;

class HitungHariKerja {
    private $liburs = [];
    
    public function __construct() {
        $this->init();
    }
    
    public function count($mulai, $selesai) {
        $start = new \DateTime($mulai);
        $end = new \DateTime($selesai);
        $end->modify('+1 day');

        $interval = $end->diff($start);

        $days = $interval->days;

        $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);

        foreach($period as $dt) {
            $curr = $dt->format('D');
            if ($curr == 'Sat' || $curr == 'Sun' || in_array($dt->format('Y-m-d 00:00:00'), $this->liburs)) {
                $days--;
            }
        }
        return $days;
    }

    protected function init() {
       $this->liburs = Libur::lists('tanggal')->toArray();
    }

    public function fakeInit() {
        return $this->liburs = array(0=>'2015-07-11',1=>'2015-07-14');
    }
}

