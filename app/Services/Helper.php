<?php
	function getLang() {
		return Session::get('lang','id');
	}
	function getMyQrCode() {
		if (!Auth::user()) return "";
		$perusahaan = App\Model\RegPerusahaan::find(Auth::user()->no_perusahaan);
		if (!$perusahaan) return "";
		return $perusahaan->id_qrcode;
	}
	function isRole($role) {
		if (!Auth::user()) return false;
		if (!Auth::user()->role) {
			Auth::user()->role = Auth::user()->roles()->get();
		}
		return Auth::user()->role->contains('nama_roles',$role);
	}
	function getInstansi() {
		if (!Auth::user()) return null;
		return Auth::user()->id_m_instansi;
	}
	function setLang($lang) {
		if (!in_array($lang,['id','en']))
			$lang = 'id';
		Session::put('lang',$lang);
	}
	function createSidebar() {
		Navigator::setTemplate('vendor.navigator.template.ace');
		Navigator::set('Home', url('home'), 'home');
		// Navigator::set('Dashboard', url('dashboardbaru'), 'dashboard');  
		
		// MOM SPM Dashboard - 29/03/2016 //
		if (isRole('monitoring')) {
			Navigator::set('Dashboard Kementerian', null, 'dashboard')->child(function ($nav) {
				$nav->set('Pengajuan Izin Kementerian', url('monitoring-pengajuan'), 'circle');
				$nav->set('Kualitas Layanan Kementerian', url('komparasi-layanan'), 'circle');
				//$nav->set('Jumlah Pengajuan Kementerian', url('jumlah-pengajuan'), 'circle');
				//$nav->set('Performansi Kinerja Kementerian', url('monitoring-performansi'), 'circle'); 
				return $nav;
			});
			Navigator::set('Dashboard PTSP', null, 'desktop')->child(function ($nav) {
				$nav->set('Pengajuan Izin PTSP', url('monitoring-pengajuan-ptsp'), 'circle');
				$nav->set('Kualitas Layanan PTSP', url('komparasi-layanan-ptsp'), 'circle');
				//$nav->set('Jumlah Pengajuan PTSP', url('jumlah-pengajuan-ptsp'), 'circle');
				//$nav->set('Performansi Kinerja PTSP', url('monitoring-performansi-ptsp'), 'circle'); 
				return $nav;
			});
			Navigator::set('Daftar Aktivitas Kementerian', null, 'users')->child(function($nav) { 
				$nav->set('Daftar Konsultasi', url('konsultasi'), 'table');
				$nav->set('Daftar Perizinan', url('trackingregister'), 'table');
				return $nav;
			}); 
			// Navigator::set('Investor Tracking', url('investortracking'), 'user-secret');
		}
		
		if (isRole('kl')) { 
			Navigator::set('Dashboard Kementerian', null, 'dashboard')->child(function ($nav) {
				$nav->set('Pengajuan Izin Kementerian', url('monitoring-pengajuan'), 'circle');
				$nav->set('Kualitas Layanan Kementerian', url('komparasi-layanan'), 'circle');
				//$nav->set('Jumlah Pengajuan Kementerian', url('jumlah-pengajuan'), 'circle');
				//$nav->set('Performansi Kinerja Kementerian', url('monitoring-performansi'), 'circle'); 
				return $nav;
			});
			Navigator::set('Daftar Aktivitas Kementerian', null, 'users')->child(function($nav) { 
				$nav->set('Daftar Konsultasi', url('konsultasi'), 'table');
				$nav->set('Daftar Perizinan', url('trackingregister'), 'table');
				return $nav;
			}); 
			// Navigator::set('Investor Tracking', url('investortracking'), 'user-secret');
		}
		
		if (isRole('ptsp')) { 
			Navigator::set('Dashboard PTSP', null, 'desktop')->child(function ($nav) {
				$nav->set('Pengajuan Izin PTSP', url('monitoring-pengajuan-ptsp'), 'table');
				$nav->set('Kualitas Layanan PTSP', url('komparasi-layanan-ptsp'), 'table');
				//$nav->set('Jumlah Pengajuan PTSP', url('jumlah-pengajuan-ptsp'), 'table');
				//$nav->set('Performansi Kinerja PTSP', url('monitoring-performansi-ptsp'), 'table'); 
				return $nav;
			});
			Navigator::set('Daftar Aktivitas PTSP', null, 'users')->child(function($nav) {  
				$nav->set('Daftar Perizinan', url('trackingregisterptsp'), 'table');
				return $nav;
			}); 
			// Navigator::set('Investor Tracking', url('investortracking'), 'user-secret');
		}
		
		if (isRole('admin')) {
			Navigator::set('Dashboard Kementerian', null, 'dashboard')->child(function ($nav) {
				$nav->set('Pengajuan Izin Kementerian', url('monitoring-pengajuan'), 'circle');
				$nav->set('Kualitas Layanan Kementerian', url('komparasi-layanan'), 'circle');
				//$nav->set('Jumlah Pengajuan Kementerian', url('jumlah-pengajuan'), 'circle');
				//$nav->set('Performansi Kinerja Kementerian', url('monitoring-performansi'), 'circle'); 
				return $nav;
			});
			Navigator::set('Dashboard PTSP', null, 'desktop')->child(function ($nav) {
				$nav->set('Pengajuan Izin PTSP', url('monitoring-pengajuan-ptsp'), 'table');
				$nav->set('Kualitas Layanan PTSP', url('komparasi-layanan-ptsp'), 'table');
				//$nav->set('Jumlah Pengajuan PTSP', url('jumlah-pengajuan-ptsp'), 'table');
				//$nav->set('Performansi Kinerja PTSP', url('monitoring-performansi-ptsp'), 'table'); 
				return $nav;
			});
			Navigator::set('Daftar Aktivitas Kementerian', null, 'users')->child(function($nav) { 
				$nav->set('Daftar Konsultasi', url('konsultasi'), 'table');
				$nav->set('Daftar Perizinan', url('trackingregister'), 'table');
				return $nav;
			}); 
			// Navigator::set('Rekap Pengajuan', url('rekap-pengajuan'), 'file');
			// Navigator::set('Kualitas Layanan', url('komparasi-layanan'), 'bar-chart');  
			Navigator::set('Manajemen Data', null, 'database')->child(function($nav) {
				$nav->set('User ', url('user'), 'user');
				$nav->set('Kategori Konsultasi', url('konsultasi-kategori'), 'th-large');
				$nav->set('Jenis Perizinan ', url('jenisperizinan'), 'file-o');
				$nav->set('Flow Izin Terintegrasi ', url('flowizinterintegrasi'), 'files-o');
				$nav->set('Instansi ', url('instansi'), 'building'); 
				$nav->set('Galeri Gambar SOP ', url('gallery'), 'image');
				//$nav->set('Hari Libur', url('libur'), 'circle-o');

				return $nav;
			});
			Navigator::set('Logs ', url('logs'), 'history');
			Navigator::set('Deskripsi SOP ', url('sop-deskripsi'), 'book');
		}
		// MOM SPM Dashboard - 29/03/2016 //
		
		/*
		if (isRole('admin') || isRole('executive')) {
			Navigator::set('Logs ', url('logs'), 'history');
			Navigator::set('Deskripsi SOP ', url('sop-deskripsi'), 'book');
		}
		if ((isRole('check_in') || isRole('check_out')) && !isRole('executive')) {
			Navigator::set('Pengajuan Permohonan', url('tracking'), 'sign-in');
			if (!isRole('admin'))
			Navigator::set('Daftar Pengajuan', url('tracking/debug'), 'cog');
		}
		if (isRole('konsultasi')) {
			Navigator::set('Konsultasi', null, 'users')->child(function($nav) {
				$nav->set('Input', url('konsultasi/create'), 'plus');
				$nav->set('Daftar Konsultasi', url('konsultasi'), 'table');
				return $nav;
			});
		}
		if (isRole('search_check')) {
			Navigator::set('Daftar Pengajuan', null, 'file')->child(function($nav) {
				$nav->set('Semua ', url('trackingregister'), 'table');
				$nav->set('Cari ', url('trackingregister/cari'), 'search');
				return $nav;
			});
		}
		if (isRole('investor_tracking')) {
			// if (!isRole('admin')){ 
				// Navigator::set('Pengajuan Izin', url('pengajuanizin'), 'file');
			// }
		}
		if (isRole('investor')) {
			Navigator::set('Tracking Integrasi', url('investortracking'), 'user-secret');
			Navigator::set('Pengajuan Izin ', url('pengajuanizin'), 'file');
		} */
		
		Navigator::set('Investor Tracking', url('investortracking'), 'user-secret');
		Navigator::set('Edit Profil', url('change-my-detail'), 'user');
		Navigator::set('Log out', url('auth/logout'), 'key');
	}
	function getTopBar() {
		return [
			['name'=>'Check In','icon'=>'sign-in','url'=>'#'],
			['name'=>'Check Out','icon'=>'sign-out','url'=>'#'],
		];
	}
	function getUserName() {
		return Auth::user()?Auth::user()->nama_user!=''&&Auth::user()->nama_user?Auth::user()->nama_user:Auth::user()->id_m_user:'';
	}
	function getFullDate($time = 0) {
		if ($time) {
			$time = strtotime($time);
		} else {
			return "-";
		}
		return listHari()[date("w",$time)].", ".date("j",$time)."&nbsp;".listBulan()[date("n",$time)]."&nbsp;".date("Y",$time);
	}
	function getWeekDates($date) {
		$week =  date('W', strtotime($date));
		$year =  date('Y', strtotime($date));
		$from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of monday in week
		$to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week
		return [$from,$to];
	}  
	function listBulan() {
		return [""=>"-",1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
	}
	function listBulanShort() {
		return [""=>"-",1=>'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
	}
	function listHari() {
		return ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"];
	}
	function getRoleChoice() {
		return [
			'user_fo' => 'User Front Office',
			'user_fo_instansi' => 'User Front Office Instansi',
			'user_tu' => 'User Tata Usaha',
			'user_tu_instansi' => 'User Tata Usaha Instansi',
			'investor' => 'Investor',
			'eksekutif' => 'Eksekutif',
			'admin' => 'Admin',
		];
	}
	function getRoleSet() {
		return [
			'user_fo' => ['check_in','konsultasi'],
			'user_tu' => ['check_out'],
			'user_fo_instansi' => ['check_in','check_in_instansi','konsultasi'],
			'user_tu_instansi' => ['check_out','check_out_instansi'],
			'investor' => ['investor'],
			'eksekutif' => ['executive','konsultasi','edit_check','search_check','investor_tracking'],
			'admin' => ['investor_tracking','check_in','check_out','edit_check','search_check','konsultasi','admin'],
		];
	}
	function getRoleIds($roles) {
		$return = [];
		foreach ($roles as $role) {
			$return[] = DB::table(TABLESPACE.'.m_roles')->where('nama_roles',$role)->first()->id_m_roles;
		}
		return $return;
	}
	function checkRole($pv) {
		foreach (getRoleSet() as $role => $privileges) {
			if (count(array_diff($pv,$privileges)) == 0)
				return $role;
		}
		return null;
	}
	function constName($name) {
		return str_replace(' ','_',strtoupper($name));
	}
	
	// Open Tambahan dari aplikasi Dashboard //
	function hitung_durasi($jam_login, $jam_logout) {
		$jam_login = new DateTime($jam_login);
		$jam_logout = new DateTime($jam_logout);
		$hitung_durasi = $jam_logout->diff($jam_login);
		
		return array('format' => $hitung_durasi->format('%h Jam %i menit %s detik'), 'jam' => $hitung_durasi->format('%h'), 'menit' => $hitung_durasi->format('%i'), 'detik' => $hitung_durasi->format('%s'), 'ts' => strtotime("" . $hitung_durasi->format('%h') . $hitung_durasi->format('%i') . $hitung_durasi->format('%s') . ""), 'satuan_menit' => intval(toMenit($hitung_durasi->format('%h'), $hitung_durasi->format('%i'), $hitung_durasi->format('%s'))));
	}
	
	function getUserId() {
		$user = \Auth::user();
		$userId = $user->id_m_user;
		return $userId;
	}
	
	function toMenit($h, $i, $s) {
		return ($h * 60) + ($i) + ($s / 60);
	}
	
	function currentYear() {
		return \Carbon\Carbon::now()->year;
	}
	// Close Tambahan dari aplikasi Dashboard //
?>
