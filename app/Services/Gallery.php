<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

	class Gallery extends \App\Model\BaseModel
	{
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.T_GALLERY';
        protected $primaryKey   = 'id_gallery';
        protected $sequence     = 'GALLERY_ID_GALLERY_SEQ';
	   	protected $fillable=[
	        'img_location'
	    ];

	}
