<?php
namespace App\Services;

class WordTemplate extends \PhpOffice\PhpWord\TemplateProcessor
{    
    public function setImageValue($search, $replace)
    {
        // Sanity check
        if (!file_exists($replace))
        {
            return;
        }
        // Delete current image
        $this->zipClass->deleteName('word/media/' . $search);
        // Add a new one
        $this->zipClass->addFile($replace, 'word/media/' . $search);
    }
    public function setUTF8Val($name,$value)
    {
        $this->setValue($name, htmlspecialchars(
            $value,
            ENT_COMPAT, 'UTF-8'));
        //$this->setValue($name,$value);
    }
}
