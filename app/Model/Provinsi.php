<?php namespace App\Model;
use yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

    class Provinsi extends Eloquent {
        protected $table        = 'SPIPARAM.TP_PROVINSI';
        protected $primaryKey   = 'id_provinsi';
        
        public $timestamps      = false;
        public $guarded         = ['create_tms','create_user'];
        
    }
?>
