<?php namespace App\Model;

    class Libur extends BaseModel {
        //use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = 'SPIPARAM.TP_HARI_LIBUR_NASIONAL';
        protected $primaryKey   = 'tanggal';
        
        public $timestamps      = false;
        
    }
?>
