<?php namespace App\Model;

use App\Model\Relation\HasManyTrackingRegisterDetail;
use App\Model\Relation\BelongsToDirektorat;
use App\Model\Relation\BelongsToBidangUsaha;
use App\Model\Relation\BelongsToInstansi;

    class JenisPerizinan extends BaseModel {
    	use HasManyTrackingRegisterDetail,BelongsToDirektorat,BelongsToBidangUsaha,BelongsToInstansi;
        //use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_JENIS_PERIZINAN';
        protected $primaryKey   = 'id_m_jenis_perizinan';
        protected $sequence     = TABLESPACE.'.M_JENIS_PERIZINAN_SEQ';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user','deleted_at'];
        
	    public function flow_detail_izin()
	    {
	        return $this->hasMany('App\Model\FlowDetailIzin', 'id_m_jenis_perizinan', 'id_m_jenis_perizinan');
	    }
    }
?>
