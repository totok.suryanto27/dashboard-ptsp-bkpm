<?php namespace App\Model;

    class TrackingRegisterDetailStop extends BaseModel {
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.TRACKING_REGISTER_DETAIL_STOP';
        protected $primaryKey   = 'id_stop';
        protected $sequence     = TABLESPACE.'.TRDS_ID_STOP_SEQ';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user'];
        
    }
?>
