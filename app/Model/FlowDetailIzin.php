<?php namespace App\Model;

use App\Model\Relation\BelongsToFlowDetail;
use App\Model\Relation\BelongsToJenisPerizinan;
use App\Model\Relation\BelongsToFlowIzinTerintegrasi;

    class FlowDetailIzin extends BaseModel {
    	use BelongsToFlowDetail,BelongsToJenisPerizinan,BelongsToFlowIzinTerintegrasi;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_FLOW_DETAIL_IZIN';
        protected $primaryKey   = 'id_m_flow_detail_izin';
        protected $sequence     = 'MFDI_ID_MFDI_SEQ';
        
        protected $visible = ['id_m_flow_detail_izin', 'id_m_flow_izin_terintegrasi'];
        
        public $timestamps      = true;
        
    }
?>
