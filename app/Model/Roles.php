<?php namespace App\Model;

use App\Model\Relation\BelongsToManyUsers;

    class Roles extends BaseModel {
        use BelongsToManyUsers;
        protected $table        = TABLESPACE.'.m_roles';
        protected $primaryKey   = 'id_m_roles';
        
        public $timestamps      = true;
        
    }
?>
