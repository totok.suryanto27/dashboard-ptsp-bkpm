<?php namespace App\Model\Relation;

trait BelongsToManyUsers {

    public function users()
    {
        return $this->belongsToMany('App\Users',TABLESPACE.'.m_user_roles','id_m_roles', 'id_m_user');
    }
}

?>
