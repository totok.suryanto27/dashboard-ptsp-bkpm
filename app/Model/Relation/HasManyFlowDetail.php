<?php namespace App\Model\Relation;

trait HasManyFlowDetail {

    public function flow_detail()
    {
        return $this->hasMany('App\Model\FlowDetail');
    }
}

?>
