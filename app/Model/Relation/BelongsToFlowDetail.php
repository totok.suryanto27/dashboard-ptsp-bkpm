<?php namespace App\Model\Relation;

trait BelongsToFlowDetail {

    public function flow_detail()
    {
        return $this->belongsTo('App\Model\FlowDetail','id_m_flow_detail','id_m_flow_detail');
    }
}

?>
