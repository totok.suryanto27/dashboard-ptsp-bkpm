<?php namespace App\Model\Relation;

trait HasManyFlowDetailIzin {

    public function flow_detail_izin()
    {
        return $this->hasMany('App\Model\FlowDetailIzin', 'id_m_flow_izin_terintegrasi', 'id_m_flow_izin_terintegrasi')->orderBy('urutan','ASC');
    }
}

?>
