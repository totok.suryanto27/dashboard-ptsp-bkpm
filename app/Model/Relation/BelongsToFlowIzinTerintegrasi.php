<?php namespace App\Model\Relation;

trait BelongsToFlowIzinTerintegrasi {

    public function flow_izin_terintegrasi()
    {
        return $this->belongsTo('App\Model\FlowIzinTerintegrasi');
    }
}

?>
