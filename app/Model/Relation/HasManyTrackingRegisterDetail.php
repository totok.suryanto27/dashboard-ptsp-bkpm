<?php namespace App\Model\Relation;

trait HasManyTrackingRegisterDetail {

    public function tracking_register_detail()
    {
        return $this->hasMany('App\Model\TrackingRegisterDetail');
    }
}

?>
