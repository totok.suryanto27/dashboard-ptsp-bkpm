<?php namespace App\Model\Relation;

trait BelongsToJenisPerizinan {

    public function jenis_perizinan()
    {
        return $this->belongsTo('App\Model\JenisPerizinan','id_m_jenis_perizinan','id_m_jenis_perizinan');
    }
}

?>
