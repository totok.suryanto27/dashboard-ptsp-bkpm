<?php namespace App\Model\Relation;

trait BelongsToProvinsi {

    public function provinsi()
    {
        return $this->belongsTo('App\Model\Provinsi','id_provinsi','id_provinsi');
    }
}

?>
