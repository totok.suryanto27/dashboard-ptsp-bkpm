<?php namespace App\Model\Relation;

trait BelongsToRegPerusahaan {

    public function reg_perusahaan()
    {
        return $this->belongsTo('App\Model\RegPerusahaan','id_perusahaan','no_perusahaan');
    }
}

?>
