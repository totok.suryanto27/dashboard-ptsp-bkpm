<?php namespace App\Model\Relation;

trait BelongsToPerusahaan {

    public function perusahaan()
    {
        return $this->belongsTo('App\Model\Perusahaan','id_perusahaan','id_perusahaan');
    }
}

?>
