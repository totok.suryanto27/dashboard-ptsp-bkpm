<?php namespace App\Model\Relation;

trait BelongsToInstansi {

    public function instansi()
    {
        return $this->belongsTo('App\Model\Instansi', 'id_m_instansi', 'id_m_instansi');
    }
}

?>
