<?php namespace App\Model\Relation;

trait HasOneHeader {

    public function tr_header()
    {
        return $this->hasOne('App\Model\TrackingRegisterHeader');
    }
}

?>
