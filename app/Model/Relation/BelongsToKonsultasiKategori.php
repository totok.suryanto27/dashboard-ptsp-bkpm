<?php namespace App\Model\Relation;

trait BelongsToKonsultasiKategori {

    public function kategori()
    {
        return $this->belongsTo('App\Model\KonsultasiKategori','id_konsul_cat','id_konsul_cat');
    }
}

?>
