<?php namespace App\Model\Relation;

trait BelongsToManyRoles {

    public function roles()
    {
        return $this->belongsToMany('App\Model\Roles',TABLESPACE.'.m_user_roles','id_m_user', 'id_m_roles');
    }
}

?>
