<?php namespace App\Model\Relation;

trait BelongsToBidangUsaha {

    public function bidang_usaha()
    {
        return $this->belongsTo('App\Model\BidangUsaha');
    }
}

?>
