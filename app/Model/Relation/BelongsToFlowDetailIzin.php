<?php namespace App\Model\Relation;

trait BelongsToFlowDetailIzin {

    public function flow_detail_izin()
    {
        return $this->belongsTo('App\Model\FlowDetailIzin');
    }
}

?>
