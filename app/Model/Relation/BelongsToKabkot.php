<?php namespace App\Model\Relation;

trait BelongsToKabkot {

    public function kabkot()
    {
        return $this->belongsTo('App\Model\Kabkot','id_kab_kota','id_kabkot');
    }
}

?>
