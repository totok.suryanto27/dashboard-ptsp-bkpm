<?php namespace App\Model\Relation;

trait BelongsToDirektorat {

    public function direktorat()
    {
        return $this->belongsTo('App\Model\Direktorat','id_m_direktorat','id_m_direktorat');
    }
}

?>
