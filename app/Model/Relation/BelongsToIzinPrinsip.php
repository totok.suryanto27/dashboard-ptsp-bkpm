<?php namespace App\Model\Relation;

trait BelongsToIzinPrinsip {

    public function izin_prinsip()
    {
        return $this->belongsTo('App\Model\IzinPrinsip');
    }
}

?>
