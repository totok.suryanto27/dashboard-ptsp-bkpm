<?php namespace App\Model\Relation;

trait HasManyDirektorat {

    public function direktorat()
    {
        return $this->hasMany('App\Model\Direktorat');
    }
}

?>
