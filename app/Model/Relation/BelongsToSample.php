<?php namespace App\Model\Relation;

trait BelongsToSample {

    public function sample()
    {
        return $this->belongsTo('App\Model\Sample');
    }
}

?>