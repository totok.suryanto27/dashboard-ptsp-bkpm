<?php namespace App\Model;

use DB;

use App\Model\Relation\HasManySOPSLA;
use App\Model\Relation\BelongsToBidangUsaha;
use App\Model\Relation\HasManyFlowDetailIzin;

    class FlowIzinTerintegrasi extends BaseModel {
    	use HasManySOPSLA,BelongsToBidangUsaha,HasManyFlowDetailIzin;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI';
        protected $primaryKey   = 'id_m_flow_izin_terintegrasi';
        protected $sequence     = TABLESPACE.'.MFIT_ID_MFIT_SEQ';
        
        public $timestamps      = true;
        
        static function getByQRCode($id) {
        	return DB::table(TABLESPACE.'.T_PT_FLOW_IZIN_TERINTEGRASI')
	        	->join(TABLESPACE.'.M_FLOW_IZIN_TERINTEGRASI', 'M_FLOW_IZIN_TERINTEGRASI.ID_M_FLOW_IZIN_TERINTEGRASI', '=', 'T_PT_FLOW_IZIN_TERINTEGRASI.ID_M_FLOW_IZIN_TERINTEGRASI')
	        	->select('M_FLOW_IZIN_TERINTEGRASI.ID_M_FLOW_IZIN_TERINTEGRASI','NAMA_FLOW','KETERANGAN','NO_SOP', 'TANGGAL_PENGAJUAN')
	        	->where('id_qrcode',$id)->get();
        }
    }
?>
