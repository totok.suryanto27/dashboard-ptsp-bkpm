<?php namespace App\Model;

use App\Model\Relation\HasManyDirektorat;

    class Instansi extends BaseModel {
    	use HasManyDirektorat;
        //use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_INSTANSI';
        protected $primaryKey   = 'id_m_instansi';
        protected $sequence     = TABLESPACE.'.INS_ID_INSTANSI_SEQ';
        
        public $timestamps      = true;
        
    }
?>
