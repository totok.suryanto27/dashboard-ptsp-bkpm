<?php namespace App\Model;

use App\Model\Relation\HasManyIzinPrinsip;
use App\Model\Relation\BelongsToProvinsi;
use App\Model\Relation\BelongsToKabkot;

    class RegPerusahaan extends BaseModel {
    	use HasManyIzinPrinsip,BelongsToProvinsi,BelongsToKabkot;
        protected $table        = TABLESPACE.'.T_REG_PT';
        protected $primaryKey   = 'no_perusahaan';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user'];
        
    }
?>
