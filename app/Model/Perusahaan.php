<?php namespace App\Model;

use App\Model\Relation\HasManyIzinPrinsip;
use App\Model\Relation\BelongsToProvinsi;
use App\Model\Relation\BelongsToKabkot;

    class Perusahaan extends BaseModel {
    	use HasManyIzinPrinsip,BelongsToProvinsi,BelongsToKabkot;
        protected $table        = 'SPIPISE.T_PERUSAHAAN';
        protected $primaryKey   = 'id_perusahaan';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user'];
        
    }
?>
