<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

use App\Model\Relation\BelongsToSample;
use App\Model\Relation\HasManySamples;

    class Sample extends Model {
        use BelongsToSample, HasManySamples;
        protected $table        = 'sample';
        protected $primaryKey   = 'sample_id';
        
        public $timestamps      = false;
        
        public function samplesWhat() {
            return $this->samples()->where('x', '=', 'what')->get();
        }
    }
?>