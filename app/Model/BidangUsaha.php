<?php namespace App\Model;

    class BidangUsaha extends BaseModel {
        protected $table        = TABLESPACE.'.M_BIDANG_USAHA';
        protected $primaryKey   = 'id_m_bidang_usaha';
        
        public $timestamps      = true;
        
    }
?>
