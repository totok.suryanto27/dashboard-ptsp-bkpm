<?php namespace App\Model;
use Auth;
use App\Model\Relation\HasOneStop;
use App\Model\Relation\BelongsToJenisPerizinan;
use App\Model\Relation\BelongsToDirektorat;
use App\Model\Relation\BelongsToInstansi;
use App\Model\Relation\BelongsToFlowDetail;
use App\Model\Relation\BelongsToFlowDetailIzin;
use App\Model\Relation\BelongsToBidangUsaha;
use App\Model\Relation\BelongsToIzinPrinsip;

    class TrackingRegisterDetail extends BaseModel {
    	use HasOneStop,BelongsToJenisPerizinan,BelongsToDirektorat,
    	BelongsToInstansi,BelongsToFlowDetail,BelongsToFlowDetailIzin,
    	BelongsToBidangUsaha,BelongsToIzinPrinsip;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        //protected $table        = TABLESPACE.'.TRACKING_REGISTER_DETAIL';
        //protected $sequence     = TABLESPACE.'.TRD_ID_TRD_SEQ';
        protected $table        = 'SPIPISE'.'.VIEW_TRACK_REGISTER_DETAIL';
        //protected $sequence     = 'SPIPISE'.'.TRD_ID_TRD_SEQ';
        //protected $primaryKey   = 'id_tracking_register_detail'; 
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user','id_tracking_register_detail'];
        
        const ON_GOING_BIASA = "Dalam Proses";
        const ON_GOING_TERLAMBAT = "Proses Terlambat";
        const ON_GOING_WARNING = "Peringatan";
        const ON_TIME = "Selesai Tepat Waktu";
        const DELAY = "Selesai Terlambat";
        const STOP = "Batal";
        const BELUM_DIAJUKAN = "Belum Diajukan";
        //const TUNDA = "Tunda";
        const SUDAH_DIAMBIL = "Sudah Diambil";
        const BELUM_DIAMBIL = "Belum Diambil";
        
        public static $status_color = [
            self::ON_GOING_BIASA => '#00A8F2',
            self::ON_GOING_TERLAMBAT => "#FF9000",
            self::ON_GOING_WARNING => "#E8FB00",
            self::ON_TIME => "#00bc23",
            self::DELAY => "#ED002F",
            self::STOP => "#7c3a00",
            self::BELUM_DIAJUKAN => "#b7b7b7",
            //self::TUNDA => "#7109AA",
            self::SUDAH_DIAMBIL => "#00bc23",
            self::BELUM_DIAMBIL => "#b7b7b7",
        ];

        static function getOpenIzin($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('id_qrcode',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->whereRaw('tanggal_selesai is null')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('id_qrcode',$id)->whereRaw('tanggal_selesai is null')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
            );
        }
        
        static function getOpenIzinNpwp($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('npwp',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->whereRaw('tanggal_selesai is null')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('npwp',$id)->whereRaw('tanggal_selesai is null')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
            );
        }

        static function getIzinSelesai($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('id_qrcode',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->whereRaw('tanggal_ambil is null')->where('flag','S')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_SELESAI','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('id_qrcode',$id)->whereRaw('tanggal_ambil is null')->where('proses_flag','S')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_SELESAI','ID_TRACKING_REGISTER_DETAIL']
            );
        }
        
        static function getIzinSelesaiNpwp($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('npwp',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->whereRaw('tanggal_ambil is null')->where('flag','S')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_SELESAI','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('npwp',$id)->whereRaw('tanggal_ambil is null')->where('proses_flag','S')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_SELESAI','ID_TRACKING_REGISTER_DETAIL']
            );
        }

        static function getIzinTunda($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('id_qrcode',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->where('performance','C')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('id_qrcode',$id)->where('performance','C')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
            );
        }
        
        static function getIzinTundaNpwp($id) {
            if (isRole('check_out_instansi'))
                return TrackingRegisterDetail::where('npwp',$id)->where('id_m_instansi',Auth::user()->id_m_instansi)->where('performance','C')->get(
                    ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
                );
            return TrackingRegisterDetail::where('npwp',$id)->where('performance','C')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL']
            );
        }

        static function getIzinTunggal($id) {
            return TrackingRegisterDetail::where('id_qrcode',$id)->whereRaw('id_m_flow_izin_terintegrasi is null')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL','ID_M_JENIS_PERIZINAN']
            );
        }

        static function getIzinTunggalNpwp($id) {
            return TrackingRegisterDetail::where('npwp',$id)->whereRaw('id_m_flow_izin_terintegrasi is null')->get(
                ['NAMA_JENIS_PERIZINAN','TANGGAL_PENGAJUAN','TANGGAL_THRESHOLD','ID_TRACKING_REGISTER_DETAIL','ID_M_JENIS_PERIZINAN']
            );
        }

        static function getIzinTerIntegrasi($id) {
            return TrackingRegisterDetail::where('id_qrcode',$id)->whereRaw('id_m_flow_izin_terintegrasi is not null')->groupBy('id_m_flow_izin_terintegrasi')->get(
                ['id_m_flow_izin_terintegrasi']
            );
        }

        public function getStatus() {
            if(isset($this->performance)) {
                switch ($this->performance) {
                        case "I":
                        case "O":
                            return self::ON_TIME;
                        case "D":
                            return self::DELAY;
                        case "R":
                            return self::STOP;
                        case "C":
                            return self::TUNDA;
                    }
            }
            else if(isset($this->status)){
				 switch ($this->status) { 
                        case "1":
                            return "Proses Pembuatan Izin di K/L";
                        case "2":
                            return "Proses Penomoran Perizinan di BKPM";
                        case "3":
                            return "Proses Ditolak Oleh K/L";
                        case "4":
                            return "Penomoran Perizinan di BKPM Selesai";
                    }
			}
            else if(date("Y-m-d H:i:s") >= $this->tanggal_threshold && $this->tanggal_threshold != null) {
                return self::ON_GOING_TERLAMBAT;
            } 
            else if(date("Y-m-d H:i:s") >= $this->tanggal_warning && $this->tanggal_warning != null) {
                return self::ON_GOING_WARNING;
            } 
            else if(!isset($this->tanggal_selesai)) {
                return self::ON_GOING_BIASA;
            }
        }
        
        public function getComment() {
            $comment = '';
            if ($this->komentar_check_in) {
                $comment .= $this->komentar_check_in."<br>";
            }
            if ($this->komentar_check_out) {
                $comment .= $this->komentar_check_out."<br>";
            }
            $trds = $this->trd_stop()->first();
            if ($trds) {
                if ($trds->alasan) {
                    $comment .= $trds->alasan."<br>";
                }
            }
            return $comment==''?'-':$comment;
        }
    }
?>
