<?php namespace App\Model;
use App\Model\Relation\BelongsToInstansi;
use App\Model\Relation\BelongsToDirektorat;
use App\Model\Relation\BelongsToKonsultasiKategori;

    class Konsultasi extends BaseModel {
    	use BelongsToDirektorat,BelongsToInstansi,BelongsToKonsultasiKategori;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.t_konsultasi';
        protected $primaryKey   = 'id_konsultasi';
        protected $sequence     = TABLESPACE.'.KONSULTASI_ID_KONSULTASI_SEQ';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user'];
        
    }
?>
