<?php namespace App\Model;

use App\Model\Relation\BelongsToDirektorat;
use App\Model\Relation\BelongsToInstansi;
use App\Model\Relation\BelongsToBidangUsaha;
use App\Model\Relation\BelongsToIzinPrinsip;

    class TrackingRegisterHeader extends BaseModel {
    	use BelongsToBidangUsaha,BelongsToIzinPrinsip,BelongsToDirektorat,
    	BelongsToInstansi;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.TRACKING_REGISTER_HEADER';
        protected $primaryKey   = 'id_qrcode';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user'];
        
    }
?>
