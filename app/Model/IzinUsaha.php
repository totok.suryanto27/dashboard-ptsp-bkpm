<?php namespace App\Model;

use App\Model\Relation\HasManyTrackingRegisterDetail;
use App\Model\Relation\BelongsToFlowIzinTerintegrasi;
use App\Model\Relation\BelongsToRegPerusahaan;
use App\Model\Relation\BelongsToPerusahaan;

    class IzinUsaha extends BaseModel {
    	use HasManyTrackingRegisterDetail,BelongsToFlowIzinTerintegrasi,BelongsToRegPerusahaan,BelongsToPerusahaan;
        protected $table        = 'IUTSPIPISE.T_IUT_PERMOHONAN';
        protected $primaryKey   = 'id_permohonan';
        
        public $timestamps      = true;
        
    }
?>
