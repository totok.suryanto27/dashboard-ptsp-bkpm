<?php namespace App\Model;

    class SOPSLA extends BaseModel {
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_SOP_SLA';
        protected $primaryKey   = 'id_m_sop_sla';
        protected $sequence     = TABLESPACE.'.MSOPSLA_ID_MSOPSLA_SEQ';
        
        public $timestamps      = true;
        
    }
?>
