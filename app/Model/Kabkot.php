<?php namespace App\Model;
use yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

    class Kabkot extends Eloquent {
        protected $table        = 'SPIPARAM.TP_KABKOT';
        protected $primaryKey   = 'id_kabkot';
        
        public $timestamps      = false;
        
    }
?>
