<?php namespace App\Model;
use Auth;
use yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Request;
use DB;

class BaseModel extends Eloquent {
	const CREATED_AT = 'CREATE_TMS';
	const UPDATED_AT = 'UPDATE_TMS';
	
	protected $dates = ['deleted_at'];

	public static function boot()
	{
		parent::boot();
		
		static::creating(function($model) {
			if (!isset(static::$nocreator))
			$model->create_user = Auth::user()?Auth::user()->id_m_user:Request::ip();
			return true;
		});
		static::updating(function($model) {
			if (!isset(static::$nocreator))
			$model->update_user = Auth::user()?Auth::user()->id_m_user:Request::ip();
			$tablename = static::getTableName();
			$pk = strtolower(static::getPrimaryKeyName());
			$original = [];
			$dirty = $model->getDirty();
			foreach($dirty as $attribute => $value){
				switch ($attribute) {
					default:
						$original[$attribute] = $model->getOriginal($attribute);
						break;
				}
			}
			DB::table(TABLESPACE.'.USER_LOG_ACTION')->insert([
				'id_m_user' => Auth::user()?Auth::user()->id_m_user:'NOT LOGGED IN',
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => $tablename,
				'model_id' => $model->$pk,
				'action' => 'update',
				'before' => json_encode($original),
				'after' => json_encode($dirty),
			]);
			return true;
		});
		
		static::created(function($model) {
			$pk = static::getPrimaryKeyName();
			DB::table(TABLESPACE.'.USER_LOG_ACTION')->insert([
				'id_m_user' => Auth::user()?Auth::user()->id_m_user:'NOT LOGGED IN',
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => static::getTableName(),
				'model_id' => $model->$pk,
				'action' => 'create',
			]);
		});
		
		static::deleting(function($model) {
			$pk = static::getPrimaryKeyName();
			DB::table(TABLESPACE.'.USER_LOG_ACTION')->insert([
				'id_m_user' => Auth::user()?Auth::user()->id_m_user:'NOT LOGGED IN',
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => static::getTableName(),
				'model_id' => $model->$pk,
				'action' => 'delete',
			]);
		});
	}
	
	public static function getTableName()
	{
		return with(new static)->getTable();
	}
	
	public static function getPrimaryKeyName()
	{
		return with(new static)->getKeyName();
	}
}
