<?php namespace App\Model;

use App\Model\Relation\HasManyTrackingRegisterDetail;
use App\Model\Relation\BelongsToFlowIzinTerintegrasi;
use App\Model\Relation\BelongsToRegPerusahaan;
use App\Model\Relation\BelongsToPerusahaan;
use App\Model\Relation\HasOneHeader;

    class IzinPrinsip extends BaseModel {
    	use HasManyTrackingRegisterDetail,BelongsToFlowIzinTerintegrasi,BelongsToRegPerusahaan,BelongsToPerusahaan,HasOneHeader;
        protected $table        = 'IPSPIPISE.T_IP_PERMOHONAN';
        protected $primaryKey   = 'id_permohonan';
        
        public $timestamps      = true;
        
    }
?>
