<?php namespace App\Model;

    class KonsultasiKategori extends BaseModel {
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.t_konsultasi_kategori';
        protected $primaryKey   = 'id_konsul_cat';
        protected $sequence     = TABLESPACE.'.konsultasi_id_konsul_kat_seq';
        
        public $timestamps      = true;
        public $guarded         = ['create_tms','create_user','update_tms','update_user'];
        
    }
?>
