<?php namespace App\Model;

use App\Model\Relation\BelongsToInstansi;

    class Direktorat extends BaseModel {
    	use BelongsToInstansi;
        //use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_DIREKTORAT';
        protected $primaryKey   = 'id_m_direktorat';
        protected $sequence     = 'DIR_ID_DIREKTORAT_SEQ';
        
        public $timestamps      = true;
        
    }
?>
