<?php namespace App\Model;
use App\Model\Relation\BelongsToInstansi;
use App\Model\Relation\BelongsToJenisPerizinan;

    class SopDeskripsi extends BaseModel {
        use \Illuminate\Database\Eloquent\SoftDeletes;
        use BelongsToJenisPerizinan,BelongsToInstansi;
        
        protected $table        = TABLESPACE.'.M_SOP_DESKRIPSI';
        protected $primaryKey   = 'id';
        protected $sequence     = TABLESPACE.'.INS_ID_SOP_DESKRIPSI_SEQ';
        
        public $timestamps      = false;
        public static $nocreator= true;
        
        public function children()
        {
            return $this->hasMany('App\Model\SopDeskripsi','id_parent','id_m_sop_deskripsi');
        }
	    public function parent()
	    {
	        return $this->belongsTo('App\Model\SopDeskripsi','id_parent','id_m_sop_deskripsi')->where('bahasa',$this->bahasa)->first();
	    }
        
    }
?>
