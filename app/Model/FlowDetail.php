<?php namespace App\Model;

use App\Model\Relation\BelongsToFlowIzinTerintegrasi;
use App\Model\Relation\BelongsToInstansi;
use App\Model\Relation\HasManyFlowDetailIzin;

    class FlowDetail extends BaseModel {
    	use BelongsToFlowIzinTerintegrasi,BelongsToInstansi,HasManyFlowDetailIzin;
        use \Illuminate\Database\Eloquent\SoftDeletes;
        
        protected $table        = TABLESPACE.'.M_FLOW_DETAIL';
        protected $primaryKey   = 'id_m_flow_detail';
        
        public $timestamps      = true;
        
    }
?>
