<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHits extends Model
{
    protected $table = TABLESPACE.".USER_HITS";
}
