@extends('backend')
@section('title','Flow Izin Terintegrasi')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'No Sop','required'=>false,'name'=>'nama_instansi','value'=>$flow_izin_terintegrasi->no_sop])
    @include('form.view_styled',['label'=>'Nama Flow','required'=>false,'name'=>'nama_flow','value'=>$flow_izin_terintegrasi->nama_flow])
    @include('form.view_styled',['label'=>'Keterangan','required'=>false,'name'=>'keterangan','value'=>$flow_izin_terintegrasi->keterangan])
    @include('form.view_styled',['label'=>'Id Bidang Usaha','required'=>false,'name'=>'id_m_bidang_usaha','value'=>$flow_izin_terintegrasi->id_m_bidang_usaha])
    @foreach($flow_izin_terintegrasi->flow_detail_izin as $izin)
        @include('form.view_styled',['label'=>'Jenis Perizinan '.$izin->urutan,'required'=>false,'name'=>'nama_instansi_eng','value'=>$izin->jenis_perizinan->nama_jenis_perizinan])
    @endforeach
   
    </div>
    <div class="space-4"></div>
  </div>
  <a href="{{url('/flowizinterintegrasi/edit', $flow_izin_terintegrasi->id_m_flow_izin_terintegrasi)}}" class="btn btn-success">Flow Izin Terintegrasi Edit</a>
  <a href="{{url('/flowizinterintegrasi/editdetail', $flow_izin_terintegrasi->id_m_flow_izin_terintegrasi)}}" class="btn btn-success">Detail Izin Edit</a>
  </div>
@endsection
@section('styles')
<style>
.profile-info-name {
    width:400px;
}
.profile-info-value {
    margin-left:400px;
}
</style>
@endsection