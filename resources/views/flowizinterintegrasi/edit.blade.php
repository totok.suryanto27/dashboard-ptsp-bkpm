@extends('backend')
@section('title','Flow Izin Terintegrasi')
@section('content')

<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('flowizinterintegrasi/update', $flow_izin_terintegrasi->id_m_flow_izin_terintegrasi) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.number',['label'=>'No SOP','required'=>false,'name'=>'no_sop','value'=> $flow_izin_terintegrasi->no_sop])
        @include('form.text',['label'=>'Nama Flow','required'=>false,'name'=>'nama_flow','value'=> $flow_izin_terintegrasi->nama_flow])
        @include('form.text',['label'=>'Keterangan','required'=>false,'name'=>'keterangan','value'=> $flow_izin_terintegrasi->keterangan])
        @include('form.select2',['label'=>'Nama Bidang Usaha','required'=>false,'name'=>'nama_bidang_usaha','data'=>App\Model\BidangUsaha::lists('nama_bidang_usaha','id_m_bidang_usaha'),'empty'=>'-- Pilih --','value'=> $flow_izin_terintegrasi->id_m_bidang_usaha])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
console.log("asd");
$("#addBtn").on("click", function() {
    console.log("asd");
    var ctr = $("#additional").find(".extra").length;
        var $ddl = $("#jenis_perizinan_container").clone();
        $ddl.attr("id", "ddl" + ctr);
        $ddl.addClass("extra");
        $ddl.find("#jenis_perizinan").attr("name","jenis_perizinan_"+ctr);
        $ddl.find("#jenis_perizinan").attr("id","jenis_perizinan_"+ctr);

        $("#additional").append($ddl);
    
});
</script>
@endsection