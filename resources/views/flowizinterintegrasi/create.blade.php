@extends('backend')
@section('title','Flow Izin Terintegrasi')
@section('content')

<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('flowizinterintegrasi/store') }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.number',['label'=>'No SOP','required'=>false,'name'=>'no_sop'])
        @include('form.text',['label'=>'Nama Flow','required'=>false,'name'=>'nama_flow'])
        @include('form.text',['label'=>'Keterangan','required'=>false,'name'=>'keterangan'])
        @include('form.select2',['label'=>'Nama Bidang Usaha','required'=>false,'name'=>'nama_bidang_usaha','data'=>App\Model\BidangUsaha::lists('nama_bidang_usaha','id_m_bidang_usaha'),'empty'=>'-- Pilih --'])
        @include('form.select3',['label'=>'Jenis Perizinan','required'=>false,'name'=>'jenis_perizinan','data'=>App\Model\JenisPerizinan::lists('nama_jenis_perizinan','id_m_jenis_perizinan'),'empty'=>'-- Pilih --'])
        
        <span id="additional"></span>
        <input id="addBtn" type="button"  class="btn" value=" + " />
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
$("#addBtn").on("click", function() {
    var ctr = $("#additional").find(".extra").length;
        var $ddl = $("#jenis_perizinan_container").clone();
        $ddl.attr("id", "jenis_perizinan_" + ctr+"_container");
        $ddl.addClass("extra");
        $ddl.find("#jenis_perizinan").attr("name","jenis_perizinan_"+ctr);
        $ddl.find("#jenis_perizinan").attr("id","jenis_perizinan_"+ctr);

        $("#additional").append($ddl);
    
});

$("#minusBtn").on("click", function() {
var ctr = $("#additional").find(".extra").length -1;
var $ddl = $("#ddl"+ctr);
$ddl.remove();
$ddl.find("#jenis_perizinan_"+ctr).remove();
});

$(document).on('click','.btn-minus', function() {
    var lastId = $("body").find("[id*='jenis_perizinan']").length/2;
    if(lastId > 1) {
     var myID = $(this).prev().children().attr('id');
     var $ddl =$("#"+myID+"_container");
     console.log(myID);
     $ddl.remove();
     $(this).remove();
    } else{
        alert("Tidak boleh kosong");
    }

});
</script>
@endsection