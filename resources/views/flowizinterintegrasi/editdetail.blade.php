@extends('backend')
@section('title','Flow Izin Terintegrasi')
@section('content')

<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('flowizinterintegrasi/updatedetail', $id) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
            @if(sizeof($flowdetailizin->toArray())>0)
            @foreach($flowdetailizin as $izin) 
                @include('form.select3',['label'=>'Jenis Perizinan '. $izin->urutan,'required'=>false,'name'=>'jenis_perizinan_'.$izin->urutan,'data'=>App\Model\JenisPerizinan::lists('nama_jenis_perizinan','id_m_jenis_perizinan'),'empty'=>'-- Pilih --','value'=> $izin->id_m_jenis_perizinan])
                
            @endforeach
            @else
            @include('form.select3',['label'=>'Jenis Perizinan 1','required'=>false,'name'=>'jenis_perizinan_1','data'=>App\Model\JenisPerizinan::lists('nama_jenis_perizinan','id_m_jenis_perizinan'),'empty'=>'-- Pilih --'])
            @endif
       <span id="additional"></span>
        <div class="space-4"></div>
        <input id="addBtn" type="button"  class="btn" value=" + " />
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
console.log("asd");

var lastcontainer='#'+'jenis_perizinan_1_container';

$("#addBtn").on("click", function() {
    
        var lastId = $("body").find("[id*='jenis_perizinan']").length/2;
        lastcontainer='#'+'jenis_perizinan_'+ lastId+'_container';

        var ctr = $("#additional").find(".extra").length;
        var $ddl = $(lastcontainer).clone();
        $ddl.attr("id", "jenis_perizinan_" + (lastId)+1+"_container");
        $ddl.addClass("extra");
        $ddl.find("#jenis_perizinan_"+lastId).attr("name","jenis_perizinan_"+(lastId+1));
        $ddl.find("#jenis_perizinan_"+lastId).attr("id","jenis_perizinan_"+(lastId+1));

        $("#additional").append($ddl);
        rearrage();
});

$(document).on('click','.btn-minus', function() {
    var lastId = $("body").find("[id*='jenis_perizinan']").length/2;
    if(lastId > 1) {
     var myID = $(this).prev().children().attr('id');
     var $ddl =$("#"+myID+"_container");
     $ddl.remove();
     $(this).remove();
     rearrage();
    } else{
        alert("Tidak boleh kosong");
    }

});

function rearrage() {
    var x =$("body").find("[id*='jenis_perizinan']");
    var i =1;
    var index=1;

    x.each(function() {
        if(i%2==1) {
            $('#'+this.id).attr("id","jenis_perizinan_"+index+"_container");
            var label = '#'+this.id +' > div > div > label';
            $(label).html("Jenis Perizinan "+index);
            lastcontainer = '#'+this.id;

         //  console.log(this.id+" "+index);

        }else{
           // console.log(this.id+" sebelum "+index);
            $('#'+this.id).attr("id","jenis_perizinan_"+(index));
            $('#'+this.id).attr("name","jenis_perizinan_"+(index));
           // console.log(this.id+" "+index);
        }
        
        if(i%2==0) {
            index++;
        }
        i=i+1;
    })

    //console.log(x.length);
}
</script>
@endsection