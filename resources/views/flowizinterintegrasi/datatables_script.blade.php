<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id_m_flow_izin_terintegrasi', name: 'id_m_flow_izin_terintegrasi'},
                {data: 'nama_flow', name: 'nama_flow'},
                {data: 'no_sop', name: 'no_sop'},
                {data: 'keterangan', name: 'keterangan'},
                {data: 'id_m_bidang_usaha', name: 'id_m_bidang_usaha'},
                

                {data: 'actions', name: 'actions'},
            ],
            order: [[1,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
