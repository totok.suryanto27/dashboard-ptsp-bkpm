{{ Navigator::setActive(url('flowizinterintegrasi')) }}
@extends('backend')
@section('title','Flow Izin Terintegrasi')
@section('content')

 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
 <a href="{{url('/flowizinterintegrasi/create')}}" class="btn btn-success">Flow Izin Terintegrasi Baru</a>
  <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Nama FLow</th>
         <th>No Sop</th>
         <th>Keterangan</th>
         <th>id_m_bidang_usaha</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('flowizinterintegrasi.datatables_script',['url'=>url('flowizinterintegrasi/getData')])
<script>
var params={};
drawTable('#search-result');
</script>
@endsection