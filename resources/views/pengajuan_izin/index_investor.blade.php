{{ Navigator::setActive(url('pengajuanizin')) }}
@extends('backend')
@section('title','Pengajuan Izin')
@section('content')
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Nama Perizinan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>SLA</th>
         <th>Status</th>
         <th>Read</th>
     </tr>
     </thead>

     <tbody>
     </tbody>
 </table>
@include('_includes.legend')
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
@include('_includes.legend_styles')
</style>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
@include('pengajuan_izin.datatables_script_investor',['url'=>url('pengajuanizin/search')])
<script>
var params = {};

create();
function create() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawTable('#search-result');
}
</script>

@endsection