@extends('backend')
@section('title','Pengajuan Izin')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'ID QR Code','required'=>false,'name'=>'id_qr_code','value'=>$tracking->id_qrcode])
    @include('form.view_styled',['label'=>'Nama Perizinan','required'=>false,'name'=>'nama_perizinan','value'=>$tracking->nama_jenis_perizinan])
    @include('form.view_styled',['label'=>'Tanggal Mulai','required'=>false,'name'=>'tanggal_pengajuan','value'=>$tracking->tanggal_pengajuan])
    @include('form.view_styled',['label'=>'Tanggal Selesai','required'=>false,'name'=>'tanggal_selesai','value'=>$tracking->tanggal_selesai])
    @include('form.view_styled',['label'=>'Tanggal Estimasi','required'=>false,'name'=>'tanggal_selesai_estimasi','value'=>$tracking->tanggal_selesai_estimasi])
    @include('form.view_styled',['label'=>'SLA','required'=>false,'name'=>'sla','value'=>$tracking->sla])
    @include('form.view_styled',['label'=>'Lama Proses','required'=>false,'name'=>'lama_proses','value'=>$tracking->lama_proses?$tracking->lama_proses." Hari":'-'])
    @include('form.view_styled',['label'=>'Status','required'=>false,'name'=>'status','value'=>$tracking->status])
    @if ($tracking->komentar_check_in && $tracking->komentar_check_in != '')
    @include('form.view_styled',['label'=>'Komentar (Registrasi)','required'=>false,'name'=>'komentar_check_in','value'=>$tracking->komentar_check_in])
    @endif
    @if ($tracking->komentar_check_out && $tracking->komentar_check_out != '')
    @include('form.view_styled',['label'=>'Komentar (Izin Selesai)','required'=>false,'name'=>'komentar_check_out','value'=>$tracking->komentar_check_out])
    @endif
    <?php $stop = $tracking->trd_stop()->first(); ?>
    @if ($stop)
    @include('form.view_styled',['label'=>'Komentar (STOP)','required'=>false,'name'=>'komentar_stop','value'=>$stop->alasan_stop])
    @endif
    </div>
    
    <div class="space-4"></div>
  </div>
  </div>
@endsection
@section('styles')
<style>
.profile-info-name {
    width:400px;
}
.profile-info-value {
    margin-left:400px;
}
</style>
@endsection