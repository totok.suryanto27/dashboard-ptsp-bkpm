<script>
var initialized = false;
var table;
function drawTable(table_id, button) {
    button.start();
    console.log(button);
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id_qrcode', name: 'id_qrcode'},
                {data: 'nama_instansi', name: 'nama_instansi'},
                {data: 'tanggal_masuk', name: 'tanggal_masuk'},
                {data: 'tanggal_selesai', name: 'tanggal_selesai'},
                {data: 'tanggal_estimasi', name: 'tanggal_threshold'},
                {data: 'sla', name: 'sla'},
                {data: 'status', name: 'status',
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).attr('class', cellData?cellData.replace(/ /g,'_').toUpperCase():'');
                    },
                },
                {data: 'read', name: 'read', searchable: false, orderable: false},
            ],
            order: [[2,'desc']],
            initComplete: function() {
                button.stop();
            }
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
        button.stop();
    }
}
</script>
