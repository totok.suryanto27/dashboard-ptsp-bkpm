<li class="disabled">
    <a href="{{ $item->url }}">{!! $item->icon !!} {{ $item->text }}</a>
</li>