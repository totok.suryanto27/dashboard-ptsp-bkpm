<li class="active" >
    <a href="#">{!! $item->iconFa() !!} {{ $item->text }}<span class="fa arrow"></span></a>

    {!! $child !!}
</li>