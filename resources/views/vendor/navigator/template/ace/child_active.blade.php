<li class="treeview active">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" >
        {!! $item->iconFa() !!} <span class="menu-text">{{ $item->text }}</span>
    </a>
    
    {!! $child !!}

</li>