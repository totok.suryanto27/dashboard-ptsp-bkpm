<li class="disabled">
	<a href="{{ $item->url }}">{!! $item->iconFa() !!} <span class="menu-text"> {{ $item->text }}</span></a>
</li>