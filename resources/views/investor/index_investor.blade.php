{{ Navigator::setActive(url('investortracking')) }}
<?php $no_title = true; ?>
@extends('backend')

@section('title','Tracking')
@section('content')
<div class="tabbable" style="margin-left: -20px;">
    <div class="row" style="margin: 20px 0 0 0;">
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Investor 
                </div>
                <div class="panel-body left-panel">
                    @include('investor.form_search')
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Detail
                </div>
                <div class="panel-body left-panel">
                    @include('investor.form_detail')
                </div>
            </div>
        </div>
    </div>
</div>
@include('_includes.legend',['belum_diajukan'=>true])
<div class="table-wrapper">
<table id="table" class="table table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Pengajuan Izin</h4>
      </div>
      <div class="modal-body">
         <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Nama Jenis Perizinan','required'=>false,'name'=>'fci_nama_jenis_perizinan_1'])
    @include('form.view_styled',['label'=>'Status','required'=>false,'name'=>'fci_status_1'])
    @include('form.view_styled',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'fci_tanggal_pengajuan_1','value'=>getFullDate()])
    @include('form.view_styled',['label'=>'Estimasi Selesai','required'=>false,'name'=>'fci_tanggal_selesai_estimasi_1'])
    @include('form.view_styled',['label'=>'Tanggal Selesai','required'=>false,'name'=>'fci_tanggal_selesai_1'])
    @include('form.view_styled',['label'=>'SOP','required'=>false,'name'=>'fci_sop_1'])
    @include('form.view_styled',['label'=>'Lama Proses','required'=>false,'name'=>'fci_lama_proses_1'])
    </div>
      </div>
    
    </div>
  </div>
</div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.form-buttons {
    text-align: right;
}
.form-control {
    border: none;
    padding: 0;
}
.form-control.autosize-transition {
    border: 1px solid #d5d5d5;
    margin-bottom: 4px;
}
.form-group input {
    height: 30px;
    padding-top: 0;
    padding-bottom: 0;
    margin-bottom: 4px;
}
.form-control-static {
    font-weight: bold;
    padding-top: 4px;
}
.form-group {
    margin-bottom: 0;
}
.no-padding {
    padding: 0;
}
.left-panel, .right-panel {
    padding: 15px;
}
.form-horizontal .radio {
    padding-top: 0;
}
.radio label {
    padding-left: 0;
}

.box-izin {
    padding : 5px 10px;
    margin: 5px 10px;
    cursor: pointer;
    border: 1px solid #000000;
}

select.form-control {
    border: 1px solid #d5d5d5;
}
@include('_includes.legend_styles')
.table-wrapper {
  overflow-x: scroll;
  width: 100%;
}
.profile-info-name {
    width:200px;
}
.profile-info-value {
    margin-left:200px;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/jquery/jquery.autocomplete.min.js') }}" type="text/javascript"></script>
<script>
$("#s_no_ip").on('keyup change',function() {
    $("#fci_id_qrcode").val($(this).val());
    $("#fci_no_ip").html($(this).val());
    $("#fco_no_ip").html($(this).val());
    $("#fk_no_ip").html($(this).val());
});
$("#sr_nama_perusahaan").autocomplete({
   serviceUrl: "{{url('tracking/search-perusahaan')}}", 
   paramName: 'q',
   onSelect: function (suggestion) {
       $('#s_no_ip').val(suggestion.data);
       $("#s_no_ip").change();
       f_search_perusahaan(suggestion.data);
   },
});
$('textarea[class*=autosize]').autosize();
$(".right-panel").slimScroll({
    height: '440px'
});
$('.date-picker').datepicker({
    autoclose: true
});
var search_perusahaan = ['nama_perusahaan','no_perusahaan','npwp','alamat','nama_kabkot',
    'nama_provinsi','telepon','email','nama_pimpinan'];
var search_detail_izin = ['nama_instansi','nama_bidang_usaha','tanggal_pengajuan','status','nama_direktorat','tanggal_selesai_estimasi','tanggal_selesai','sop','lama_proses'];
var search_detail_izin_modal = ['nama_jenis_perizinan','id_qrcode','tanggal_pengajuan','status','tanggal_selesai_estimasi','tanggal_selesai','sop','lama_proses'];
var binds_nama_perusahaan = ['fco','fci'];
var binds_konsultasi = ['nama_perusahaan','alamat','telepon','email'];
var konsultasi_form = ['fk_id_m_instansi','fk_id_m_direktorat','fk_nama_tamu','fk_jawaban',
    'fk_nama_perusahaan','fk_alamat','fk_telepon','fk_email','fk_judul_konsultasi','fk_pertanyaan'];
var check_in_form = ['fci_id_m_instansi','fci_id_m_direktorat','fci_id_m_jenis_perizinan'];
var check_out_form = ['fco_id_m_instansi','fco_id_m_direktorat'];
var jenis_izin;
var fontcolor = "black";
//var jenis_izin = {!! json_encode(App\Model\JenisPerizinan::get(['nama_jenis_perizinan','id_m_jenis_perizinan','sop','id_m_instansi','id_m_direktorat'])) !!};
var instansi = {!! json_encode(App\Model\Instansi::get(['nama_instansi','id_m_instansi'])) !!};
var direktorat = {!! json_encode(App\Model\Direktorat::get(['nama_direktorat','id_m_direktorat'])) !!};
$('#s_no_ip').keydown(function(event) {
    if (event.keyCode == 13) {
        f_search_perusahaan($(this).val());
        return false;
     }
});
$(document).on('click','.box-izin', function() {
    console.log($(this).attr('value'));
    var id = $(this).attr('value');
   $.getJSON("{{url('investortracking/detail')}}",{id_m_jenis_perizinan: id,id_m_flow_izin_terintegrasi: $("#jenis_perizinan").val(),
            id_qrcode: $("#fci_id_qrcode").val()},function(data) {
                console.log(data);
            for (i = 0; i < search_detail_izin_modal.length; i++) {
                if(data[search_detail_izin_modal[i]] == "NULL") data[search_detail_izin_modal[i]] = "-";
                $("#fci_"+search_detail_izin_modal[i]+"_1").html(data[search_detail_izin_modal[i]]);
            }
            $('#myModal').modal('show');
        });

});

//f_search_perusahaan($('#s_no_ip').val());
$('#s_no_ip').change(function() {
f_search_perusahaan($('#s_no_ip').val());
});
f_search_perusahaan('');
function f_search_perusahaan(id) {
    $.ajax({
        cache: false,
        data: {id: id},
        url: "{{url('investortracking/perusahaan')}}",
        success: function(data) {
            $('#jenis_perizinan').children().remove();
            $('#jenis_perizinan').append('<option >-</option>');
            $.each(data.izin_terintegrasi, function(k, v) {
                console.log(v);
                $('<option>').val('i_'+v.id_m_flow_izin_terintegrasi).text(v.nama_flow).appendTo('#jenis_perizinan');
            });
            $.each(data.izin_tunggal, function(k, v) {
                console.log(v);
                $('<option>').val(v.id_m_jenis_perizinan).text(v.nama_jenis_perizinan).appendTo('#jenis_perizinan');
            });
            $("#fci_nama_perusahaan").html(data['nama_perusahaan']);
            if ($.isEmptyObject(data)) {
                $("#s_"+search_perusahaan[0]).html("IP Tidak ditemukan")
                for (i = 1; i < search_perusahaan.length; i++) {
                    $("#s_"+search_perusahaan[i]).html("");
                }
            } else {
                for (i = 0; i < search_perusahaan.length; i++) {
                    $("#s_"+search_perusahaan[i]).html(data[search_perusahaan[i]]);
                }
                var additional = "";
                for (i = 0; i < data.izin_terintegrasi.length; i++) {
                    additional += '\
                    <div class="radio">\
                        <label>\
                            <input name="fci_id_m_flow_terintegrasi" type="radio" class="ace" value="'+data.izin_terintegrasi[i].id_m_flow_izin_terintegrasi+'">\
                            <span class="lbl"> '+data.izin_terintegrasi[i].nama_flow+' (Tanggal Pengajuan: '+data.izin_terintegrasi[i].tanggal_pengajuan+')</span>\
                        </label>\
                    </div>';
                }
                $("#fci_id_m_flow_terintegrasi_additional").html(additional);
                additional = "";
                
                $("#fco_id_tracking_register_detail_additional").html(additional);
            }
            for (i = 0; i < binds_nama_perusahaan.length; i++) {
                $("#"+binds_nama_perusahaan[i]+"_nama_perusahaan").html($("#s_nama_perusahaan").html());
            }
        },
        error: function(xhr,error,thrown) {
            console.log(error);
            alert(error);
        }
    });
}
$('#btn-reset').click(function() { resetForm() });
function resetForm(direct) {
    if (!direct) l_btn_reset.start();
    
    for (i = 0; i < search_perusahaan.length; i++) {
        $("#s_"+search_perusahaan[i]).html("");
    }
    
    for (i = 0; i < check_in_form.length; i++) {
        $("#"+check_in_form[i]).val(null);
    }
    
    for (i = 0; i < check_out_form.length; i++) {
        $("#"+check_out_form[i]).val(null);
    }
    
    for (i = 0; i < binds_nama_perusahaan.length; i++) {
        $("#"+binds_nama_perusahaan[i]+"_nama_perusahaan").html("");
    }
    
    $("#fci_tanggal_selesai_estimasi").html("");
    $('#s_no_ip').val("");
    $("#s_no_ip").change();
    $("#sr_nama_perusahaan").val("");
    $("#fci_id_m_jenis_perizinan").val("");
    $("#fci_id_m_flow_terintegrasi_additional").html("");
    $("#fco_id_tracking_register_detail_additional").html("");
    if (!direct) setTimeout(function(){l_btn_reset.stop();},500);
}
$('#jenis_perizinan').change(function() {
    $("#table thead").remove();
    $("#table tbody").remove();
    $("#fci_nama_instansi").html("");
    $("#fci_nama_izin").html("");
    $("#fci_nama_direktorat").html("");
    $("#fci_status").html("");
    $("#fci_tanggal_pengajuan").html("");
    $("#fci_tanggal_selesai_estimasi").html("");
    if(this.value.substr(0,2) == "i_") {
        $.getJSON("{{url('investortracking/table')}}",{id_m_flow_izin_terintegrasi: $("#jenis_perizinan").val().replace('i_',''), id_qrcode: $("#fci_id_qrcode").val()},function(data) {
            
            var myTable = $("#table");
            var thead = myTable.find("thead tr");
            var thRows =  myTable.find("tr:has(th)");
            var tbody = myTable.find("tbody tr");
            if (thead.length===0){
                thead = $("<thead><tr></tr></thead>").appendTo(myTable);    
            }
            if (tbody.length===0){
                tbody = $("<tbody><tr></tr></tbody>").appendTo(myTable);    
            }
            Object.keys(data).map(function(value, index) {
                //console.log(value+" "+index);
                $('#table thead tr').append($("<th >"+value+"</th>"));
                //$().appendTo("thead");
                var construct="<td>";
                for( var i =0;i < data[value].length;i++) {
                    var color = cellcolor(data[value][i]["status"]);
                    if(color=="green")fontcolor="red";
                    else fontcolor="black";
                    construct+="<div class='box-izin' value='"+data[value][i]["id"]+"' style='background-color:"+color+";color:"+fontcolor+"'>"+data[value][i]["izin"]+"</div>";
                }
                construct+="</td>";
                //$(construct).appendTo("tbody");
                $('#table tbody tr').append($(construct));
                
            });
            

        });
    }else {
        $.getJSON("{{url('investortracking/detail')}}",{id_m_jenis_perizinan: $("#jenis_perizinan").val(),
            id_qrcode: $("#fci_id_qrcode").val()},function(data) {
                console.log(data);
            for (i = 0; i < search_detail_izin.length; i++) {
                if(data[search_detail_izin[i]] == "NULL") data[search_detail_izin[i]] = "-";
                $("#fci_"+search_detail_izin[i]).html(data[search_detail_izin[i]]);
            }
            var status = $('#fci_status').html();
            var color = cellcolor(status);
            $('#fci_status').css('background-color',color);
        });

    }

})



function cellcolor(status) {
    status = status.toUpperCase().replace(/ /g,"_");
    switch (status) {
        @foreach (App\Model\TrackingRegisterDetail::$status_color as $status => $color)
        case "{{constName($status)}}":
            return "{{$color}}";
        @endforeach
    }
}
function findInArray(arr,attr,param,res) {
    var data = $.grep(arr,function(e) { return e[attr]===param; });
    return data.length?data[0][res]:'-';
}


</script>
@endsection
