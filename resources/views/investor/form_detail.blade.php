    <form class="form-horizontal" id="detail">
    @include('form.hidden',['name'=>'fci_id_qrcode'])
    <label class ="form-control">Jenis Izin</label>
    <select id="jenis_perizinan" name="fci_id_m_jenis_perizinan" class="form-control"><option>-</option></select>
    
    </form>
    <div class="space-4">
    </div>
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Instansi','required'=>false,'name'=>'fci_nama_instansi'])
    @include('form.view_styled',['label'=>'Nama Izin','required'=>false,'name'=>'fci_nama_izin'])
    @include('form.view_styled',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fci_nama_perusahaan'])
    <?php //@include('form.view_styled',['label'=>'Direktorat','required'=>false,'name'=>'fci_nama_direktorat']) ?>
    @include('form.view_styled',['label'=>'Bidang Usaha','required'=>false,'name'=>'fci_nama_bidang_usaha'])
    @include('form.view_styled',['label'=>'Status','required'=>false,'name'=>'fci_status'])
    @include('form.view_styled',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'fci_tanggal_pengajuan','value'=>getFullDate()])
    @include('form.view_styled',['label'=>'Estimasi Selesai','required'=>false,'name'=>'fci_tanggal_selesai_estimasi'])
    @include('form.view_styled',['label'=>'Tanggal Selesai','required'=>false,'name'=>'fci_tanggal_selesai'])
    @include('form.view_styled',['label'=>'SOP','required'=>false,'name'=>'fci_sop'])
    @include('form.view_styled',['label'=>'Lama Proses','required'=>false,'name'=>'fci_lama_proses'])
    </div>
    
