<form class="form-horizontal">
    @if (isRole('investor'))
    @include('form.select',['label'=>'IP','required'=>false,'readonly'=>true,'name'=>'s_no_ip','empty'=>'Pilih IP','data'=>App\Model\IzinPrinsip::whereHas('perusahaan',function($q) {$q->where('npwp_perusahaan',Auth::user()->npwp);})->lists('id_permohonan','id_permohonan')])
    @else

	@include('form.text',['label'=>'NPWP','required'=>false,'name'=>'sr_npwp'])
	@include('form.text',['label'=>'Nama&nbsp;Perusahaan','required'=>false,'name'=>'sr_nama_perusahaan'])
	@include('form.text',['label'=>'IP','required'=>false,'name'=>'s_no_ip'])
	<div id="s_no_ip_choice_container">
	@include('form.select',['label'=>'IP','required'=>false,'name'=>'s_no_ip_choice','data'=>[]])
	</div>
    <div>
    <a id="btn-cari" class="btn btn-primary btn-sm pull-right ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari</a>
    </div>
    @endif
</form>
<div class="form-horizontal" id="search-result" style="
    margin-top: 50px;
">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Nama Perusahaan','name'=>'s_nama_perusahaan'])
    @include('form.view_styled',['label'=>'No Perusahaan','name'=>'s_no_perusahaan'])
    @include('form.view_styled',['label'=>'NPWP','name'=>'s_npwp'])
    @include('form.view_styled',['label'=>'Alamat','name'=>'s_alamat'])
    @include('form.view_styled',['label'=>'Kota / Kabupaten','name'=>'s_nama_kabkot'])
    @include('form.view_styled',['label'=>'Provinsi','name'=>'s_nama_provinsi'])
    @include('form.view_styled',['label'=>'Telepon','name'=>'s_telepon'])
    @include('form.view_styled',['label'=>'Email','name'=>'s_email'])
    @include('form.view_styled',['label'=>'Pimpinan','name'=>'s_nama_pimpinan'])
    </div>
</div>
