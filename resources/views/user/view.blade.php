@extends('backend')
@section('title','User')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <table class="table table-bordered table-invoice">
        <tr>
            <td class="width30">Instansi</td>
            <td class="width70"></td>
        </tr>
        <tr>
            <td>Direktorat</td>
            <td></td>
        </tr>
        <tr>
            <td>Nama User</td>
            <td>{{$user->nama_user}}</td>
        </tr>
        <tr>
            <td>Password</td>
            <td>{{$user->password}}</td>
        </tr>
        <tr>
            <td>Nama Instansi</td>
            <td>{{$user->nama_instansi}}</td>
        </tr>
        <tr>
            <td>Nama Roles</td>
            <td>{{$user->nama_roles}}</td>
        </tr>
        <tr>
            <td>Nama Direktorat</td>
            <td>{{$user->nama_direktorat}}</td>
        </tr>
        <tr>
            <td>Telepon</td>
            <td>{{$user->telepon}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>{{$user->jabatan}}</td>
        </tr>
        @if ($user->npwp)
        <tr>
            <td>NPWP</td>
            <td>{{$user->npwp}}</td>
        </tr>
        @endif
        
    </table>
    <div class="space-4"></div>
  </div>
  </div>
@endsection