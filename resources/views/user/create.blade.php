@extends('backend')
@section('title','User')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('user/store') }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.text',['label'=>'ID Nama User','required'=>true,'name'=>'id_m_user'])
        @include('form.text',['label'=>'Nama User','required'=>true,'name'=>'nama_user'])
        @include('form.text',['label'=>'Password','required'=>true,'name'=>'password'])
        @include('form.text',['label'=>'Jabatan','required'=>false,'name'=>'jabatan'])
        @include('form.text',['label'=>'Telp','required'=>false,'name'=>'telp'])
        @include('form.text',['label'=>'Email','required'=>false,'name'=>'email'])
        @include('form.text',['label'=>'NPWP','required'=>false,'name'=>'npwp'])
        @include('form.select2',['label'=>'Nama Instansi','required'=>false,'name'=>'id_m_instansi','data'=>App\Model\Instansi::lists('nama_instansi','id_m_instansi'),'empty'=>'-- Pilih --'])
        @include('form.select2',['label'=>'Nama Direktorat','required'=>false,'name'=>'id_m_direktorat','data'=>App\Model\Direktorat::lists('nama_direktorat','id_m_direktorat'),'empty'=>'-- Pilih --'])
        @include('form.select2',['label'=>'Role','required'=>true,'name'=>'role','data'=>getRoleChoice(),'empty'=>'-- Pilih --'])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
var arr =[];
$("#addBtn").on("click", function() {
    var ctr = $("#additional").find(".extra").length;
        var $ddl = $("#roles_container").clone();
        $ddl.attr("id", "roles_" + ctr+"_container");
        $ddl.addClass("extra");
        $ddl.find("#roles").attr("id","roles_"+ctr);

        $("#additional").append($ddl);
    
});
$(document).on('click','.btn-minus', function() {
    var lastId = $("body").find("[id*='roles']").length/2;
    if(lastId > 1) {
     var myID = $(this).prev().children().attr('id');
     var $ddl =$("#"+myID+"_container");

     console.log(myID);
     $ddl.remove();
     $(this).remove();
    } else{
        alert("Tidak boleh kosong");
    }
                
});

/*$(document).on('change','.select2me', function() {
        for(var i=0;i<arr.length;i++) {
            $('.select2me').not(this).find('option[value="'+arr[i]+'"]').hide();
        }
        var value = $(this).val();
        arr.push(value);
        
       $('.select2me').not(this).find('option[value="'+value+'"]').hide();
       console.log(arr);
             

});*/
/*$(document).on('focus','.select2me', function() {
        var prevval = $(this).val();
        var index = arr.indexOf(prevval);
         if (index > -1) {
            arr.splice(index, 1);
        }
        console.log(prevval);
             

});*/

/* $('.select2me').on('change', function(event ) {
       
   });*/
</script>
@endsection 