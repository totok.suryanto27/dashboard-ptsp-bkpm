{{ Navigator::setActive(url('user')) }}
@extends('backend')
@section('title','User')
@section('content')

 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
  <a href="{{url('/user/create')}}" class="btn btn-success">User Baru</a>
 <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Nama</th>
         <th>Jabatan</th>
         <th>Telp</th>
         <th>Email</th>
         <th>Nama Instansi</th>
         <th>Nama Direktorat</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
<script>
//$(document).ready(function() {
//    oTable = $('#konsultasi').DataTable({
//        "processing": true,
//        "serverSide": true,
//        "ajax": {
//            "url": "{{url('konsultasi/getData')}}",
//            "type": "GET"
//        },
//        "columns": [
//            {data: 'nama_instansi', name: 'nama_instansi'},
//            {data: 'nama_direktorat', name: 'nama_direktorat'},
//            {data: 'nama_tamu', name: 'nama_tamu'},
//            {data: 'nama_perusahaan', name: 'nama_perusahaan'},
//            {data: 'alamat', name: 'alamat'},
//            {data: 'telepon', name: 'telepon'},
//            {data: 'email', name: 'email'},
//            {data: 'judul_konsultasi', name: 'judul_konsultasi'},
//            {data: 'pertanyaan', name: 'pertanyaan'},
//            {data: 'jawaban', name: 'jawaban'},
//            {data: 'nama_petugas', name: 'nama_petugas'},
//            {data: 'actions', name: 'actions'}
//        ]
//    });
//});
</script>
@include('user.datatables_script',['url'=>url('user/getData')])
<script>
var params = {};
$('.do-search').click(function() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawSuratTable('#search-result');
});
$("#search-form").hide();
$(".toggle-search").click(function() {$("#search-form").toggle();});
drawTable('#search-result');
</script>
@endsection