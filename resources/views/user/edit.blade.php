@extends('backend')
@section('title','Edit Profile')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('user/update',$user->id_m_user) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.text',['label'=>'ID Nama User','required'=>true,'name'=>'id_m_user','value'=>$user->id_m_user])
        @include('form.text',['label'=>'Nama User','required'=>true,'name'=>'nama_user','value'=>$user->nama_user])
        @include('form.text',['label'=>'Password','required'=>false,'name'=>'password'])
        @include('form.text',['label'=>'Jabatan','required'=>false,'name'=>'jabatan','value'=>$user->jabatan])
        @include('form.text',['label'=>'Telp','required'=>false,'name'=>'telp','value'=>$user->telp])
        @include('form.text',['label'=>'Email','required'=>false,'name'=>'email','value'=>$user->email])
        @include('form.text',['label'=>'NPWP','required'=>false,'name'=>'npwp','value'=>$user->npwp])
        @include('form.select2',['label'=>'Nama Instansi','required'=>false,'name'=>'id_m_instansi','data'=>App\Model\Instansi::lists('nama_instansi','id_m_instansi'),'empty'=>'-- Pilih --','value'=>$user->id_m_instansi])
        @include('form.select2',['label'=>'Nama Direktorat','required'=>false,'name'=>'id_m_direktorat','data'=>App\Model\Direktorat::lists('nama_direktorat','id_m_direktorat'),'empty'=>'-- Pilih --','value'=>$user->id_m_direktorat])
        @include('form.select2',['label'=>'Role','required'=>true,'name'=>'role','data'=>getRoleChoice(),'empty'=>'-- Pilih --','value'=>checkRole($user->roles->lists('nama_roles')->toArray())])
        <?php /* New kind of role selection
        <?php $i = 1; ?>
        @if(sizeof($user->roles->toArray())>0)
        @foreach($user->roles as $role)
            @include('form.select4',['label'=>'Roles','class'=>'roles','required'=>false,'name'=>'roles[]','id'=>'roles_'.$i++,'data'=>App\Model\Roles::lists('nama_roles','id_m_roles'),'empty'=>'-- Pilih --','value'=>$role->id_m_roles])
        @endforeach
        @else
            @include('form.select4',['label'=>'Roles','class'=>'roles','required'=>false,'name'=>'roles[]','id'=>'roles','data'=>App\Model\Roles::lists('nama_roles','id_m_roles'),'empty'=>'-- Pilih --'])
        @endif
        <span id="additional"></span>
        <input id="addBtn" type="button"  class="btn" value=" + " />
        */ ?>
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
var lastId =$("body").find("[id*='roles']").length/2;
$("#addBtn").on("click", function() {
    var ctr = $("#additional").find(".extra").length;
        var $ddl = $(".roles").first().clone();
        $ddl.attr("id", "roles_" + ctr+"_container");
        $ddl.addClass("extra");
        $ddl.find("#roles").attr("id","roles_"+ctr);

        $("#additional").append($ddl);
    
});
$(document).on('click','.btn-minus', function() {

    lastId = $("body").find("[id*='roles']").length/2;
    if(lastId > 1) {
     var $ddl = $(this).parent().parent().parent().parent();
     
     $ddl.remove();
     $(this).remove();
    } else{
        alert("Tidak boleh kosong");
    }

});
/*$(document).ready(function() {
    $('.select2me').map(function(){
              $('.select2me').not(this).find('option[value="'+this.value+'"]').hide(); 
          })
})
$(document).on('change','.select2me', function() {
    var prevValue = $(this).data('previous');

    //console.log($(this).data('previous'));
       $('.select2me').not(this).find('option[value="'+prevValue+'"]').show();    
       var value = $(this).val();
       $(this).data('previous',value);
       $('.select2me').not(this).find('option[value="'+value+'"]').hide();
});*/

 
</script>
@endsection