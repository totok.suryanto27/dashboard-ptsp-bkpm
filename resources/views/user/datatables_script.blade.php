<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id_m_user', name: 'id_m_user'},
                {data: 'nama_user', name: 'nam_user'},
                {data: 'jabatan', name: 'jabatan'},
                {data: 'telp', name: 'telp'},
                {data: 'email', name: 'email'},
                {data: 'id_m_instansi', name: 'id_m_instansi'},
                {data: 'id_m_direktorat', name: 'id_m_direktorat'},
                

                {data: 'actions', name: 'actions'},
            ],
            order: [[1,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
