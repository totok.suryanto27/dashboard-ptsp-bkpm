@extends('backend')
@section('title','Lihat Jenis Perizinan')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Nama Izin','required'=>false,'name'=>'nama_jenis_perizinan','value'=>$jenis_perizinan->nama_jenis_perizinan])
    @include('form.view_styled',['label'=>'Singkatan','required'=>false,'name'=>'abbrv','value'=>$jenis_perizinan->abbrv])
    @include('form.view_styled',['label'=>'SOP (Hari)','required'=>false,'name'=>'sop','value'=>$jenis_perizinan->sop])
    @include('form.view_styled',['label'=>'Threshold (Hari)','required'=>false,'name'=>'threshold','value'=>$jenis_perizinan->threshold])
    <?php
    $direktorat = $jenis_perizinan->direktorat()->first();
    $bidang_usaha = $jenis_perizinan->bidang_usaha()->first();
    $instansi = $jenis_perizinan->instansi()->first();
    //@include('form.view_styled',['label'=>'Direktorat','required'=>false,'name'=>'id_m_direktorat','value'=>$direktorat?$direktorat->nama_direktorat:'-'])
    ?>
    @include('form.view_styled',['label'=>'Bidang Usaha','required'=>false,'name'=>'m_id_bidang_usaha','value'=>$bidang_usaha?$bidang_usaha->nama_bidang_usaha:'-'])
    @include('form.view_styled',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','value'=>$instansi?$instansi->nama_instansi:'-'])
    @include('form.view_styled',['label'=>'Izin Tunggal','required'=>false,'name'=>'standalone','value'=>$jenis_perizinan->standalone=='1'?'Ya':'Tidak'])
    </div>
    
    <div class="space-4"></div>
  </div>
    <a href="{{url('/jenisperizinan/edit',$jenis_perizinan->id_m_jenis_perizinan)}}" class="btn btn-success"><i class="fa fa-edit"></i> Edit Jenis Perizinan</a>
  </div>
@endsection
@section('styles')
<style>
.profile-info-name {
    width:400px;
}
.profile-info-value {
    margin-left:400px;
}
</style>
@endsection