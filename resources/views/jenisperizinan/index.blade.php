{{ Navigator::setActive(url('jenisperizinan')) }}
@extends('backend')
@section('title','Jenis Perizinan')
@section('content')

 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
 <a href="{{url('/jenisperizinan/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Jenis Perizinan Baru</a>
  <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Nama Izin</th>
         <th>Singkatan</th>
         <th>SOP (hari)</th>
         <th>Threshold (hari)</th>
         <th>Instansi</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('jenisperizinan.datatables_script',['url'=>url('jenisperizinan/getData')])
<script>
var params={};
drawTable('#search-result');
</script>
@endsection