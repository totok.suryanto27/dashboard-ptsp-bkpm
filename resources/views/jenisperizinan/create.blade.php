@extends('backend')
@section('title','Jenis Perizinan')
@section('content')

<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('jenisperizinan/store') }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.text',['label'=>'Nama Izin','required'=>false,'name'=>'nama_jenis_perizinan'])
        @include('form.text',['label'=>'Singkatan','required'=>false,'name'=>'abbrv'])
        @include('form.number',['label'=>'SOP (Hari)','required'=>false,'name'=>'sop'])
        @include('form.number',['label'=>'Threshold (Hari)','required'=>false,'name'=>'threshold'])
        <?php //@include('form.text',['label'=>'Nama Direktorat','required'=>false,'name'=>'nama_direktorat']) ?>
        @include('form.text',['label'=>'Nama Bidang Usaha','required'=>false,'name'=>'nama_bidang_usaha'])
        @include('form.text',['label'=>'Nama Instansi','required'=>false,'name'=>'nama_instansi'])
        @include('form.select',['label'=>'Izin Tunggal','required'=>false,'name'=>'standalone','data'=>['Tidak','Ya']])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.form-buttons {
    text-align: right;
}
.form-control.select2-container {
    border: none;
    padding: 0;
}
.form-control.autosize-transition {
    border: 1px solid #d5d5d5;
    margin-bottom: 4px;
}
.form-group input {
    height: 30px;
    padding-top: 0;
    padding-bottom: 0;
    margin-bottom: 4px;
}
.form-control-static {
    font-weight: bold;
    padding-top: 4px;
}
.form-group {
    margin-bottom: 0;
}
.no-padding {
    padding: 0;
}
.left-panel, .right-panel {
    padding: 15px;
}
.form-horizontal .radio {
    padding-top: 0;
}
.radio label {
    padding-left: 0;
}
.profile-user-info {
    margin: 0;
}
.profile-info-name {
    width: 178px;
}
.profile-info-value {
    margin-left: 188px;
    padding-left: 0px;
}
#search-result {
    margin-top: 40px;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/jquery/jquery.autocomplete.min.js') }}" type="text/javascript"></script>
<script>

var jenis_izin = {!! json_encode(App\Model\JenisPerizinan::with('flow_detail_izin')->get(['nama_jenis_perizinan','id_m_jenis_perizinan','sop','id_m_instansi','id_m_direktorat'])) !!};
var instansi = {!! json_encode(App\Model\Instansi::get(['nama_instansi','id_m_instansi'])) !!};
var direktorat = {!! json_encode(App\Model\Direktorat::get(['nama_direktorat','id_m_direktorat'])) !!};
var bidang_usaha = {!! json_encode(App\Model\BidangUsaha::get(['nama_bidang_usaha','id_m_bidang_usaha'])) !!};


$("#nama_instansi").select2({
        data: function() { 
        return {results: $.map(instansi,function(v) { 
            return {id: v.id_m_instansi,text: v.nama_instansi}; 
        })};
    }
});
$("#nama_bidang_usaha").select2({
    data: function() { 
        return {results: $.map(bidang_usaha,function(v) { 
            return {id: v.id_m_bidang_usaha,text: v.nama_bidang_usaha}; 
        })};
    }
});
$("#nama_direktorat").select2({
    data: function() { 
        return {results: $.map(direktorat,function(v) { 
            return {id: v.id_m_direktorat,text: v.nama_direktorat}; 
        })};
    }
});
</script>
@endsection
