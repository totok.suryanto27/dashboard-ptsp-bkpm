<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
            {data: 'id_m_jenis_perizinan', name: 'id_m_jenis_perizinan'},
            {data: 'nama_jenis_perizinan', name: 'nama_jenis_perizinan'},
            {data: 'abbrv', name: 'abbrv'},
            {data: 'sop', name: 'sop'},
            {data: 'threshold', name: 'threshold'},
            {data: 'instansi.nama_instansi', name: 'id_m_instansi'},
  
            {data: 'actions', name: 'actions', searchable: false, orderable: false}
            ],
            order: [[1,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>

