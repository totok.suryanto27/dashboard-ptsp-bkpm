<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    console.log(d);
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
            {data: 'time', name: 'time'},
            {data: 'id_m_user', name: 'id_m_user'},
            {data: 'action', name: 'action'},
            {data: 'model', name: 'model'},
            {data: 'model_id', name: 'model_id'},
            ],
            order: [[0,'desc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
