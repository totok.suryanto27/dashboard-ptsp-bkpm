{{ Navigator::setActive(url('logs')) }}
@extends('backend')
@section('title','Logs')
@section('content')
 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Time</th>
         <th>Nama</th>
         <th>Actions</th>
         <th>Table</th>
         <th>Data ID</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('logs.datatables_script',['url'=>url('logs/getData')])
<script>
var params = {};

drawTable('#search-result');
</script>
@endsection