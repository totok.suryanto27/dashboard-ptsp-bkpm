<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Optional theme -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> -->
    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/ace-fonts.css') }}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/waktu.js') }}"></script>
  </head>
  <body>
    <header>
      <div class="menu-top">
        <div class="container">
          <nav class="navbar navbar-inverse">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="{{Request::is('/')?'active':''}}"><a href="{{ url('/') }}"><i class="fa fa-home"> </i> {{ trans('pages.home') }} <span class="sr-only">(current)</span></a></li>
                  <li class="{{Request::is('perizinan-kl')?'active':''}}"><a href="{{ url('perizinan-kl') }}">{{ trans('pages.perizinan-kl') }}</a></li>
                  <li class="{{Request::is('page/perizinan-online')?'active':''}}"><a href="{{ url('page/perizinan-online') }}">{{ trans('pages.perizinan-online') }}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a class="{{getLang()=='id'?'active':''}}" href="{{ url('setlang/id') }}">Indonesia</a></li>
                  <li><a class="{{getLang()=='en'?'active':''}}" href="{{ url('setlang/en') }}">English</a></li>
                  @if (Auth::guest())
                  <li><a href="{{ url('/auth/login') }}">Login <i class="fa fa-user"></i></a></li>
                  <?php //<li><a href="{{ url('/auth/register') }}">Register</a></li> ?>
                  @else
                  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()?Auth::user()->nama_user:'' }} <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/home') }}">Tracking</a></li>
                  <li><a href="/dashboard_bkpm/index.php">Advanced Dashboard</a></li>
                  <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                  </ul>
                  </li>
                  @endif
                </ul>
              </div>
          </nav>
        </div>
      </div>
      <div class="head-block">
        <div class="container">
          <div class="line1"></div>
          <div class="middle-block">
            <div class="row-eq-height">
              <!-- <div class="col-md-2"> -->
                <div class="log">
                  <img src="{{ asset('images/'.trans('pages.logo')) }}" class="img-responsive logo"/>
                </div>
              <!-- </div> -->
              <div class="col-md-12 bg">
                <div class="row">
                  <div class="col-md-6 head-left">
                    <h4>SPM Dashboard</h4>
                    <h3><strong>OSS Center</strong><span style="font-size:14px"> Ver 2.0</span></h3>
                  </div>
                  <div class="col-md-6 head-right">
                    <div class="row">
                      <p class="text-right">
                        <span><script type="text/JavaScript">
                        @if (getLang() == "en")
                        document.write(getDayStrEn())
                        document.write(getDateStrEn())
                        @else
                        document.write(getDayStr())
                        document.write(getDateStr())
                        @endif
                        </script></span>
                      </p>
                      <p class="text-right" id="clock"></p>
                      <div class="mb-5"></div>
                      <div class="col-md-8 col-md-offset-4">
                        <div class="row">
                          <form>
                            <div class="form-group">
                              <input class="form-control" type="text" placeholder="{{ trans('pages.search') }}"/>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="menu-top2">
      <div class="container">
        <p>{{ trans('pages.welcome') }}</p></p>
      </div>
    </div>
    <div class="container">
      <div class="col-md-12 content">
        <div class="row">
          <div class="col-md-3 content-left">
            <div class="a">
              <a href="{{ url('perizinan-kl') }}" class="btn btn-lg btn-block btn-nipz">
                <h4>{{ trans('pages.pokl') }}</h4>
                <p><small>{{ trans('pages.pokl-desc') }}</small></p> 
              </a>
            </div>
            <div class="b">
              <a href="{{ url('page/perizinan-online') }}" class="btn btn-lg btn-block btn-nipz">
                <h4>{{ trans('pages.potptspp') }}</h4>
                <p><small>{{ trans('pages.potptspp-desc') }}</small></p> 
              </a>
            </div>
            <!-- <div class="c">
              <a href="#" class="btn btn-lg btn-block btn-nipz">
                <h4>Daftar Perizinan di PTSP Pusat</h4>
                <p><small>Daftar perizinan yang pelaksanaannya melalui PTSP Pusat namun belum online</small></p> 
              </a>
            </div>
            <div class="col-md-12 d">
              <div class="row">
                <a href="#" class="btn btn-lg btn-block btn-nipz">
                  <h4>Lacak Status Perizinan</h4>
                </a>
                <div class="col-md-12">
                  <form>
                    <div class="form-group">
                      <input class="form-control" type="text" placeholder="Masukkan kode QR"/>
                    </div>
                  </form>
                </div>
              </div>
            </div> -->
          </div>
          @yield('content')
        </div>
      </div>
    </div>

    <footer>
      <p class="text-center">Indonesia Investment Coordinating Board</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="{{ asset('js/bootstrap.min.js') }}"></script>-->
    <script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script>
    $(document).ready(function(){
      $('#dp1').datepicker({autoclose : true});
      $('#dp2').datepicker({autoclose : true});
      $('#dp3').datepicker({autoclose : true});
    });
    </script>
    @yield('scripts')
    <script src="{{ asset('js/time.js') }}"></script>
  </Body>
  
</html>
