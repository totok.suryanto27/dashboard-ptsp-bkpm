<?php
global $headerless;
?>
@extends(isset($headerless)?'headerless':'backend')
@section('title','Konsultasi')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <table class="table table-bordered table-invoice">
        <tr>
            <td class="width30">Instansi</td>
            <td class="width70"></td>
        </tr>
        <?php /*<tr>
            <td>Direktorat</td>
            <td></td>
        </tr>*/?>
        <tr>
            <td>Nama Instansi</td>
            <td>{{$konsultasi->nama_instansi}}</td>
        </tr>
        <tr>
            <td>Nama Direktorat</td>
            <td>{{$konsultasi->nama_direktorat}}</td>
        </tr>
        <tr>
            <td>Nama Tamu</td>
            <td>{{$konsultasi->nama_tamu}}</td>
        </tr>
        <tr>
            <td>Nama Perusahaan</td>
            <td>{{$konsultasi->nama_perusahaan}}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>{{$konsultasi->alamat}}</td>
        </tr>
        <tr>
            <td>Telepon</td>
            <td>{{$konsultasi->telepon}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$konsultasi->email}}</td>
        </tr>
        <tr>
            <td>Judul Konsultasi</td>
            <td>{{$konsultasi->judul_konsultasi}}</td>
        </tr>
        <tr>
            <td>Kategori Konsultasi</td>
            <td></td>
        </tr>
        <tr>
            <td>Pertanyaan</td>
            <td>{{$konsultasi->pertanyaan}}</td>
        </tr>
        <tr>
            <td>Jawaban</td>
            <td>{{$konsultasi->jawaban}}</td>
        </tr>
        <tr>
            <td>Nama Petugas</td>
            <td>{{$konsultasi->nama_petugas}}</td>
        </tr>
    </table>
    <div class="space-4"></div>
  </div>
  </div>
@endsection