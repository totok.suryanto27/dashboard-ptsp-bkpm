<?php
global $headerless;
?>
{{ Navigator::setActive(url('konsultasi')) }}
@extends(isset($headerless)?'headerless':'backend')
@section('title','Konsultasi')
@section('content')

 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
 <?php /*
 <a class="btn btn-primary pull-right toggle-search"><i class="fa fa-search"></i> Cari Konsultasi </a>
 <form id="search-form">
    <div class="form-body form-horizontal">
        <div class="row">
        <div class="col-md-6">
            <h3>Biodata Pengirim</h3>
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::lists('nama_instansi','id_m_instansi')
            ,'empty'=>'Semua'])
            @include('form.select2',['label'=>'Direktorat','required'=>false,'name'=>'id_m_direktorat','data'=>
                App\Model\Direktorat::lists('nama_direktorat','id_m_direktorat')
            ,'empty'=>'Semua'])
            @include('form.text',['label'=>'Nama Instansi','required'=>false,'name'=>'nama_instansi'])
            @include('form.text',['label'=>'Nama Direktorat','required'=>false,'name'=>'nama_direkotrat'])
            @include('form.textarea',['label'=>'Nama Tamu','required'=>false,'name'=>'nama_tamu'])
            @include('form.textarea',['label'=>'Nama Perusahaan','required'=>false,'name'=>'nama_perusahaan'])
        </div>
        <div class="col-md-6">
            <h3>Contact Pengirim</h3>
            @include('form.text',['label'=>'Alamat','required'=>false,'name'=>'alamat'])
            @include('form.text',['label'=>'Telepon','required'=>false,'name'=>'telepon'])
            @include('form.text',['label'=>'Email','required'=>false,'name'=>'email'])
        </div>
        <div class="col-md-6">
            <h3>Detail Konsultasi</h3>
            @include('form.text',['label'=>'Judul Konsultasi','required'=>false,'name'=>'judul_konsultasi'])
            @include('form.textarea',['label'=>'Pertanyaan','required'=>false,'name'=>'pertanyaan'])
            @include('form.textarea',['label'=>'Jawaban','required'=>false,'name'=>'jawaban'])
            @include('form.text',['label'=>'Nama Petugas','required'=>false,'name'=>'nama_petugas'])
        </div>
        </div>
    </div>
    <div class="form-actions right">
        <a class="btn btn-primary pull-right do-search"><i class="fa fa-search"></i> Cari </a>
    </div>
 </form>
 <hr>
 */?>
<form id="search-form">
<div class="panel">
    Tanggal Konsultasi
    <div class="row">
        <div class="col-md-6">
            @include('form.date',['label'=>'Dari','required'=>false,'name'=>'tanggal_mulai'])
            @include('form.date',['label'=>'Sampai','required'=>false,'name'=>'tanggal_selesai'])
        </div>
        <div class="col-md-6">
        @if (Request::has('id_m_instansi'))
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::where('id_m_instansi',Request::get('id_m_instansi'))->lists('nama_instansi','id_m_instansi')
            ,'value'=>Request::get('id_m_instansi')])
        @elseif (getInstansi())
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::where('id_m_instansi',getInstansi())->lists('nama_instansi','id_m_instansi')
            ,'value'=>getInstansi()])
        @else
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::lists('nama_instansi','id_m_instansi')
            ,'empty'=>'Semua'])
        @endif
            @include('form.select2',['label'=>'Kategori','required'=>false,'name'=>'id_konsul_cat','data'=>
                App\Model\KonsultasiKategori::lists('nama_konsul_cat','id_konsul_cat')
            ,'empty'=>'Semua'])
            <a class="btn btn-primary do-search"><i class="fa fa-search"></i> Cari</a>
            <a class="btn btn-success do-export"><i class="fa fa-download"></i> Download Excel</a>
        </div>
    </div>
</div>
</form>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Biodata</th>
         <th>Kontak</th>
         <th>Konsultasi</th>
         <th>Aksi</th>
     </tr>
     </thead>
 </table>
@endsection
@section('styles')
<style>
.align-keterangan1 {
    width: 85px;
    vertical-align: top;
    display: inline-block;
}
.align-keterangan2 {
    width: 60px;
    vertical-align: top;
    display: inline-block;
}
.align-keterangan3 {
    width: 75px;
    vertical-align: top;
    display: inline-block;
}
.align-isi1 {
    width: 185px;
    font-weight: 700;
    display: inline-block;
}
.align-isi2 {
    width: 160px;
    font-weight: 700;
    display: inline-block;
}
.align-isi3 {
    width: 275px;
    font-weight: 700;
    display: inline-block;
}
.form-control.select2me {
    border: none;
    padding: 0;
}
#search-result_wrapper {
    padding-bottom: 100px;
}
</style>
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('scripts')
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script>
$('.date-picker').datepicker({
    autoclose: true
});

//$(document).ready(function() {
//    oTable = $('#konsultasi').DataTable({
//        "processing": true,
//        "serverSide": true,
//        "ajax": {
//            "url": "{{url('konsultasi/getData')}}",
//            "type": "GET"
//        },
//        "columns": [
//            {data: 'nama_instansi', name: 'nama_instansi'},
//            {data: 'nama_direktorat', name: 'nama_direktorat'},
//            {data: 'nama_tamu', name: 'nama_tamu'},
//            {data: 'nama_perusahaan', name: 'nama_perusahaan'},
//            {data: 'alamat', name: 'alamat'},
//            {data: 'telepon', name: 'telepon'},
//            {data: 'email', name: 'email'},
//            {data: 'judul_konsultasi', name: 'judul_konsultasi'},
//            {data: 'pertanyaan', name: 'pertanyaan'},
//            {data: 'jawaban', name: 'jawaban'},
//            {data: 'nama_petugas', name: 'nama_petugas'},
//            {data: 'actions', name: 'actions'}
//        ]
//    });
//});
</script>
@include('konsultasi.datatables_script',['url'=>url(isset($headerless)?'konsultasi/KHVydg2iyer9kjhsac':'konsultasi/search')])
<script>
$(".select2me").select2();
var params = {};
$('.do-search').click(function() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawSuratTable('#search-result');
    //$("#search-form").hide(); $(".toggle-search").show();
});
$('.do-export').click(function() {
    var data = $('#search-form').serialize();
    window.location = "{{url(isset($headerless)?'konsultasi/FNudifer932jeknfxa':'konsultasi/export')}}?"+data;
});
//$("#search-form").hide();
$(".toggle-search").click(function() {$("#search-form").show(); $(".toggle-search").hide();});
$(document).ready(function() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawSuratTable('#search-result');
});
</script>
@endsection