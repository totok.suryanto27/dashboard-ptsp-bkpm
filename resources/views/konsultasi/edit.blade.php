<?php
global $headerless;
?>
@extends(isset($headerless)?'headerless':'backend')
@section('title','Konsultasi')
@section('content')
<div class="col-xs-12">
  <div class="row">
      <form method="POST" action="{{ url(isset($headerless)?'konsultasi/LKcdmskl32l4jozckk':'konsultasi/update', $konsultasi->id_konsultasi) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="id_m_instansi" class="col-sm-2 control-label">Instansi</label>
            <div class="col-md-10">
                <select class="form-control select2me" id="id_m_instansi" name="id_m_instansi">
                    @foreach (App\Model\Instansi::lists('nama_instansi','id_m_instansi') as $key => $value)
                    <option value="{{$key}}"{{$konsultasi->id_m_instansi==$key?' selected':''}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="nama_tamu" class="col-sm-2 control-label">Nama Tamu</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="nama_tamu" name="nama_tamu" value="{{$konsultasi->nama_tamu}}">
            </div>
        </div>
         <div class="form-group">
            <label for="nama_perusahaan" class="col-sm-2 control-label">Nama Perusahaan</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{$konsultasi->nama_perusahaan}}">
            </div>
        </div>
         <div class="form-group">
            <label for="alamat" class="col-sm-2 control-label">Alamat</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{$konsultasi->alamat}}">
            </div>
        </div>
         <div class="form-group">
            <label for="telepon" class="col-sm-2 control-label">Telepon</label>
            <div class="col-md-10">
                <input type="number" class="form-control" id="telepon" name="telepon" value="{{$konsultasi->telepon}}">
            </div>
        </div>
         <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-md-10">
                <input type="email" class="form-control" id="email" name="email" value="{{$konsultasi->email}}">
            </div>
        </div>
        <div class="form-group">
            <label for="judul_konsultasi" class="col-sm-2 control-label">Judul Konsultasi</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="judul_konsultasi" name="judul_konsultasi" value="{{$konsultasi->judul_konsultasi}}">
            </div>
        </div>
         <div class="form-group">
            <label for="id_konsul_cat" class="col-sm-2 control-label">Kategori Konsultasi</label>
            <div class="col-md-10">
                <select class="form-control select2me" id="id_konsul_cat" name="id_konsul_cat">
                    @foreach (App\Model\KonsultasiKategori::lists('nama_konsul_cat','id_konsul_cat') as $key => $value)
                    <option value="{{$key}}"{{$konsultasi->id_konsul_cat==$key?' selected':''}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="pertanyaan" class="col-sm-2 control-label">Pertanyaan</label>
            <div class="col-md-10">
                 <textarea class="form-control" rows="8" id="pertanyaan" name="pertanyaan">{{$konsultasi->pertanyaan}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="jawaban" class="col-sm-2 control-label">Jawaban</label>
            <div class="col-md-10">
                 <textarea class="form-control" rows="8" id="jawaban" name="jawaban">{{$konsultasi->jawaban}}</textarea>
            </div>
        </div>
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
      </form>
  </div>
  </div>
@endsection
@section('styles')
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.form-control.select2-container {
    border: none;
    padding: 0;
}
</style>
@endsection
@section('scripts')
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script>
$(".select2me").select2();
</script>
@endsection