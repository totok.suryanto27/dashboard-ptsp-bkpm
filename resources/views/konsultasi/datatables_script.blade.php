<?php
global $headerless;
?>
<script>
var initialized = false;
var table;
function drawSuratTable(table_id) {
	if (!initialized) {
		table = $(table_id).DataTable({
		    ajax: {
		    	url: "{{$url}}",
		    	data: function(d) {
		    		d.params = params;
		    		return d;
			    },
		    },
		    processing: true,
		    serverSide: true,
		    columns: [
		        {data: 'id_konsultasi', name: 'id_konsultasi', searchable: false},
		        {data: 'biodata', name: 'biodata', searchable: false, orderable: false},
	            {data: 'contact', name: 'contact', searchable: false, orderable: false},
	            {data: 'konsultasi', name: 'konsultasi', searchable: false, orderable: false},
		        {data: 'actions', name: 'actions', searchable: false, orderable: false}
		    ],
			order: [[0,'desc']]
		});
		initialized = true;

		@if (isset($headerless))
		$($("#search-result_wrapper").find(".row")[2]).css('position','fixed').css('bottom','0').css('width','100%').css('left','0');
		@endif
	} else {
		table.ajax.reload();

		@if (isset($headerless))
		$($("#search-result_wrapper").find(".row")[2]).css('position','fixed').css('bottom','0').css('width','100%').css('left','0');
		@endif
	}
}
</script>
