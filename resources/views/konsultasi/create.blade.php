{{ Navigator::setActive(url('konsultasi/create')) }}
<?php $no_title = true; ?>
@extends('backend')

@section('title','Input Konsultasi')
@section('content')
    <div class="row" style="margin: 20px 0 0 0;">
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default" style="margin-bottom: 0">
                <div class="panel-heading">
                    Cari Perusahaan
                </div>
                <div class="panel-body left-panel">
                    @include('tracking.form_izin_prinsip')
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default" style="margin-bottom: 0">
                <div class="panel-heading">
                    Input Konsultasi
                    <a id="btn-reset" class="btn btn-sm ladda-button pull-right" data-style="zoom-out" style="margin-top: -6px;">
                        <i class="fa fa-cog"></i> Reset
                    </a>
                </div>
                <div class="panel-body right-panel">
                    @include('tracking.form_konsultasi')
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Izin</h4>
      </div>
      <div class="modal-body">
         <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Izin Terintegrasi','required'=>false,'name'=>'fmo_nama_izin_terintegrasi'])
    @include('form.view_styled',['label'=>'Nama Jenis Perizinan','required'=>false,'name'=>'fmo_nama_jenis_perizinan'])
    @include('form.view_styled',['label'=>'Status','required'=>false,'name'=>'fmo_status'])
    @include('form.view_styled',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'fmo_tanggal_pengajuan'])
    @include('form.view_styled',['label'=>'Estimasi Selesai','required'=>false,'name'=>'fmo_tanggal_selesai_estimasi'])
    </div>
      </div>
    
    </div>
  </div>
</div>
@include('tracking.form_buat_perusahaan')
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.form-buttons {
    text-align: right;
}
.form-control:not(select):not(input) {
    border: none;
    padding: 0;
}
.form-control.autosize-transition {
    border: 1px solid #d5d5d5;
    margin-bottom: 4px;
}
.form-group input {
    height: 30px;
    padding-top: 0;
    padding-bottom: 0;
    margin-bottom: 4px;
}
.form-control-static {
    font-weight: bold;
    padding-top: 4px;
}
.form-group {
    margin-bottom: 0;
}
.no-padding {
    padding: 0;
}
.left-panel, .right-panel {
    padding: 15px;
}
.form-horizontal .radio {
    padding-top: 0;
}
.radio label {
    padding-left: 0;
}
.profile-user-info {
    margin: 0;
}
.profile-info-name {
    width: 178px;
}
.profile-info-value {
    margin-left: 188px;
    padding-left: 0px;
}
#search-result {
    margin-top: 40px;
}
.profile-input-value {
    width: 95%;
    margin: -3px;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/jquery/jquery.autocomplete.min.js') }}" type="text/javascript"></script>
<script>
$("#s_no_ip_choice_container").hide();
$("#s_no_ip_choice").change(function() {
    if (this.value == "") {
        for (i = 0; i < hidden_npwp.length; i++) {
            $("#"+hidden_npwp[i]).parent().parent().hide();
        }
        $('#s_no_ip').val("-, NPWP: "+npwp);
        $("#s_no_ip").change();
        f_search_perusahaan("-, NPWP: "+npwp);
    } else {
        for (i = 0; i < hidden_npwp.length; i++) {
            if (hidden_npwp[i] == "s_no_ip" && $("#s_no_ip_choice_container").is(":visible")) continue;
            $("#"+hidden_npwp[i]).parent().parent().show();
        }
        $('#s_no_ip').val(this.value);
        $("#s_no_ip").change();
        f_search_perusahaan(this.value);
    }
});
$("#s_no_ip").on('keyup change',function() {
    $("#fci_id_qrcode").val($(this).val());
    $("#fci_no_ip").html($(this).val());
    $("#fco_no_ip").html($(this).val());
    $("#fk_no_ip").html($(this).val());
});
$("#sr_nama_perusahaan").autocomplete({
    serviceUrl: "{{url('tracking/search-perusahaan')}}", 
    paramName: 'q',
    onSelect: function (suggestion) {
        npwp = suggestion.npwp;
        $('#sr_npwp').val(suggestion.npwp);
        if ($.isEmptyObject(suggestion.data)) {
            $("#s_no_ip_choice_container").hide();
            for (i = 0; i < hidden_npwp.length; i++) {
                $("#"+hidden_npwp[i]).parent().parent().hide();
            }
            $('#s_no_ip').val("-, NPWP: "+suggestion.npwp);
            $("#s_no_ip").change();
            f_search_perusahaan("-, NPWP: "+suggestion.npwp);
        } else {
            $("#s_no_ip").parent().parent().hide();
            $("#s_no_ip_choice_container").show();
            $("#s_no_ip_choice").replaceOptions($.merge([{id: '', text: 'Pilih IP'}],$.map(suggestion.data,function(v,k) {
                return {id: k,text: v}; 
            })));
            $("#s_no_ip_choice").change();
        }
    },
});
$("#sr_npwp").autocomplete({
    serviceUrl: "{{url('tracking/search-npwp')}}", 
    paramName: 'q',
    onSelect: function (suggestion) {
        npwp = suggestion.npwp;
        $('#sr_nama_perusahaan').val(suggestion.nama_perusahaan);
        if ($.isEmptyObject(suggestion.data)) {
            $("#s_no_ip_choice_container").hide();
            for (i = 0; i < hidden_npwp.length; i++) {
                $("#"+hidden_npwp[i]).parent().parent().hide();
            }
            $('#s_no_ip').val("-, NPWP: "+suggestion.npwp);
            $("#s_no_ip").change();
            f_search_perusahaan("-, NPWP: "+suggestion.npwp);
        } else {
            $("#s_no_ip").parent().parent().hide();
            $("#s_no_ip_choice_container").show();
            $("#s_no_ip_choice").replaceOptions($.merge([{id: '', text: 'Pilih IP'}],$.map(suggestion.data,function(v,k) {
                return {id: k,text: v}; 
            })));
            $("#s_no_ip_choice").change();
        }
    },
});
$('textarea[class*=autosize]').autosize();
$(".right-panel").slimScroll({
    height: '484px',
    alwaysVisible: true,
});
$('.date-picker').datepicker({
    autoclose: true
});
var search_perusahaan = ['nama_perusahaan','no_perusahaan','npwp','alamat','nama_kabkot',
    'nama_provinsi','telepon','email','nama_pimpinan'];
var binds_nama_perusahaan = ['fco','fci'];
var binds_konsultasi = ['nama_perusahaan','alamat','telepon','email'];
var konsultasi_form = ['fk_id_m_direktorat','fk_nama_tamu','fk_jawaban',
    'fk_nama_perusahaan','fk_alamat','fk_telepon','fk_email','fk_judul_konsultasi','fk_pertanyaan'];
var check_in_form = ['fci_id_m_instansi','fci_id_m_direktorat','fci_id_m_jenis_perizinan'];
var check_out_form = ['fco_id_m_instansi','fco_id_m_direktorat'];
var hidden_npwp = ['s_no_ip','fk_no_ip','fci_no_ip','fco_no_ip','fci_id_m_flow_terintegrasi_new_terintegrasi'];
var search_detail_izin_modal = ['nama_izin_terintegrasi','nama_jenis_perizinan','id_qrcode','tanggal_pengajuan','status','tanggal_selesai_estimasi'];
var l_btn_cari = Ladda.create(document.querySelector('#btn-cari'));
var l_btn_reset = Ladda.create(document.querySelector('#btn-reset'));
var l_btn_buat_perusahaan = Ladda.create(document.querySelector('#btn-buat-perusahaan'));
@if (isRole('check_in_instansi') || isRole('check_out_instansi'))
var jenis_izin = {!! json_encode(App\Model\JenisPerizinan::with('flow_detail_izin')->where('id_m_instansi',Auth::user()->id_m_instansi)->get(['nama_jenis_perizinan','id_m_jenis_perizinan','sop','id_m_instansi','id_m_direktorat'])) !!};
var instansi = {!! json_encode(App\Model\Instansi::where('id_m_instansi',Auth::user()->id_m_instansi)->get(['nama_instansi','id_m_instansi'])) !!};
@else
var jenis_izin = {!! json_encode(App\Model\JenisPerizinan::with('flow_detail_izin')->get(['nama_jenis_perizinan','id_m_jenis_perizinan','sop','id_m_instansi','id_m_direktorat'])) !!};
var instansi = {!! json_encode(App\Model\Instansi::get(['nama_instansi','id_m_instansi'])) !!};
@endif
var kabkot = {!! App\Model\Kabkot::whereNotNull('mulai_exist')->whereNull('akhir_exist')->get(['nama_kabkot','id_kabkot','id_provinsi']) !!};
var direktorat = {!! json_encode(App\Model\Direktorat::get(['nama_direktorat','id_m_direktorat'])) !!};
var form_buat_perusahaan = false;
var npwp = "";
$('#s_no_ip').keydown(function(event) {
    if (event.keyCode == 13) {
        f_search_perusahaan($(this).val());
        return false;
     }
});
$('#btn-cari').click(function() {f_search_perusahaan($('#s_no_ip').val())});
function f_search_perusahaan(id) {
    l_btn_cari.start();
    $("#btn-buat-perusahaan").hide();
    $.ajax({
        cache: false,
        data: {id: id},
        url: "{{url('tracking/perusahaan')}}",
        success: function(data) {
            l_btn_cari.stop();
            if ($.isEmptyObject(data)) {
                $("#s_"+search_perusahaan[0]).html("IP Tidak ditemukan");
                $("#btn-buat-perusahaan").show();
                for (i = 1; i < search_perusahaan.length; i++) {
                    $("#s_"+search_perusahaan[i]).html("");
                }
            } else {
                for (i = 0; i < search_perusahaan.length; i++) {
                    $("#s_"+search_perusahaan[i]).html(data[search_perusahaan[i]]);
                }
                var additional = "";
                for (i = 0; i < data.izin_terintegrasi.length; i++) {
                    additional += '<tr>\
                        <td><div class="radio"><label>\
                            <input name="fci_id_m_flow_terintegrasi" type="radio" class="ace" value="'+data.izin_terintegrasi[i].id_m_flow_izin_terintegrasi+'">\
                            <span class="lbl">'+data.izin_terintegrasi[i].nama_flow+'</span>\
                        </label></div></td>\
                        <td>'+data.izin_terintegrasi[i].tanggal_pengajuan+'</td>\
                        <td><a target="_blank" href="{{url('investortracking')}}?id='+id+'&jenis_perizinan=i_'+data.izin_terintegrasi[i].id_m_flow_izin_terintegrasi+'"><i class="fa fa-search"></i></a></td>\
                    </tr>';
                }
                additional += "";
                if (data.izin_terintegrasi.length) {
                    $("#fci_id_m_flow_terintegrasi_additional").html(additional);
                    for (i = 0; i < hidden_npwp.length; i++) {
                        if (hidden_npwp[i] == "s_no_ip" && $("#s_no_ip_choice_container").is(":visible")) continue;
                        $("#"+hidden_npwp[i]).parent().parent().show();
                    }
                    //$("#fci_id_m_flow_terintegrasi_new_terintegrasi").parent().parent().hide();
                } else {
                    $("#fci_id_m_flow_terintegrasi_additional").html("");
                }
                additional = "<table class='table table-bordered table-hover'>\
                    <thead>\
                        <tr>\
                            <td>Nama</td>\
                            <td>Tanggal Pengajuan</td>\
                            <td>Deadline</td>\
                            <td></td>\
                        </tr>\
                    </thead><tbody>";
                for (i = 0; i < data.izin_open.length; i++) {
                    additional += '<tr>\
                        <td>\
                        <div class="radio">\
                            <label>\
                                <input name="fco_id_tracking_register_detail" type="radio" class="ace" value="'+data.izin_open[i].id_tracking_register_detail+'">\
                                <span class="lbl"> <b>'+data.izin_open[i].nama_jenis_perizinan+'</b></span>\
                            </label>\
                        </div></td>\
                        <td>'+data.izin_open[i].tanggal_pengajuan+'</td>\
                        <td>'+data.izin_open[i].tanggal_threshold+'</td>\
                        <td><a class="izin_detail" data-id="'+id+'" data-id_tracking_register_detail="'+data.izin_open[i].id_tracking_register_detail+'"><i class="fa fa-search"></i></a></td>\
                    </tr>';
                }
                if (data.izin_open.length == 0) {
                    additional += "<tr><td colspan='3'>Tidak ada izin yang bisa di-check-out</td></tr>";
                }
                additional += "</tbody></table>";
                $("#fco_id_tracking_register_detail_additional").html(additional);
            }
            for (i = 0; i < binds_nama_perusahaan.length; i++) {
                $("#"+binds_nama_perusahaan[i]+"_nama_perusahaan").html($("#s_nama_perusahaan").html());
            }
            for (i = 0; i < binds_konsultasi.length; i++) {
                $("#fk_"+binds_konsultasi[i]).val($("#s_"+binds_konsultasi[i]).html());
            }
        },
        error: function(xhr,error,thrown) {
            l_btn_cari.stop();
            console.log(error);
            alert(error);
        }
    });
}
$('#btn-reset').click(function() { resetForm() });
function resetForm(direct) {
    if (!direct) l_btn_reset.start();
    
    for (i = 0; i < search_perusahaan.length; i++) {
        $("#s_"+search_perusahaan[i]).html("");
        $("input[name=s_"+search_perusahaan[i]+"]").val("");
    }
    
    for (i = 0; i < konsultasi_form.length; i++) {
        $("#"+konsultasi_form[i]).val(null);
    }
    
    for (i = 0; i < check_in_form.length; i++) {
        $("#"+check_in_form[i]).val(null);
    }
    
    for (i = 0; i < check_out_form.length; i++) {
        $("#"+check_out_form[i]).val(null);
    }
    
    for (i = 0; i < binds_nama_perusahaan.length; i++) {
        $("#"+binds_nama_perusahaan[i]+"_nama_perusahaan").html("");
    }
    
    $("#fci_tanggal_selesai_estimasi").html("");
    $("#s_no_ip").parent().parent().show();
    $('#s_no_ip').val("");
    $("#s_no_ip").change();
    $("#s_no_ip_choice_container").hide();
    $("#sr_nama_perusahaan").val("");
    $("#sr_npwp").val("");
    reloadFciJenisPerizinan(-1);
    $("#fci_id_m_jenis_perizinan").val("");
    $("#fci_id_m_flow_terintegrasi_additional").html("");
    $("#fco_id_tracking_register_detail_additional").html("");
    for (i = 0; i < hidden_npwp.length; i++) {
        $("#"+hidden_npwp[i]).parent().parent().show();
    }
    $("#btn-buat-perusahaan").show();
    if (!direct) setTimeout(function(){l_btn_reset.stop();},500);
}
$("#bp_id_provinsi").select2();
$("#bp_id_provinsi").change(function() {
    $("#bp_id_kabkot").select2('destroy');
    var id_provinsi = $(this).val();
    $("#bp_id_kabkot").select2({
        data: function() { 
            return {results: $.map(kabkot,function(v) {
                if (!id_provinsi || v.id_provinsi == id_provinsi)
                return {id: v.id_kabkot,text: v.nama_kabkot}; 
            })};
        }
    });
});
$("#fci_id_m_jenis_perizinan").change(function() {
    $("#fci_tanggal_selesai_estimasi").html("Loading...");
    $.getJSON("{{url('tracking/hitung-estimasi')}}",{id_m_jenis_perizinan: $("#fci_id_m_jenis_perizinan").val()},function(data) {
        $("#fci_tanggal_selesai_estimasi").html(data);
    });
    $("#fci_nama_instansi").html(
        findInArray(instansi,'id_m_instansi',
            findInArray(jenis_izin,'id_m_jenis_perizinan',$("#fci_id_m_jenis_perizinan").val(),'id_m_instansi')
            ,'nama_instansi'));
    $("#fci_nama_direktorat").html(
        findInArray(direktorat,'id_m_direktorat',
            findInArray(jenis_izin,'id_m_jenis_perizinan',$("#fci_id_m_jenis_perizinan").val(),'id_m_direktorat')
            ,'nama_direktorat'));
});
reloadFciJenisPerizinan(-1);
function reloadFciJenisPerizinan(sop) {
    $("#fci_id_m_jenis_perizinan").select2('destroy');
    $("#fci_id_m_jenis_perizinan").select2({
        data: function() { 
            return {results: $.map(jenis_izin,function(v) {
                if (!sop || '-' != findInArray(v.flow_detail_izin,'id_m_flow_izin_terintegrasi',sop,'id_m_flow_izin_terintegrasi'))
                return {id: v.id_m_jenis_perizinan,text: v.nama_jenis_perizinan}; 
            })};
        }
    });
}
$("#fk_id_konsul_cat").select2();

$("#fk_id_m_instansi").select2({
    data: function() { 
        return {results: $.map(instansi,function(v) { 
            return {id: v.id_m_instansi,text: v.nama_instansi}; 
        })};
    }
});
$("#fk_id_m_direktorat").select2({
    data: function() { 
        return {results: $.map(direktorat,function(v) { 
            return {id: v.id_m_direktorat,text: v.nama_direktorat}; 
        })};
    }
});
function findInArray(arr,attr,param,res) {
    var data = $.grep(arr,function(e) { return e[attr]===param; });
    return data.length?data[0][res]:'-';
}
$("#fci_id_m_flow_terintegrasi_choice_container").hide();
$("#fci_id_m_flow_terintegrasi_choice").select2();
$("#fci_id_m_flow_terintegrasi_choice").change(function() {
    reloadFciJenisPerizinan(this.value);
});
$(document).on('change','input[type=radio][name=fci_id_m_flow_terintegrasi]',function() {
    if (this.value == 'new_terintegrasi') {
        $("#fci_id_m_flow_terintegrasi_choice_container").show();
        reloadFciJenisPerizinan(-1);
    } else {
        $("#fci_id_m_flow_terintegrasi_choice_container").hide();
        if (this.value == 'new_tunggal') {
            reloadFciJenisPerizinan();
        } else {
            reloadFciJenisPerizinan(this.value);
        }
    }
});
$("#btn-form-buat-perusahaan").click(function() {
    l_btn_buat_perusahaan.start();
    $.ajax({
        type: "POST",
        url: "{{url('tracking/buat-perusahaan')}}",
        data: $("#form_buat_perusahaan").serialize(),
        success: function(ret) {
            if (ret.success) {
                notify("Pendaftaran Perusahaan Berhasil",ret.msg);
                resetForm(true);
                for (i = 0; i < hidden_npwp.length; i++) {
                    $("#"+hidden_npwp[i]).parent().parent().hide();
                }
                $('#s_no_ip').val("-, NPWP: "+ret.npwp);
                $("#s_no_ip").change();
                f_search_perusahaan("-, NPWP: "+ret.npwp);
                $('#buatPerusahaanModal').modal('show');
            } else {
                notify("Pendaftaran Perusahaan Gagal",ret.msg,"error");
            }
            l_btn_buat_perusahaan.stop();
        },
        error: function() {
            notify("Pendaftaran Perusahaan Gagal","Perusahaan gagal didaftarkan","error");
            l_btn_buat_perusahaan.stop();
        }
    });
});
$("#btn-buat-perusahaan").click(function() {
    $('#buatPerusahaanModal').modal('show');
});
$(document).on('click','.izin_detail',function() {
    $.getJSON("{{url('tracking/detail')}}",{
        id: $(this).data('id'),
        id_tracking_register_detail: $(this).data('id_tracking_register_detail')
    },function(data) {
        for (i = 0; i < search_detail_izin_modal.length; i++) {
            if(data[search_detail_izin_modal[i]] == "NULL") data[search_detail_izin_modal[i]] = "-";
            $("#fmo_"+search_detail_izin_modal[i]).html(data[search_detail_izin_modal[i]]);
        }
        $('#myModal').modal('show');
    });
});
var l_btn_submit_konsultasi = Ladda.create(document.querySelector('#submit_form_konsultasi'));
$("#submit_form_konsultasi").click(function() { submitKonsultasi() });
function submitKonsultasi() {
    l_btn_submit_konsultasi.start();
    $.ajax({
        type: "POST",
        url: "{{url('tracking/konsultasi')}}",
        data: $("#form_konsultasi").serialize(),
        success: function(ret) {
            if (ret.success) {
                notify("Penyimpanan Berhasil",ret.msg);
                resetForm(true);
            } else
                notify("Penyimpanan Gagal",ret.msg,"error");
            l_btn_submit_konsultasi.stop();
        },
        error: function() {
            notify("Penyimpanan Bagal","Data konsultasi gagal disimpan","error");
            l_btn_submit_konsultasi.stop();
        }
    });
}
(function($, window) {
  $.fn.replaceOptions = function(options) {
    var self, $option;

    this.empty();
    self = this;

    $.each(options, function(index, option) {
      $option = $("<option></option>")
        .attr("value", option.id)
        .text(option.text);
      self.append($option);
    });
  };
})(jQuery, window);
</script>
@endsection
