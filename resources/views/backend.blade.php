<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Optional theme -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> -->
    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('assets/css/ace-fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/ace.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/ace2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/plugins/datatables/media/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/plugins/toastr/toastr.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dockmodal.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/select2.css') }}">
    <script src="{{ asset('assets/js/ace-extra.min.js') }}"></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('styles')
	<style>
	.head-block .logo {
	  padding: 5px;
	  height: 70px;
	  margin-bottom: 2px;
	}
	.head-left {
	  padding: 15px 0 0 15px;
	}
	.page-content {
	  padding-bottom: 0;
	}
	.head-right {
		padding-top: 12px;
	}
	span.menu-text {
		font-size: 13px;
	}
	.img-max200 {
	  max-width: 200px;
	  max-height: 200px;
	}
	.fa {
	  display:inline;
	}
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/waktu.js') }}"></script>
  </head>
  <body>
    <header>
      <div class="head-block">
        <div class="container">
          <div class="line1"></div>
          <div class="middle-block">
            <div class="row-eq-height">
              <!-- <div class="col-md-2"> -->
                <div class="log">
                  <img src="{{ asset('images/'.trans('pages.logo')) }}" class="img-responsive logo"/>
                </div>
              <!-- </div> -->
              <div class="col-md-12 bg">
                <div class="row">
                  <div class="col-md-6 head-left">
                    <h4>SPM Dashboard</h4>
                    <h3><strong>OSS Center</strong> <span style="font-size:14px">Ver 2.0</span></h3>
<!--
                    <h5>Ver 2.0</h5>
-->
                  </div>
                  <div class="col-md-6 head-right">
                    <div class="row">
                      <p class="text-right">
                        <span><script type="text/JavaScript">
                        @if (getLang() == "en")
                        document.write(getDayStrEn())
                        @else
                        document.write(getDayStr())
                        @endif
                        document.write(getDateStr())
                        </script></span>
                      </p>
                      <p class="text-right" id="clock"></p>
                      <p class="text-right">Welcome {{Auth::user() ? Auth::user()->nama_user : ''}}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    
    <div class="main-container" id="main-container">
      <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>

        <div class="sidebar" id="sidebar">
          <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
          </script>
          
          {!! Navigator::show() !!}

          <div class="sidebar-collapse" id="sidebar-collapse">
            <i class="icon- fa fa-angle-double-left" data-icon1="fa fa-angle-double-left" data-icon2="fa fa-angle-double-right"></i>
          </div>

          <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
          </script>
        </div>

        <div class="main-content">
          @if (!isset($no_title))
          <div class="page-header">
            <h1>@yield('title')</h1>
          </div>
          @endif
          <div class="page-content">
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <!-- PAGE CONTENT BEGINS -->
                @yield('content')
                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div><!-- /.main-content -->

      </div><!-- /.main-container-inner -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="{{ asset('js/bootstrap.min.js') }}"></script>-->
    <script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/ace-elements.min.js') }}"></script> 
    <script src="{{ asset('assets/js/ace.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendor/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('vendor/plugins/datatables/extensions/Plugins/api/fnReloadAjax.js') }}"></script>
    <script src="{{ asset('vendor/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.docker.js') }}"></script>
	<script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script>
    $(document).ready(function(){
      $('#dp1').datepicker({autoclose : true});
      $('#dp2').datepicker({autoclose : true});
      $('#dp3').datepicker({autoclose : true});
    });
    </script>
    <script src="{{ asset('js/time.js') }}"></script>
    @yield('scripts')
    <script>
    asset_url = "{{asset('')}}";
	var base_url = "{{url()}}";  
	 
	
    var prevent;
    function notifyConfirm(e) {
      console.log('hello');
      if (!confirm("Apakah anda yakin?")) {
        e.preventDefault();
        prevent = true;
        return false;
      }
      return true;
    }
    function notify(message,description,alert) {

        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: "slideDown",
            timeOut: 5000,
        };
        if (alert == "error") {
            toastr.error(description, message);
        } else {
            toastr.success(description, message);
        }
    }
    @if (Session::has('message'))
        notify("{{ session('message_title','Info') }}","{{ session('message') }}","{{ session('message_type','ok') }}");
    @endif
    </script>
  </body>
</html>
