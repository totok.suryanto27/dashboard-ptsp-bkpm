@extends('backend')
@section('title','Kategori Konsultasi')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <table class="table table-bordered table-invoice">
        <tr>
            <td>ID</td>
            <td>{{$user->id_konsul_cat}}</td>
        </tr>
        <tr>
            <td>Nama Kategori Konsultasi</td>
            <td>{{$user->nama_konsul_cat}}</td>
        </tr>
        
    </table>
    <div class="space-4"></div>
  </div>
  </div>
@endsection