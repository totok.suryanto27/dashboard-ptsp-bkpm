{{ Navigator::setActive(url('konsultasi-kategori')) }}
@extends('backend')
@section('title','Kategori Konsultasi')
@section('content')

 <a href="{{url('/konsultasi-kategori/create')}}" class="btn btn-success">Kategori Konsultasi Baru</a>
 <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Nama Kategori Konsultasi</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('konsultasi_kategori.datatables_script',['url'=>url('konsultasi-kategori/getData')])
<script>
var params = {};
$('.do-search').click(function() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawSuratTable('#search-result');
});
$("#search-form").hide();
$(".toggle-search").click(function() {$("#search-form").toggle();});
drawTable('#search-result');
</script>
@endsection