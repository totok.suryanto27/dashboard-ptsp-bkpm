<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id_konsul_cat', name: 'id_konsul_cat'},
                {data: 'nama_konsul_cat', name: 'nama_konsul_cat'},
                {data: 'actions', name: 'actions'},
            ],
            order: [[0,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
