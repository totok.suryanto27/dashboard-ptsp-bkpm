@extends('backend')
@section('title','Kategori Konsultasi')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('konsultasi-kategori/store') }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.text',['label'=>'Nama Kategori Konsultasi','required'=>true,'name'=>'nama_konsul_cat'])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
</script>
@endsection 