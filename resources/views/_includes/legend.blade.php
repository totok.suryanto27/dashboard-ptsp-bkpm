<div class='my-legend'>
    @if (!isset($tanpa_judul))
    <div class='legend-title'>Legenda</div>
    @endif
    <div class='legend-scale'>
        <ul class='legend-labels'>
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::ON_GOING_BIASA)}}'></span>{{App\Model\TrackingRegisterDetail::ON_GOING_BIASA}}</li>
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::ON_GOING_WARNING)}}'></span>{{App\Model\TrackingRegisterDetail::ON_GOING_WARNING}}</li>
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::ON_GOING_TERLAMBAT)}}'></span>{{App\Model\TrackingRegisterDetail::ON_GOING_TERLAMBAT}}</li>
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::ON_TIME)}}'></span>{{App\Model\TrackingRegisterDetail::ON_TIME}}</li>
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::STOP)}}'></span>{{App\Model\TrackingRegisterDetail::STOP}}</li>
            <?php //<li><span class='{{constName(App\Model\TrackingRegisterDetail::TUNDA)}}'></span>{{App\Model\TrackingRegisterDetail::TUNDA}}</li> ?>
            @if (isset($belum_diajukan))
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::BELUM_DIAJUKAN)}}'></span>{{App\Model\TrackingRegisterDetail::BELUM_DIAJUKAN}}</li>
            @endif
            <li><span class='{{constName(App\Model\TrackingRegisterDetail::DELAY)}}'></span>{{App\Model\TrackingRegisterDetail::DELAY}}</li>
        </ul>
    </div>
</div>
