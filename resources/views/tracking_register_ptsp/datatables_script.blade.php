<script>
var initialized = false;
var table;
function drawTable(table_id, button) {
@if (!isRole('check_in_instansi'))
    button.start();
@endif
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id_qrcode', name: 'id_qrcode'},
                {data: 'nama_jenis_perizinan', name: 'nama_jenis_perizinan'},
                {data: 'nama_instansi', name: 'nama_instansi'},
                {data: 'nama_perusahaan', name: 'nama_perusahaan'},
                {data: 'tanggal_masuk', name: 'tanggal_masuk'},
                {data: 'tanggal_selesai', name: 'tanggal_selesai'},
                {data: 'tanggal_estimasi', name: 'tanggal_threshold'},
                {data: 'sla', name: 'sla'},
                {data: 'status', name: 'status',
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).attr('class', cellData?cellData.replace(/ /g,'_').toUpperCase():'');
                    },
                },
                {data: 'edit', name: 'edit', searchable: false, orderable: false},
            ],
            order: [[4,'desc']],
            initComplete: function() {
@if (!isRole('check_in_instansi'))
                button.stop();
@endif
            }
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
@if (!isRole('check_in_instansi'))
        button.stop();
@endif
    }
}
</script>
