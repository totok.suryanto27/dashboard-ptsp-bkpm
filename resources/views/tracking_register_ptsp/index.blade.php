{{ Navigator::setActive(url('trackingregisterptsp')) }}
@extends('backend')
@section('title','Tracking PTSP')
@section('content')
    <!--form class="form-horizontal" id="search-form">
    <div id="search-form">
        <input type="text" placeholder"Qr code" name="qrcode"/>
        <a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari IP</a>
        <a id="btn-export" class="btn btn-success btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
    </div>
    </form-->
	<form id="search-form">
		<div class="panel">
			Tanggal Perizinan
			<div class="row">
				<div class="col-md-6">
					@include('form.date',['label'=>'Mulai','required'=>false,'name'=>'tanggal_mulai'])
					@include('form.date',['label'=> 'Selesai','required'=>false,'name'=>'tanggal_selesai'])
					
					<a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari IP</a>
					<a id="btn-export" class="btn btn-success btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
				</div> 
			</div>
		</div>
	</form>
 <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID QR Code</th>
         <th>Nama Perizinan</th>
         <th>Instansi</th>
         <th>Perusahaan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>SLA</th>
         <th>Status</th>
         <th>Aksi</th>
     </tr>
     </thead>

     <tbody>
     </tbody>
 </table>
@include('_includes.legend')
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
@include('_includes.legend_styles')
</style>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
@include('tracking_register_ptsp.datatables_script',['url'=>url('trackingregisterptsp/search')])
<script>
$('.date-picker').datepicker({
    autoclose: true
});

var params = {};
var l_btn_cari = Ladda.create(document.querySelector('#btn-cari'));
create();
$('#btn-cari').click(function() {
    create();
});
$('#btn-export').click(function() {
    window.location = "{{url('trackingregisterptsp/export')}}";
});

$('#search-form').submit(function(e) {
    create();
    e.preventDefault(); 
});

function create() {
    var data = $('#search-form').serializeArray();
    @if (getInstansi())
    params = {id_m_instansi: '{{getInstansi()}}' };
    @else
    params = {};
    @endif
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawTable('#search-result', l_btn_cari);
}
//drawTable('#search-result');
</script>

@endsection