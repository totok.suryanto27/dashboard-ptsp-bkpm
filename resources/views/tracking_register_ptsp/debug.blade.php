{{ Navigator::setActive(url('tracking/debug')) }}
@extends('backend')
@section('title','Daftar Pengajuan')
@section('content')
@if (!isRole('check_in_instansi'))
    <form class="form-horizontal" id="search-form">
    <div id="search-form">
        <input type="text" placeholder"Qr code" name="qrcode"/>
        <a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari</a>
    </div>
    </form>
 <hr>
 @endif
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID QR Code</th>
         <th>Nama Perizinan</th>
         <th>Instansi</th>
         <th>Perusahaan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>SLA</th>
         <th>Status</th>
         <th>Aksi</th>
     </tr>
     </thead>

     <tbody>
     </tbody>
 </table>
@include('_includes.legend')
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
@include('_includes.legend_styles')
</style>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
@include('tracking_register_ptsp.datatables_script',['url'=>url('trackingregisterptsp/search')])
<script>
var params = {};
@if (!isRole('check_in_instansi'))
var l_btn_cari = Ladda.create(document.querySelector('#btn-cari'));
@else
var l_btn_cari = null;
@endif
drawTable('#search-result', l_btn_cari);

$('#btn-cari').click(function() {
    create();
});

$('#search-form').submit(function(e) {
    create();
    e.preventDefault(); 
});

function create() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawTable('#search-result', l_btn_cari);
}
//drawTable('#search-result');
</script>

@endsection