<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row" id="{{$name}}_container">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<select class="form-control select2me" id="{{$name}}" name="{{$name}}" {!! $required?'required':''!!}>
					@if (isset($empty))
					<option value="">{{$empty}}</option>
					@endif
					@foreach ($data as $key => $value)
					<option value="{{$key}}"{{$val===(string)$key?' selected':''}}>{{$value}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
</div>
