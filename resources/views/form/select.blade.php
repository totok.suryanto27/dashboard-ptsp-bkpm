<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<select class="form-control" id="{{$name}}" name="{{$name}}">
					@if (isset($empty))
					<option value="">{{$empty}}</option>
					@endif
					@foreach ($data as $key => $value)
					<option value="{{$key}}"{{$val==$key?' selected':''}}>{{$value}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
</div>
