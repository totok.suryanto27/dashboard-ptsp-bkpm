<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<div class="radio-list">
					@foreach ($data as $key => $value)
					<label>
					<input type="radio" name="{{$name}}" id="optionsRadios1" value="{{$key}}" {{$val==$key?' checked':''}}> {{$value}}
					</label>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
