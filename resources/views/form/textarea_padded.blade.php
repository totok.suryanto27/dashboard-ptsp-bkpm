<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label><b>{{$label}}</b> {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<textarea name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> class="form-control autosize-transition" rows="{{$rowheight or '3'}}" placeholder="{{$placeholder or ''}}">{{$value or ''}}</textarea>
		</div>
	</div>
</div>
