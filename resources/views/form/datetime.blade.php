<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:'')));
	  $val_date = $val==''?'':substr($val,0,10);
	  $val_time = $val==''?'':substr($val,-8,5); ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<input id="{{$name}}" name="{{$name}}" multiple="" type="hidden" value="{{$val}}"/>
				<input type="text" id="{{$name}}-date" value="{{ $val_date }}" data-target="{{$name}}" data-date-format="yyyy-mm-dd" class="form-control date-picker datetime" aria-required="{{$required?'true':'false'}}" aria-describedby="{{$name}}-datepicker-error" placeholder="Tanggal">
				<span id="{{$name}}-datepicker-error" class="help-block help-block-error"></span>
				<input type="text" id="{{$name}}-time" value="{{ $val_time }}" data-target="{{$name}}" data-time-format="hh:ii:ss" class="form-control time-picker datetime" aria-required="{{$required?'true':'false'}}" aria-describedby="{{$name}}-timepicker-error" placeholder="Jam">
				<span id="{{$name}}-timepicker-error" class="help-block help-block-error"></span>
			</div>
		</div>
	</div>
</div>
