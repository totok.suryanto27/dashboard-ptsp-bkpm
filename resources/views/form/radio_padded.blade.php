<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
<table class='table table-bordered table-hover'>
                    <thead>
                        <tr>
                            <td>Nama</td>
                            <td>Tanggal Pengajuan</td>
                            <td></td>
                        </tr>
                    </thead>
				<tbody id="{{$name}}_additional">
				
				</tbody>
				<tbody>
				@foreach ($data as $key => $value)
				<tr><td colspan="3">
				<div class="radio">
					<label>
						<input id="{{$name}}_{{$key}}" name="{{$name}}" type="radio" class="ace" value="{{$key}}" {{$val==$key?' checked':''}}>
						<span class="lbl"> {{$value}}</span>
					</label>
				</div>
				</td></tr>
				@endforeach
</tbody></table>
			</div>
		</div>
	</div>
</div>
