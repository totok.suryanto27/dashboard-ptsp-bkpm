<?php $val = (isset($value)?$value:(isset($model->$name)?$model->$name:'')) ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}}</label>
			<div class="col-md-8">
				<p class="form-control-static" id="{{$name}}">
					{{ $val }}
				</p>
			</div>
		</div>
	</div>
</div>
