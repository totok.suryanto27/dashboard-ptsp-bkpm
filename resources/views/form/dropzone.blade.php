<?php $val = (old($name)?old($name):(isset($model)?json_encode(App\Model\SuratFile::toIDArray($model->$name)):(isset($value)?$value:'[]'))) ?>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <div class="col-md-12">
        <label class="control-label">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
      </div>
      <div class="col-md-12">
        <input id="{{$name}}-upload" name="{{$name}}" multiple="" type="hidden" value="{{$val}}"/>
        <div class="dropzone dropzone-previews dz-clickable" id="{{to_camel_case($name,true)}}"></div>
      </div>
    </div>
  </div>
</div>
<script>
      Dropzone.options.{{to_camel_case($name,true)}} = {
          url: "{{ url($url)}}",
          maxFilesize: 64, // MB
          init: function() {
          this.on("sending", function(file, xhr, formData) {
            //formData.append("name", "ganteng"); // Append all the additional input data of your form here!
          });
        },

        success: function(file, response) {
          if (response == "ERROR-UPLOAD" || response == "ERROR-EXTENSION") {
            var _ref;
            notify(response,'Terdapat kesalahan pada saat upload file '+file.name,'error');
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
          }
          var elmt = $("#{{$name}}-upload");
          var arr = JSON.parse(elmt.val());
          file.surat_file_id = response;
          arr.push(response);
          elmt.val(JSON.stringify(arr));
        },
        addRemoveLinks: true,
        removedfile: function(file) {
          var _ref;
          console.log(file);
          deleteFile(file.surat_file_id,"#{{$name}}-upload");
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
      };
 </script>
