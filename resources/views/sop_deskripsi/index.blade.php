{{ Navigator::setActive(url('sop-deskripsi')) }}
@extends('backend')
@section('title','SOP Deskripsi')
@section('content')

 <a href="{{url('/sop-deskripsi/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Deskripsi SOP Baru</a>
  <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Sub</th>
         <th>Instansi</th>
         <th>Nama SOP</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('sop_deskripsi.datatables_script',['url'=>url('sop-deskripsi/getData')])
<script>
var params={};
drawTable('#search-result');
</script>
@endsection