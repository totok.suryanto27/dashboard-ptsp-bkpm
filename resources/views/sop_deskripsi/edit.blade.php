@extends('backend')
@section('title','SOP Deskripsi')
@section('content')

<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('sop-deskripsi/update/'.$id) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.number',['label'=>'ID SOP Deskripsi','required'=>true,'name'=>'id_m_sop_deskripsi','value'=>$sop_deskripsis['id']->id_m_sop_deskripsi])
        @include('form.number',['label'=>'ID Parent SOP Deskripsi','required'=>true,'name'=>'id_parent','value'=>$sop_deskripsis['id']->id_parent])
        @include('form.text',['label'=>'Nama Instansi','required'=>false,'name'=>'id_m_instansi','value'=>$sop_deskripsis['id']->id_m_instansi])
        @include('form.text',['label'=>'Nama Direktorat','required'=>false,'name'=>'id_m_direktorat','value'=>$sop_deskripsis['id']->id_m_direktorat])
        @include('form.text',['label'=>'Jenis Perizinan','required'=>false,'name'=>'id_m_jenis_perizinan','value'=>$sop_deskripsis['id']->id_m_jenis_perizinan])
        </div>
        <div class="row">
        <div class="col-md-8">
        <h2>Bahasa Indonesia</h2>
        @include('form.text',['label'=>'Nama SOP','required'=>true,'name'=>'nama_sop[id]','value'=>$sop_deskripsis['id']->nama_sop])
        <div class="textwrapper">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_a_id" data-toggle="tab">Persyaratan</a></li>
            <li><a href="#tab_b_id" data-toggle="tab">SLA</a></li>
            <li><a href="#tab_c_id" data-toggle="tab">Prosedur</a></li>
            <li><a href="#tab_d_id" data-toggle="tab">Dasar Hukum</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_a_id">
              <textarea name="persyaratan[id]">{!! $sop_deskripsis['id']->persyaratan !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_b_id">
              <textarea name="sla[id]">{!! $sop_deskripsis['id']->sla !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_c_id">
              <textarea name="prosedur[id]">{!! $sop_deskripsis['id']->prosedur !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_d_id">
              <textarea name="dasar_hukum[id]">{!! $sop_deskripsis['id']->dasar_hukum !!}</textarea>
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-8">
        <h2>English</h2>
        @include('form.text',['label'=>'SOP Name','required'=>true,'name'=>'nama_sop[en]','value'=>$sop_deskripsis['en']->nama_sop])
        <div class="textwrapper">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_a_en" data-toggle="tab">Requirements</a></li>
            <li><a href="#tab_b_en" data-toggle="tab">SLA</a></li>
            <li><a href="#tab_c_en" data-toggle="tab">Procedure</a></li>
            <li><a href="#tab_d_en" data-toggle="tab">Legal Basis</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_a_en">
              <textarea name="persyaratan[en]">{!! $sop_deskripsis['en']->persyaratan !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_b_en">
              <textarea name="sla[en]">{!! $sop_deskripsis['en']->sla !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_c_en">
              <textarea name="prosedur[en]">{!! $sop_deskripsis['en']->prosedur !!}</textarea>
            </div>
            <div class="tab-pane" id="tab_d_en">
              <textarea name="dasar_hukum[en]">{!! $sop_deskripsis['en']->dasar_hukum !!}</textarea>
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.form-buttons {
    text-align: right;
}
.form-control.select2-container {
    border: none;
    padding: 0;
}
.form-control.autosize-transition {
    border: 1px solid #d5d5d5;
    margin-bottom: 4px;
}
.form-group input {
    height: 30px;
    padding-top: 0;
    padding-bottom: 0;
    margin-bottom: 4px;
}
.form-control-static {
    font-weight: bold;
    padding-top: 4px;
}
.form-group {
    margin-bottom: 0;
}
.no-padding {
    padding: 0;
}
.left-panel, .right-panel {
    padding: 15px;
}
.form-horizontal .radio {
    padding-top: 0;
}
.radio label {
    padding-left: 0;
}
.profile-user-info {
    margin: 0;
}
.profile-info-name {
    width: 178px;
}
.profile-info-value {
    margin-left: 188px;
    padding-left: 0px;
}
#search-result {
    margin-top: 40px;
}
.tab-content {
    border: none;
    padding: 0;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/jquery/jquery.autocomplete.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/bower_components/tinymce/tinymce.min.js')}}"></script>
<script>

var jenis_izin = {!! json_encode(App\Model\JenisPerizinan::get(['nama_jenis_perizinan','id_m_jenis_perizinan','id_m_instansi'])) !!};
var instansi = {!! json_encode(App\Model\Instansi::get(['nama_instansi','id_m_instansi'])) !!};
var direktorat = {!! json_encode(App\Model\Direktorat::get(['nama_direktorat','id_m_direktorat','id_m_instansi'])) !!};

$("#id_parent").change(function() {
    if ($(this).val() == "")
        $(".textwrapper").show();
    else
        $(".textwrapper").hide();
}).trigger("change");
$("#id_m_instansi").select2({
    data: function() { 
        return {results: $.map(instansi,function(v) { 
            return {id: v.id_m_instansi,text: v.nama_instansi}; 
        })};
    }
});
$("#id_m_instansi").change(function() {
    reloadFciJenisPerizinan($(this).val());
});
reloadFciJenisPerizinan("{{$sop_deskripsis['id']->id_m_instansi}}");
function reloadFciJenisPerizinan(id_m_instansi) {
    $("#id_m_jenis_perizinan").select2({
        data: function() { 
            return {results: $.map(jenis_izin,function(v) { 
                if (v.id_m_instansi == id_m_instansi)
                return {id: v.id_m_jenis_perizinan,text: v.nama_jenis_perizinan}; 
            })};
        }
    });

    $("#id_m_direktorat").select2({
        data: function() { 
            return {results: $.map(direktorat,function(v) {
                if (v.id_m_instansi == id_m_instansi)
                return {id: v.id_m_direktorat,text: v.nama_direktorat}; 
            })};
        }
    });
}

tinymce.init({
    selector: "textarea",
    menubar: false,
    resize: "both",
    relative_urls: false,
    plugins: ["autoresize", "image", "code", "lists", "code","example", "link","preview","advlist"],
    indentation : '20pt',
    image_list: "{{url('gallery/image-list')}}",
    toolbar: [
        "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | bullist numlist | preview"
    ]});

</script>
@endsection
