<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
            {data: 'id_m_sop_deskripsi', name: 'id_m_sop_deskripsi'},
            {data: 'id_parent', name: 'id_parent'},
            {data: 'instansi.nama_instansi', name: 'id_m_instansi'},
            {data: 'nama_sop', name: 'nama_sop'},
            {data: 'actions', name: 'actions', searchable: false, orderable: false}
            ],
            order: [[2,'asc'],[0,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>

