@extends('backend')
@section('title', 'Monitoring Pengguna ' . $id)
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="form-group">
                <label for="select-bulan">Bulan&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <select name="bulan" id="bulan" style="width:20%;" >
                    <option value="1" <?php echo ($month == 1) ? 'selected' : ''; ?>>Januari</option>
                    <option value="2" <?php echo ($month == 2) ? 'selected' : ''; ?>>Februari</option>
                    <option value="3" <?php echo ($month == 3) ? 'selected' : ''; ?>>Maret</option>
                    <option value="4" <?php echo ($month == 4) ? 'selected' : ''; ?>>April</option>
                    <option value="5" <?php echo ($month == 5) ? 'selected' : ''; ?>>Mei</option>
                    <option value="6" <?php echo ($month == 6) ? 'selected' : ''; ?>>Juni</option>
                    <option value="7" <?php echo ($month == 7) ? 'selected' : ''; ?>>Juli</option>
                    <option value="8" <?php echo ($month == 8) ? 'selected' : ''; ?>>Agustus</option>
                    <option value="9" <?php echo ($month == 9) ? 'selected' : ''; ?>>September</option>
                    <option value="10" <?php echo ($month == 10) ? 'selected' : ''; ?>>Oktober</option>
                    <option value="11" <?php echo ($month == 11) ? 'selected' : ''; ?>>November</option>
                    <option value="12" <?php echo ($month == 12) ? 'selected' : ''; ?>>Desember</option>
                     
                </select>
            </div>
            <div id="grf-aktivitas-bulan" style="width:100%;height:800px;"></div>
        </div> 
        
        
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="form-group">
                <label for="select-tahun">Tahun&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <select name="tahun" id="tahun" style="width:20%;">
                    <option value="">--Pilih Tahun--</option>
                    <option value="2018" <?php echo ($year == '2018') ? 'selected' : '' ?>>2018</option>
                    <option value="2017" <?php echo ($year == '2017') ? 'selected' : '' ?>>2017</option>
                    <option value="2016" <?php echo ($year == '2016') ? 'selected' : '' ?>>2016</option>
                    <option value="2015" <?php echo ($year == '2015') ? 'selected' : '' ?>>2015</option>
                    
                    
                </select>
            </div>
            <div id="grf-aktivitas-tahun" style="width:100%;height:400px;"></div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script>
var year = <?php echo $year ?>,
month = <?php echo $month ?>;
var user = "<?php echo $_GET['user'] ?>";
$('#bulan').select2();
$('#tahun').select2();
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="{{ asset('monitoring_pengguna/montoring-detail.js') }}"></script>

@stop