<form class="profile-user-info profile-user-info-striped" id="form_konsultasi">
	{!! csrf_field() !!}
	@include('form.view_styled',['label'=>'Tanggal','required'=>false,'name'=>'fk_tanggal','value'=>getFullDate(date("Y-m-d"))])
	@include('form.select2_styled',['label'=>'Kategori Konsultasi','required'=>false,'name'=>'fk_id_konsul_cat','data'=>App\Model\KonsultasiKategori::lists('nama_konsul_cat','id_konsul_cat')])
	@include('form.text_styled',['label'=>'Instansi','required'=>false,'name'=>'fk_id_m_instansi'])
	<?php /*@include('form.text_styled',['label'=>'Direktorat','required'=>false,'name'=>'fk_id_m_direktorat']) */?>
	@include('form.text_styled',['label'=>'Nama Tamu','required'=>false,'name'=>'fk_nama_tamu'])
	@include('form.text_styled',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fk_nama_perusahaan'])
	@include('form.text_styled',['label'=>'Alamat','required'=>false,'name'=>'fk_alamat'])
	@include('form.text_styled',['label'=>'Telepon','required'=>false,'name'=>'fk_telepon'])
	@include('form.text_styled',['label'=>'Email','required'=>false,'name'=>'fk_email'])
	@include('form.text_styled',['label'=>'Judul Konsultasi','required'=>false,'name'=>'fk_judul_konsultasi'])
	@include('form.textarea_styled',['label'=>'Pertanyaan','required'=>false,'name'=>'fk_pertanyaan'])
	@include('form.textarea_styled',['label'=>'Jawaban','required'=>false,'name'=>'fk_jawaban'])
	@if (Request::has('nama_petugas'))
	@include('form.view_styled',['label'=>'Nama Petugas','required'=>false,'name'=>'fk_nama_petugas','value'=>Request::get('nama_petugas')])
	@include('form.hidden',['name'=>'fk_nama_petugas','value'=>Request::get('nama_petugas')])
	@else
	@include('form.view_styled',['label'=>'Nama Petugas','required'=>false,'name'=>'fk_nama_petugas','value'=>getUserName()])
	@endif
</form>
<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_konsultasi"><i class="fa fa-sign-out"></i> Submit</a>
</div>
