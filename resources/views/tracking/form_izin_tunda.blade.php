<form class="form-horizontal" id="form_izin_tunda">
	{!! csrf_field() !!}
	@include('form.view',['label'=>'IP','required'=>false,'name'=>'fit_no_ip'])
	@include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fit_nama_perusahaan'])
	<div class="row">
		<div class="col-md-12">
			<b>Pilih Izin</b>
			<div id="fit_id_tracking_register_detail_additional">
			</div>
		</div>
	</div>
	@include('form.view',['label'=>'Tanggal Tunda','required'=>false,'name'=>'fit_tanggal_selesai','value'=>getFullDate(date("Y-m-d"))])
	@include('form.textarea',['label'=>'Alasan Tunda','required'=>false,'name'=>'fit_alasan_stop'])
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_izin_tunda"><i class="fa fa-sign-out"></i> Simpan Tunda Izin</a>
	</div>
</form>
