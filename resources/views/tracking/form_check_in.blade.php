<form class="form-horizontal" id="form_check_in">
	{!! csrf_field() !!}
	@include('form.hidden',['name'=>'fci_id_qrcode'])
	@include('form.radio_padded',['label'=>'Pilih Izin Terintegrasi','required'=>false,'name'=>'fci_id_m_flow_terintegrasi','data'=>["new_terintegrasi"=>"Izin Terintegrasi Baru","new_tunggal"=>"Izin Tunggal"]])
	@include('form.select2',['label'=>'Flow Terintegrasi','required'=>false,'name'=>'fci_id_m_flow_terintegrasi_choice','data'=>App\Model\FlowIzinTerintegrasi::lists('nama_flow','id_m_flow_izin_terintegrasi'),'empty'=>'-- Pilih --'])
	@include('form.text',['label'=>'Jenis Izin','required'=>false,'name'=>'fci_id_m_jenis_perizinan'])
	@include('form.textarea',['label'=>'Komentar','required'=>false,'name'=>'fci_komentar_check_in'])
<div class="profile-user-info profile-user-info-striped">
	@include('form.view_styled',['label'=>'IP','required'=>false,'name'=>'fci_no_ip'])
	@include('form.view_styled',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fci_nama_perusahaan'])
	@include('form.view_styled',['label'=>'Instansi','required'=>false,'name'=>'fci_nama_instansi','data'=>App\Model\Instansi::lists('nama_instansi','id_m_instansi'),'empty'=>'-- Pilih --'])
	<?php /*@include('form.view_styled',['label'=>'Direktorat','required'=>false,'name'=>'fci_nama_direktorat','data'=>App\Model\Direktorat::lists('nama_direktorat','id_m_direktorat'),'empty'=>'-- Pilih --']) */?>
	@include('form.view_styled',['label'=>'Bidang Usaha','required'=>false,'name'=>'fci_nama_bidang_usaha'])
	@include('form.view_styled',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'fci_tanggal_pengajuan','value'=>getFullDate(date("Y-m-d"))])
	@include('form.view_styled',['label'=>'Tanggal Estimasi','required'=>false,'name'=>'fci_tanggal_selesai_estimasi'])
</div>
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_check_in"><i class="fa fa-sign-out"></i> Simpan Registrasi</a>
	</div>
</form>
