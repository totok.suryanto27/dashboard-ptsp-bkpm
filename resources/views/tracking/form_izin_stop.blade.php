<form class="form-horizontal" id="form_izin_stop">
	{!! csrf_field() !!}
	@include('form.view',['label'=>'IP','required'=>false,'name'=>'fis_no_ip'])
	@include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fis_nama_perusahaan'])
	<div class="row">
		<div class="col-md-12">
			<b>Pilih Izin</b>
			<div id="fis_id_tracking_register_detail_additional">
			</div>
		</div>
	</div>
	@include('form.view',['label'=>'Tanggal Stop','required'=>false,'name'=>'fis_tanggal_selesai','value'=>getFullDate(date("Y-m-d"))])
	@include('form.textarea',['label'=>'Alasan Stop','required'=>false,'name'=>'fis_alasan_stop'])
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_izin_stop"><i class="fa fa-sign-out"></i> Simpan Stop Izin</a>
	</div>
</form>
