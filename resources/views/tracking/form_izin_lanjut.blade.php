<form class="form-horizontal" id="form_izin_lanjut">
	{!! csrf_field() !!}
	@include('form.view',['label'=>'IP','required'=>false,'name'=>'fil_no_ip'])
	@include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fil_nama_perusahaan'])
	<div class="row">
		<div class="col-md-12">
			<b>Pilih Izin</b>
			<div id="fil_id_tracking_register_detail_additional">
			</div>
		</div>
	</div>
	@include('form.view',['label'=>'Tanggal Lanjut','required'=>false,'name'=>'fil_tanggal_selesai','value'=>getFullDate(date("Y-m-d"))])
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_izin_lanjut"><i class="fa fa-sign-out"></i> Simpan Izin Lanjut</a>
	</div>
</form>
