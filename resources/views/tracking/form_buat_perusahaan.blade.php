<div class="modal fade" id="buatPerusahaanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Perusahaan</h4>
      </div>
      <div class="modal-body">
        <form id="form_buat_perusahaan">
          {!! csrf_field() !!}
    			@include('form.text',['required'=>false,'label'=>'Nama Perusahaan','name'=>'bp_nama_perusahaan'])
    			@include('form.text',['required'=>false,'label'=>'NPWP','name'=>'bp_npwp'])
    			@include('form.text',['required'=>false,'label'=>'Alamat','name'=>'bp_alamat'])
    			@include('form.select2',['required'=>false,'label'=>'Provinsi','name'=>'bp_id_provinsi','empty'=>'Pilih Provinsi','data'=>App\Model\Provinsi::whereNotNull('mulai_exist')->whereNull('akhir_exist')->lists('nama_provinsi','id_provinsi')])
          @include('form.text',['required'=>false,'label'=>'Kota / Kabupaten','name'=>'bp_id_kabkot'])
    			@include('form.text',['required'=>false,'label'=>'Telepon','name'=>'bp_telepon'])
    			@include('form.text',['required'=>false,'label'=>'Email','name'=>'bp_email'])
    			@include('form.text',['required'=>false,'label'=>'Pimpinan','name'=>'bp_nama_pimpinan'])
        </form>
      </div>
      <div class="modal-footer">
        <a id="btn-form-buat-perusahaan" class="btn btn-success pull-right btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-plus"></i> Perusahaan</a>
      </div>
    </div>
  </div>
</div>
