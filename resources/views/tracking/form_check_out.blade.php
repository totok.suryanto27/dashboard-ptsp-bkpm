<form class="form-horizontal" id="form_check_out">
	{!! csrf_field() !!}
	@include('form.view',['label'=>'IP','required'=>false,'name'=>'fco_no_ip'])
	@include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fco_nama_perusahaan'])
	<div class="row">
		<div class="col-md-12">
			<b>Pilih Izin</b>
			<div id="fco_id_tracking_register_detail_additional">
			</div>
		</div>
	</div>
	@include('form.view',['label'=>'Tanggal Selesai','required'=>false,'name'=>'fco_tanggal_selesai','value'=>getFullDate(date("Y-m-d"))])
	@include('form.textarea',['label'=>'Komentar','required'=>false,'name'=>'fco_komentar_check_out'])
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_check_out"><i class="fa fa-sign-out"></i> Simpan Izin Selesai</a>
	</div>
</form>
