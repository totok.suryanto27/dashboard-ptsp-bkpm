<form class="form-horizontal" id="form_ambil_izin">
	{!! csrf_field() !!}
	@include('form.view',['label'=>'IP','required'=>false,'name'=>'fai_no_ip'])
	@include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'fai_nama_perusahaan'])
	<div class="row">
		<div class="col-md-12">
			<b>Pilih Izin</b>
			<div id="fai_id_tracking_register_detail_additional">
			</div>
		</div>
	</div>
	@include('form.view',['label'=>'Tanggal Ambil','required'=>false,'name'=>'fai_tanggal_selesai','value'=>getFullDate(date("Y-m-d"))])
	<div class="form-buttons">
	<a class="btn btn-primary ladda-button" id="submit_form_ambil_izin"><i class="fa fa-sign-out"></i> Simpan Ambil Izin</a>
	</div>
</form>
