<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
@page {
    margin: 1.5cm;
}

body {
  font-family: sans-serif;
    margin: 0.5cm 0;
    text-align: justify;
}

#footer {
  position: fixed;
  left: 0;
    right: 0;
    color: #aaa;
    font-size: 0.9em;
}

#footer {
  bottom: 0;
}

#footer table {
    width: 100%;
    border-collapse: collapse;
    border: none;
}

#header td,
#footer td {
  padding: 0;
    width: 50%;
}

hr {
  page-break-after: always;
  border: 0;
}

.boxed {
  border: 1px solid black;
}

.title {
  font-size: 18pt;
}
.subtitle {
  font-size: 14pt;
}

p {
  text-align: left;
  font-size: 1em;
  margin: 0 0 2pt 0;
  padding: 0;
}
.padded {
  padding: 10pt;
}
</style>
</head>
<body>

<div id="footer">
  SPM Tracking PTSP Pusat | {{date("d M Y H:i:s")}}
</div>

<p align="center" class="title">
    <strong>PELAYANAN TERPADU SATU PINTU (PTSP) PUSAT</strong>
</p>
<p align="center" class="title">
    <strong>
        SISTEM PENELUSURAN PERIZINAN
        <br/>
        BERBASIS QRCODE TUNGGAL
    </strong>
</p>
<br/>
<br/>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="126" valign="top" class="padded">
                <p align="center">
                    <img width="170" height="170" src="{{$image_qrcode}}"/>
                </p>
                <p align="center">
                    No QRCode
                    <br/>
                    {{$id_qrcode}}
                </p>
            </td>
            <td width="308" valign="top" class="padded">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Nomor Izin Prinsip
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$no_ip}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Nomor Perusahaan
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$no_perusahaan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Nama Perusahaan
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_perusahaan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    NPWP
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$npwp}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Bidang Usaha
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_bidang_usaha}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top" colspan="3">
                                <p>
                                    Alamat Kedudukan Perusahaan :
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Alamat Kantor
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$alamat_perusahaan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Kota
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_kabkot}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Provinsi
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_provinsi}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Telepon
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$no_telepon}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Faksimili
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$no_fax}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Email
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$email}}
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top" class="padded">
                <p>
                    Alamat akses investor: {{$site_url}}<a href="http://spm-tracking.bkpm.go.id/"></a>
                </p>
                <p>
                    Nomor QRCode ini dipergunakan untuk menelusuri status perizinan perusahaan pada kementerian / lembaga di PTSP Nasional. Selalu lampirkan
                    nomor ini pada saat pengajuan.
                </p>
            </td>
        </tr>
    </tbody>
</table>
<hr/>
<p align="center" class="title">
    <strong>PELAYANAN TERPADU SATU PINTU (PTSP) PUSAT</strong>
</p>
<p align="center" class="title">
    <strong><u>TANDA TERIMA PENDAFTARAN</u></strong>
    <br/>
    <em class="subtitle">Application Receipt</em>
</p>
<br/>
<br/>
<table border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="126" valign="top" class="padded">
                <p align="center">
                    <img border="0" width="170" height="170" src="{{$image_qrcode}}"/>
                    No QRCode
                    <br/>
                    {{$id_qrcode}}
                </p>
            </td>
            <td width="308" valign="top" class="padded">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    NOMOR PERMOHONAN
                                    <br/>
                                    <em>Application Number</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$no_ip}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top" colspan="3">
                                <p>
                                    Sudah Terima Dari (<em>Received From</em>)
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Nama Perusahaan
                                    <br/>
                                    <em>Company Name</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_perusahaan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Jenis Permohonan
                                    <br/>
                                    <em>Kind of Application</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                   {{$nama_jenis_perizinan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    BIdang Usaha
                                    <br/>
                                    <em>Business Fields</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                   {{$nama_bidang_usaha}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Kementerian / Lembaga
                                    <br/>
                                    <em>Ministry / Institution</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$nama_instansi}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Lama SOP / SLG
                                    <br/>
                                    <em>SOP / SLG Duration</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$sop}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Tanggal Pengajuan
                                    <br/>
                                    <em>Filing date</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$tanggal_pengajuan}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="134" valign="top">
                                <p>
                                    Tanggal Estimasi Selesai
                                    <br/>
                                    <em>Finish estimation date</em>
                                </p>
                            </td>
                            <td width="5" valign="top">:</td>
                            <td width="186" valign="top">
                                <p>
                                    {{$tanggal_selesai_estimasi}}
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top" class="padded">
                <p>
                    Alamat akses investor: {{$site_url}}<a href="http://spm-tracking.bkpm.go.id/"></a>
                </p>
                <p>
                    Nomor QRCode ini dipergunakan untuk menelusuri status perizinan perusahaan pada kementerian / lembaga di PTSP Nasional. Selalu lampirkan
                    nomor ini pada saat pengajuan.
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p class="boxed" style="margin: 15pt;padding:10pt;width:300px;">
    Pengurusan perizinan dan non-perizinan
    <br/>
    PTSP Pusat tidak dikenakan biaya
</p>
<div style="margin-left: 330px">
    <p align="center">
        Jakarta, {{substr($tanggal_pengajuan,strpos($tanggal_pengajuan,',')+1)}}
    </p>
    <br/>
    <br/>
    <br/>
    <p align="center">
        <u>_________________</u>
    </p>
    <p align="center">
        {{$nama_petugas}}
    </p>
</div>
</body> </html>
