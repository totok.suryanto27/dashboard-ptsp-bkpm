@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Perizinan Online K/L</a></li>
              <li class="active">Kementerian Lingkungan Hidup dan Kehutanan</li>
            </ol>
              <div class="nipz-content">
                <h4>Daftar Perizinan</h4>
                <ol>
                    <li>Izin Usaha Pemanfaatan Hasil Hutan Kayu Pada Hutan Alam (IUPHHK-HA)</li>
                    <li>Izin Usaha Pemanfaatan Hasil Hutan Kayu Hutan Tanaman Industri Pada Hutan Tanaman (IUPHHK-HTI)</li>
                    <li>Izin Usaha Pemanfaatan Hasil Hutan Kayu Restorasi Ekosistem Dalam Hutan Alam (IUPHHK-RE)</li>
                    <li>Perpanjangan Izin Usaha Pemanfaatan Hasil Hutan Kayu Pada Hutan Alam (IUPHHK-HA)</li>
                    <li>Izin Usaha Pemanfaatan Penyerapan Karbon dan/atau Penyimpanan Karbon (UP RAP-KARBON dan/atau UP PAN-KARBON) Pada Hutan Lindung</li>
                    <li>Izin Usaha Pemanfaatan Penyerapan Karbon dan/atau Penyimpanan Karbon (UP RAP-KARBON dan/atau UP PAN-KARBON) Pada Hutan Produksi</li>
                  <li>Izin Usaha Industri Primer Hasil Hutan Kayu di atas 6.000 m3/Tahun</li>
                  <li>Izin Perluasan Izin Usaha Industri Primer Hasil Hutan Kayu di atas 6.000 m3/Tahun</li>
                  <li>Izin Usaha Pemanfaatan Kawasan Silvo Pastura pada Hutan Produksi</li>
                  <li>Izin Pinjam Pakai Kawasan Hutan</li>
                  <li>Pelepasan Kawasan Hutan</li>
                  <li>Izin Usaha Penyediaan Sarana Wisata Alam</li>
                  <li>Izin Lembaga Konservasi</li>
                  <li>Izin Pengusahaan Taman Buru</li>
                  <li>Izin Peminjaman Satwa Liar Dilindungi ke Luar Negeri untuk Kepentingan Pengembangbiakan (Breeding Loan)</li>
                  <li>Izin Usaha Pemanfaatan Air untuk Skala Menengah dan Skala Besar di Suaka Margasatwa, Taman Nasional, Taman Wisata Alam, dan Taman Hutan Raya</li>
                  <li>Izin Usaha Pemanfaatan Energi Air untuk Skala Menengah dan Skala Besar di Suaka Margasatwa, Taman Nasional, dan Taman Hutan Raya</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_bahasa/SOP LINGKUNGAN HIDUP DAN KEHUTANAN.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
@endsection
