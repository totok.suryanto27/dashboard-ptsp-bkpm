@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Perizinan Online K/L</a></li>
              <li class="active">Kementerian Ketenagakerjaan</li>
            </ol>
              <div class="nipz-content">
                <h4>Daftar Perizinan</h4>
                <ol>
                  <li>Izin Usaha :
                    <ul>
                        <li>Izin Usaha Lembaga Pelatihan Kerja (LPK)</li>
                        <li>Izin Usaha Jasa Penempatan Tenaga Kerja Indonesia di Dalam Negeri</li>
                        <li>Izin Usaha Penyediaan Jasa Pekerja/Buruh</li>
                    </ul>
                  </li>
                  <li>Rekomendasi Teknis Izin Usaha Lembaga Pelatihan Kerja (LPK)</li>
                  <li>Rekomendasi Teknis Izin Usaha Jasa Penempatan Tenaga Kerja Indonesia di Dalam Negeri</li>
                  <li>Rencana Penggunaan Tenaga Kerja Asing (RPTKA)</li>
                  <li>T.A 01 untuk Izin Tenaga Kerja Asing</li>
                  <li>Rekomendasi Teknis Izin Menggunakan Tenaga Kerja Asing (IMTA)</li>
                </ol>
            <div class="mb-10"></div>
            <a class="btn btn-primary" href="{{ asset('sop_bahasa/SOP KEMENTERIAN KETENAGAKERJAAN.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
@endsection
