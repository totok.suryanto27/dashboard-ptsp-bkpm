@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
              <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Beranda</a></li>
                <li><a href="{{ url('page/perizinan-kl') }}">Perizinan Online K/L</a></li>
                <li class="active">Kementerian ESDM</li>
              </ol>
              <div class="nipz-content">
                <h4>Daftar Perizinan</h4>
                <ol>
                  <li>Izin Usaha Penyediaan Tenaga Listrik</li>
                  <li>Izin Operasi (Sertifikat Laik Operasi - SLO)</li>
                  <li>Penetapan Wilayah Usaha</li>
                  <li>Izin Usaha Jasa Penunjang Tenaga Listrik</li>
                  <li>Izin Jual Beli Tenaga Listrik Lintas Negara</li>
                  <li>Izin Pemanfaatan Jaringan Tenaga Listrik untuk Kepentingan Telekomunikasi, Multimedia dan Informatika</li>
                  <li>Penugasan Survey Pendahuluan Panas Bumi (PSP)</li>
                  <li>Izin Panas Bumi</li>
                  <li>Persetujuan Usaha Penunjang Panas Bumi</li>
                  <li>Izin Penggunaan Gudang Bahan Peledak Panas Bumi</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_bahasa/SOP KEM ESDM.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection
