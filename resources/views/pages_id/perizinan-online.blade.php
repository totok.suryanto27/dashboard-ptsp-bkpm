@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li class="active">Perizinan Online Terintegrasi PTSP Pusat</li>
            </ol>
              <div class="nipz-content">
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/listrik2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenaga-listrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/industri2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/pertanian2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/perhubungan2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/wisata2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="{{ asset('images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection
