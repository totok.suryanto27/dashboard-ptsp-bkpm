@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li><a href="{{ url('page/perizinan-online') }}">Perizinan Online Terintegrasi PTSP Pusat</a></li>
              <li>Bidang Usaha Ketenagalistrikan</li>
            </ol>
              <div class="nipz-content">
                <img class="img-responsive" src="{{ asset('images/f1.png') }}"/>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('ptsp/ketenagalistrikan.pdf') }}" download><i class="fa fa-download"></i> Download</a>
            </div>
          </div>
@endsection
