@extends('app')
@section('title','Perizinan K/L - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li class="active">Perizinan Online K/L</li>
            </ol>
              <div class="nipz-content">
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perdagangan') }}">
                      <img src="{{ asset('images/logokem/kemendag.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Perdagangan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perindustrian') }}">
                      <img src="{{ asset('images/logokem/kemenperin.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perhubungan') }}">
                      <img src="{{ asset('images/logokem/kemenhub.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-esdm') }}">
                      <img src="{{ asset('images/logokem/esdm.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian ESDM</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pariwisata') }}">
                      <img src="{{ asset('images/logokem/pariwisata.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pertanian') }}">
                      <img src="{{ asset('images/logokem/pertanian.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kesehatan') }}">
                      <img src="{{ asset('images/logokem/kesehatan.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Kesehatan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kominfo') }}">
                      <img src="{{ asset('images/logokem/kominfo.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Kominfo</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-dikbud') }}">
                      <img src="{{ asset('images/logokem/kemendikbud.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Pendidikan & Kebudayaan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kepri') }}">
                      <img src="{{ asset('images/logokem/laut.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Kelautan & Perikanan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-humkam') }}">
                      <img src="{{ asset('images/logokem/hukum.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Hukum & HAM</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-keuangan') }}">
                      <img src="{{ asset('images/logokem/keuangan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Keuangan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-ketenagakerjaan') }}">
                      <img src="{{ asset('images/logokem/ketenagakerjaan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Ketenagakerjaan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pupera') }}">
                      <img src="{{ asset('images/logokem/pu.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian PU & PERA</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bpn') }}">
                      <img src="{{ asset('images/logokem/bpn.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Agraria/BPN</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kehutanan') }}">
                      <img src="{{ asset('images/logokem/lingkunganhidup.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Lingkungan Hidup dan Kehutanan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pertahanan') }}">
                      <img src="{{ asset('images/logokem/menhan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kementerian Pertahanan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kepolisian') }}">
                      <img src="{{ asset('images/logokem/polisi.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Kepolisian RI</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bpom') }}">
                      <img src="{{ asset('images/logokem/bpom.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">BPOM</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bsn') }}">
                      <img src="{{ asset('images/logokem/bsn.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">BSN</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pln') }}">
                      <img src="{{ asset('images/logokem/pln.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">PLN</p>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="{{ asset('images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="{{ asset('images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="{{ asset('images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection
