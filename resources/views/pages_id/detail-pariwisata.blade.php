@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Perizinan Online K/L</a></li>
              <li class="active">Kementerian Pariwisata</li>
            </ol>
            <div class="nipz-content">
            <h4>Daftar Perizinan</h4>
            <ol>
              <li>Pendaftaran Usaha Daya Tarik Wisata</li>
              <li>Pendaftaran Usaha Kawasan Pariwisata</li>
              <li>Pendaftaran Usaha :</li>
                            <ul>
                                <li>Jasa Transportasi</li>
                                <li>Jasa Perjalanan Wisata</li>
                                <li>Jasa Makanan & Minuman</li>
                                <li>Penyediaan Akomodasi</li>
                                <li>Penyelenggaraan Kegiatan Hiburan dan Rekreasi</li>
                                <li>Penyelenggaraan Pertemuan, Perjalanan Isentif, Konfrensi dan Pameran</li>
                                <li>Jasa Informasi Pariwisata</li>
                                <li>Jasa Konsultan Pariwisata</li>
                                <li>Wisata Tirta</li>
                                <li>Usaha Spa</li>
                            </ul>
              <li>Surat Izin Produksi (SIP) Film oleh Produser Film/TV Asing di Indonesia</li>
              <li>Izin Usaha Perfilman Jasa Teknik Film</li>
              <li>Izin Usaha Perfilman Pengedaran Film</li>
              <li>Izin Usaha Perfilman Pengarsipan Film</li>
              <li>Izin Usaha Perfilman Ekspor Film</li>
              <li>Izin Usaha Perfilman Impor Film</li>
              <li>Rekomendasi Terkait Pemberian Izin Lokasi Syuting</li>
            </ol>
            <div class="mb-10"></div>
            <a class="btn btn-primary" href="{{ asset('sop_bahasa/SOP KEMENTERIAN PARIWISATA.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection
