@extends('app')
@section('title','Perizinan Online Terintegrasi PTSP Pusat - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Beranda</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Perizinan Online K/L</a></li>
              <li class="active">Kementerian Pertanian</li>
            </ol>
            <div class="nipz-content">
              <h4>Daftar Perizinan</h4>
              <ol>
                <li>Izin Usaha :
                            <ul>
                                <li>Tanaman Pangan</li>
                                <li>Hortikultura</li>
                                <li>Perkebunan</li>
                                <li>Peternakan</li>
                                <li>Obat Hewan (Produsen)</a></li>
                            </ul>
                </li>
                <li>Rekomendasi Teknis :
                            <ul>
                                <li>Budidaya Tanaman Pangan</li>
                                <li>Industri Pengolahan Hasil Perkebunan</li>
                                <li>Perkebunan yang Terintegrasi antara Budidaya dengan Industri Pengolahan Hasil Perkebunan</li>
                            </ul>
                </li>
                <li>Rekomendasi Teknis Perbenihan/Pembibitan/Budidaya</li>
              </ol>
              <div class="mb-10"></div>
              <a class="btn btn-primary" href="{{ asset('sop_bahasa/SOP KEMENTERIAN PERTANIAN.pdf') }}" download><i class="fa fa-download"></i> Download</a> 
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection
