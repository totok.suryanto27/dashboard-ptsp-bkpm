@extends('backend')
@section('title','Instansi')
@section('content')
<div class="col-xs-12">
  <div class="row">
  @if ($instansi->filename_logo && $instansi->filename_logo != '')
  <div class="col-md-4">
      <a href="{{asset('images/logokem/'.$instansi->filename_logo)}}" target="_blank"><img src="{{asset('images/logokem/'.$instansi->filename_logo)}}" style="max-width: 100%"></a>
  </div>
  @endif
  <div class="col-md-8">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Nama Instansi (Indonesia)','required'=>false,'name'=>'nama_instansi','value'=>$instansi->nama_instansi])
    @include('form.view_styled',['label'=>'Nama Instansi (English)','required'=>false,'name'=>'nama_instansi_eng','value'=>$instansi->nama_instansi_eng])
    @include('form.view_styled',['label'=>'Singkatan','required'=>false,'name'=>'abbrv','value'=>$instansi->abbrv])
    @include('form.view_styled',['label'=>'Dasar Hukum','required'=>false,'name'=>'dasar_hukum','value'=>$instansi->dasar_hukum])
    @include('form.view_styled',['label'=>'Tampilkan Instansi Pada Halaman Perizinan Online K/L','required'=>false,'name'=>'isbko','value'=>($instansi->isbko=='T'?'Ya':'Tidak').'<br><br>'])

    </div>
  </div>
    <div class="space-4"></div>
  </div>
  <a href="{{url('/instansi/edit', $instansi->id_m_instansi)}}" class="btn btn-success"><i class="fa fa-edit"></i> Edit Instansi</a>
  @if ($instansi->filename && $instansi->filename != '')
  <br><br>SOP Bahasa Indonesia<br>
      <a class="btn btn-primary" href="{{asset('sop_bahasa/'.$instansi->filename)}}" target="_blank"><i class="fa fa-download"></i> {{$instansi->filename}}</a>
  @endif
  @if ($instansi->filename_eng && $instansi->filename_eng != '')
  <br><br>SOP English<br>
      <a class="btn btn-primary" href="{{asset('sop_english/'.$instansi->filename_eng)}}" target="_blank"><i class="fa fa-download"></i> {{$instansi->filename_eng}}</a>
  @endif
  
  </div>
@endsection
@section('styles')
<style>
.profile-info-name {
    width:200px;
}
.profile-info-value {
    margin-left:200px;
}
</style>
@endsection