<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    console.log(d);
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
            {data: 'id_m_instansi', name: 'id_m_instansi'},
            {data: 'nama_instansi', name: 'nama_instansi'},
            {data: 'filename', name: 'filename'},
            {data: 'nama_instansi_eng', name: 'nama_instansi_eng'},
            {data: 'filename_eng', name: 'filename_eng'},
            {data: 'abbrv', name: 'abbrv'},
            {data: 'actions', name: 'actions', searchable: false, orderable: false}
            ],
            order: [[0,'asc']],
            search: {
                caseInsensitive:false
            }

        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
