@extends('backend')
@section('title','Instansi')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('instansi/store') }}" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.text',['label'=>'Nama Instansi (Indonesia)','required'=>false,'name'=>'nama_instansi'])
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4"> File SOP (Indonesia)</label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="filename"/>
                    </div>
                </div>
            </div>
        </div>
        @include('form.text',['label'=>'Nama Instansi (English)','required'=>false,'name'=>'nama_instansi_eng'])
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4"> File SOP (English)</label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="filename_eng"/>
                    </div>
                </div>
            </div>
        </div>
        @include('form.text',['label'=>'Singkatan','required'=>false,'name'=>'abbrv'])
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4"> Logo</label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="filename_logo"/>
                    </div>
                </div>
            </div>
        </div>
        @include('form.textarea',['label'=>'Dasar Hukum','required'=>false,'name'=>'dasar_hukum'])
        @include('form.select',['label'=>'Tampilkan Instansi Pada Halaman Perizinan Online K/L','required'=>false,'name'=>'isbko','data'=>['T'=>'Ya','F'=>'Tidak']])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/bower_components/tinymce/tinymce.min.js')}}"></script>
<script>
tinymce.init({
    selector: "textarea",
    menubar: false,
    resize: "both",
    relative_urls: false,
    plugins: ["autoresize", "image", "code", "lists", "code","example", "link","preview","advlist"],
    indentation : '20pt',
    image_list: "{{url('gallery/image-list')}}",
    toolbar: [
        "undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright | bullist numlist | preview"
    ]});

</script>
@endsection
