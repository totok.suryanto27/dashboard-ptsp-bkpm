{{ Navigator::setActive(url('instansi')) }}
@extends('backend')
@section('title','Instansi')
@section('content')
 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
 <a href="{{url('/instansi/create')}}" class="btn btn-success">Instansi Baru</a>
  <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID</th>
         <th>Nama Instansi</th>
         <th>File SOP Indonesia</th>
         <th>Nama Instansi English</th>
         <th>File SOP  English</th>
         <th>Singkatan</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('instansi.datatables_script',['url'=>url('instansi/getData')])
<script>
var params = {};
$('.do-search').click(function() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawSuratTable('#search-result');
});
$("#search-form").hide();
$(".toggle-search").click(function() {$("#search-form").toggle();});
drawTable('#search-result');
</script>
@endsection