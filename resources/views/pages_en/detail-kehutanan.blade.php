@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Ministries and Institutions Online Licensing</a></li>
              <li class="active">Ministry of Environment and Forestry</li>
            </ol>
              <div class="nipz-content">
                <h4>Types of License :</h4>
                <ol>
                  <li>Business License for Utilization of Wood Products in Natural Forest (IUPHHKHA)</li>
                  <li>Business License for Utilization of Industrial Plant Forest Wood Products in Plantation Forest (IUPHHTKHTI)</li>
                  <li>Business license for Utilization of Ecosystem Restoration Forest Wood Products (IUPHHKRE)</li>
                  <li>Extension of License for Timber Forest Product Utilization in Natural Forest (IUPHHKHA)</li>
                  <li>Business license for Utilization of Carbon Absorption and/or Storage (UP RAP-KARBON) and/or UP PAN-KARBON) in Protected Forest</li>
                  <li>Business license for Utilization of Carbon Absoption and/or carbon storage (UP RAP-KARBON) and/or UP PAN-KARBON) in Production forest</li>
                  <li>Industry License Primary Forest above 6,000 m3/year</li>
                  <li>Expansion Permit of Business License of Timber Forest Product Primary Industry over 6,000 m3/year</li>
                  <li>Business license for Utilization of Silvo Pastura Area in Production forest (IUPK-SP)</li>
                  <li>Permit for borrow and use of forest area</li>
                  <li>Release of forest areas</li>
                  <li>Business license for Provision of Nature Tourism Facilities</li>
                  <li>License as a conservation agency</li>
                  <li>License of exploitation hunting parks</li>
                  <li>Permit for breeding loan</li>
                  <li>License of Water Use For Medium And Large Scale in Wildlife, National Parks, Nature Parks, Nature Parks and Forest Park</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_english/SOP-MINISTRY OF ENVIRONMENT AND FORESTRY.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection