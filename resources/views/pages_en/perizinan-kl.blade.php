@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li class="active">Ministries and Institutions Online Licensing</li>
            </ol>
              <div class="nipz-content">
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perdagangan') }}">
                      <img src="{{ asset('images/logokem/kemendag.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Trade</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perindustrian') }}">
                      <img src="{{ asset('images/logokem/kemenperin.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Industry</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-perhubungan') }}">
                      <img src="{{ asset('images/logokem/kemenhub.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Transportation</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-esdm') }}">
                      <img src="{{ asset('images/logokem/esdm.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Energy and Mineral Resource</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pariwisata') }}">
                      <img src="{{ asset('images/logokem/pariwisata.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Tourism</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pertanian') }}">
                      <img src="{{ asset('images/logokem/pertanian.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Agriculture</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kesehatan') }}">
                      <img src="{{ asset('images/logokem/kesehatan.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Health</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kominfo') }}">
                      <img src="{{ asset('images/logokem/kominfo.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Communication and Informatics</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-dikbud') }}">
                      <img src="{{ asset('images/logokem/kemendikbud.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Education and Culture</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kepri') }}">
                      <img src="{{ asset('images/logokem/laut.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of  Marine Affairs and Fisheries</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-humkam') }}">
                      <img src="{{ asset('images/logokem/hukum.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Law and Human Rights</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-keuangan') }}">
                      <img src="{{ asset('images/logokem/keuangan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Finance</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-ketenagakerjaan') }}">
                      <img src="{{ asset('images/logokem/ketenagakerjaan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Manpower</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pupera') }}">
                      <img src="{{ asset('images/logokem/pu.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Public Works and Public Housing</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bpn') }}">
                      <img src="{{ asset('images/logokem/bpn.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Agrarian / National Land Agency</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kehutanan') }}">
                      <img src="{{ asset('images/logokem/lingkunganhidup.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Environment and Forestry</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pertahanan') }}">
                      <img src="{{ asset('images/logokem/menhan.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Ministry of Defense</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-kepolisian') }}">
                      <img src="{{ asset('images/logokem/polisi.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">National Police of Republic of Indonesia</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bpom') }}">
                      <img src="{{ asset('images/logokem/bpom.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">National Agency of Drug and Food Control</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-bsn') }}">
                      <img src="{{ asset('images/logokem/bsn.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">National Standardization Agency</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail-pln') }}">
                      <img src="{{ asset('images/logokem/pln.jpg') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">State Electricity Company</p>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection