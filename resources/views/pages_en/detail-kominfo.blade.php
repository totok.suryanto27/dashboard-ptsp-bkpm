@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Ministries and Institutions Online Licensing</a></li>
              <li class="active">Ministry of Communication and Informatics</li>
            </ol>
              <div class="nipz-content">
                <h4>Types of License :</h4>
                <ol>
                  <li>Postal organization :
                    <ul>
                      <li>National;</li>
                      <li>Province;</li>
                      <li>Regency/city</li>
                    </ul>
                  </li>
                  <li>Verification of postal operation</li>
                  <li>Organization of telecommunication network</li>
                  <li>Organization of telecommunication service</li>
                  <li>Broadcasting organization :
                    <ul>
                      <li>Private-owned broadcasting agencies</li>
                      <li>Subscription broadcasting agencies</li>
                    </ul>
                  </li>
                  <li>Principle permit of telecommunication network</li>
                  <li>Principle permit for organizing telecommunication service</li>
                  <li>Principle permit for organizing telecommunication service for legal entities</li>
                  <li>Radio station license</li>
                  <li>Certification for telecommunication equipment and device</li>
                  <li>Testing of telecommunication equipment and devices</li>
                  <li>Registration for host of electronic system</li>
                  <li>Determination of testing agency of telecommunication equipment</li>
                  <li>Stipulation of telecommunication device testing agency</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_english/SOP-MINISTRY OF COMM AND INFORMATICS.pdf') }}" download><i class="fa fa-download"></i> Download</a> 
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection