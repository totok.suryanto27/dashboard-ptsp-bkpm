@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Ministries and Institutions Online Licensing</a></li>
              <li class="active">National Standardization Agency</li>
            </ol>
              <div class="nipz-content">
                <h4>Types of License :</h4>
                <ol>
                  <li>Arrangement for Application to Obtain Accreditation for Conformity Assessment Agency</li>
                  <li>Arrangement to Obtain Certificate of Indonesian National Standard (SNI) Product</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_english/SOP-NATIONAL STANDARDIZATION AGENCY.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection