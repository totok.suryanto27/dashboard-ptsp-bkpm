@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('page/perizinan-kl') }}">Ministries and Institutions Online Licensing</a></li>
              <li class="active">Ministry of Tourism</li>
            </ol>
              <div class="nipz-content">
                <h4>Types of License :</h4>
                <ol>
                  <li>Business license registration :
                    <ul>
                      <li>Transportation service</li>
                      <li>Travel service</li>
                      <li>Food and beverages service</li>
                      <li>Accommodation provision</li>
                      <li>Entertainment and Recreation Organization</li>
                      <li>Meeting organization, incentive travel, conference and exhibition</li>
                      <li>Tourism information service</li>
                      <li>Tourism consultant service</li>
                      <li>Water tourism</li>
                      <li>Spa business</li>
                    </ul>
                  </li>
                  <li>Registration of tourist attraction business</li>
                  <li>Tourism resort business registration</li>
                  <li>Film production permit (SIP) by foreign film/TV producers in Indonesia</li>
                  <li>Film and film technical service business license</li>
                  <li>Business license for film industry, filem circulation</li>
                  <li>Film industry business license, film archive</li>
                  <li>Film industry business license, film export</li>
                  <li>Film industry business license, film import</li>
                  <li>Recommendation related to granting of permit for shooting location</li>
                </ol>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('sop_english/SOP-MINISTRY OF TOURISM.pdf') }}" download><i class="fa fa-download"></i> Download</a>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="images/f1.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Ketenagalistrikan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="images/f2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perindustrian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="images/f3.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Pertanian</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="images/f4.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Perhubungan</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="images/f5.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Bidang Usaha Kawasan Pariwisata</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection