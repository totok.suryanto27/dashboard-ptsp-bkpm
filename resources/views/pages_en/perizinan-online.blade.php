@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li class="active">Integrated Online Licensing OSS Center</li>
            </ol>
              <div class="nipz-content">
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f1') }}">
                      <img src="{{ asset('images/listrik2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Electricity Business Sector</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f2') }}">
                      <img src="{{ asset('images/industri2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Industry Business Sector</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f3') }}">
                      <img src="{{ asset('images/pertanian2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Agriculture Business Sector</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f4') }}">
                      <img src="{{ asset('images/perhubungan2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Transportation Business Sector</p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/f5') }}">
                      <img src="{{ asset('images/wisata2.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Tourism Region Business Sector</p>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <div class="thumbnail">
                    <a href="{{ url('page/detail') }}">
                      <img src="images/contoh.png') }}" class="img-responsive nipz-content"/>
                      <div class="caption">
                        <p class="text-center">Direktorat<br>Jenderal Pajak</p>
                      </div>
                    </a>
                  </div>
                </div> -->
            </div>
          </div>
@endsection