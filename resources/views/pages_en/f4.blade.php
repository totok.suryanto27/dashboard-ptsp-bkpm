@extends('app')
@section('title','Home - Dashboard BKPM')
@section('content')
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('page/perizinan-online') }}">Integrated Online Licensing OSS Center</a></li>
              <li class="active">Transportation Business Sector</li>
            </ol>
              <div class="nipz-content">
                <img class="img-responsive" src="{{ asset('images/f4.png') }}"/>
                <div class="mb-10"></div>
                <a class="btn btn-primary" href="{{ asset('ptsp/perhubungan.pdf') }}" download><i class="fa fa-download"></i> Download</a>
            </div>
          </div>
@endsection