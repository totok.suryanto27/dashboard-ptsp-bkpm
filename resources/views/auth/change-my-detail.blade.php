{{ Navigator::setActive(url('change-my-detail/')) }}
@extends('backend')

@section('title','Ubah Profil')

@section('content')
<?php $model = Auth::user(); ?>
<form method="POST" action="{{ Request::url() }}" accept-charset="UTF-8" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Maaf!</strong> Terdapat error dari masukan Anda.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <div class="row">
    <div class="col-md-8">
      <div class="form-horizontal">
      <div class="form-body">
        @include('form.text',['label'=>'NIP','required'=>false,'name'=>'nip','value'=>$model->nip])
        @include('form.text',['label'=>'Nama','required'=>false,'name'=>'nama_user','value'=>$model->nama_user])
        @include('form.text',['label'=>'Jabatan','required'=>false,'name'=>'jabatan','value'=>$model->jabatan])
        @include('form.text',['label'=>'Email','required'=>false,'name'=>'email','value'=>$model->email])
        @include('form.text',['label'=>'No. Telp','required'=>false,'name'=>'telp','value'=>$model->telp])
        <br><br><br>
        <span>Ubah Foto (Isi hanya jika ingin mengubah foto, ukuran file maks. 1 MB)</span>
        <br>
        <img src="{{URL::asset('user/photos/'.$model->getProfilePicture()) }}" class="img-max200">
        <br>
        @include('form.file',['label'=>'Foto Baru','required'=>false,'name'=>'foto'])
        <br><br><br>
        <h3>Ubah Password (Isi hanya jika ingin mengubah password)</h3>
        @include('form.password',['label'=>'Password Baru','required'=>false,'name'=>'password'])
        @include('form.password',['label'=>'Password (Ulangi)','required'=>false,'name'=>'password_ulangi'])
      </div>
      </div>
  <div class="form">
  <div class="form-actions">
  <div class="row">
    <div class="col-sm-offset-4 col-sm-2">
    <input class="btn btn-primary" type="submit" value="Simpan">
    </div>
  </div>
  </div>
        </div>
  </div>
</form>
@endsection

@section('scripts')
@endsection