@extends('backend')
@section('title', 'Rekap Pengajuan' )
@section('content')
<div class="row">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? '' : 'active'; ?>"><a href="#Kementrian" aria-controls="Kementrian" role="tab" data-toggle="tab">Instansi</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="Bidang">
               
                <div class="row">
                    <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian">Tahun</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-tahun" id="tahunan_bidang">
                                    <?php 
                                    $increment = 0;
                                     for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                        ?>
                                            <option value="<?php echo $i; ?>" 
                                                <?php 
                                                        if($i == $current_year){
                                                            echo "selected";
                                                         }
                                                 ?>
                                            >
                                                <?php echo $i; ?>
                                            </option>    
                                        <?php
                                        $increment++;
                                    } ?>
                                    

                                </select>
                                </div>
                     </div>

                </div>

                <div class="row">
                    <div class="col-md-6" id="bidang_charts">
                        
                    </div>
                    <div class="col-md-6" id="bidang_pie_charts">
                        
                    </div>
                </div>
                    <div class="row" style="margin:5px;">
                  <!--  <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_bln">
                                            <thead>
                                                <td>Nama Bidang</td>
                                                <td>Total Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                            </thead>
                                            <tbody id="tabel-bdg">
                                               
                                            </tbody>
                                        </table>-->
                </div>

            </div>
            <div role="tabpanel" class="tab-pane active" id="Kementrian">
                <div class="row">
                    <div class="col-md-5">
                                <div class="form-group">
                                    <label for="kementrian_bln">Instansi</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian_bln" id="kementrian_bln">
                                    @if (!getInstansi())
                                    <option value="-">Semua Instansi</option>
                                    @endif
                                    <?php foreach($list_kementrian as $val) : ?>
                                    
                                    <?php if(isset($_GET['id_m_direktorat'])) { ?>
                                        <option <?php echo ($_GET['id_m_direktorat'] == $val->id_m_instansi) ? 'selected' : '' ?> value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } else { ?>
                                    <option value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } ?>
                                    <?php endforeach; ?>

                                </select>
                                </div>
                    </div>

                    <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian">Tahun</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-tahunanbln" id="tahunan_bln">
                                    <?php 
                                    $increment = 0;
                                    for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                        ?>
                                            <option value="<?php echo $i; ?>" 
                                                <?php 
                                                        if($i == $current_year){
                                                            echo "selected";
                                                        }
                                                 ?>
                                            >
                                                <?php echo $i; ?>
                                            </option>    
                                        <?php
                                        $increment++;
                                    } ?>
                                    

                                </select>
                                </div>
                     </div>

                </div>

                <div class="row">
                    <div class="col-md-6" id="bar_charts">
                        
                    </div>
                    <div class="col-md-6" id="pie_charts">
                        
                    </div>
                </div>

                <div class="row" style="margin:5px;">
                    <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_bln">
                                            <thead>
                                                <td>Bulan</td>
                                                <td>Total Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                            </thead>
                                            <tbody id="tabel-bulanan">
                                               
                                            </tbody>
                                        </table>
                </div>

            </div>
            
        </div>
    </div>

</div>
@stop 
@section('scripts')
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="{{ asset('rekap_pengajuan/rekap-pengajuan-v2.js') }}"></script>
<!--<script src="{{ asset('rekap_pengajuan/rekap-pengajuan-bidang.js') }}"></script>-->
@stop