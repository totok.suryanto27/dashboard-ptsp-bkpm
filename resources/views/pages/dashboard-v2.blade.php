@extends('backend')
@section('title', 'Dashboard' )
@section('content')
<div class="row">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? 'active' : '' ?>"><a href="#Bidang" aria-controls="Bidang" role="tab" data-toggle="tab">Bidang</a></li>
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? '' : 'active'; ?>"><a href="#Kementrian" aria-controls="Kementrian" role="tab" data-toggle="tab">Instansi</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="Bidang">
               
                <div class="row" style="padding:10px;" align="center">
                    <?php 
                    foreach ($list_instansi as $val): ?>
                        <div class="col-md-4" 
                        id="Bidang_<?php echo $val->id_m_flow_izin_terintegrasi; ?>">
                        </div>    
                    <?php endforeach ?>
                </div>
 
            </div>
            <div role="tabpanel" class="tab-pane active" id="Kementrian">
                
                        <div class="row">
                            <!-- select kementrian -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian" id="kementrian">
                                    @if (!getInstansi())
                                    <option value="-">Semua Instansi</option>
                                    @endif
                                    <?php foreach($list_kementrian as $val) : ?>
                                    
                                    <?php if(isset($_GET['id_m_instansi'])) { ?>
                                        <option <?php echo ($_GET['id_m_direktorat'] == $val->id_m_instansi) ? 'selected' : '' ?> value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } else { ?>
                                    <option value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } ?> 
                                    <?php endforeach; ?>

                                </select>
                                </div>
                            </div>
                            <!-- end select kementrian -->
                            <div class="col-md-2">
                                    <select name="filter-dashboard-type-kementrian"
                                    style="width:100%;" id="filter-type-kementrian" >
                                        <option value="1" selected>Harian</option>
                                        <option value="2">Bulanan</option>
                                        <option value="3" >Tahunan</option>
                                        <option value="4">Periodik</option>
                                        
                                        </select>
                            </div>
                            <div class="col-md-6">

                                 <!-- Filter tipe bulanan -->
                                 <div class="row" id="bulanan_kementrian" style="display:none">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="m_kementrian_bulanan" id="m_kementrian_bulanan">
                                            <?php foreach($list_bulan as $val) : ?>
                                    
                                        <option <?php echo ($current_month == $val['no']) ? 'selected' : '' ?> value="<?= $val['no'] ?>"><?= $val['bulan'] ?></option>
                                   
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_kementrian_bulan" id="tahun_kementrian_bulan">
                                             <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="tahunan_kementrian" style="display:none">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_kementrian" id="tahun_kementrian">
                                            <option value="2015" >2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="periodik_kementrian" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_kemen_periodik_from" name="tanggal1" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_kemen_periodik_to" name="tanggal2" 
                                            placeholder="hingga tanggal"
                                            value="<?php
                                             echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_periodik" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <div class="row" id="harian_kementrian" style="display:block;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            id="dp_kemen_daily" name="tanggal3" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_harian" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <!-- end tipe bulanan -->
                            </div>
                            <br style="clear:both;">                                    
                            <hr>


                        </div>

                        <!-- panel result -->
                            <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                <span id="judul_kementrian">Dashboard Seluruh Instansi</span>
                                    

                                </div>
                                <div class="panel-body">
                                        
                                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="content" style="padding:0">
                                            <div class="a" style="color:#FFF; box-sizing:border-box; padding:10px 0; text-align:center;" >
                                                <div style="font-size:30px"><i class="fa fa-check-circle"></i><br>
                                                    Izin Masuk<br>
                                                    <span id='total_pengajuan'>0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php /*
                                    <div class="col-md-8">
                                        <div class="row table-bidang dashboard-info" style="padding-left:20px;">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <i class="fa fa-bell"></i>
                                                </div>
                                                <div class="col-md-5">
                                                    <span>Finish On Time</span>
                                                </div>
                                                <div class="col-md-1">
                                                    <div id="total_on_time_bidang">0</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <i class="fa fa-exclamation-circle"></i>
                                                </div>
                                                <div class="col-md-5">
                                                    <span>Finish Late</span>
                                                </div>
                                                <div class="col-md-1">
                                                    <div id="total_terlambat_bidang">0 </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <i class="fa fa-list-alt"></i>
                                                </div>
                                                <div class="col-md-5">
                                                    <span>Total Pengajuan</span>
                                                </div>
                                                <div class="col-md-1">
                                                    <div id="total_selesai_bidang">
                                                    0
                                                     </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    */ ?>
                                </div>
                                <br>
                                <br>
                                <div class="row" id="container-on-proses">
                                    <div class="col-md-4">
                                        <div class="content" style="padding:0">
                                            <div class="b" style="color:#FFF; box-sizing:border-box; padding:10px 0; text-align:center;" >
                                                <div style="font-size:30px"><i class="fa fa-gear fa-spin"></i><br>
                                                    Izin On Proses<br>
                                                    <span id="total_sum_proses" class="total_sum_proses">
                                                        0
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row table-bidang dashboard-info" style="padding-left:20px;">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <i class="fa fa-gear fa-spin"></i>
                                                </div>
                                                <div class="col-md-5">
                                                    <span>On Process</span>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="total_izin_on_proses_bidang" id="total_izin_on_proses_bidang">
                                                        0
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <i class="fa fa-exclamation-circle"></i>
                                                </div>
                                                <div class="col-md-5">
                                                    <span>On Process Late</span>
                                                </div>
                                                <div class="col-md-1">
                                                    <div id="total_izin_proses_delay_bidang">
                                                    0
                                                     </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                                
                                </div>
                            </div>
                        </div>
                        <!-- end panel result -->

            </div>
            
        </div>
    </div>

</div>
@stop
@section('scripts')
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/modules/drilldown.js"></script>
<script src="{{ asset('dashboard/dashboard-kementrian.js') }}"></script>
<script src="{{ asset('dashboard/dashboard-drilldown.js') }}"></script>
<script type="text/javascript">
    var global_year = "<?php echo $date_current['current_year']; ?>";
    var global_month = "<?php echo $date_current['current_month']; ?>";
</script>
<style type="text/css">
.highcharts-series-group{
    width: 300px;
    height: 300px;
}
</style>
@stop