@extends('backend')
@section('title', 'Kualitas Layanan PTSP' )
@section('content')
<div class="row">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <!---li role="presentation" class=""><a href="#Bidang" aria-controls="Bidang" role="tab" data-toggle="tab">Bidang</a></li-->
            <li id="pressKementrian" role="presentation" class="active"><a href="#Kementrian" aria-controls="Kementrian" role="tab" data-toggle="tab">Instansi</a></li>
            <li id="pressIzin" role="presentation" class=""><a href="#Izin" aria-controls="Izin" role="tab" data-toggle="tab">Izin</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!--div role="tabpanel" class="tab-pane fade" id="Bidang"> 
                <div class="row">
                        <div class="col-md-2">
                                    <select name="filter-dashboard-type-bidang"
                                    style="width:100%;" id="filter-type-bidang" > 
                                        <option value="2" >Bulanan</option>
                                        <option value="3" selected>Tahunan</option> 
                                        </select>
                            </div>
                            <div class="col-md-6">
 
                                 <div class="row" id="bulanan_bidang" style="display:none">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="m_bidang_bulanan" id="m_bidang_bulanan">
                                            <?php foreach($list_bulan as $val) : ?>
                                    
                                        <option <?php echo ($current_month == $val['no']) ? 'selected' : '' ?> value="<?= $val['no'] ?>"><?= $val['bulan'] ?></option>
                                   
                                    <?php endforeach; ?>

                                        </select>
                                    </div>
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_bidang_bulan" id="tahun_bidang_bulan">
                                             <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="tahunan_bidang" style="display:block">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_bidang" id="tahun_bidang">
                                            <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="periodik_bidang" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_bidang_periodik_from" name="tanggal1" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_bidang_periodik_to" name="tanggal2" 
                                            placeholder="hingga tanggal"
                                            value="<?php
                                             echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2"> 
                                    </div>
                                 </div>

                                 <div class="row" id="harian_bidang" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            id="dp_bidang_daily" name="tanggal3" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_harian" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div> >
                                    </div>
                                 </div>
 
                            </div>
                </div>

                <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                <span id="judul_bidang">Komparasi Layanan Seluruh Bidang</span>
                                    

                                </div>
                                <div class="panel-body">
                                        
                                        <div class="row" >
                                            <div class="col-md-12" id="grafik_bidang"></div>
                                        </div>

                                        <div class="row">
                                            <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_bln">
                                            <thead>
                                                <td>Nama Bidang</td>
                                                <td>Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                                <td>Performansi</td>
                                            </thead>
                                            <tbody id="tabel-bidang">
                                               
                                            </tbody>
                                        </table>

                                            
                                        </div>
                                 </div>
                                
                            </div>
                        </div>

            </div-->
            <div role="tabpanel" class="tab-pane active" id="Kementrian">
                
                        <div class="row">
                            <!-- select kementrian -->
                            <div class="col-md-4" style="display:none;">
                                <div class="form-group" style="display:none;">
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian" id="kementrian">
                                    @if (!getInstansi())
                                    <option value="-">Semua PTSP</option>
                                    @endif
                                    <?php foreach($list_ptsp as $val) : ?>
                                    
                                    <?php if(isset($_GET['id_ptsp'])) { ?>
                                        <option <?php echo ($_GET['id_m_direktorat'] == $val->id_ptsp) ? 'selected' : '' ?> value="<?= $val->id_ptsp ?>"><?= $val->singkatan ?></option>
                                    <?php } else { ?>
                                    <option value="<?= $val->id_ptsp ?>"><?= $val->singkatan ?></option>
                                    <?php } ?> 
                                    <?php endforeach; ?>

                                </select>
                                </div>
                            </div>
                            <!-- end select kementrian -->
                            <div class="col-md-2">
                                    <select name="filter-dashboard-type-kementrian"
                                    style="width:100%;" id="filter-type-kementrian" >
<!--
                                        <option value="1">Harian</option>
-->
                                        <option value="2" >Bulanan</option>
                                        <option value="3" selected>Tahunan</option>
                                        <option value="4">Periodik</option>
                                        
                                        </select>
                            </div>
                            <div class="col-md-6">

                                 <!-- Filter tipe bulanan -->
                                 <div class="row" id="bulanan_kementrian" style="display:none">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="m_kementrian_bulanan" id="m_kementrian_bulanan">
                                             <?php foreach($list_bulan as $val) : ?>
                                    
                                        <option <?php echo ($current_month == $val['no']) ? 'selected' : '' ?> value="<?= $val['no'] ?>"><?= $val['bulan'] ?></option>
                                   
                                    <?php endforeach; ?>

                                        </select>
                                    </div>
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_kementrian_bulan" id="tahun_kementrian_bulan">
                                             <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="tahunan_kementrian" style="display:block">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_kementrian" id="tahun_kementrian">
                                            <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="periodik_kementrian" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_kemen_periodik_from" name="tanggal1" placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            id="dp_kemen_periodik_to" name="tanggal2" 
                                            placeholder="hingga tanggal"
                                            value="<?php
                                             echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_periodik" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <div class="row" id="harian_kementrian" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            id="dp_kemen_daily" name="tanggal3" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_harian" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <!-- end tipe bulanan -->
                            </div>
                            <br style="clear:both;">                                    
                            <hr>


                        </div>

                        <!-- panel result -->
                            <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                <span id="judul_kementrian">Komparasi Persentase Ketepatan Waktu Seluruh PTSP </span>
                                    

                                </div>
                                <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12" id="container-speed">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_kemen">
                                            <thead>
                                                <td>Nama Kementerian</td>
                                                <td>Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                                <td>Performansi</td>
                                            </thead>
                                            <tbody id="tabel-kemen">
                                               
                                            </tbody>
                                        </table>

                                            
                                        </div>
                                   </div>
                                
                            </div>
                        </div>
                        <!-- end panel result -->

            </div>
            <div role="tabpanel" class="tab-pane" id="Izin">
               
                        <div class="row">
                            <!-- select izin -->
                            <!--div class="col-md-4" style="display:none;">
                                <div class="form-group" style="display:none;">
                                    <select class="form-control" style="padding:0; border:none;" name="nama-izin" id="izin">
										<option value="-">Semua Izin</option>
										<?php foreach($list_izin as $val) : ?>
										
										<?php if(isset($_GET['id_m_jenis_perizinan'])) { ?>
											<option <?php echo ($_GET['id_m_jenis_perizinan'] == $val->id_m_jenis_perizinan) ? 'selected' : '' ?> value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
										<?php } else { ?>
										<option value="<?= $val->id_m_jenis_perizinan ?>"><?= $val->nama_jenis_perizinan ?></option>
										<?php } ?> 
										<?php endforeach; ?> 
									</select>
                                </div>
                            </div-->
                            <!-- end select izin -->
                            <!-- select izin -->
                            <div class="form-group col-md-4"> 
								<select class="form-control" style="padding:0; border:none;" name="nama-instansi" id="instansi">
									<option value="-">Semua PTSP</option>
									<?php foreach($list_ptsp as $val) : ?> 
										<option value="<?= $val->id_ptsp ?>"><?= $val->singkatan ?></option> 
									<?php endforeach; ?> 
								</select> 
                            </div>
                            <!-- end select izin -->
                            <div class="col-md-2">
                                    <select name="filter-dashboard-type-izin"
                                    style="width:100%;" id="filter-type-izin" >
                                        <!--<option value="1">Harian</option>-->
                                        <option value="2" >Bulanan</option>
                                        <option value="3" selected>Tahunan</option>
                                        <<option value="4">Periodik</option>>
                                        
                                        </select>
                            </div>
                            <div class="col-md-6">

                                 <!-- Filter tipe bulanan -->
                                 <div class="row" id="bulanan_izin" style="display:none">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="m_izin_bulanan" id="m_izin_bulanan">
                                             <?php foreach($list_bulan as $val) : ?>
                                    
                                        <option <?php echo ($current_month == $val['no']) ? 'selected' : '' ?> value="<?= $val['no'] ?>"><?= $val['bulan'] ?></option>
                                   
                                    <?php endforeach; ?>

                                        </select>
                                    </div>
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_izin_bulan" id="tahun_izin_bulan">
                                             <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div>

                                 <div class="row" id="tahunan_izin" style="display:block">
                                    <div class="form-group col-md-6" >
                                        <select class="form-control" style="padding:0; border:none;" name="tahun_izin" id="tahun_izin">
                                            <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                        </select>
                                    </div>
                                 </div> 
                                 <div class="row" id="periodik_izin" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
											<input type="text" style="height:26px;" 
											id="dp_izi_periodik_from" name="tanggal1" 
											placeholder="dari tanggal"
											value="<?php echo $date_current['current_day']."-".
											$date_current['current_month']."-".
											$date_current['current_year']
											; ?>" 
											class="form-control" />
										</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
											<input type="text" style="height:26px;" 
											id="dp_izi_periodik_to" name="tanggal2" 
											placeholder="hingga tanggal"
											value="<?php
											 echo $date_current['current_day']."-".
											$date_current['current_month']."-".
											$date_current['current_year']
											; ?>" 
											class="form-control" />
										</div>
                                    </div>
                                    <div class="col-md-2">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_periodik" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <div class="row" id="harian_izin" style="display:none;">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" style="height:26px;" 
                                            placeholder="dari tanggal"
                                            value="<?php echo $date_current['current_day']."-".
                                            $date_current['current_month']."-".
                                            $date_current['current_year']
                                            ; ?>" 
                                            id="dp_kemen_daily" name="tanggal3" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                       <!-- <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button id="btn_kemen_harian" 
                                            style="height:26px;padding: 0px 9px;"
                                            class="btn btn-sm btn-primary">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>-->
                                    </div>
                                 </div>

                                 <!-- end tipe bulanan -->
                            </div>
                            <br style="clear:both;">                                    
                            <hr>


                        </div>

                        <!-- panel result -->
                            <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                <span id="judul_izin">Komparasi Persentase Ketepatan Waktu Seluruh Izin</span>
                                    

                                </div>
                                <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12" id="container-speed-izin">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_kemen">
                                            <thead>
                                                <td>Nama Izin</td>
                                                <td>Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                                <td>Performansi</td>
                                            </thead>
                                            <tbody id="tabel-izi">
                                               
                                            </tbody>
                                        </table>

                                            
                                        </div>
                      			 </div>
                                
                            </div>
                        </div>
                        <!-- end panel result -->

            </div>
            
        </div>
    </div>

</div>
@stop
@section('scripts')
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="{{ asset('komparasi_layanan/komparasi-layanan-kementrian-ptsp.js') }}"></script>
<script src="{{ asset('komparasi_layanan/komparasi-layanan-izin-ptsp.js') }}"></script> 
<script type="text/javascript">
    var global_year = "<?php echo $date_current['current_year']; ?>";
    var global_month = "<?php echo $date_current['current_month']; ?>";
</script>
@stop
