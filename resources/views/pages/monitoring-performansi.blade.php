@extends('backend')
@section('title', 'Monitoring Peformansi')
@section('content')
<div class="row">
               <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="#Bidang" aria-controls="Bidang" role="tab" data-toggle="tab">Bulanan </a></li>
                <li role="presentation" class="active"><a href="#Kementrian" aria-controls="Kementrian" role="tab" data-toggle="tab">Tahunan</a></li>
                
              </ul> 
 
              <!-- tab bidang --> 
 
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in" id="Bidang">
                        <div class="row">
  
                              
                            <div class="col-md-4">
                                <div class="form-group">
                                     <label for="tanggal1">Bulan </label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-bulan" id="bulan">
                                    <option value="">Pilih Bulan</option>
                                    <?php foreach($list_bulan as $val) : ?>
                                    
                                        <option <?php echo ($current_month == $val['no']) ? 'selected' : '' ?> value="<?= $val['no'] ?>"><?= $val['bulan'] ?></option>
                                   
                                    <?php endforeach; ?>

                                </select>
                                </div>
                
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian">Tahun</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-tahunanbln" id="tahunan_bln">
                                   <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>                                    

                                </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian_bln">Instansi</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian_bln" id="kementrian_bln">
                                    @if (!getInstansi())
                                    <option value="-">Semua Instansi</option>
                                    @endif
                                    <?php foreach($list_kementrian as $val) : ?>
                                    
                                    <?php if(isset($_GET['id_m_direktorat'])) { ?>
                                        <option <?php echo ($_GET['id_m_direktorat'] == $val->id_m_instansi) ? 'selected' : '' ?> value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } else { ?>
                                    <option value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } ?>
                                    <?php endforeach; ?>

                                </select>
                                </div>
                            </div>

                            
                            <br style="clear:both;">
                            <hr>
                        </div>

                        <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                <span id="judul">Monitoring Performansi Bulan 
                                <?php echo $current_month_name; ?>
                                <?php echo $current_year; ?>
                                </span>

                                </div>
                                <div class="panel-body">
                                <div  align="center" >
                                    <div class="row">
                                                <div id="chart_bulanan"></div>
                                        </div>

                                </div>
                                    <hr>
                                    <div class="row" id="row_tabel_bulanan">
                                        <table class="table table-striped table-bordered table-hover" 
                                        id="tbl_bln">
                                            <thead>
                                                <td>Nama Instansi</td>
                                                <td>Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                                <td>Persentase On Time</td>
                                                <td>Persentase Terlambat</td>

                                            </thead>
                                            <tbody id="tabel-bulanan">
                                               
                                            </tbody>
                                        </table>
                                        <br>
                                        
                                    </div>
                                    <div class="row">
                                        <table
                                        class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <td>Total Pengajuan</td>
                                                <td>Total Selesai</td>
                                                <td>Total Selesai Terlambat</td>
                                                <td>Total Selesai On Time</td>
                                                
                                            </thead>
                                            <tbody id="resume_bulan">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                   
                                </div>
                            </div>
                        </div> 
                </div>

                <div role="tabpanel" class="tab-pane fade in active" id="Kementrian">
                        <div class="row">
                   

                            <!-- select kementrian -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian">Tahun</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-tahunan" id="tahunan">
                                    <?php 
                                                $increment = 0;
                                                for ($i=$current_year-5; $i < $current_year+5; $i++) { 
                                                    ?>
                                                        <option value="<?php echo $i; ?>" 
                                                            <?php 
                                                                    if($i == $current_year){
                                                                        echo "selected";
                                                                    }
                                                             ?>
                                                        >
                                                            <?php echo $i; ?>
                                                        </option>    
                                                    <?php
                                                    $increment++;
                                                } ?>
                                    

                                </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kementrian">Instansi</label>
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian" id="kementrian">
                                    @if (!getInstansi())
                                    <option value="-">Semua Instansi</option>
                                    @endif
                                    <?php foreach($list_kementrian as $val) : ?>
                                    
                                    <?php if(isset($_GET['id_m_direktorat'])) { ?>
                                        <option <?php echo ($_GET['id_m_direktorat'] == $val->id_m_instansi) ? 'selected' : '' ?> value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } else { ?>
                                    <option value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option>
                                    <?php } ?>
                                    <?php endforeach; ?>

                                </select>
                                </div>
                            </div>
                            <!-- end select kementrian -->

                            <br style="clear:both;">                                    
                            <hr>


                        </div>

                        <!-- panel result -->
                            <div class="row" style="margin:0px 1px 0px 1px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading " style="overflow:hidden;">
                                    <span id="judul_tahun">
                                    Monitoring Performansi Tahun <?php echo $current_year; ?>
                                    </span>                               
                                </div>
                                <div class="panel-body">
                                    <div  align="center" >
                                        <div class="row">
                                                <div id="chart_tahunan"></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row" id="row_tabel_tahunan">
                                        <table class="table table-striped table-bordered table-hover"
                                        id="tbl_thn">
                                            <thead>
                                                <td>Nama Instansi</td>
                                                <td>Pengajuan</td>
                                                <td>Selesai</td>
                                                <td>Selesai Terlambat</td>
                                                <td>Selesai On Time</td>
                                                <td>Persentase On Time</td>
                                                <td>Persentase Terlambat</td>
                                            </thead>
                                            <tbody id="tabel-tahunan">
                                               
                                            </tbody>
                                        </table>
                                        <br>
                                        

                                    </div>

                                    <div class="row">
                                        <table
                                        class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <td>Total Pengajuan</td>
                                                <td>Total Selesai</td>
                                                <td>Total Selesai Terlambat</td>
                                                <td>Total Selesai On Time</td>
                                            </thead>
                                            <tbody id="resume_tahun">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                        <!-- end panel result -->
                </div><!-- Tab kementrian-->
              </div>

</div>

@stop
@section('scripts')
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script type="text/javascript">
    var global_tahunan = '<?php echo $current_year; ?>';
	var base_url = "{{url()}}";  
</script>
<script src="{{ asset('monitoring_performansi/monitoring_performansi_v2.js') }}"></script>
<style type="text/css">
    .hilang{
        display: none;
    }
</style>
@stop