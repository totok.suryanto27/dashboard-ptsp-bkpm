@extends('backend')
@section('title', 'Monitoring Jumlah Pengajuan')
@section('content')
<div class="row">
	<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? 'active' : '' ?>"><a href="#Bulanan" aria-controls="Bidang" role="tab" data-toggle="tab">Bulanan</a></li>
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? '' : 'active'; ?>"><a href="#Tahunan" aria-controls="Kementrian" role="tab" data-toggle="tab">Tahunan</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in" id="Bulanan">
            	<div class="row">
            		<div class="col-md-12" id="grafik_bulanan">
            		
            		</div>
            	</div>
            </div>
            <div role="tabpanel" class="tab-pane fade in active" id="Tahunan">
            	<div class="row">
	            	<div class="col-md-12" id="grafik_tahunan">
	            		
	            	</div>
            	</div>
            </div>
        </div>
</div>
@stop
@section('scripts')
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script type="text/javascript">
	var global_year = "<?php echo $date_current['current_year']; ?>";
	var global_month = "<?php echo $date_current['current_month']; ?>";
</script>
<script src="{{ asset('monitoring_pengajuan/jumlah-pengajuan.js') }}"></script>


@stop