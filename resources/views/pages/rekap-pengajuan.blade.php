@extends('backend')
@section('title', 'Rekap Pengajuan' )
@section('content')
<div class="row">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? 'active' : '' ?>"><a href="#Bidang" aria-controls="Bidang" role="tab" data-toggle="tab">Bidang</a></li>
            <li role="presentation" class="<?php echo (isset($_GET['tahunbidang'])) ? '' : 'active'; ?>"><a href="#Kementrian" aria-controls="Kementrian" role="tab" data-toggle="tab">Kementerian</a></li>
            
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade <?php echo (isset($_GET['tahunbidang'])) ? 'in active' : '' ?>" id="Bidang">
                <div class="row"> 
                    {{-- <div style="position:absolute; right:0; top:0;">
                        <button data-openfilter="#filter-bidang" class="btn btn-primary filter"><i class="fa fa-filter"></i></button>
                    </div> --}}
                    <div class="col-md-4">
                        
                        <div class="row">
                            <div class="col-md-5">
                                
                                <select name="filter-tahun" id="filter-tahun-bidang" class="form-control" style="padding:0; border:none;">
                                    <option value="">pilih tahun</option>
                                    <?php foreach($years as $val) : ?>
                                    <option <?php echo (isset($_GET['tahunbidang']) && !empty($_GET['tahunbidang']) && $_GET['tahunbidang'] == $val ) ? 'selected' : '' ?> value="<?php echo $val ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                                    
                                </select>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                    <div id="filter-bidang" style="display:none">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal1">Dari Tanggal</label>
                                <input type="text" id="dp1" name="tanggal1" class="form-control" />
                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal2">Hingga Tanggal</label>
                                <input type="text" id="dp2" name="tanggal2" class="form-control" />
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="progress" id="container-prg-grf-user" style="margin-left:10px; margin-right:10px;">
                        <div id="prg-grf-user" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            {{-- <span class="sr-only">45% Complete</span> --}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="grf-rekap-pengajuan"></div>
                    </div>
                    <div class="col-md-6">
                        <div style="width:100%; height:100%">
                            <div id="komparasi">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered table-striped" id="tabel-bidang">
                            <thead>
                                <tr>
                                    <th rowspan="2" >Tahun</th>
                                    <th rowspan="2" >Bidang</th>
                                    <th colspan="3" style="text-align:center;">Pengajuan</th>
                                </tr>
                                <tr>
                                    
                                    <th>Total</th>
                                    <th>Selesai</th>
                                    <th>On-Process</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php foreach ($bulan2 as $val) : ?>
                                
                                
                                <tr>
                                    <td><?php echo $val->tahun ?></td>
                                    <td><?php echo $val->nama_bulan ?></td>
                                    <td><?php echo $val->total ?></td>
                                    <td><?php echo $val->selesai ?></td>
                                    <td><?php echo $val->on_time ?></td>
                                </tr>
                                <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade <?php echo (isset($_GET['tahunbidang'])) ? '' : 'in active'; ?>" id="Kementrian">
                <div class="row">
                    {{-- <div style="position:absolute; right:0; top:0;">
                        <button data-openfilter="#filter-kementrian" class="btn btn-primary filter"><i class="fa fa-filter"></i></button>
                    </div> --}}
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <select class="form-control" style="padding:0; border:none;" name="nama-kementrian" id="filter-kementrian">
                                        <option value="" selected>--Pilih Kementerian--</option>
                                        <?php foreach($list_kementrian as $val) : ?>
                                        <option <?php echo (isset($_GET['kementrian']) && !empty($_GET['kementrian']) && $_GET['kementrian'] == $val->nama_instansi) ? 'selected' : '' ?> value="<?= $val->nama_instansi ?>"><?= $val->nama_instansi ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <select name="filter-tahun" id="filter-tahun-kementrian" class="form-control" style="padding:0; border:none;">
                                    <option value="">pilih tahun</option>
                                    <?php foreach($years as $val) : ?>
                                    <option <?php echo (isset($_GET['tahun']) && !empty($_GET['tahun']) && $_GET['tahun'] == $val) ? 'selected' : '' ?> value="<?php echo $val ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                                    
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div id="filter-kementrian" style="display:none">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal3">Dari Tanggal</label>
                                <input type="text" id="dp3" name="tanggal3" class="form-control" />
                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal4">Hingga Tanggal</label>
                                <input type="text" id="dp4" name="tanggal4" class="form-control" />
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="progress" id="container-prg-grf-user" style="margin-left:10px; margin-right:10px;">
                        <div id="prg-grf-user" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            {{-- <span class="sr-only">45% Complete</span> --}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="grf-rekap-pengajuan-kementrian"></div>
                    </div>
                    <div class="col-md-6">
                        <div style="width:100%; height:100%">
                            <div id="komparasi-kementrian">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered table-striped" id="table-kementrian">
                            <thead>
                                <tr>
                                    <th rowspan="2" >Tahun</th>
                                    <th rowspan="2" >Bulan</th>
                                    <th colspan="3" style="text-align:center;">Pengajuan</th>
                                </tr>
                                <tr>
                                    
                                    <th>Total</th>
                                    <th>Selesai</th>
                                    <th>On-Process</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php foreach ($bulan as $val) : ?>
                                
                                
                                <tr>
                                    <td><?php echo $val->tahun ?></td>
                                    <td><?php echo $val->nama_bulan ?></td>
                                    <td><?php echo $val->total ?></td>
                                    <td><?php echo $val->selesai ?></td>
                                    <td><?php echo $val->on_time ?></td>
                                </tr>
                                <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    {{-- <div class="col-md-12 content">
        <div class="nipz-content">
            <div id="grf-rekap-pengajuan" style="width:100%; height:100%"></div>
        </div>
    </div> --}}
</div>
@stop
@section('scripts')
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="{{ asset('rekap_pengajuan/rekap-pengajuan.js') }}"></script>
@stop