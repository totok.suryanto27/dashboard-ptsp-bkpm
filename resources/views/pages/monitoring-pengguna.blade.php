@extends('backend')
@section('title', 'Monitoring Pengguna')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
                <div id="grf-aktivitas-pengguna"></div>
        </div>
    </div> 
    <div class="col-md-12">
        <?php $time = date('d-m-Y', strtotime(\Carbon\Carbon::now())) ?>
        <table class="table table-hover table-striped table-bordered" id="tabel-pengguna">
            
            <thead>
                <tr>
                    <th>No</th>
                    <th>ID User</th>
                    <th>Waktu Aktif</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $i  =1;
            foreach ($list_durasi as $data) {
                ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><a href="monitoring-pengguna/detail?user=<?php echo $data['nama'] ?>"><?php echo $data['waktu_aktif'] ?></a></td>
                    </tr>
                <?php 
                $i++;
            } ?>
                
            </tbody>
        </table>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript">
var infoUserActivity = "<?php echo $time ?>";
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="{{ asset('monitoring_pengguna/monitoring-pengguna.js') }}"></script>
@stop