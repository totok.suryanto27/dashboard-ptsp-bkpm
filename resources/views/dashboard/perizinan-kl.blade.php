@extends('app')
@section('title',trans('pages.pokl').' - Dashboard BKPM')
@section('content')
  <div class="col-md-9">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">{{ trans('pages.home') }}</a></li>
      <li class="active">{{ trans('pages.pokl') }}</li>
    </ol>
    <div class="kl-content">
@foreach ($instansi as $count => $i)
      <div class="col-md-3">
        <div class="thumbnail">
          <a href="javascript:;" data-target="#perizinan-kl-list" onclick="loadModal({{$i->id_m_instansi}},'{{ $i->{trans('pages.var-nama-instansi')} }}')">
            <img src="{{ asset('images/logokem/'.$i->filename_logo) }}" class="img-responsive nipz-content"/>
            <div class="caption">
              <p class="text-center">{{ $i->{trans('pages.var-nama-instansi')} }}</p>
            </div>
          </a>
        </div>
      </div>
      @if ($count % 4 == 3)
      <div class="clearfix visible-md"></div>
      @endif
@endforeach
    </div>
  </div>
<div id="perizinan-kl-list" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="modal-label" class="modal-title"></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scripts')
<script>
function loadModal(id,name) {
  $('#modal-label').html(name);
  $('#perizinan-kl-list').find('.modal-body').html("Loading...").load("{{ url('perizinan-kl/')}}/"+id);
  $('#perizinan-kl-list').modal('show');
}
</script>
@endsection