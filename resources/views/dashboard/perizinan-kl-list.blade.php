@if ($instansi->dasar_hukum)
  <h4>{{ trans('pages.legal_basis') }}</h4>
  <p>{!!$instansi->dasar_hukum!!}</p>
@endif
  <a class="btn btn-primary" href="{{ asset(trans('pages.var-sop-instansi-folder').'/'.$instansi->{trans('pages.var-sop-instansi')}) }}" download><i class="fa fa-download"></i> Download SOP</a>
  <br/>
<div class="nopadding8">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php $show_default = $izin_kl_group->count() < 2 ?>
@foreach ($izin_kl_group as $id_m_direktorat => $izin_kl)
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="heading-{{$id_m_direktorat}}">
@if ($id_m_direktorat)
<?php $direktorat = App\Model\Direktorat::find($id_m_direktorat);
      $nama_direktorat = $direktorat?$direktorat->nama_direktorat:$instansi->nama_instansi ?>
@else
<?php $nama_direktorat = $instansi->nama_instansi?>
@endif
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$id_m_direktorat}}" aria-expanded="true" aria-controls="collapse-{{$id_m_direktorat}}">
            {{$nama_direktorat}}
          </a>
        </h4>
      </div>
      <div id="collapse-{{$id_m_direktorat}}" class="panel-collapse collapse {{$show_default?'in':''}}" role="tabpanel" aria-labelledby="heading-{{$id_m_direktorat}}">
        <div class="panel-body">
          <div class="row">
@foreach ($izin_kl->sortBy('nama_sop') as $count => $i)
    @if (!$i->children->isEmpty())
      @foreach ($i->children as $j)
      @if ($j->bahasa == $i->bahasa)
      <div class="col-md-6 perizinan-kl-list">
      <a href="{{ url('perizinan-kl/'.$instansi->id_m_instansi.'/'.$j->id_m_sop_deskripsi) }}">
        <div class="inner_col">
          <i class='fa fa-globe'></i> {{$i->nama_sop}} {{$j->nama_sop}}
        </div>
      </a>
      </div>
      @endif
      @endforeach
    @else
      <div class="col-md-6 perizinan-kl-list">
      <a href="{{ url('perizinan-kl/'.$instansi->id_m_instansi.'/'.$i->id_m_sop_deskripsi) }}">
        <div class="inner_col">
          <i class='fa fa-globe'></i> {{$i->nama_sop}}
        </div>
      </a>
      </div>
    @endif
@endforeach
  <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>

@endforeach
</div>
</div>
<br/>
