{{ Navigator::setActive(url('home')) }}
@extends('backend')

@section('title','Dashboard Eksekutif')
@section('content')
<div class="row">
	<div class="col-sm-8 infobox-container">
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::ON_GOING_BIASA)}}" data-url="{{url('trackingregister/cari/on_going')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['on_going'] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::ON_GOING_BIASA}}</div>
			</div>
		</div>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::ON_GOING_WARNING)}}" data-url="{{url('trackingregister/cari/on_going_warning')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['on_going_warning'] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::ON_GOING_WARNING}}</div>
			</div>
		</div>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::ON_GOING_TERLAMBAT)}}" data-url="{{url('trackingregister/cari/on_going_delay')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['on_going_delay'] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::ON_GOING_TERLAMBAT}}</div>
			</div>
		</div>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::ON_TIME)}}" data-url="{{url('trackingregister/cari/O')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['O'] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::ON_TIME}}</div>
			</div>
		</div>
		<?php /*<div class="infobox {{constName(App\Model\TrackingRegisterDetail::TUNDA)}}" data-url="{{url('trackingregister/cari/4')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter[4] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::TUNDA}}</div>
			</div>
		</div>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::STOP)}}" data-url="{{url('trackingregister/cari/3')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter[3] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::STOP}}</div>
			</div>
		</div>*/ ?>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::DELAY)}}" data-url="{{url('trackingregister/cari/D')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['D'] }}</span>
				<div class="infobox-content">{{App\Model\TrackingRegisterDetail::DELAY}}</div>
			</div>
		</div>
		<div class="infobox {{constName(App\Model\TrackingRegisterDetail::BELUM_DIAJUKAN)}}" data-url="{{url('trackingregister')}}">
			<div class="infobox-icon">
				<i class="icon- fa fa-file-o"></i>
			</div>
			<div class="infobox-data">
				<span class="infobox-data-number">{{ $counter['semua'] }}</span>
				<div class="infobox-content">Semua</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('styles')
<style>
@foreach (App\Model\TrackingRegisterDetail::$status_color as $status => $color)
.{{constName($status)}} {
color:{{$color}};
}
.{{constName($status)}}>.infobox-icon>[class*="icon-"] {
background-color:{{$color}};
}
@endforeach
.infobox {
width: 250px;
cursor: pointer;
}
</style>
@endsection
@section('scripts')
<script>
$(".infobox").click(function() {
	document.location = $(this).data('url');
});
</script>
@endsection
