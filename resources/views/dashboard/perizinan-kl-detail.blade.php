<?php $parent = $sop_deskripsi->parent();
$nama_sop = $parent?$parent->nama_sop." ".$sop_deskripsi->nama_sop:$sop_deskripsi->nama_sop ?>
@extends('app')
@section('title',$nama_sop.' - Dashboard BKPM')
@section('content')
  <div class="col-md-9">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">{{ trans('pages.home') }}</a></li>
      <li><a href="{{ url('perizinan-kl') }}">{{ trans('pages.pokl') }}</a></li>
      <li><a href="{{ url('perizinan-kl') }}">{{ $instansi->{trans('pages.var-nama-instansi')} }}</a></li>
      <li class="active capz">{{$nama_sop}}</li>
    </ol>
    <div class="kl-content">
      <h2 class="capz">{{$nama_sop}}</h2>
      <img src="{{ asset('images/logokem/'.$instansi->filename_logo) }}" class="img-responsive nipz-content" style="float:left; margin: 10px; max-height: 100px"/>
      <h3>{{ $instansi->{trans('pages.var-nama-instansi')} }}</h3>
      <div class="clearfix"></div>
      <div class="col-md-12">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_a" data-toggle="tab">{{ trans('pages.requirements') }}</a></li>
          <li><a href="#tab_b" data-toggle="tab">{{ trans('pages.sla') }}</a></li>
          <li><a href="#tab_c" data-toggle="tab">{{ trans('pages.procedure') }}</a></li>
          <li><a href="#tab_d" data-toggle="tab">{{ trans('pages.legal_basis') }}</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_a">
            {!! $parent?$parent->persyaratan:$sop_deskripsi->persyaratan !!}
          </div>
          <div class="tab-pane" id="tab_b">
            {!! $parent?$parent->sla:$sop_deskripsi->sla !!}
          </div>
          <div class="tab-pane" id="tab_c">
            {!! $parent?$parent->prosedur:$sop_deskripsi->prosedur !!}
          </div>
          <div class="tab-pane" id="tab_d">
            {!! $parent?$parent->dasar_hukum:$sop_deskripsi->dasar_hukum !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
</script>
@endsection