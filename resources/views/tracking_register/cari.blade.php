<!--
{{ Navigator::setActive(url('trackingregister/cari')) }}
-->
{{ Navigator::setActive(url('trackingregister')) }}
@extends('backend')
@section('title','Tracking')
@section('content')
 <form id="search-form" 
 @if ($status)
 style="display: none"
 @endif
 >
    <div class="form-body form-horizontal">
        <div class="row">
        <div class="col-sm-6">
            <h3>Perusahaan</h3>
            @include('form.text',['label'=>'No IP','required'=>false,'name'=>'qrcode'])
            @include('form.text',['label'=>'Nama Perusahaan','required'=>false,'name'=>'nama_perusahaan'])
            <h3>Izin</h3>
        @if (getInstansi())
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::where('id_m_instansi',getInstansi())->lists('nama_instansi','id_m_instansi')
            ,'value'=>getInstansi()])
        @else
            @include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
                App\Model\Instansi::lists('nama_instansi','id_m_instansi')
            ,'empty'=>'Semua'])
        @endif
            <?php /*@include('form.select2',['label'=>'Direktorat','required'=>false,'name'=>'id_m_direktorat','data'=>
                App\Model\Direktorat::lists('nama_direktorat','id_m_direktorat')
            ,'empty'=>'Semua'])*/?>
            @include('form.select2',['label'=>'Jenis Izin','required'=>false,'name'=>'id_m_jenis_perizinan','data'=>
                App\Model\JenisPerizinan::lists('nama_jenis_perizinan','id_m_jenis_perizinan')
            ,'empty'=>'Semua'])
            @include('form.select',['label'=>'Status','required'=>false,'name'=>'performance','data'=>[
                ''=> 'Semua',
                'O' => App\Model\TrackingRegisterDetail::ON_TIME,
                'D' => App\Model\TrackingRegisterDetail::DELAY,
                //'R' => App\Model\TrackingRegisterDetail::STOP,
                //'C' => App\Model\TrackingRegisterDetail::TUNDA,
                'on_going' => App\Model\TrackingRegisterDetail::ON_GOING_BIASA,
                'on_going_delay' => App\Model\TrackingRegisterDetail::ON_GOING_TERLAMBAT,
                'on_going_warning' => App\Model\TrackingRegisterDetail::ON_GOING_WARNING,
            ],'value'=>$status])
        </div>
        <div class="col-sm-6">
            <h3>Detail Izin</h3>
            @include('form.date',['label'=>'Tanggal Pengajuan Dari','required'=>false,'name'=>'tanggal_pengajuan_dari'])
            @include('form.date',['label'=>'Sampai','required'=>false,'name'=>'tanggal_pengajuan_sampai'])
            @include('form.date',['label'=>'Tanggal Selesai Dari','required'=>false,'name'=>'tanggal_selesai_dari'])
            @include('form.date',['label'=>'Sampai','required'=>false,'name'=>'tanggal_selesai_sampai'])
            @include('form.date',['label'=>'Estimasi Selesai Dari','required'=>false,'name'=>'tanggal_selesai_estimasi_dari'])
            @include('form.date',['label'=>'Sampai','required'=>false,'name'=>'tanggal_selesai_estimasi_sampai'])
        </div>
        </div>
    </div>
    <div class="form-actions right">
        <a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari</a>
        <a class="btn btn-success btn-sm ladda-button btn-export" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
    </div>
 </form>
 @if ($status)
 <a class="btn btn-success btn-sm ladda-button btn-export" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
 @endif
 <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
		 <tr class="bg-info">
         <th>ID QR Code</th>
         <th>Nama Perizinan</th>
         <th>Instansi</th>
         <th>Perusahaan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>No Izin</th>
         <th>SLA</th>
         <th>Status</th>
     </tr>
     <!--tr class="bg-info">
         <th>ID QR Code</th>
         <th>Nama Perizinan</th>
         <th>Instansi</th>
         <th>Perusahaan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>SLA</th>
         <th>Status</th>
         @if (isRole('edit_check'))
         <th>Edit</th>
         @endif
     </tr-->
     </thead>

     <tbody>

     </tbody>
 </table>
@include('_includes.legend')
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
.right {
    text-align: right
}
.form-control.select2me {
    border: none;
    padding: 0;
}
.help-block {
    margin-top: 0px;
    margin-bottom: 0px;
}
@include('_includes.legend_styles')
</style>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/jquery/jquery.autocomplete.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@include('tracking_register.datatables_script',['url'=>url('trackingregister/search')])
<script>
var params = {};
var l_btn_cari = Ladda.create(document.querySelector("#btn-cari"));

$("#nama_perusahaan").autocomplete({
   serviceUrl: "{{url('tracking/search-perusahaan')}}", 
   paramName: "q",
   onSelect: function (suggestion) {
       $("#qrcode").val(suggestion.data);
       $("#qrcode").change();
   },
});

$('.date-picker').datepicker({
    autoclose: true
});
$(".select2me").select2();
@if ($status)
create();
@endif
$('#btn-cari').click(function() {
    create();
});
$('.btn-export').click(function() {
    var data = $('#search-form').serialize();
    window.location = "{{url('trackingregister/export')}}?"+data;
});

$('#search-form').submit(function(e) {
    create();
    e.preventDefault(); 
});

function create() {
    var data = $('#search-form').serializeArray();
    params = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    drawTable('#search-result', l_btn_cari);
}
//drawTable('#search-result');
</script>

@endsection
