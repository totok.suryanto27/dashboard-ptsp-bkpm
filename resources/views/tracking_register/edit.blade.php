@extends('backend')
@section('title','Tracking Register')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('trackingregister/update',$tracking->id_tracking_register_detail) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.view',['label'=>'ID Tracking','required'=>false,'name'=>'id_tracking_register_detail','value'=>$tracking->id_tracking_register_detail])
        @include('form.view',['label'=>'IP','required'=>false,'name'=>'id_qrcode','value'=>$tracking->id_qrcode])
        @include('form.view',['label'=>'Nama Perusahaan','required'=>false,'name'=>'nama_perusahaan','value'=>$tracking->nama_perusahaan])
        @include('form.view',['label'=>'JenisPerizinan','required'=>false,'name'=>'nama_jenis_perizinan','value'=>$tracking->nama_jenis_perizinan?$tracking->nama_jenis_perizinan:'Lain-lain'])
        @include('form.view',['label'=>'Instansi','required'=>false,'name'=>'nama_instansi','value'=>$tracking->nama_instansi!='NULL'?$tracking->nama_instansi:'Lain-lain'])
        <?php /*@include('form.view',['label'=>'Direktorat','required'=>false,'name'=>'nama_direktorat','value'=>$tracking->nama_direktorat!='NULL'?$tracking->nama_direktorat:'Lain-lain'])*/ ?>
        @include('form.view',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'v_tanggal_pengajuan','value'=>getFullDate($tracking->tanggal_pengajuan)])
        @include('form.view',['label'=>'Tanggal Estimasi','required'=>false,'name'=>'tanggal_threshold','value'=>getFullDate($tracking->tanggal_threshold)])
        @include('form.view',['label'=>'Tanggal Selesai','required'=>false,'name'=>'v_tanggal_selesai','value'=>$tracking->tanggal_selesai?getFullDate($tracking->tanggal_selesai):""])
        @if ($tracking->komentar_check_in && $tracking->komentar_check_in != '')
        @include('form.view',['label'=>'Komentar (Registrasi)','required'=>false,'name'=>'v_komentar_check_in','value'=>$tracking->komentar_check_in])
        @endif
        @if ($tracking->komentar_check_out && $tracking->komentar_check_out != '')
        @include('form.view',['label'=>'Komentar (Izin Selesai)','required'=>false,'name'=>'v_komentar_check_out','value'=>$tracking->komentar_check_out])
        @endif
        <?php $stop = $tracking->trd_stop()->first(); ?>
        @if ($stop)
        @include('form.view',['label'=>'Komentar (STOP)','required'=>false,'name'=>'v_komentar_stop','value'=>$stop->alasan_stop])
        @endif
        <h3>Edit Data</h3>
        @include('form.date',['label'=>'Tanggal Pengajuan','required'=>false,'name'=>'tanggal_pengajuan','value'=>date('Y-m-d',strtotime($tracking->tanggal_pengajuan))])
        @include('form.date',['label'=>'Tanggal Selesai','required'=>false,'name'=>'tanggal_selesai','value'=>$tracking->tanggal_selesai?date('Y-m-d',strtotime($tracking->tanggal_selesai)):""])
        @include('form.text',['label'=>'Komentar (Registrasi)','required'=>false,'name'=>'komentar_check_in','value'=>$tracking->komentar_check_in])
        @if ($stop)
        @include('form.text',['label'=>'Komentar (STOP)','required'=>false,'name'=>'komentar_stop','value'=>$stop->alasan_stop])
        @else
        @include('form.text',['label'=>'Komentar (Izin Selesai)','required'=>false,'name'=>'komentar_check_out','value'=>$tracking->komentar_check_out])
        @endif
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-4 col-md-8">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
@section('styles')
<style>
.form-group input {
    height: 30px;
    padding-top: 0;
    padding-bottom: 0;
    margin-bottom: 4px;
}
.form-control-static {
    font-weight: bold;
    padding-top: 4px;
}
.form-group {
    margin-bottom: 0;
}
</style>
@section('scripts')
<script>
$('.date-picker').datepicker({
    autoclose: true
});
</script>
@endsection