 
{{ Navigator::setActive(url('trackingregister')) }}
@extends('backend')
@section('title','Tracking Perizinan')
@section('content')
    <!--form class="form-horizontal" id="search-form">
    <div id="search-form">
        <input type="text" placeholder"Qr code" name="qrcode"/>
        <a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari IP</a>
        <a id="btn-export" class="btn btn-success btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
    </div>
    </form-->
	<form id="search-form">
		<div class="panel">
			Tanggal Perizinan
			<div class="row">
				<div class="col-md-6">
					@include('form.date',['label'=>'Dari','required'=>false,'name'=>'tanggal_pengajuan_dari'])
					@include('form.date',['label'=>'Sampai','required'=>false,'name'=>'tanggal_pengajuan_sampai'])
				</div>
				<!--div class="col-md-6" style="margin-bottom:11px;">
					<span style="margin-right:40px;"> Instansi </span>
					<select onchange="fetch_select(this.value);" style="width:350px;height:35px;" name="id_m_instansi" id="id_m_instansi">
						<option value=""> -- Pilih Instansi -- </option>
						<option value="">Semua Instansi</option>
						<?php foreach($list_kementrian as $val) : ?>
							<option value="<?= $val->id_m_instansi ?>"><?= $val->nama_instansi ?></option> 
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-md-6" style="margin-bottom:15px;">
					<span style="margin-right:68px;">Izin</span> 
					<select id="id_m_jenis_perizinan" name="id_m_jenis_perizinan" style="width:350px;height:35px;"></select>
				</div--> 
				<!--
				@if (Request::has('id_m_instansi'))
					@include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
						App\Model\Instansi::where('id_m_instansi',Request::get('id_m_instansi'))->lists('nama_instansi','id_m_instansi')
					,'value'=>Request::get('id_m_instansi')])
				@elseif (getInstansi())
					@include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
						App\Model\Instansi::where('id_m_instansi',getInstansi())->lists('nama_instansi','id_m_instansi')
					,'value'=>getInstansi()])
				@else
					@include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
						App\Model\Instansi::lists('nama_instansi','id_m_instansi')
					,'empty'=>'Semua'])
				@endif 
				--> 
				<div class="col-md-6">
				@include('form.select2',['label'=>'Instansi','required'=>false,'name'=>'id_m_instansi','data'=>
					App\Model\Instansi::lists('nama_instansi','id_m_instansi')
				,'empty'=>'Semua'])
				@include('form.select2',['label'=>'Izin','required'=>false,'name'=>'id_m_jenis_perizinan','data'=>
					App\Model\JenisPerizinan::lists('nama_jenis_perizinan','id_m_jenis_perizinan')
				,'empty'=>'Semua'])
				</div>
				</br></br>
				<div class="col-md-6">
					<a id="btn-cari" class="btn btn-primary btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-search"></i> Cari</a>
					<a id="btn-export" class="btn btn-success btn-sm ladda-button" data-style="zoom-out"><i class="fa fa-download"></i> Download Excel</a>
				</div>
			</div>
		</div>
	</form>
 <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>ID QR Code</th>
         <th>Nama Perizinan</th>
         <th>Instansi</th>
         <th>Perusahaan</th>
         <th>Tanggal Mulai</th>
         <th>Tanggal Selesai</th>
         <th>Tanggal Estimasi</th>
         <th>No Izin</th>
         <th>SLA</th>
         <th>Status</th>
     </tr>
     </thead>

     <tbody>
     </tbody>
 </table>
@include('_includes.legend')
@endsection
@section('styles')
<style> 
.form-control.select2me {
    border: none;
    padding: 0;
}
#search-result_wrapper {
    padding-bottom: 100px;
}
</style>
<link rel="stylesheet" href="{{ asset('vendor/plugins/ladda/ladda.min.css') }}" />
<link href="{{ asset('vendor/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
<style>
@include('_includes.legend_styles')
</style>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@include('tracking_register.datatables_script',['url'=>url('trackingregister/search')])
<script>
//tambahan
$('.date-picker').datepicker({
    autoclose: true
});
$(".select2me").select2();

var params = {};
var l_btn_cari = Ladda.create(document.querySelector('#btn-cari'));
create();
$('#btn-cari').click(function() {
    create();
});
//$('#btn-export').click(function() {
    //window.location = "{{url('trackingregister/export')}}";
//});
$('#btn-export').click(function() {
    var data = $('#search-form').serializeArray();
	//@if (getInstansi())
    //params = {id_m_instansi: '{{getInstansi()}}' };
    //@else
    //params = {};
    //@endif
    window.location = "{{url('trackingregister/export')}}";
    //window.location = "{{url('trackingregister/export')}}?"+data;
});


$('#search-form').submit(function(e) {
    create();
    e.preventDefault(); 
});

function create() {
	
    var data = $('#search-form').serializeArray();
		//alert(data);
    @if (getInstansi())
    params = {id_m_instansi: '{{getInstansi()}}' };
    @else
    params = {};
    @endif
    for (var i = 0; i < data.length; i++) {
        if (data[i].value != "") {
            params[data[i].name] = data[i].value;
        }
    }
    //console.log(params);
    drawTable('#search-result', l_btn_cari);
}
//drawTable('#search-result');

function fetch_select(val) {
	 $.ajax({
			 type: 'GET',
			 url: base_url + '/trackingregister/listizin/'+val,
			 data: {
					//id_instansi:val
			 },
		 success: function (response) {   
			document.getElementById("id_m_jenis_perizinan").innerHTML=response; 
		 }
	 });
}

</script>

@endsection
