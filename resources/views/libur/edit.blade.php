@extends('backend')
@section('title','Libur')
@section('content')
<div class="col-xs-12">
    <div class="row">
      <form method="POST" action="{{ url('libur/update',$libur->tanggal_libur) }}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-8">
        @include('form.date',['label'=>'Tanggal Libur','required'=>false,'name'=>'tanggal_libur','value'=>date("Y-m-d", strtotime($libur->tanggal_libur) )])
        @include('form.text',['label'=>'Tahun','required'=>false,'name'=>'tahun','value'=>$libur->tahun])
        @include('form.text',['label'=>'Deskripsi','required'=>false,'name'=>'deskripsi','value'=>$libur->deskripsi])
        @include('form.radio_padded',['label'=>'Akhir Pekan','required'=>false,'value'=>$libur->akhir_pekan,'name'=>'akhir_pekan','data'=>["T"=>"YA","F"=>"TIDAK"]])
        <div class="space-4"></div>
        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="icon-ok bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="icon-undo bigger-110"></i>
                    Reset
                </button>
            </div>
        </div>
        </div>
      </form>
    </div>
</div>
@endsection
