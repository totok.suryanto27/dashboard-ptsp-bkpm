@extends('backend')
@section('title','Libur')
@section('content')
<div class="col-xs-12">
  <div class="row">
    <div class="profile-user-info profile-user-info-striped">
    @include('form.view_styled',['label'=>'Tanggal libur','required'=>false,'name'=>'tanggal_libur','value'=>$libur->tanggal_libur])
    @include('form.view_styled',['label'=>'Tahun','required'=>false,'name'=>'tahun','value'=>$libur->tahun])
    @include('form.view_styled',['label'=>'Deskripsi','required'=>false,'name'=>'deskripsi','value'=>$libur->deskripsi])
    @include('form.view_styled',['label'=>'Akhir Pekan','required'=>false,'name'=>'akhir_pekan','value'=>$libur->akhir_pekan])
    </div>
    
    <div class="space-4"></div>
  </div>
  <a href="{{url('/libur/edit',$libur->tanggal_libur)}}" class="btn btn-success">Libur Edit</a>
  </div>
@endsection
@section('styles')
<style>
.profile-info-name {
    width:400px;
}
.profile-info-value {
    margin-left:400px;
}
</style>
@endsection