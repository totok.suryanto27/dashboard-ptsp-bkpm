<script>
var initialized = false;
var table;
function drawTable(table_id) {
    if (!initialized) {
        table = $(table_id).DataTable({
            ajax: {
                url: "{{$url}}",
                data: function(d) {
                    console.log(d);
                    d.params = params;
                    return d;
                },
            },
            processing: true,
            serverSide: true,
            columns: [
            {data: 'tanggal_libur', name: 'tanggal_libur'},
            {data: 'tahun', name: 'tahun'},
            {data: 'deskripsi', name: 'deskripsi'},
            {data: 'akhir_pekan', name: 'akhir_pekan'},
            {data: 'actions', name: 'actions', searchable: false, orderable: false}
            ],
            order: [[0,'asc']]
        });
        initialized = true;
        
    } else {
        table.ajax.reload();
    }
}
</script>
