@extends('backend')
@section('title','Libur')
@section('content')
 <?php //<a href="{{url('/konsultasi/create')}}" class="btn btn-success">Konsultasi Baru</a> ?>
  <a href="{{url('/libur/create')}}" class="btn btn-success">Libur Baru</a>
   <hr>
 <table id="search-result" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Tanggal libur</th>
         <th>Tahun</th>
         <th>Akhir Pekan</th>
         <th>Deskripsi</th>
         <th>Actions</th>
     </tr>
     </thead>
 </table>
@endsection
@section('scripts')
@include('libur.datatables_script',['url'=>url('libur/getData')])
<script>
var params = {};

drawTable('#search-result');
</script>
@endsection