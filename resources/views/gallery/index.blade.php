{{ Navigator::setActive(url('gallery')) }}
@extends('backend')
@section('title','Galeri Gambar & File')
@section('styles')
<style type="text/css">
    ul {         
        padding:0 0 0 0;
        margin:0 0 0 0;
    }
    ul li img {
        cursor: pointer;
    }
    #dropzoneFileUpload {
        min-height: 200px;
        display: flex;
        align-items: center;
        text-align: center;
        cursor: pointer;
    }
</style>
@stop

@section('content')

<div id="dropzone">
	<div class="dropzone" id="dropzoneFileUpload" >
	</div>
</div>


<div style="margin-top:100px">
   <ul class="row">
        @foreach ($img as $image)
        <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4" style="list-style:none; margin-bottom:25px;">
            <img class="img-responsive" src="{{url('uploads/'.$image->img_location)}}">
            {{$image->img_location}} <a href="{{url('gallery/delete',$image->id_gallery)}}" class="red" onclick="notifyConfirm(event)">x</a>
        </li>
        @endforeach
   </ul>
</div>



@endsection

@section('scripts')
<script src="{{ asset('/bower_components/dropzone/dist/dropzone.js')}}"></script>
<script type="text/javascript">
    var baseUrl = "{{ url('/') }}";
    var token = "{{ Session::getToken() }}";
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzoneFileUpload", {
        url: baseUrl + "/gallery/upload",
        params: {
            _token: token
        }
    });
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        addRemoveLinks: true,
        accept: function(file, done) {

        },
    };
</script>
@endsection
