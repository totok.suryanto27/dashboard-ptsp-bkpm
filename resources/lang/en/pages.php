<?php

return [
    "home" => "Home",
    "perizinan-online" => "Integrated License",
    "perizinan-kl" => "Online License",
    "welcome" => "Welcome to OSS Center Dashboard",
    "search" => "Search",
    "potptspp" => "Integrated Online Licensing OSS Center",
    "potptspp-desc" => "",
    "pokl" => "Ministries and Institutions Online Licensing",
    "pokl-desc" => "",
    "logo" => "logo.jpg",
    "var-nama-instansi" => "nama_instansi_eng",
    "var-sop-instansi" => "filename_eng",
    "var-sop-instansi-folder" => "sop_english",
    "requirements" => "Requirements",
    "sla" => "SLA",
    "procedure" => "Procedure",
    "legal_basis" => "Legal Basis",
];
