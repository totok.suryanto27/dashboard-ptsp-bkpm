<?php

return [
    "home" => "Beranda",
    "perizinan-online" => "Izin Terintegrasi",
    "perizinan-kl" => "Izin Online",
    "welcome" => "Selamat Datang di Dashboard OSS Center",
    "search" => "Cari",
    "potptspp" => "Perizinan Online Terintegrasi PTSP Pusat",
    "potptspp-desc" => "Perizinan online ini sudah mengintegrasi keseluruhan proses perizinan antar masing-masing K/L sehingga tidak perlu mengunjungi K/L lainnya.",
    "pokl" => "Perizinan Online K/L",
    "pokl-desc" => "Perizinan yang online dan dilakukan oleh masing- masing K/L",
    "logo" => "logo-id.jpg",
    "var-nama-instansi" => "nama_instansi",
    "var-sop-instansi" => "filename",
    "var-sop-instansi-folder" => "sop_bahasa",
    "requirements" => "Persyaratan",
    "sla" => "SLA",
    "procedure" => "Prosedur",
    "legal_basis" => "Dasar Hukum",

];
