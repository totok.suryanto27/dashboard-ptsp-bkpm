<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "Passwords minimal 6 karakter dan sama dengan konfirmasi password.",
    "user" => "Kami tidak dapat menemukan user dengan email tersebut.",
    "token" => "Password reset token tidak valid.",
    "sent" => "Kami telah mengirimkan link reset password kepada anda!",
    "reset" => "Password anda berhasil direset!",

];
