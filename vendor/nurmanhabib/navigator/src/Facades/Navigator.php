<?php

namespace Nurmanhabib\Navigator\Facades;

use Illuminate\Support\Facades\Facade;

class Navigator extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'navigator';
    }

}